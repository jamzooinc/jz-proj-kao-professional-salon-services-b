/*
   2013年8月6日上午 09:46:35
   使用者: ajen
   伺服器: (local)
   資料庫: KpssDb
   應用程式: 
*/

/* 為了避免任何可能發生資料遺失的問題，您應該先詳細檢視此指令碼，然後才能在資料庫設計工具環境以外的位置執行。*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Employees ADD
	SalesOffice nvarchar(10) NULL,
	SalesGroup nvarchar(10) NULL,
	SalesPerson nvarchar(10) NULL,
	SalesEmployee nvarchar(20) NULL
GO
ALTER TABLE dbo.Employees SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
