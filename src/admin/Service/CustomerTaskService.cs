﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Globalization;
    using System.Linq.Expressions;
    using Models;

    public enum CustomerTaskType { Memo = 1, Assignment = 2 }
    
    public class CustomerTaskService : BaseService
    {
        public CustomerTaskService()
            : base()
        {
        }
        public static CustomerTask Mapping(CustomerTaskModel p)
        {
            CustomerTask result = new CustomerTask();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Date = p.Date;
            result.Title = p.Title;
            result.Category = p.Category;
            result.Type = p.Type;
            result.Status = p.Status;
            result.CloseDate = p.CloseDate;
            result.FromEmp = p.FromEmp;
            result.ToEmp = p.ToEmp;
            result.DeadLine = p.DeadLine;
            result.Context = p.Context;
            result.DepName = p.DepName;

            return result;
        }
        public static CustomerTaskModel Mapping(CustomerTask p)
        {
            CustomerTaskModel result = new CustomerTaskModel();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Date = p.Date;
            result.Title = p.Title;
            result.Category = p.Category;
            result.Type = p.Type;
            result.Status = p.Status;
            result.CloseDate = p.CloseDate;
            result.FromEmp = p.FromEmp;
            result.ToEmp = p.ToEmp;
            result.DeadLine = p.DeadLine;
            result.Context = p.Context;
            result.DepName = p.DepName;
         
            return result;
        }

        public static Expression<Func<CustomerTask, CustomerTaskModel>> Map = p => 
         new CustomerTaskModel() { 
            Id = p.Id,
            Number = p.Number,
            Date = p.Date,
            Title = p.Title,
            Category = p.Category,
            Type = p.Type,
            Status = p.Status,
            CloseDate = p.CloseDate,
            FromEmp = p.FromEmp,
            ToEmp = p.ToEmp,
            DeadLine = p.DeadLine,
            Context = p.Context,
            DepName = p.DepName
        };



        #region Web Ap
        
        /// <summary>
        /// 取得某個人的待交辦事項 記錄
        /// </summary>
        /// <param name="EmpSn">目前登入業務的SN</param>
        /// <param name="EmpId">選擇的業務編號(For 業務主管)</param>
        /// <param name="Type">Memo : 待辦事項 /Assignment : 交辦事項</param>
        /// <param name="page">篩選的Pager</param>
        /// <returns></returns>
        public CustomerTaskListModel GetList(string EmpSn, string EmpId, CustomerTaskType Type, CustomerTaskListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            CustomerTaskListModel model = new CustomerTaskListModel();

            if (cntEmp != null)
            {
                string strType = Type.ToString();

                var o_query = from p in basedb.CustomerTask
                              where p.Type == strType
                            select p;

                //駐區業務
                if (cntEmp.IsInRoles(KpssRole.駐區業務))
                {
                    //待辦事項
                    if (Type == CustomerTaskType.Memo)
                    {
                        o_query = o_query.Where(p => p.ToEmp == cntEmp.SN);
                    }
                    else if (Type == CustomerTaskType.Assignment)
                    {
                        o_query = o_query.Where(p => p.FromEmp == cntEmp.SN);
                    }
                }
                else if (cntEmp.IsInRoles(KpssRole.區業務主管))
                {
                    //待辦事項
                    if (Type == CustomerTaskType.Memo)
                    {
                        o_query = o_query.Where(p => p.ToEmp == EmpId);
                    }
                    else if (Type == CustomerTaskType.Assignment)
                    {
                        o_query = o_query.Where(p => p.FromEmp == EmpId);
                    }
                }

                if (!string.IsNullOrEmpty(page.SearchText))
                {
                    o_query = o_query.Where(p => p.Number.Equals(page.SearchText));
                }

                var query = o_query.OrderByDescending(p => p.Date);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        /// <summary>
        /// 取得事項
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomerTaskModel Get(string EmpSN, string id)
        {
            CustomerTaskModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                Data = base.basedb.CustomerTask.Where(p => p.Id == gid).Select(Map).FirstOrDefault();
            }
            
            return Data;
        }

        public bool Create(string EmpSN, CustomerTaskModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();
            entity.Date = DateTime.ParseExact(entity.strDate, "yyyy/MM/dd", CultureInfo.InvariantCulture);
            entity.DeadLine = DateTime.ParseExact(entity.strDeadLine, "yyyy/MM/dd", CultureInfo.InvariantCulture);
            entity.Type = "NONE";
            entity.Category = "NONE";
            entity.Status = 0;

            if (entity.DeadLine < entity.Date)
            {
                ErrorMsg.Add("strDeadLine", "結束時間小於起始時間");
                return false;
            }

            EmpService es = new EmpService();

            if (es.Get(entity.ToEmp) == null)
            {
                ErrorMsg.Add("ToEmp", "無此業務");
                return false;
            }

            try
            {
                CustomerTask dbEntity = Mapping(entity);
                //dbEntity.Number = DateTime.Now.ToString("yyyyMMddHHmmssfff") + EmpSN;
              

                using (basedb)
                {
                    base.basedb.CustomerTask.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("CustomerTaskService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("CustomerTaskService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            var Data = base.basedb.CustomerTask.Where(p => p.Number == id).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                   

                    base.basedb.CustomerTask.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(CustomerTaskModel entity)
        {
            using (base.basedb)
            {
                CustomerTask dbEntity = base.basedb.CustomerTask.Where(p => p.Number == entity.Number).FirstOrDefault();

                if (dbEntity != null)
                {
                    //if (dbEntity.

                    //dbEntity.EmpSN = entity.EmpSN;
                    //dbEntity.Name = entity.Name;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
        }
        #endregion
      
    }
}