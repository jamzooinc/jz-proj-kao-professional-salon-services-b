﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    using Extensions;
    
    public class SmartSchedulingService : BaseService, ISQLiteDbConverter<SmartScheduling>
    {
        public SmartSchedulingService()
            : base()
        {
        }

        public static SmartScheduling Mapping(SmartSchedulingModel p)
        {
            SmartScheduling result = new SmartScheduling();

            result.Id = p.Id;
            result.RootKpiItemId = p.RootKpiItemId;
            result.ChildKpiItemId = p.ChildKpiItemId;
            result.RootKpiItem = p.RootKpiItem;
            result.ChildKpiItem = p.ChildKpiItem;
            result.CustomerName = p.CustomerName;
            result.Number = p.Number;
            result.Year = p.Year;
            result.Month = p.Month;
            result.FromDep = p.FromDep;
            result.EmpSN = p.EmpSN;

            return result;
        }
        public static SmartSchedulingModel Mapping(SmartScheduling p)
        {
            SmartSchedulingModel result = new SmartSchedulingModel();

            result.Id = p.Id;
            result.RootKpiItemId = p.RootKpiItemId;
            result.ChildKpiItemId = p.ChildKpiItemId;
            result.RootKpiItem = p.RootKpiItem;
            result.ChildKpiItem = p.ChildKpiItem;
            result.CustomerName = p.CustomerName;
            result.Number = p.Number;
            result.Year = p.Year;
            result.Month = p.Month;
            result.FromDep = p.FromDep;
            result.EmpSN = p.EmpSN;

            return result;
        }

        public static Expression<Func<SmartScheduling, SmartSchedulingModel>> Map = p =>
         new SmartSchedulingModel()
         {
            Id = p.Id,
            RootKpiItemId = p.RootKpiItemId,
            ChildKpiItemId = p.ChildKpiItemId,
            RootKpiItem = p.RootKpiItem,
            ChildKpiItem = p.ChildKpiItem,
            CustomerName = p.CustomerName,
            Number = p.Number,
            Year = p.Year,
            Month = p.Month,
            FromDep = p.FromDep,
            EmpSN = p.EmpSN,
         };

        #region Web App

        public bool Delete(Guid Id)
        {
            try
            {
                var delete = basedb.SmartScheduling.Where(p => p.Id == Id).FirstOrDefault();

                basedb.SmartScheduling.DeleteObject(delete);

                //計算 KPI 項目的客戶數量 ( dbo.kpi 表格中的 customerCount )
                Kpi kpi = basedb.Kpi.Where(p => p.Year == delete.Year
                    && p.Month == delete.Month
                    && p.EmpSN == delete.EmpSN
                    && p.KpiItemId == delete.ChildKpiItemId).FirstOrDefault();

                if (kpi != null)
                {
                    kpi.CustomerCount -= 1;
                }
                basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Create(
                string CurrentEmp,
                Guid ChildKpiItemId,
                string Number,
                int Year,
                int Month,
                string EmpSN
            )
        {
            KpiItemService ks = new KpiItemService();
            var KpiItem = (from c in basedb.KpiItem
                           join p in basedb.KpiItem
                           on c.RootId equals p.Id
                           where c.Id == ChildKpiItemId
                           select 
                           new { 
                                RootId = c.RootId,
                                ChildId = c.Id,
                                RootName = p.Name,
                                ChildName = c.Name
                           }).FirstOrDefault();
            if (KpiItem == null)
            {
                throw new Exception("KPI 項目不存在");
            }

            var Customer = basedb.Customer.Where(p => p.Number == Number).FirstOrDefault();
            if (Customer == null)
            {
                throw new Exception("該客戶不存在");
            }

            SmartScheduling dbEntity = new SmartScheduling();
            dbEntity.Id = Guid.NewGuid();
            dbEntity.RootKpiItemId = KpiItem.RootId;
            dbEntity.RootKpiItem = KpiItem.RootName;
            dbEntity.ChildKpiItemId = KpiItem.ChildId;
            dbEntity.ChildKpiItem = KpiItem.ChildName;
            dbEntity.Number = Customer.Number;
            dbEntity.CustomerName = Customer.Name;
            dbEntity.Year = Year;
            dbEntity.Month = Month;
            dbEntity.FromDep = CurrentEmp;
            dbEntity.EmpSN = EmpSN;

            try
            {
                basedb.SmartScheduling.AddObject(dbEntity);
                //計算 KPI 項目的客戶數量 ( dbo.kpi 表格中的 customerCount )
                Kpi kpi = basedb.Kpi.Where(p => p.Year == Year
                    && p.Month == Month
                    && p.EmpSN == EmpSN
                    && p.KpiItemId == KpiItem.ChildId).FirstOrDefault();

                if (kpi != null)
                {
                    kpi.CustomerCount += 1;
                }

                basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SmartSchedulinListModel GetList(int Year, int Month, string EmpSN, Guid ChildKpiItemId)
        {
            SmartSchedulinListModel model = new SmartSchedulinListModel();
            model.Year = Year;
            model.Month = Month;
            model.EmpSN = EmpSN;
            model.ChildKpiItemId = ChildKpiItemId;

            var o_query = from p in basedb.SmartScheduling
                          where p.Year == Year
                          && p.Month == Month
                          && p.EmpSN == EmpSN
                          && p.ChildKpiItemId == ChildKpiItemId
                          select p;

            var query = o_query.OrderBy(p => p.Id);

            try
            {
                model.Data = query.Select(Map).ToList();
            }
            catch (Exception ex)
            {

                throw;
            }

            return model;
        }

        #endregion


        /// <summary>
        /// SQLite專用的
        /// </summary>
        /// <param name="Conn"></param>
        /// <param name="Trans"></param>
        /// <param name="source"></param>
        public void Run(SQLiteConnection Conn, SQLiteTransaction Trans, IQueryable<SmartScheduling> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.SmartScheduling( 
                    Id, RootKpiItemId, ChildKpiItemId, RootKpiItem, ChildKpiItem, Number, CustomerName, Year, Month, FromDep, EmpSN, Comment
                    )
                    Values 
                    (@Id, @RootKpiItemId, @ChildKpiItemId, @RootKpiItem, @ChildKpiItem, @Number, @CustomerName, @Year, @Month, @FromDep, @EmpSN, @Comment)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@RootKpiItemId", Row.RootKpiItemId.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@ChildKpiItemId", Row.ChildKpiItemId.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@RootKpiItem", Row.RootKpiItem));
                cmdInsert.Parameters.Add(new SQLiteParameter("@ChildKpiItem", Row.ChildKpiItem));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Number", Row.Number));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CustomerName", Row.CustomerName));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Year", Row.Year));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Month", Row.Month));
                cmdInsert.Parameters.Add(new SQLiteParameter("@FromDep", Row.FromDep));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Comment", Row.Comment));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}