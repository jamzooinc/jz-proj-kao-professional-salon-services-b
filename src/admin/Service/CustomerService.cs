﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    
    public class CustomerService : BaseService
    {
        public CustomerService()
            : base()
        {
        }
        public static Customer Mapping(CustomerModel p)
        {
            Customer result = new Customer();

            result.NID = p.NID;
            result.Type = p.Type;
            result.SN = p.SN;
            result.Name = p.Name;
            result.NameAbbr = p.NameAbbr;
            result.Owner = p.Owner;
            result.Tel = p.Tel;
            result.Tel_2 = p.Tel_2;
            result.Fax = p.Fax;
            result.Email = p.Email;
            result.EmpCount = p.EmpCount;
            result.Dep = p.Dep;
            result.FirstTransDate = p.FirstTransDate;
            result.LastTransDate = p.LastTransDate;
            result.SalesLevel = p.SalesLevel;
            result.StaffLevel = p.StaffLevel;
            result.Brand = p.Brand;
            result.Area = p.Area;
            result.CreateDate = p.CreateDate;
            result.ToType1Date = p.ToType1Date;
            result.EnrollmentAddr = p.EnrollmentAddr;
            result.TransportAddr = p.TransportAddr;
            result.Postal = p.Postal;
            result.Payment = p.Payment;
            result.BranchCount = p.BranchCount;
            result.CloseDate = p.CloseDate;
            result.Postal_2 = p.Postal_2;
            result.PaymentEmp = p.PaymentEmp;
            result.FirstTransDate_KMS = p.FirstTransDate_KMS;
            result.CloseDate_2 = p.CloseDate_2;
            result.Rating = p.Rating;


            #region 追加欄位
            result.ProductType1 = p.ProductType1;
            result.ProductType2 = p.ProductType2;
            result.ProductType3 = p.ProductType3;
            result.ProductType4 = p.ProductType4;
            result.ProductType5 = p.ProductType5;
            result.Fee1 = p.Fee1;
            result.Fee2 = p.Fee2;
            result.Fee3 = p.Fee3;
            result.Fee4 = p.Fee4;
            result.Fee5 = p.Fee5;
            result.OwnerNick = p.OwnerNick;
            result.OwnerTitle = p.OwnerTitle;
            result.OwnerNum = p.OwnerNum;
            result.OwnerEmail = p.OwnerEmail;
            result.OwnerTel = p.OwnerTel;
            result.OpenningDate = p.OpenningDate;
            result.Decorating = p.Decorating;
            result.NewPoint = p.NewPoint;
            result.OrderForm = p.OrderForm;
            result.ProductSource = p.ProductSource;
            result.MaterialSource = p.MaterialSource;
            result.MaterialRating = p.MaterialRating;
            result.ExtProLine = p.ExtProLine;
            result.Staff = p.Staff;
            result.BranchType = p.BranchType;
            result.OH = p.OH;
            result.ServiceType = p.ServiceType;
            result.Billing = p.Billing;
            result.S1 = p.S1;
            result.S2 = p.S2;
            result.S3 = p.S3;
            result.KMS1 = p.KMS1;
            result.KMS2 = p.KMS2;
            result.KMS3 = p.KMS3;
            result.OTC1 = p.OTC1;
            result.OTC2 = p.OTC2;
            result.WebSite = p.WebSite;
            #endregion

            result.TransportAddr_City = p.TransportAddr_City;
            result.TransportAddr_Area = p.TransportAddr_Area;
            result.TransportAddr_Postal = p.TransportAddr_Postal;
            result.TransportAddr_en = p.TransportAddr_en;
            result.SalesCode = p.SalesCode;
            result.SapCode = p.SapCode;

            return result;
        }
        public static CustomerModel Mapping(Customer p)
        {
            CustomerModel result = new CustomerModel();

            result.NID = p.NID;
            result.Number = p.Number;
            result.Type = p.Type;
            result.SN = p.SN;
            result.Name = p.Name;
            result.NameAbbr = p.NameAbbr;
            result.Owner = p.Owner;
            result.Tel = p.Tel;
            result.Tel_2 = p.Tel_2;
            result.Fax = p.Fax;
            result.Email = p.Email;
            result.EmpCount = p.EmpCount;
            result.Dep = p.Dep;
            result.FirstTransDate = p.FirstTransDate;
            result.LastTransDate = p.LastTransDate;
            result.SalesLevel = p.SalesLevel;
            result.StaffLevel = p.StaffLevel;
            result.Brand = p.Brand;
            result.Area = p.Area;
            result.CreateDate = p.CreateDate;
            result.ToType1Date = p.ToType1Date;
            result.EnrollmentAddr = p.EnrollmentAddr;
            result.TransportAddr = p.TransportAddr;
            result.Postal = p.Postal;
            result.Payment = p.Payment;
            result.BranchCount = p.BranchCount;
            result.CloseDate = p.CloseDate;
            result.Postal_2 = p.Postal_2;
            result.PaymentEmp = p.PaymentEmp;
            result.FirstTransDate_KMS = p.FirstTransDate_KMS;
            result.CloseDate_2 = p.CloseDate_2;
            result.Rating = p.Rating;

            #region 追加欄位
            result.ProductType1 = p.ProductType1;
            result.ProductType2 = p.ProductType2;
            result.ProductType3 = p.ProductType3;
            result.ProductType4 = p.ProductType4;
            result.ProductType5 = p.ProductType5;
            result.Fee1 = p.Fee1;
            result.Fee2 = p.Fee2;
            result.Fee3 = p.Fee3;
            result.Fee4 = p.Fee4;
            result.Fee5 = p.Fee5;
            result.OwnerNick = p.OwnerNick;
            result.OwnerTitle = p.OwnerTitle;
            result.OwnerNum = p.OwnerNum;
            result.OwnerEmail = p.OwnerEmail;
            result.OwnerTel = p.OwnerTel;
            result.OpenningDate = p.OpenningDate;
            result.Decorating = p.Decorating;
            result.NewPoint = p.NewPoint;
            result.OrderForm = p.OrderForm;
            result.ProductSource = p.ProductSource;
            result.MaterialSource = p.MaterialSource;
            result.MaterialRating = p.MaterialRating;
            result.ExtProLine = p.ExtProLine;
            result.Staff = p.Staff;
            result.BranchType = p.BranchType;
            result.OH = p.OH;
            result.ServiceType = p.ServiceType;
            result.Billing = p.Billing;
            result.S1 = p.S1;
            result.S2 = p.S2;
            result.S3 = p.S3;
            result.KMS1 = p.KMS1;
            result.KMS2 = p.KMS2;
            result.KMS3 = p.KMS3;
            result.OTC1 = p.OTC1;
            result.OTC2 = p.OTC2;
            result.WebSite = p.WebSite;
            #endregion

            result.TransportAddr_City = p.TransportAddr_City;
            result.TransportAddr_Area = p.TransportAddr_Area;
            result.TransportAddr_Postal = p.TransportAddr_Postal;
            result.TransportAddr_en = p.TransportAddr_en;
            result.SalesCode = p.SalesCode;
            result.SapCode = p.SapCode;

            return result;
        }

        public static Expression<Func<Customer, CustomerModel>> Map = p => 
         new CustomerModel() { 
            NID = p.NID,
            Number = p.Number,
            EmpSN = p.EmpSN,
            Type = p.Type,
            SN = p.SN,
            Name = p.Name,
            NameAbbr = p.NameAbbr,
            Owner = p.Owner,
            Tel = p.Tel,
            Tel_2 = p.Tel_2,
            Fax = p.Fax,
            Email = p.Email,
            EmpCount = p.EmpCount,
            Dep = p.Dep,
            FirstTransDate = p.FirstTransDate,
            LastTransDate = p.LastTransDate,
            SalesLevel = p.SalesLevel,
            StaffLevel = p.StaffLevel,
            Brand = p.Brand,
            Area = p.Area,
            CreateDate = p.CreateDate,
            ToType1Date = p.ToType1Date,
            EnrollmentAddr = p.EnrollmentAddr,
            TransportAddr = p.TransportAddr,
            Postal = p.Postal,
            Payment = p.Payment,
            BranchCount = p.BranchCount,
            CloseDate = p.CloseDate,
            Postal_2 = p.Postal_2,
            PaymentEmp = p.PaymentEmp,
            FirstTransDate_KMS = p.FirstTransDate_KMS,
            CloseDate_2 = p.CloseDate_2,
            Rating = p.Rating,
            #region 追加欄位
            ProductType1 = p.ProductType1,
            ProductType2 = p.ProductType2,
            ProductType3 = p.ProductType3,
            ProductType4 = p.ProductType4,
            ProductType5 = p.ProductType5,
            Fee1 = p.Fee1,
            Fee2 = p.Fee2,
            Fee3 = p.Fee3,
            Fee4 = p.Fee4,
            Fee5 = p.Fee5,
            OwnerNick = p.OwnerNick,
            OwnerTitle = p.OwnerTitle,
            OwnerNum = p.OwnerNum,
            OwnerEmail = p.OwnerEmail,
            OwnerTel = p.OwnerTel,
            OpenningDate = p.OpenningDate,
            Decorating = p.Decorating,
            NewPoint = p.NewPoint,
            OrderForm = p.OrderForm,
            ProductSource = p.ProductSource,
            MaterialSource = p.MaterialSource,
            MaterialRating = p.MaterialRating,
            ExtProLine = p.ExtProLine,
            Staff = p.Staff,
            BranchType = p.BranchType,
            OH = p.OH,
            ServiceType = p.ServiceType,
            Billing = p.Billing,
            S1 = p.S1,
            S2 = p.S2,
            S3 = p.S3,
            KMS1 = p.KMS1,
            KMS2 = p.KMS2,
            KMS3 = p.KMS3,
            OTC1 = p.OTC1,
            OTC2 = p.OTC2,
            WebSite = p.WebSite,
            
            #endregion
            IsAccountClose = p.IsAccountClose,
            AccountCloseDate = p.AccountCloseDate,
            AccountCloseNotes = p.AccountCloseNotes,

            TransportAddr_City = p.TransportAddr_City,
            TransportAddr_Area = p.TransportAddr_Area,
            TransportAddr_Postal = p.TransportAddr_Postal,
            TransportAddr_en = p.TransportAddr_en,
            SalesCode = p.SalesCode,
            SalesOffice = p.SalesOffice,
            SapCode = p.SapCode
        };

        /// <summary>
        /// 判斷該客戶是否屬於該業務的
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="CustomerNumber"></param>
        /// <returns></returns>
        public bool IsMyCustomer(string EmpSN, string CustomerNumber)
        {
            var query = (from p in basedb.Customer
                         where 
                         p.EmpSN == EmpSN && 
                         p.Number == CustomerNumber
                         select p.Number).FirstOrDefault();

            return (query != null);
        }

        /// <summary>
        /// Excel 匯入
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="dErrMsg"></param>
        /// <returns></returns>
        public bool ExcelImport(string Path, out Dictionary<string, object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();

            #region 先清空原本的客戶資料 (需要 roll back?)

            // 2013.10.18 改成不刪除
            // 合約客戶刪掉, type = 1
            // 潛在客戶轉合約, type = 3
            var deleteCustomer = from p in basedb.Customer
                                 where p.Type == 1 && p.Type == 3
                                 select p;

            foreach (var d in deleteCustomer)
            {
                basedb.Customer.DeleteObject(d);
            }

            try
            {
                //刪除全部
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                dErrMsg.Add(
                    "delete customer",
                    ex.Message);
                return false;
            }

            #endregion

            var excel = new ExcelQueryFactory(Path);
            excel.AddMapping("送貨地址_區域", "送貨地址-區域");
            excel.AddMapping("送貨地址_縣市", "送貨地址-縣市");
            excel.AddMapping("郵遞區號_送貨地址", "郵遞區號-送貨地址");
            excel.AddMapping("銷貨級數", "銷貨級數");
            excel.AddMapping("SalesOffice", "Sales office");
            excel.AddMapping("SalesCode", "Sales Code");
            excel.AddMapping("SapCode", "Sap Code");

            var query = from c in excel.Worksheet<CustomerExcelModel>("Data")
                        select c;

            foreach (CustomerExcelModel row in query)
            {
                try
                {
                    //basedb.SaveChanges();
                    if (row.客戶編號 != null)
                    {
                        Customer ExcelCustomer = row.Mapping();
                        
                        Customer dbCustomer = basedb.Customer.Where(p => p.Number == ExcelCustomer.Number).FirstOrDefault();

                        //代表後台已經有了，Type 不更新
                        if (dbCustomer != null)
                        {
                            //update
                            
                            dbCustomer.Name = ExcelCustomer.Name;
                            dbCustomer.Tel = ExcelCustomer.Tel;
                            dbCustomer.Area = ExcelCustomer.Area;
                            dbCustomer.Type = ExcelCustomer.Type;
                            dbCustomer.EmpSN = ExcelCustomer.EmpSN;
                            dbCustomer.SapCode = ExcelCustomer.SapCode;
                            dbCustomer.PaymentEmp = ExcelCustomer.PaymentEmp;
                            dbCustomer.Rating = ExcelCustomer.Rating;
                            basedb.SaveChanges();
                            dErrMsg.Add(row.客戶編號, "更新成功");
                        }
                        else
                        {
                            //dbCustomer.NID = Guid.NewGuid();
                            ExcelCustomer.NID = Guid.NewGuid();
                            //代表後台沒有，Type 不更新
                            //insert,預設匯入為 (合約客戶：由後台匯入之客戶資料
                            ExcelCustomer.Type = 1;
                            ExcelCustomer.CreateDate = DateTime.Now;
                            basedb.Customer.AddObject(ExcelCustomer);

                            basedb.SaveChanges();
                            dErrMsg.Add(row.客戶編號, "新增成功");
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(row.客戶編號))
                    {
                        dErrMsg.Add(row.客戶編號 + Guid.NewGuid(), ex.Message);
                    }
                }
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                dErrMsg.Add("SaveChangesError", ex.Message);
            }

            #region 寫 log
            //if (dErrMsg.Keys.Count() > 0)
            //{
            //    //把這次錯誤的 LOG 記下來
            //    string strFileName = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            //    string ServerPath = HttpContext.Current.Server.MapPath("~/App_Data/Customer/ImportLog/");
            //    if (!System.IO.Directory.Exists(ServerPath))
            //    {
            //        System.IO.Directory.CreateDirectory(ServerPath);
            //    }
            //    string filePath = ServerPath + strFileName + ".txt";
            //    if (!System.IO.File.Exists(filePath))
            //    {
            //        System.IO.File.Create(filePath);
            //    }

            //    using (System.IO.StreamWriter w = System.IO.File.AppendText(filePath))
            //    {
            //        foreach (string key in dErrMsg.Keys)
            //        {
            //            string strF = string.Format("業務編號{0}:{1}", dErrMsg[key].ToString());
            //            Log(strF, w);
            //        }
            //    }
            //}
            #endregion

            return dErrMsg.Keys.Count() == 0;
        }

        public static void Log(string logMessage, System.IO.TextWriter w)
        {
            w.WriteLine("{0} {1} === {2} \r\n", 
                DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString(),
                logMessage
                );
        }


        #region Web Ap
        public CustomerListModel GetList(string EmpSn, CustomerListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);
            CustomerListModel model = new CustomerListModel();
            model.Number = page.Number;
            model.EmpSN = page.EmpSN;
            model.Area = page.Area;

            if (cntEmp != null)
            {
                var o_query = from p in basedb.Customer
                            select p;

                if (cntEmp.IsInRoles(KpssRole.駐區業務))
                {
                    o_query = o_query.Where(p => p.EmpSN == cntEmp.SN);
                }

                //地區篩選
                if (!string.IsNullOrEmpty(page.Area))
                {
                    o_query = o_query.Where(p => p.TransportAddr_Area == page.Area);
                }

                //業務
                if (!string.IsNullOrEmpty(page.EmpSN))
                {
                    o_query = o_query.Where(p => p.EmpSN == page.EmpSN);
                }

                if (!string.IsNullOrEmpty(page.Name))
                {
                    o_query = o_query.Where(p => p.Name.Contains(page.Name));
                }

                if (!string.IsNullOrEmpty(page.CustomerName))
                {
                    o_query = o_query.Where(p => p.Name.Contains(page.CustomerName));
                }

                //客戶編號
                if (!string.IsNullOrEmpty(page.Number))
                {
                    o_query = o_query.Where(p => p.Number == page.Number);
                }

                //統一編號
                if (!string.IsNullOrEmpty(page.SN))
                {
                    o_query = o_query.Where(p => p.SN == page.SN);
                }


                var query = o_query.OrderBy(p => p.EmpSN);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.EmpSN).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        public CustomerModel Get(string id)
        {
            CustomerModel Data = base.basedb.Customer.Where(p => p.Number == id).Select(Map).FirstOrDefault();

            return Data;
        }

        public bool Create(string EmpSN, CustomerModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();
            try
            {
                Customer dbEntity = Mapping(entity);
                dbEntity.NID = Guid.NewGuid();
                dbEntity.Number = DateTime.Now.ToString("yyyyMMddHHmmssfff") + EmpSN;
                dbEntity.EmpSN = EmpSN;
                dbEntity.CreateDate = DateTime.Now;

                base.basedb.Customer.AddObject(dbEntity);
                base.basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("customerService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("customerService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            AccountModel current = AccountModel.Create(EmpSN);

            var Data = base.basedb.Customer.Where(p => p.Number == id).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    if (!current.IsInRoles(KpssRole.管理員))
                    {
                        if (Data.EmpSN != EmpSN)
                        {
                            throw new Exception("不是您所屬的客戶，不得刪除");
                        }
                    }

                    base.basedb.Customer.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(CustomerModel entity)
        {
            using (base.basedb)
            {
                Customer dbEntity = base.basedb.Customer.Where(p => p.Number == entity.Number).FirstOrDefault();

                if (dbEntity != null)
                {
                    //if (dbEntity.

                    dbEntity.EmpSN = entity.EmpSN;
                    dbEntity.Name = entity.Name;
                    dbEntity.SapCode = entity.SapCode;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
        }
        #endregion
      
        #region API Function

        /// <summary>
        /// 修改客戶資料
        /// </summary>
        /// <param name="CustomerID">客戶統一編號</param>
        /// <param name="Customer">客戶的基本資料</param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public bool Update(string EmpSN, CustomerModel Customer, out Dictionary<string, object> ErrorMessage)
        {
            AccountModel currentUser = AccountModel.Create(EmpSN);

            ErrorMessage = new Dictionary<string, object>();

            var o_query = from p in basedb.Customer
                          where p.Number == Customer.Number
                          select p;

            if (!currentUser.IsInRoles(KpssRole.管理員, KpssRole.業務秘書))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpSN);
            }

            //必須篩選該業務所屬的客戶
            var DbEntity = o_query.FirstOrDefault();

            if (DbEntity != null)
            {
                DbEntity.IsAccountClose = Customer.IsAccountClose;
                if (DbEntity.IsAccountClose)    //已關戶
                {
                    DbEntity.CloseDate = DateTime.Now;
                    if (Customer.AccountCloseNotess != null)
                    {
                        DbEntity.AccountCloseNotes =
                            string.Join(",", Customer.AccountCloseNotess);
                        if (Customer.AccountCloseNotess.Contains("其他"))
                        {
                            DbEntity.AccountCloseNotes += "@@-其他-@@" + Customer.AccountCloseNotesText;
                        }
                    }
                }
                else    //不關戶的話，就清空 關戶日 及關戶備註
                {
                    DbEntity.AccountCloseDate = null;   
                    DbEntity.AccountCloseNotes = null;
                }

                //DbEntity.Number = Customer.Number;
                DbEntity.Type = Customer.Type;  //狀態
                if (DbEntity.Type == 1)         //轉合約客戶
                {
                    //代表他是第一次轉合約客戶
                    if (!DbEntity.ToType1Date.HasValue)
                    {
                        DbEntity.ToType1Date = DateTime.Now;
                    }
                }
                DbEntity.SN = Customer.SN;      //統編
                DbEntity.Name = Customer.Name;  //姓名
                DbEntity.Tel = Customer.Tel;    //電話
                DbEntity.Tel_2 = Customer.Tel_2;//手機
                DbEntity.EnrollmentAddr = Customer.EnrollmentAddr;
                DbEntity.TransportAddr = Customer.TransportAddr;
                DbEntity.Brand = Customer.Brand;
                DbEntity.SalesLevel = Customer.SalesLevel;
                DbEntity.Owner = Customer.Owner;
                DbEntity.StaffLevel = Customer.StaffLevel;

                //當為合約客戶時
                if (Customer.Type == 3)
                {
                    if (!string.IsNullOrEmpty(Customer.NewNumber))
                    {
                        DbEntity.Number = Customer.NewNumber;
                    }
                }

                if (Customer.NewPoints != null)
                    DbEntity.NewPoint = string.Join(",", Customer.NewPoints);

                if (Customer.OrderForms != null)
                    DbEntity.OrderForm = string.Join(",", Customer.OrderForms);

                if (Customer.ProductSources != null)
                    DbEntity.ProductSource = string.Join(",", Customer.ProductSources);

                if (Customer.MaterialSources != null)
                    DbEntity.MaterialSource = string.Join(",", Customer.MaterialSources);

                if (Customer.MaterialRatings != null)
                    DbEntity.MaterialRating = string.Join(",", Customer.MaterialRatings);

                if (Customer.ExtProLines != null)
                    DbEntity.ExtProLine = string.Join(",", Customer.ExtProLines);

                if (Customer.BranchTypes != null)
                    DbEntity.BranchType = string.Join(",", Customer.BranchTypes);

                if (Customer.ServiceTypes != null)
                    DbEntity.ServiceType = string.Join(",", Customer.ServiceTypes);

                if (Customer.Billings != null)
                    DbEntity.Billing = string.Join(",", Customer.Billings);


                DbEntity.TransportAddr_City = Customer.TransportAddr_City;
                DbEntity.TransportAddr_Area = Customer.TransportAddr_Area;
                DbEntity.TransportAddr_Postal = Customer.TransportAddr_Postal;
                DbEntity.TransportAddr_en = Customer.TransportAddr_en;
                DbEntity.SalesCode = Customer.SalesCode;
                DbEntity.SapCode = Customer.SapCode;
                DbEntity.SalesOffice = Customer.SalesOffice;
                //聯絡人更新 (目前只會有一個)
                //Customer.ContactName
                //Customer.ContactTel
                //Customer.ContactEmail

                try
                {
                    basedb.SaveChanges();
                }
                catch (Exception ex)
                {
                    ErrorMessage.Add("server", ex.Message);
                }
            }
            else
            {
                ErrorMessage.Add("customer", "不屬於您的客戶");
            }

            return (ErrorMessage.Keys.Count() == 0) ? true : false;
        }

        /// <summary>
        /// 上傳客戶的照片
        /// </summary>
        /// <param name="EmpNo"></param>
        /// <param name="Photo"></param>
        /// <returns></returns>
        public bool AddPhoto(string EmpNo, string CustomerNumber,string Title, HttpPostedFileBase Photo, out Dictionary<string, object> ErrorMessage)
        {
            ErrorMessage = new Dictionary<string, object>();
            
            CustomerPhoto photo = new CustomerPhoto();
            photo.Number = CustomerNumber;
            photo.Title = Title;
            photo.FName = Library.FileUploadUtils.SaveFile("Customer", Photo);
            photo.CreateDate = DateTime.Now;
            try
            {
                //insert to database
                basedb.CustomerPhoto.AddObject(photo);
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMessage.Add("server error", ex.Message);
            }

            return (ErrorMessage.Keys.Count() == 0) ? true : false;
        }

        public List<CustomerModel> GetAll()
        {
            return Query(null, null, null, null, null); 
        }

        public List<CustomerModel> GetAllByEmp(string EmpNo)
        {
            return Query(EmpNo, null, null, null, null);
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="EmpNo"></param>
        /// <param name="Phone"></param>
        /// <param name="Name"></param>
        /// <param name="Number"></param>
        /// <param name="Area"></param>
        /// <returns></returns>
        public List<CustomerModel> Query(string EmpNo, string Phone, string Name, string Number, string Area)
        {
            var o_query = from p in basedb.Customer
                          select p;

            //先挑出屬於自已的 Customer
            if (!string.IsNullOrEmpty(Phone))
            {
                //o_query = o_query.Where(p => p
            }

            #region Filter
            if (!string.IsNullOrEmpty(Phone))
            {
                o_query = o_query.Where(p => p.Tel.Contains(Phone));
            }

            if (!string.IsNullOrEmpty(Name))
            {
                o_query = o_query.Where(p => p.Name.Contains(Name));
            }

            if (!string.IsNullOrEmpty(Number))
            {
                o_query = o_query.Where(p => p.SN.Contains(Number));
            }

            if (!string.IsNullOrEmpty(Area))
            {
                o_query = o_query.Where(p => p.Area.Contains(Area));
            }
            #endregion

            var query = from p in o_query
                        select new CustomerModel()
                        {
                            Type = p.Type,
                            Number = p.Number ?? string.Empty,
                            SN = p.SN,
                            Name = p.Name ?? string.Empty,
                            Tel = p.Tel ?? string.Empty,
                            Area = p.Area ?? string.Empty,
                            SalesLevel = p.SalesLevel ?? string.Empty,
                            StaffLevel = p.StaffLevel ?? string.Empty
                        };

            return query.ToList();
        }

        #endregion
    }
}