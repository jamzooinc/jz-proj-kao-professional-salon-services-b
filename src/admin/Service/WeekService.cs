﻿using System;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using admin.Models;
using admin.Extensions;

namespace admin.Service
{
    using Extensions;
    using Library;
    using System.Data;
    using System.Data.SqlClient;

    public class WeekService : BaseService, ISQLiteDbConverter<Week>
    {
        public static Expression<Func<Week, WeekModel>> Map = p =>
        new WeekModel()
        {
            Id = p.Id,
            EmpSN = p.EmpSN,
            Year = p.Year,
            Month = p.Month,
            Weeks = p.Weeks,
            Comment = p.Comment,
            CreateDate = p.CreateDate,
            StartDate = p.StartDate,
            EndDate = p.EndDate
        };

        public WeekService() : base() { }

      

        /// <summary>
        /// 取得週報表
        /// </summary>
        /// <param name="Year">年度</param>
        /// <param name="Month">月份</param>
        /// <param name="Weeks">第幾週</param>
        /// <param name="EmpSN">哪位業務</param>
        /// <returns></returns>
        public WeekListModel GetCurrentWeek(int Year, int Month, int Weeks, string EmpSN)
        {
            WeekListModel page = new WeekListModel();


            var o_query = from p in basedb.Week
                          where p.EmpSN == EmpSN
                              && p.Year == Year
                              && p.Month == Month
                              && p.Weeks == Weeks
                          select p;


            var query = o_query.OrderBy(p => p.Weeks);

            try
            {
                //page.TotalRecords = query.Select(p => p.EmpSN).Count();
                page.Data = query.Select(Map).ToList();

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return page;
        }

        //Start 及 End 的年份必須相同
        public DataTable GetExcelDataByDayRange(DateTime Start, DateTime End, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iStartWeekOfYear = Start.GetWeekOfYear();
            int iEndWeekOfYear = End.GetWeekOfYear();

            if (iEndWeekOfYear > 0 && iStartWeekOfYear > 0 && ( iEndWeekOfYear >= iStartWeekOfYear))
            {
                var query = from p in basedb.Week
                            //where p.CreateDate >= sDate && p.CreateDate <= eDate
                            where 
                            p.Weeks >= iStartWeekOfYear 
                            && p.Weeks <= iEndWeekOfYear
                            && p.Year == Start.Year
                            orderby p.CreateDate
                            select new
                            {
                                年 = p.Year,
                                月 = p.Month,
                                週 = p.Weeks,
                                員工編號 = p.EmpSN,
                                員工姓名 = basedb.Employees.Where(s => s.EmpSN == p.EmpSN).Select(s => s.Name).FirstOrDefault(),
                                備註 = p.Comment,
                                建立時間 = p.CreateDate
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.員工編號 == EmpId);
                }

                DataTable dt = query.ToList().ToDataTable();

                return dt;
            }
            return null;
        }

        public DataTable GetExcelData(DateTime dtDate, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iCurrentWeekOfYear = dtDate.GetWeekOfYear();

            //2.再抓本週的日期區間
            DateTime[] currentWeeks = Utils.GetCurrentDaysOfWeek(dtDate.Year, iCurrentWeekOfYear);

            if (currentWeeks.Length > 0)
            {
                DateTime sDate = currentWeeks[0];
                DateTime eDate = currentWeeks[(currentWeeks.Length > 0 ? currentWeeks.Length - 1:0)];

                var query = from p in basedb.Week
                            //where p.CreateDate >= sDate && p.CreateDate <= eDate
                            where p.Weeks == iCurrentWeekOfYear
                            orderby p.CreateDate
                            select new { 
                                年 = p.Year,
                                月 = p.Month,
                                週 = p.Weeks,
                                員工編號 = p.EmpSN,
                                備註 = p.Comment,
                                建立時間 = p.CreateDate
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.員工編號 == EmpId);
                }

                DataTable dt = query.ToList().ToDataTable();

                return dt;
            }
            return null;
        }


        /// <summary>
        /// 依日期區間抓資料 (不能跨年
        /// </summary>
        /// <param name="Start"></param>
        /// <param name="End"></param>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        public WeekPage GetListByDateRange(DateTime Start, DateTime End, string EmpId)
        {
            if (Start.Year != End.Year)
            {
                throw new Exception("年份必須一樣");
            }

            if (End <= Start)
            {
                throw new Exception("結束日必須大於開始日");

            }

            WeekPage ViewModel = new WeekPage();
            ViewModel.EmpId = EmpId;
            ViewModel.Today = Start;
            ViewModel.Start = Start;
            ViewModel.End = End;

            //1.期間的週數
            int iStartWeekOfYear = Start.GetWeekOfYear();   //1
            int iEndWeekOfYear = End.GetWeekOfYear();       //3

            //2.再抓本週的日期區間
            DateTime[] startWeeks = Utils.GetCurrentDaysOfWeek(Start.Year, iStartWeekOfYear);
            DateTime[] endWeeks = Utils.GetCurrentDaysOfWeek(End.Year, iEndWeekOfYear);

            //3.取得本週重點
            string queryWeekTodo = (from p in basedb.Week
                                    where p.EmpSN == EmpId
                                    && p.Weeks >= iStartWeekOfYear
                                    && p.Weeks <= iEndWeekOfYear
                                    && p.Year == Start.Year
                                    select p.Comment).FirstOrDefault();

            ViewModel.WeekToDo = queryWeekTodo;


            for (int i = 0; i < End.Subtract(Start).Days; i++)
            {
                DateTime dt = Start.AddDays(i);
                DateOfWeek theDateOfThisWeek = new DateOfWeek();
                theDateOfThisWeek.Date = dt;

                //1.排程
                var o_query_schedule = from p in basedb.Schedule
                                       join c in basedb.Customer on p.Number equals c.Number
                                       where p.Date == dt.Date && p.EmpSN == ViewModel.EmpId && p.Status != -1
                                       select new CustomerWeekModel()
                                       {
                                           CustomerNumber = c.Number,
                                           CustomerName = c.Name,
                                           EmpSN = p.EmpSN,
                                           VisitDate = p.Date,
                                           VisitType = "排程",
                                           Type = p.Type,
                                           Context = p.ToDo,
                                           Status = p.Status,
                                           ReasonForError = p.Reason,
                                           VisitNote = p.ToDo
                                       };
                //2.VisitLog
                var o_query_visitlog = from v in basedb.VisitLog
                                       join c in basedb.Customer on v.CustomerNumber equals c.Number
                                       where v.Date == dt.Date && v.EmpSN == ViewModel.EmpId
                                       select new CustomerWeekModel()
                                       {
                                           CustomerNumber = c.Number,
                                           CustomerName = c.Name,
                                           EmpSN = v.EmpSN,
                                           VisitDate = v.Date,
                                           VisitType = "拜訪記錄",
                                           Type = "拜訪記錄",
                                           Context = v.Context,
                                           Status = v.Status,
                                           ReasonForError = string.Empty,
                                           VisitNote = string.Empty
                                       };


                //客戶
                var cus_query = o_query_schedule.Union(o_query_visitlog).ToList();

                //把客戶們加到該週
                if (cus_query != null)
                {
                    if (cus_query.Count() > 0)
                    {
                        theDateOfThisWeek.Customers.AddRange(cus_query);
                    }
                }

                ViewModel.DateOfThisWeek.Add(theDateOfThisWeek);
            }

            return ViewModel;
        }

        public WeekPage GetList(DateTime dtDate, string EmpId)
        {
            WeekPage ViewModel = new WeekPage();
            ViewModel.EmpId = EmpId;
            ViewModel.Today = dtDate;

            //1.取得今天是屬於今年的第幾週
            int iCurrentWeekOfYear = dtDate.GetWeekOfYear();

            //2.再抓本週的日期區間
            DateTime[] currentWeeks = Utils.GetCurrentDaysOfWeek(dtDate.Year, iCurrentWeekOfYear);

            //3.取得本週重點
            string queryWeekTodo = (from p in basedb.Week
                                 where p.EmpSN == EmpId
                                 && p.Weeks == iCurrentWeekOfYear
                                 && p.Year == dtDate.Year
                                 select p.Comment).FirstOrDefault();

            ViewModel.WeekToDo = queryWeekTodo;

            foreach (DateTime dt in currentWeeks)
            {
                DateOfWeek theDateOfThisWeek = new DateOfWeek();
                theDateOfThisWeek.Date = dt;

                //1.排程
                var o_query_schedule = from p in basedb.Schedule
                                        join c in basedb.Customer on p.Number equals c.Number
                                        where p.Date == dt.Date && p.EmpSN == ViewModel.EmpId && p.Status != -1
                                        select new CustomerWeekModel()
                                        {
                                            CustomerNumber = c.Number,
                                            CustomerName = c.Name,
                                            EmpSN = p.EmpSN,
                                            VisitDate = p.Date,
                                            VisitType = "排程",
                                            Type = p.Type,
                                            Context = p.ToDo,
                                            Status = p.Status,
                                            ReasonForError = p.Reason,
                                            VisitNote = p.ToDo
                                        };
                //2.VisitLog
                var o_query_visitlog = from v in basedb.VisitLog
                                       join c in basedb.Customer on v.CustomerNumber equals c.Number
                                       where v.Date == dt.Date && v.EmpSN == ViewModel.EmpId
                                       select new CustomerWeekModel()
                                       {
                                           CustomerNumber = c.Number,
                                           CustomerName = c.Name,
                                           EmpSN = v.EmpSN,
                                           VisitDate = v.Date,
                                           VisitType = "拜訪記錄",
                                           Type = "拜訪記錄",
                                           Context = v.Context,
                                           Status = v.Status,
                                           ReasonForError = string.Empty,
                                           VisitNote = string.Empty
                                       };


                //客戶
                var cus_query = o_query_schedule.Union(o_query_visitlog).ToList();

                //把客戶們加到該週
                if (cus_query != null)
                {
                    if (cus_query.Count() > 0)
                    {
                        theDateOfThisWeek.Customers.AddRange(cus_query);
                    }
                }

                ViewModel.DateOfThisWeek.Add(theDateOfThisWeek);
            }

            return ViewModel;
        }


        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<Week> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.Week( 
                    Id, EmpSN, Year, Month, Weeks, Comment, CreateDate, StartDate, EndDate)
                    Values 
                    (@Id, @EmpSN, @Year, @Month, @Weeks, @Comment, @CreateDate, @StartDate, @EndDate)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Year", Row.Year));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Month", Row.Month));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Weeks", Row.Weeks));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Comment", Row.Comment));
                cmdInsert.Parameters.Add(new SQLiteParameter("@StartDate", Row.StartDate.HasValue ? Row.StartDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EndDate", Row.EndDate.HasValue ? Row.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));


                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }

    }
}
