﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using admin.Models;

namespace admin.Service
{
    public class BulletinService : BaseService
    {
        public BulletinService()
        {

        }

        public static Bulletin Mapping(BulletinModel p)
        {
            Bulletin result = new Bulletin();

            result.Id = p.Id;
            result.Subject = p.Subject;
            result.Context = p.Context;
            result.Status = p.Status;
            result.CreateEmpNo = p.CreateEmpNo;
            result.FromDep = p.FromDep;
            result.CreateDate = p.CreateDate;
            result.Category = p.Category;
          
            return result;
        }
        public static BulletinModel Mapping(Bulletin p)
        {
            BulletinModel result = new BulletinModel();

            result.Id = p.Id;
            result.Subject = p.Subject;
            result.Context = p.Context;
            result.Status = p.Status;
            result.CreateEmpNo = p.CreateEmpNo;
            result.FromDep = p.FromDep;
            result.CreateDate = p.CreateDate;
            result.Category = p.Category;

            return result;
        }

        public static Expression<Func<Bulletin, BulletinModel>> Map = p =>
         new BulletinModel()
         {
            Id = p.Id,
            Subject = p.Subject,
            Context = p.Context,
            Status = p.Status,
            CreateEmpNo = p.CreateEmpNo,
            FromDep = p.FromDep,
            CreateDate = p.CreateDate,
            Category = p.Category
         };

        #region Web Ap

        /// <summary>
        /// 取得記錄
        /// </summary>
        public BulletinListModel GetList(string EmpSn, BulletinListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            BulletinListModel model = new BulletinListModel();

            if (cntEmp != null)
            {
                var o_query = from p in basedb.Bulletin
                              select p;

                if (!string.IsNullOrEmpty(page.SearchText))
                {
                    o_query = o_query.Where(p => p.Subject.Equals(page.SearchText));
                }

                var query = o_query.OrderByDescending(p => p.CreateDate);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        /// <summary>
        /// 取得公告
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BulletinModel Get(string EmpSN, string id)
        {
            BulletinModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                Data = base.basedb.Bulletin.Where(p => p.Id == gid).Select(Map).FirstOrDefault();
            }

            return Data;
        }

        public bool Create(string EmpSN, BulletinModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            try
            {
                entity.CreateEmpNo = EmpSN;
                entity.FromDep = "";
                Bulletin dbEntity = Mapping(entity);

                using (basedb)
                {
                    base.basedb.Bulletin.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("BulletinService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("BulletinService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            Guid _id;
            if (!Guid.TryParse(id, out _id))
            {
                return false;
            }
            var Data = base.basedb.Bulletin.Where(p => p.Id == _id).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    base.basedb.Bulletin.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(BulletinModel entity)
        {
            using (base.basedb)
            {
                Bulletin dbEntity = base.basedb.Bulletin.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    dbEntity.Subject = entity.Subject;
                    dbEntity.Context = entity.Context;
                    dbEntity.Status = entity.Status;
                    dbEntity.Category = entity.Category;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
        }
        #endregion

    }
}