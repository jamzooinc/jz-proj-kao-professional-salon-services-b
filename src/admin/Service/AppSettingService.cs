﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    public class AppSettingService : BaseService
    {
        /// <summary>
        /// Get all keys of app in database
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllKeys()
        {
            var query = from p in basedb.AppSetting
                        group p by new { p.FKey } into g
                        select g.Key.FKey;

            return query.ToList();
        }

        public Dictionary<string, object> Valid(string Key, decimal LastVersion)
        {
            var Last = (from p in basedb.AppSetting
                         where p.FKey == Key
                         orderby p.Version descending
                         select p).FirstOrDefault();

            if (Last != null)
            {
                if (Last.Version != LastVersion)
                {
                    Dictionary<string,object> _LastVersion = new Dictionary<string,object>();
                    _LastVersion.Add("LastVersion", Last.Version);
                    _LastVersion.Add("Value", Last.Value);

                    return _LastVersion;
                }
                return null;
            }
            else
            {
                throw new Exception("無資料");
            }

        }

    }
}