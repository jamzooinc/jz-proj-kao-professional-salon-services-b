﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;

    public class EmpService : BaseService, ISQLiteDbConverter<Employees>
    {
        public EmpService()
            : base()
        {
        }
        public static Employees Mapping(EmployeeModel p)
        {
            Employees result = new Employees();

            result.EmpSN = p.EmpSN;//PK值不可修改
            result.Name = p.Name;
            result.Name_en = p.Name_en;
            result.DepId = p.DepId;
            result.DepName = p.DepName;
            result.Permission = p.Permission;
            result.Tel = p.Tel;
            result.Mobile = p.Mobile;
            result.DeviceID = p.DeviceID;
            result.Mac = p.Mac;
            result.LoginId = p.LoginId;
            result.LoginPsd = p.LoginPsd;
            result.Email = p.Email;
            result.LoginErrorCount = p.LoginErrorCount;

            result.Title = p.Title;
            result.Area = p.Area;
            result.BossSN = p.BossSN;
            result.SalesCode = p.SalesCode;
            result.Manage1 = p.Manage1;
            result.Manage2 = p.Manage2;
            result.Manage3 = p.Manage3;
            result.PermissionTitle = p.PermissionTitle;

            result.SalesEmployee = p.SalesEmployee;
            result.SalesGroup = p.SalesGroup;
            result.SalesPerson = p.SalesPerson;
            result.SalesOffice = p.SalesOffice;

            return result;
        }
        public static EmployeeModel Mapping(Employees p)
        {
            EmployeeModel result = new EmployeeModel();

            result.EmpSN = p.EmpSN;
            result.Name = p.Name;
            result.DepName = p.DepName;
            result.Permission = p.Permission;
            result.Tel = p.Tel;
            result.Mobile = p.Mobile;
            result.DeviceID = p.DeviceID;
            result.Mac = p.Mac;
            result.LoginId = p.LoginId;
            result.LoginPsd = p.LoginPsd;
            result.Email = p.Email;
            result.LoginErrorCount = p.LoginErrorCount;

            //result.
            result.Title = p.Title;
            result.Area = p.Area;
            result.BossSN = p.BossSN;
            result.SalesCode = p.SalesCode;
            result.Manage1 = p.Manage1;
            result.Manage2 = p.Manage2;
            result.Manage3 = p.Manage3;
            result.PermissionTitle = p.PermissionTitle;

            result.SalesEmployee = p.SalesEmployee;
            result.SalesGroup = p.SalesGroup;
            result.SalesPerson = p.SalesPerson;
            result.SalesOffice = p.SalesOffice;

            return result;
        }

        public static Expression<Func<Employees, EmployeeModel>> Map = p => 
         new EmployeeModel() { 
            EmpSN = p.EmpSN,
            Name = p.Name,
            DepName = p.DepName,
            Permission = p.Permission,
            Tel = p.Tel,
            Mobile = p.Mobile,
            DeviceID = p.DeviceID,
            Mac = p.Mac,
            LoginId = p.LoginId,
            LoginPsd = p.LoginPsd,
            Email = p.Email,
            LoginErrorCount = p.LoginErrorCount,

            Title = p.Title,
            Area = p.Area,
            BossSN = p.BossSN,
            SalesCode = p.SalesCode,
            Manage1 = p.Manage1,
            Manage2 = p.Manage2,
            Manage3 = p.Manage3,
            PermissionTitle = p.PermissionTitle,

            SalesEmployee = p.SalesEmployee,
            SalesGroup = p.SalesGroup,
            SalesPerson = p.SalesPerson,
            SalesOffice = p.SalesOffice
        };

        /// <summary>
        /// Excel 匯入
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="dErrMsg"></param>
        /// <returns></returns>
        public bool ExcelImport(string Path, out Dictionary<string,object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();

            var excel = new ExcelQueryFactory(Path);
            var query = from c in excel.Worksheet<EmployeeModel>("Data")
                        select c;
            
            //fixed at 2013.05.20
            //序	部門名稱	區域	職稱	權責	部門別	Sales Code	主管別一	主管別二	主管別三	業務人員	員工姓名	英文全名

            //excel.AddMapping("Brand", "序");

            //excel.AddMapping("Title", "職稱");
            //excel.AddMapping("PermissionTitle", "權責");
            //excel.AddMapping("Manage1", "主管別一");
            //excel.AddMapping("Manage2", "主管別二");
            //excel.AddMapping("Manage3", "主管別三");


            #region 2013.08.06 import
            excel.AddMapping("DepName", "old部門別");

            excel.AddMapping("SalesOffice", "Sales Office");
            excel.AddMapping("SalesGroup", "Sales Group");
            excel.AddMapping("SalesPerson", "Sales Person");
            excel.AddMapping("SalesEmployee", "Sales Employee");
            excel.AddMapping("SalesCode", "Sales Code");

            excel.AddMapping("EmpSN", "old");
            excel.AddMapping("Name", "員工姓名");
            excel.AddMapping("Name_en", "英文全名");
            excel.AddMapping("DepId", "old部門別");

            excel.AddMapping("Area", "Sales Office");
            #endregion


            foreach (var row in query)
            {
                try
                {
                    if (!string.IsNullOrEmpty(row.EmpSN))
                    {
                        Employees dbEntity = Mapping(row);
                        if (dbEntity.SalesPerson.EndsWith("01"))
                        {
                            //業務主管
                            dbEntity.Permission = 256;         //by default  
                        }
                        else 
                        {
                            //駐區業務
                            dbEntity.Permission = 128;         //by default  
                        }
                        basedb.Employees.AddObject(dbEntity);
                        basedb.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    dErrMsg.Add(row.EmpSN, ex.Message);
                }
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                dErrMsg.Add("SaveChangesError", ex.Message);
            }

            return dErrMsg.Keys.Count() == 0;
        }


        /// <summary>
        /// 給 employee selector 使用
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <returns></returns>
        public EmployeeListModel GetListForSelector(string EmpSN)
        {
            AccountModel CurrentEmp = AccountModel.Create(EmpSN);
            EmployeeListModel l = new EmployeeListModel();

            if (CurrentEmp != null)
            {
                var o_query = from p in basedb.Employees
                              select p;

                if (CurrentEmp.IsInRoles(KpssRole.區業務主管))
                {
                    o_query = o_query.Where(p => p.SalesOffice == CurrentEmp.SalesOffice);
                }
                else if (CurrentEmp.IsInRoles(KpssRole.駐區業務))
                {
                    o_query = o_query.Where(p => p.EmpSN == CurrentEmp.SN);
                }

                var query = o_query.Select(p => new EmployeeModel() { EmpSN = p.EmpSN, Name = p.Name });

                l.Data = query.ToList();
            }

            //l.Data.Insert(0, new EmployeeModel() { Name = "全部", EmpSN = "" });

            return l;
        }

        /// <summary>
        /// 取得業務的資料
        /// </summary>
        /// <param name="EmpSn"></param>
        /// <param name="Permission"></param>
        /// <returns></returns>
        public EmployeeListModel Get業務(string EmpSN)
        {
            EmployeeListModel page = new EmployeeListModel();

            var query = from p in basedb.Employees
                        where (p.Permission & (int)KpssRole.駐區業務) == (int)KpssRole.駐區業務 && p.BossSN == EmpSN
                        orderby p.Permission descending
                        select p;

            try
            {
                //不需要分頁,直接使用即可
                page.Data = query.Select(Map).ToList();
                page.TotalRecords = page.Data.Count();

                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeListModel GetList(string EmpSn, EmployeeListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);
            if (cntEmp != null)
            {
                var o_query = from p in basedb.Employees
                            select p;

                if (!string.IsNullOrEmpty(page.StartWith))
                {
                    o_query = o_query.Where(p => p.Name.StartsWith(page.StartWith));
                }

                if (!cntEmp.IsInRoles(KpssRole.管理員))
                {
                    o_query = o_query.Where(p => p.Permission < cntEmp.Permission);
                }

                var query = o_query.OrderByDescending(p => p.Permission);
                try
                {
                    page.TotalRecords = query.Select(p => p.EmpSN).Count();
                    page.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return page;
        }

        public EmployeeModel Get(string id)
        {
            EmployeeModel Data = base.basedb.Employees.Where(p => p.EmpSN == id).Select(Map).FirstOrDefault();

            return Data;
        }

        public bool Create(EmployeeModel entity, out List<string> liErrMsgs)
        {
            liErrMsgs = new List<string>();

            if (base.basedb.Employees.Where(p => p.EmpSN == entity.EmpSN).Select(p => p.EmpSN).Count() != 0)
            {
                liErrMsgs.Add("員工編號已存在");
                return false;
            }

            try
            {
                Employees dbEntity = Mapping(entity);

                if (entity.Permissions != null)
                {
                    dbEntity.Permission = entity.Permissions.Sum();
                }

                using (basedb)
                {
                    base.basedb.Employees.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                liErrMsgs.Add(ex.Message); 
            }

            return liErrMsgs.Count() == 0;
        }

        public bool Delete(string id)
        {
            var Data = base.basedb.Employees.Where(p => p.EmpSN == id).FirstOrDefault();
            if (Data != null)
            {
                try
                {
                    base.basedb.Employees.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(EmployeeModel entity)
        {
            Employees dbEntity = base.basedb.Employees.Where(p => p.EmpSN == entity.EmpSN).FirstOrDefault();

            if (dbEntity != null)
            {
                dbEntity.EmpSN = entity.EmpSN;
                dbEntity.Name = entity.Name;
                dbEntity.DepName = entity.DepName;
                if (entity.Permissions != null)
                {
                    dbEntity.Permission = entity.Permissions.Sum();
                }
                dbEntity.Tel = entity.Tel;
                dbEntity.Mobile = entity.Mobile;
                dbEntity.DeviceID = entity.DeviceID;
                dbEntity.Mac = entity.Mac;
                dbEntity.LoginId = entity.LoginId;
                dbEntity.LoginPsd = entity.LoginPsd;
                dbEntity.Email = entity.Email;
                dbEntity.LoginErrorCount = entity.LoginErrorCount;

                dbEntity.Title = entity.Title;
                dbEntity.Area = entity.Area;
                dbEntity.BossSN = entity.BossSN;
                dbEntity.SalesCode = entity.SalesCode;
                dbEntity.Manage1 = entity.Manage1;
                dbEntity.Manage2 = entity.Manage2;
                dbEntity.Manage3 = entity.Manage3;
                dbEntity.PermissionTitle = entity.PermissionTitle;

                try
                {
                    basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此資料");
            }

            return false;
        }



        #region for Rest-API

        /// <summary>
        /// 判斷是否為自已的員工
        /// </summary>
        /// <param name="BossSN">我的編號</param>
        /// <param name="EmpSN">我員工的編號</param>
        /// <returns></returns>
        public bool IsMyEmp(string BossSN, string EmpSN)
        {
            //新的方式, update:2014-04-07
            //如果 Sales Code 相同則視為同部門
            var empEntity = basedb.Employees.Where(p => p.EmpSN == EmpSN).Select(p => p.SalesOffice).FirstOrDefault();
            var bossEntity = basedb.Employees.Where(p => p.EmpSN == BossSN).Select(p => p.SalesOffice).FirstOrDefault();

            if (empEntity != null && bossEntity != null)
            {
                return empEntity == bossEntity;
            }
            else
            {
                return false;
            }
            //舊的方式
            //var o_query = from p in basedb.Employees
            //              where p.BossSN == BossSN && p.EmpSN == EmpSN
            //              select p.EmpSN;

            //return (o_query.Count() > 0) ? true : false;
        }

        /// <summary>
        /// 更新密碼
        /// </summary>
        /// <param name="EmpNo"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <param name="ErrorMessages"></param>
        /// <returns></returns>
        public bool UpdatePassword(string EmpNo, string OldPassword, string NewPassword, out Dictionary<string,string> ErrorMessages)
        {
            ErrorMessages = new Dictionary<string, string>();

            var Emp = (from p in basedb.Employees
                         where p.EmpSN == EmpNo
                         select p).FirstOrDefault();

            if (Emp != null)
            {
                var EmpShip = (from p in basedb.Employeeship
                              where p.EmpId == Emp.EmpSN
                              select p).FirstOrDefault();

                if (EmpShip != null)
                {
                    if (EmpShip.Password == OldPassword)
                    {
                        //需驗證密碼
                        EmpShip.Password = NewPassword;
                        basedb.SaveChanges();
                    }
                    else
                    {
                        ErrorMessages.Add("OldPassword", "舊密碼錯誤");
                    }
                }
                else
                {
                    ErrorMessages.Add("OldPassword", "尚未建立密碼,請重新登入");
                }
            }
            else
            {
                ErrorMessages.Add("EmpNo", "無此員工");
            }

            return (ErrorMessages.Count == 0) ? true : false;
        }

        #endregion


        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<Employees> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.Employee ( 
                    EmpSN, Name, Name_en, DepId, DepName, Title, Permission, Tel, TelExt, Mobile, Email, BossSN, SalesCode, Manage1, Manage2, Manage3, PermissionTitle
                    )
                    Values 
                    (
                    @EmpSN, @Name, @Name_en, @DepId, @DepName, @Title, @Permission, @Tel, @TelExt, @Mobile, @Email, @BossSN, @SalesCode, @Manage1, @Manage2, @Manage3, @PermissionTitle
                    )";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Name", Row.Name));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Name_en", Row.Name_en));
                cmdInsert.Parameters.Add(new SQLiteParameter("@DepId", Row.DepId));
                cmdInsert.Parameters.Add(new SQLiteParameter("@DepName", Row.DepName));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Title", Row.Title));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Permission", Row.Permission));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Tel", Row.Tel));
                cmdInsert.Parameters.Add(new SQLiteParameter("@TelExt", Row.TelExt));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Mobile", Row.Mobile));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Email", Row.Email));
                cmdInsert.Parameters.Add(new SQLiteParameter("@BossSN", Row.BossSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@SalesCode", Row.SalesCode));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Manage1", Row.Manage1));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Manage2", Row.Manage2));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Manage3", Row.Manage3));
                cmdInsert.Parameters.Add(new SQLiteParameter("@PermissionTitle", Row.PermissionTitle));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}