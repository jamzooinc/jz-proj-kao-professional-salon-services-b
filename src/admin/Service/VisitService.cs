﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Library;
using admin.Extensions;
using System.Data;

namespace admin.Service
{
    public class VisitService : BaseService, ISQLiteDbConverter<VisitLog>
    {
        public VisitService() : base() { }

        public static VisitLog Mapping(VisitModel p)
        {
            VisitLog result = new VisitLog();

            result.Date = p.Date;
            result.CustomerNumber = p.CustomerNumber;
            result.Times = p.Times;
            result.EmpSN = p.EmpSN;
            result.Rating = p.Rating;
            result.Context = p.Context;
            result.FromTask = p.FromTask;
            result.ToTask = p.ToTask;
            result.StartTime = p.StartTime;
            result.EndTime = p.EndTime;
            result.Status = p.Status;

            return result;
        }

        public static VisitModel Mapping(VisitLog p)
        {
            VisitModel result = new VisitModel();

            result.Date = p.Date;
            result.CustomerNumber = p.CustomerNumber;
            result.Times = p.Times;
            result.EmpSN = p.EmpSN;
            result.Rating = p.Rating;
            result.Context = p.Context;
            result.FromTask = p.FromTask;
            result.ToTask = p.ToTask;
            result.StartTime = p.StartTime;
            result.EndTime = p.EndTime;
            result.Status = p.Status;

            return result;
        }

        public static VisitModel Mapping(VisitLog p, Employees m)
        {
            VisitModel result = new VisitModel();

            result.Date = p.Date;
            result.CustomerNumber = p.CustomerNumber;
            result.Times = p.Times;
            result.EmpName = m.Name;
            result.EmpSN = p.EmpSN;
            result.Rating = p.Rating;
            result.Context = p.Context;
            result.FromTask = p.FromTask;
            result.ToTask = p.ToTask;
            result.StartTime = p.StartTime;
            result.EndTime = p.EndTime;
            result.Status = p.Status;

            return result;
        }

        public static Expression<Func<VisitLog, VisitModel>> Map = p =>
        new VisitModel()
        {
            Date = p.Date,
            CustomerNumber = p.CustomerNumber,
            Times = p.Times,
            EmpSN = p.EmpSN,
            Rating = p.Rating,
            Context = p.Context,
            FromTask = p.FromTask,
            ToTask = p.ToTask,
            StartTime = p.StartTime,
            EndTime = p.EndTime,
            Status = p.Status
        };

        public static Expression<Func<VisitLog, EmployeeModel, VisitModel>> Map2 = (p, m) =>
       new VisitModel()
       {
           Date = p.Date,
           CustomerNumber = p.CustomerNumber,
           EmpName = m.Name,
           Times = p.Times,
           EmpSN = p.EmpSN,
           Rating = p.Rating,
           Context = p.Context,
           FromTask = p.FromTask,
           ToTask = p.ToTask,
           StartTime = p.StartTime,
           EndTime = p.EndTime,
           Status = p.Status
       };

        public List<SelectListItem> GetMyEmpsList()
        {
            var o_query = from p in basedb.Employees
                          select new SelectListItem() 
                          {
                                Value = p.EmpSN,
                                Text = p.Name
                          };

            return o_query.ToList();
        }

        #region for web ap

        public List<VisitModel> Q(string EmpSN, string Number , int take = 20, int skip = 0)
        {
            var o_query = from p in basedb.VisitLog
                          where p.EmpSN == EmpSN && p.CustomerNumber == Number
                          orderby p.Date descending
                          select p;

            return o_query.Select(Map).Skip(skip).Take(take).ToList();
        }

        /// <summary>
        /// 拜訪記錄
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="EmpId"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public VisitListModel GetList(string EmpSN, string EmpId, VisitListModel Pager)
        {
            return GetList(EmpSN, EmpId, Pager, "EXCEL");
        }

        /// <summary>
        /// 拜訪記錄
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="EmpId"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public VisitListModel GetList(string EmpSN, string EmpId, VisitListModel Pager, string Target)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSN);

            VisitListModel model = new VisitListModel();
            model.Status = Pager.Status;
            model.Month = Pager.Month;
            model.SearchText = Pager.SearchText;

            if (cntEmp != null)
            {
                var o_query = from p in basedb.VisitLog
                              join m in basedb.Employees on p.EmpSN equals m.EmpSN
                              select new { p, m };

                if (model.Status != -1)
                {
                    o_query = o_query.Where(p => p.p.Status == model.Status);
                }

                if (model.Month > 0)
                {
                    int currentYear = DateTime.Now.Year;
                    o_query = o_query.Where(p => p.p.Date.Month == model.Month 
                        && p.p.Date.Year == currentYear);
                }

                //如果是主管，需要先選擇自已的業務員
                if (cntEmp.IsInRoles(KpssRole.區業務主管))
                {
                    o_query = o_query.Where(p => p.p.EmpSN == EmpSN);

                    model.MyEmps = GetMyEmpsList();
                    model.EmpId = EmpSN;//原model沒有EmpId。查看DB，EmpId跟EmpSN是一樣的，故直接使用EmpSN  by：Lucien;201502/02
                }
                else if (cntEmp.IsInRoles(KpssRole.駐區業務))
                {
                    o_query = o_query.Where(p => p.p.EmpSN == EmpSN);
                }

                if (!string.IsNullOrEmpty(Pager.SearchText))
                {
                    o_query = o_query.Where(p => p.p.CustomerNumber.Contains(Pager.SearchText));
                }

                var query = o_query.OrderByDescending(p => p.p.Date);

                try
                {
                    model.Page = Pager.Page;
                    model.PageSize = Pager.PageSize;
                    model.TotalRecords = query.Select(p => p.p.EmpSN).Count();
                    if (Target != "EXCEL") 
                    {
                        model.Data = query.Skip(Pager.Skip).Take(Pager.Take)
                            .Select(p => Mapping(p.p, p.m))
                            .ToList();
                    }
                    else
                    {
                        //如果是Excel下載，就不分頁
                        model.Data = query.ToList().Select(p => Mapping(p.p, p.m)).ToList();
                            
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        #endregion

        //取得拜訪記錄
        public VisitListModel GetList(string EmpSN, string CustomerNumber, int Year, int Month)
        {
            VisitListModel Page = new VisitListModel();

            var query = from p in basedb.VisitLog
                        where p.EmpSN == EmpSN && p.CustomerNumber == CustomerNumber
                        select p;

            if (Year > 0)
            {
                query = query.Where(p => p.Date.Year == Year);
            }
            if (Month > 0)
            {
                query = query.Where(p => p.Date.Month == Month);
            }

            Page.Data = query.ToList().Select(p => Mapping(p)).ToList();

            return Page;
        }

        //新增拜訪記錄
        public bool Create(
            string EmpSN, string CustomerNumber, DateTime Date, int Rating, string Context,
            out Dictionary<string, object> ErrorMessage
            )
        {
            ErrorMessage = new Dictionary<string, object>();

            //get the older first
            VisitLog Entity = (from p in basedb.VisitLog
                          where p.Date.Date == Date.Date && p.EmpSN == EmpSN && p.CustomerNumber == CustomerNumber
                          orderby p.Times
                          select p).FirstOrDefault();

            //new 
            VisitLog dbData = new VisitLog();
            dbData.Date = Date;
            dbData.CustomerNumber = CustomerNumber;
            dbData.Times = Entity == null ? 0 : Entity.Times + 1;
            dbData.EmpSN = EmpSN;
            dbData.Status = 0;

            //startTime ~ EndTime
            dbData.StartTime = Date;
            dbData.EndTime = Date;
            //dbData.

            try
            {
                basedb.VisitLog.AddObject(dbData);
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorMessage.Add("inner error", ex.Message);
            }

            return (ErrorMessage.Keys.Count() == 0) ? true : false;
        }

        /// <summary>
        /// 移除拜訪記錄
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="CustomerNumber"></param>
        /// <param name="Date"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public bool Delete(
            string EmpSN, string CustomerNumber, DateTime Date,
            out Dictionary<string, object> ErrorMessage
            )
        {
            ErrorMessage = new Dictionary<string, object>();

            VisitLog dbEntity = (from p in basedb.VisitLog
                                where p.Date.Date == Date.Date && p.EmpSN == EmpSN && p.CustomerNumber == CustomerNumber
                                select p).FirstOrDefault();

            try
            {
                if (dbEntity != null)
                {
                    basedb.VisitLog.DeleteObject(dbEntity);
                    basedb.SaveChanges();
                }
                else
                {
                    ErrorMessage.Add("database error", "no permission");
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Add("Service error", ex.Message);
            }

            return (ErrorMessage.Keys.Count() == 0) ? true : false;
        }

        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<VisitLog> source)
        {

            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.VisitLog ( 
                    Id, Date, CustomerNumber, Times, EmpSN, Rating, Context, FromTask, ToTask, StartTime, EndTime, Status) 
                    Values 
                    (@Id, @Date, @CustomerNumber, @Times, @EmpSN, @Rating, @Context, @FromTask, @ToTask, @StartTime, @EndTime, @Status)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Date", Row.Date.ToString("yyyy/MM/dd")));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CustomerNumber", Row.CustomerNumber));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Times", Row.Times));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Rating", Row.Rating));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Context", Row.Context));
                cmdInsert.Parameters.Add(new SQLiteParameter("@FromTask", Row.FromTask));
                cmdInsert.Parameters.Add(new SQLiteParameter("@ToTask", Row.ToTask));
                cmdInsert.Parameters.Add(new SQLiteParameter("@StartTime", Row.StartTime));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EndTime", Row.EndTime));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Status", Row.Status));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }

        public System.Data.DataTable GetExcelDataByDateRange(DateTime Start, DateTime End, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iStartWeekOfYear = Start.GetWeekOfYear();
            int iEndWeekOfYear = End.GetWeekOfYear();

            if (iEndWeekOfYear > 0 && iStartWeekOfYear > 0 && (iEndWeekOfYear >= iStartWeekOfYear))
            {
                var query = from p in basedb.VisitLog
                            where p.Date >= Start && p.Date <= End
                            orderby p.Date
                            select new
                            {
                                日期 = p.Date,
                                客戶編號 = p.CustomerNumber,
                                客戶名稱 = basedb.Customer.Where(s => s.Number == p.CustomerNumber).Select(s => s.Name).FirstOrDefault(),
                                業務編號 = p.EmpSN,
                                業務姓名 = basedb.Employees.Where(s => s.EmpSN == p.EmpSN).Select(s => s.Name).FirstOrDefault(),
                                排名 = p.Rating,
                                內容 = p.Context,
                                指派者 = p.FromTask,
                                指派者姓名 = basedb.Employees.Where(s => s.EmpSN == p.FromTask).Select(s => s.Name).FirstOrDefault(),
                                被指派者 = p.ToTask,
                                被指派者姓名 = basedb.Employees.Where(s => s.EmpSN == p.ToTask).Select(s => s.Name).FirstOrDefault(),
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.業務編號 == EmpId);
                }

                DataTable dt = query.ToDataTable();

                return dt;
            }
            return null;
        }

        public System.Data.DataTable GetExcelData(DateTime dtDate, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iCurrentWeekOfYear = dtDate.GetWeekOfYear();

            //2.再抓本週的日期區間
            DateTime[] currentWeeks = Utils.GetCurrentDaysOfWeek(dtDate.Year, iCurrentWeekOfYear);

            if (currentWeeks.Length > 0)
            {
                DateTime sDate = currentWeeks[0];
                DateTime eDate = currentWeeks[(currentWeeks.Length > 0 ? currentWeeks.Length - 1 : 0)];


                var query = from p in basedb.VisitLog
                            where p.Date >= sDate && p.Date <= eDate
                            orderby p.Date
                            select new
                            {
                                日期 = p.Date,
                                客戶編號 = p.CustomerNumber,
                                業務編號 = p.EmpSN,
                                排名 = p.Rating,
                                內容 = p.Context,
                                指派者 = p.FromTask,
                                被指派者 = p.ToTask
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.業務編號 == EmpId);
                }

                DataTable dt = query.ToDataTable();

                return dt;
            }
            return null;
        }
    }
}
