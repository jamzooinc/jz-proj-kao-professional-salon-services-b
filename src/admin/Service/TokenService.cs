﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using admin.Extensions;
using admin.Models;

namespace admin.Service
{
    public class TokenService : BaseService
    {

        /// <summary>
        /// 登入後要加一個 token
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Errors"></param>
        /// <returns></returns>
        public static bool Add(string EmpSN,out string Token ,out List<string> Errors)
        {
            Errors = new List<string>();
            Token = string.Empty;

            using (KpssDb db = new KpssDb())
            {
                Token newToken = db.Token.Where(p => p.EmpSN == EmpSN).FirstOrDefault() ?? new Token();

                newToken.Expired = DateTime.Now.AddDays(1);
                newToken.Id = EmpSN.ToHash256();

                try
                {
                    if (string.IsNullOrEmpty(newToken.EmpSN))
                    {
                        newToken.EmpSN = EmpSN;
                        db.Token.AddObject(newToken);
                    }

                    db.SaveChanges();

                    Token = newToken.Id;

                    return true;
                }
                catch (Exception ex)
                {
                    Errors.Add(ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// 刪除一個 Token
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Errors"></param>
        /// <returns></returns>
        public static bool Delete(string Token, out List<string> Errors)
        {
            Errors = new List<string>();

            using (KpssDb db = new KpssDb())
            {
                var tkn = (from p in db.Token
                           where p.Id == Token
                           select p).FirstOrDefault();

                if (tkn != null)
                {
                    try
                    {
                        db.Token.DeleteObject(tkn);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Errors.Add(ex.Message);
                    }
                }
                else
                {
                    Errors.Add("Token not exist");
                }
            }
            return (Errors.Count == 0) ? true : false;
        }


        /// <summary>
        /// 激活 Token
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Errors"></param>
        /// <returns></returns>
        public static bool Actived(string Token, out List<string> Errors, out AccountModel Emp)
        {
            Errors = new List<string>();
            Emp = null;
            DateTime _now = DateTime.Now;   

            using (KpssDb db = new KpssDb())
            {
                var tkn = (from p in db.Token
                           where p.Id == Token
                           select p).FirstOrDefault();

                if (tkn != null)
                {
                    //1.先判斷是否過期 
                    if (tkn.Expired > _now)
                    {
                        try
                        {
                            Emp = AccountService.GetBySN(tkn.EmpSN);

                            //2.判斷是否有這個員工
                            if (Emp != null)
                            {
                                tkn.Expired = DateTime.Now.AddDays(1);
                                db.SaveChanges();
                            }
                            else
                            {
                                Errors.Add("無此員工");
                            }
                        }
                        catch (Exception ex)
                        {
                            Errors.Add(ex.Message);
                        }
                    }
                    else
                    {
                        Errors.Add("帳號過期,請重新登入");
                    }
                   
                }
                else
                {
                    Errors.Add("Token not exist");
                }
            }
            return (Errors.Count == 0) ? true : false;
        }
    }
}