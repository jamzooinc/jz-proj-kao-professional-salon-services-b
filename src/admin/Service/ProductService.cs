﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    using Extensions;
    using System.Web.Mvc;
    
    public class ProductService : BaseService, ISQLiteDbConverter<Product>
    {
        public ProductService()
            : base()
        {
        }

        public static Product Mapping(ProductModel p)
        {
            Product result = new Product();

            result.Id = p.Id;
            result.Brand = p.Brand;
            result.Type = p.Type;
            if (p.Types != null)
            {
                result.Type = string.Join(",", p.Types);
            }
            result.Serial = p.Serial;
            result.SN = p.SN;
            result.Pname = p.Pname;
            result.Spec = p.Spec;
            result.Summary = p.Summmary;
            result.CreateDate = p.CreateDate;
            result.Price = p.Price;

            return result;
        }
        public static ProductModel Mapping(Product p)
        {
            ProductModel result = new ProductModel();

            result.Id = p.Id;
            result.Brand = p.Brand;
            result.Type = p.Type;
            if (p.Type != null)
            {
                result.Types = p.Type.SplitToArrary(",");
            }
            result.Serial = p.Serial;
            result.SN = p.SN;
            result.Pname = p.Pname;
            result.Spec = p.Spec;
            result.Summmary = p.Summary;
            result.CreateDate = p.CreateDate;
            result.Price = p.Price;

            return result;
        }

        public static Expression<Func<Product, ProductModel>> Map = p =>
         new ProductModel()
         {
            Id = p.Id,
            Brand = p.Brand,
            Type = p.Type,
            Serial = p.Serial,
            SN = p.SN,
            Pname = p.Pname,
            Spec = p.Spec,
            Summmary = p.Summary,
            CreateDate = p.CreateDate,
            Price = p.Price
         };

        /// <summary>
        /// Excel 匯入
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="dErrMsg"></param>
        /// <returns></returns>
        public bool ExcelImport(string Path, out Dictionary<string, object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();

            var excel = new ExcelQueryFactory(Path);
            var query = from c in excel.Worksheet<ProductModel>("Data")
                        select c;

            excel.AddMapping("Brand", "大分類");
            excel.AddMapping("Type", "中分類");
            excel.AddMapping("Serial", "子類別(中文)");
            excel.AddMapping("SN", "產品序號");
            excel.AddMapping("Pname", "產品名稱");
            excel.AddMapping("Spec", "規格");
            excel.AddMapping("Price", "備註"); 

            foreach (var row in query)
            {
                try
                {
                    Product dbEntity = Mapping(row);
                    basedb.Product.AddObject(dbEntity);
                }
                catch (Exception ex)
                {
                    dErrMsg.Add(row.Id.ToString(), ex.Message);
                }
            }
            try
            {
                var ss = query.Where(p => p.Type == null);
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                dErrMsg.Add("Excel Import Error", ex.Message);
            }

            //try
            //{

            //}
            //catch (Exception ex)
            //{
            //    dErrMsg.Add("SaveChangesError", ex.Message);
            //}

            return dErrMsg.Keys.Count() == 0;
        }

        /// <summary>
        /// 取得品牌
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetBrands()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var o_query = from p in basedb.Product
                          group p by new { p.Brand } into g
                          select g.Key.Brand;

            foreach (var item in o_query)
            {
                list.Add(new SelectListItem() { Value = item, Text = item }); 
            }
            return list;
        }

        /// <summary>
        /// 取得品牌的子類別
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetSerialsByBrand(string Brand)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var o_query = from p in basedb.Product
                          where p.Brand == Brand
                          group p by new { p.Serial } into g
                          select g.Key.Serial;

            foreach (var item in o_query)
            {
                list.Add(new SelectListItem() { Value = item, Text = item });
            }
            return list;
        } 

        #region Web Ap

        /// <summary>
        /// 取得某個人的待交辦事項 記錄
        /// </summary>
        /// <param name="EmpSn">目前登入業務的SN</param>
        /// <param name="EmpId">選擇的業務編號(For 業務主管)</param>
        /// <param name="Type">Memo : 待辦事項 /Assignment : 交辦事項</param>
        /// <param name="page">篩選的Pager</param>
        /// <returns></returns>
        public ProductListModel GetList(string EmpSn, string EmpId, string serial, ProductListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            ProductListModel model = new ProductListModel();

            if (cntEmp != null)
            {
                var o_query = from p in basedb.Product
                              select p;

                if (!string.IsNullOrEmpty(serial))
                {
                    o_query = o_query.Where(p => p.Serial == serial);
                }

                if (!string.IsNullOrEmpty(page.SearchText))
                {
                    o_query = o_query.Where(p => p.Pname.Contains(page.SearchText));
                }

                var query = o_query.OrderByDescending(p => p.SN);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductModel Get(string EmpSN, string id)
        {
            ProductModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                Data = base.basedb.Product.Where(p => p.Id == gid).Select(Map).FirstOrDefault();
            }

            return Data;
        }

        public bool Create(string EmpSN, ProductModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            try
            {
                Product dbEntity = Mapping(entity);

                using (basedb)
                {
                    base.basedb.Product.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("ProductService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("ProductService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            Guid gid = Guid.Empty;
            if (!Guid.TryParse(id, out gid))
            {
                return false;
            }
            var Data = base.basedb.Product.Where(p => p.Id == gid).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    base.basedb.Product.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(string EmpSN, ProductModel entity, out Dictionary<string, Object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();
            using (base.basedb)
            {
                Product dbEntity = base.basedb.Product.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    //dbEntity = Mapping(entity);
                    dbEntity.Brand = entity.Brand;
                    if (entity.Types != null)
                    {
                        dbEntity.Type = string.Join(",", entity.Types);
                    }
                    dbEntity.Serial = entity.Serial;
                    dbEntity.SN = entity.SN;
                    dbEntity.Pname = entity.Pname;
                    dbEntity.Spec = entity.Spec;
                    dbEntity.Summary = entity.Summmary;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
            //return false;
        }
        #endregion


        /// <summary>
        /// SQLite專用的
        /// </summary>
        /// <param name="Conn"></param>
        /// <param name="Trans"></param>
        /// <param name="source"></param>
        public void Run(SQLiteConnection Conn, SQLiteTransaction Trans, IQueryable<Product> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.Product( 
                    Id, Brand, Type, Serial, SN, Pname, Spec, Summary, CreateDate)
                    Values 
                    (@Id, @Brand, @Type, @Serial, @SN, @Pname, @Spec, @Summary, @CreateDate)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Brand", Row.Brand));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Type", Row.Type));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Serial", Row.Serial));
                cmdInsert.Parameters.Add(new SQLiteParameter("@SN", Row.SN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Pname", Row.Pname));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Spec", Row.Spec));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Summary", Row.Summary));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}