﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using admin.Models;
using admin.Library;
using admin.Extensions;
using System.Data;

namespace admin.Service
{
   
    /// <summary>
    /// 排程
    /// </summary>
    public class ScheduleService : BaseService, ISQLiteDbConverter<Schedule>
    {
        #region Mapping
        public static Schedule Mapping(ScheduleModel p)
        {
            Schedule result = new Schedule();

            result.Id = p.Id;
            result.Date = p.Date;
            result.Number = p.CustomerNumber;
            result.EmpSN = p.EmpSN;
            result.Year = p.Year;
            result.Month = p.Month;
            result.Day = p.Day;
            result.Status = p.Status;
            result.StartTime = p.StartTime;
            result.EndTime = p.EndTime;
            result.ToDo = p.ToDo;
            result.Reason = p.Reason;
            result.Type = p.Type;

            return result;
        }
        public static ScheduleModel Mapping(Schedule p)
        {
            ScheduleModel result = new ScheduleModel();

            result.Id = p.Id;
            result.Date = p.Date;
            result.CustomerNumber = p.Number;
            result.EmpSN = p.EmpSN;
            result.Year = p.Year;
            result.Month = p.Month;
            result.Day = p.Day;
            result.Status = p.Status;
            result.StartTime = p.StartTime;
            result.EndTime = p.EndTime;
            result.ToDo = p.ToDo;
            result.Reason = p.Reason;
            result.Type = p.Type;

            return result;
        }

        public static Expression<Func<Schedule, ScheduleModel>> Map = p =>
         new ScheduleModel()
         {
            Id = p.Id, 
            Date = p.Date,
            CustomerNumber = p.Number,
            EmpSN = p.EmpSN,
            Year = p.Year,
            Month = p.Month,
            Day = p.Day,
            Status = p.Status,
            StartTime = p.StartTime,
            EndTime = p.EndTime,
            ToDo = p.ToDo,
            Reason = p.Reason,
            Type = p.Type
         };
        #endregion
        /// <summary>
        /// 取得
        /// </summary>
        /// <param name="Criteria"></param>
        /// <returns></returns>
        public ScheduleListModel GetList(string EmpSN, ScheduleListModel Criteria)
        {
            ScheduleListModel model = new ScheduleListModel();

            var o_query = from p in basedb.Schedule
                          join c in basedb.Customer on p.Number equals c.Number
                          where p.EmpSN == Criteria.EmpId
                          && p.Year == Criteria.Year
                          && p.Month == Criteria.Month
                          select new { p, c.Name };

            var query = o_query;

            try
            {
                model.Page = Criteria.Page;
                model.PageSize = Criteria.PageSize;
                model.TotalRecords = query.Select(p => p.p.Id).Count();
                model.Data = query.Select(p => new ScheduleModel() {
                    CustomerName = p.Name,
                    Id = p.p.Id,
                    Date = p.p.Date,
                    CustomerNumber = p.p.Number,
                    EmpSN = p.p.EmpSN,
                    Year = p.p.Year,
                    Month = p.p.Month,
                    Day = p.p.Day,
                    Status = p.p.Status,
                    StartTime = p.p.StartTime,
                    EndTime = p.p.EndTime,
                    ToDo = p.p.ToDo,
                    Reason = p.p.Reason,
                    Type = p.p.Type
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

       
        public ScheduleModel Get(Guid Id)
        {
            var o_query = (from p in basedb.Schedule
                           join c in basedb.Customer on p.Number equals c.Number
                           where p.Id == Id
                           select new { p, Name = c.Name }).FirstOrDefault();


            try
            {
                ScheduleModel Entity = Mapping(o_query.p);
                Entity.CustomerName = o_query.Name;

                Entity.strDate = Entity.Date.ToString("yyyy/MM/dd");
                Entity.strStartTime = (Entity.StartTime.HasValue) ? Entity.StartTime.Value.ToString("HH:mm:ss") : string.Empty;
                Entity.strEndTime = (Entity.EndTime.HasValue)? Entity.EndTime.Value.ToString("HH:mm:ss") : string.Empty;

                return Entity;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool IsCustomerExist(string Number)
        {
            return basedb.Customer.Where(p => p.Number == Number).Select(p => p.Number).Count() > 0;
        }
        public bool IsEmpExist(string EmpSN)
        {
            return basedb.Employees.Where(p => p.EmpSN == EmpSN).Select(p => p.EmpSN).Count() > 0;
        }

        public bool Delete(Guid Id, out ScheduleModel Sche, out Dictionary<string, object> ErrMsg)
        {
            ErrMsg = new Dictionary<string, object>();
            Sche = null;

            Schedule log = basedb.Schedule.Where(p => p.Id == Id).FirstOrDefault();


            if (log != null)
            {
                Sche = Mapping(log);
                Sche.strDate = Sche.Date.ToString("yyyy/MM/dd");
                Sche.strStartTime = (Sche.StartTime.HasValue) ? Sche.StartTime.Value.ToString("HH:mm:ss") : string.Empty;
                Sche.strEndTime = (Sche.EndTime.HasValue) ? Sche.EndTime.Value.ToString("HH:mm:ss") : string.Empty;

                basedb.Schedule.DeleteObject(log);
                try
                {

                    basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    ErrMsg.Add(" ", ex.Message);
                    return false;
                }
            }
            else
            {
                ErrMsg.Add(" ", "無資料");
                return false;
            }
        }

        public bool Create(string EmpSN, ScheduleModel Model, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            if (!IsCustomerExist(Model.CustomerNumber))
            {
                ErrorMsg.Add("CustomerNumber", "該客戶不存在");
                return false;
            }

            if (!IsEmpExist(Model.EmpSN))
            {
                ErrorMsg.Add("EmpSN", "該員工不存在");
                return false;
            }


            
            try
            {
                DateTime dtApplyDate = DateTime.ParseExact(Model.strDate, "yyyy/MM/dd", null);
                Model.Year = dtApplyDate.Year;
                Model.Month = dtApplyDate.Month;
                Model.Day = dtApplyDate.Day;
                Schedule dbEntity = Mapping(Model);

                dbEntity.Date = DateTime.ParseExact(Model.strDate, "yyyy/MM/dd", CultureInfo.InvariantCulture);
                dbEntity.StartTime = DateTime.ParseExact(Model.strStartTime, "HH:mm:ss", CultureInfo.InvariantCulture);
                dbEntity.EndTime = DateTime.ParseExact(Model.strEndTime, "HH:mm:ss", CultureInfo.InvariantCulture);
                

                base.basedb.Schedule.AddObject(dbEntity);
                base.basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("customerService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("customerService.Create.Exception", ex.Message);
                }
                return false;
            }
        }


        public void Run(SQLiteConnection Conn, SQLiteTransaction Trans, IQueryable<Schedule> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.Schedule ( 
                    Id, Date, Year, Month, Day, Number, Name, Type, ToDo, Reason, EmpId, Status, SubTitle, Summary, DayOfWeek, StartTime, EndTime) 
                    Values 
                    (@Id, @Date, @Year, @Month, @Day, @Number, @Name, @Type, @ToDo, @Reason, @EmpId, @Status, @SubTitle, @Summary, @DayOfWeek, @StartTime, @EndTime)";

                SQLiteCommand cmdBulletinInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Date", Row.Date.ToString("yyyy/MM/dd")));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Year", Row.Year));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Month", Row.Month));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Day", Row.Day));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Number", Row.Number));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Name", Row.Name));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Type", Row.Type));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@ToDo", Row.ToDo));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Reason", Row.Reason));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@EmpId", Row.EmpSN));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Status", Row.Status));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@SubTitle", Row.SubTitle));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Summary", Row.Summary));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@DayOfWeek", Row.DayOfWeek));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@StartTime", Row.StartTime));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@EndTime", Row.EndTime));

                int nEffect = cmdBulletinInsert.ExecuteNonQuery();
            }
        }

        public System.Data.DataTable GetExcelDataByDateRange(DateTime Start, DateTime End, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iStartWeekOfYear = Start.GetWeekOfYear();
            int iEndWeekOfYear = End.GetWeekOfYear();

            if (iEndWeekOfYear > 0 && iStartWeekOfYear > 0 && (iEndWeekOfYear >= iStartWeekOfYear))
            {
                var query = from p in basedb.Schedule
                            where p.Date >= Start && p.Date <= End && p.Status != -1
                            orderby p.Date
                            select new
                            {
                                日期 = p.Date,
                                年 = p.Year,
                                月 = p.Month,
                                日 = p.Day,
                                客戶編號 = p.Number,
                                客戶名稱 = basedb.Customer.Where(s => s.Number == p.Number).Select(s => s.Name).FirstOrDefault(),
                                名稱 = p.Name,
                                類型 = p.Type,
                                ToDo = p.ToDo,
                                理由 = p.Reason,
                                業務編號 = p.EmpSN,
                                業務姓名 = basedb.Employees.Where(s => s.EmpSN == p.EmpSN).Select(s => s.Name).FirstOrDefault(),
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.業務編號 == EmpId);
                }

                DataTable dt = query.ToDataTable();

                return dt;
            }
            return null;
        }

        public System.Data.DataTable GetExcelData(DateTime dtDate, string EmpId)
        {
            //1.取得今天是屬於今年的第幾週
            int iCurrentWeekOfYear = dtDate.GetWeekOfYear();

            //2.再抓本週的日期區間
            DateTime[] currentWeeks = Utils.GetCurrentDaysOfWeek(dtDate.Year, iCurrentWeekOfYear);

            if (currentWeeks.Length > 0)
            {
                DateTime sDate = currentWeeks[0];
                DateTime eDate = currentWeeks[(currentWeeks.Length > 0 ? currentWeeks.Length - 1 : 0)];


                var query = from p in basedb.Schedule
                            where p.Date >= sDate && p.Date <= eDate && p.Status != -1
                            orderby p.Date
                            select new
                            {
                                日期 = p.Date,
                                年 = p.Year,
                                月 = p.Month,
                                日 = p.Day,
                                客戶編號 = p.Number,
                                名稱 = p.Name,
                                類型 = p.Type,
                                ToDo = p.ToDo,
                                理由 = p.Reason,
                                業務編號 = p.EmpSN
                            };

                if (!string.IsNullOrEmpty(EmpId))
                {
                    query = query.Where(p => p.業務編號 == EmpId);
                }

                DataTable dt = query.ToDataTable();

                return dt;
            }
            return null;
        }
    }
}