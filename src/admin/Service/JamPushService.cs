﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JamZoo.JamPush.Demo;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    using Extensions;

    public class JamPushService : BaseService
    {
        public JamPushService()
            : base()
        {
        }

        public static JamPush Mapping(JamPushModel p)
        {
            JamPush result = new JamPush();
                  
            result.Id = p.Id;
            result.Title = p.Title;
            result.Summary = p.Summary;
            result.Status = p.Status;
            result.Reponse = p.Response;
            result.Request = p.Request;
            result.CreateEmpSN = p.CreateEmpSN;
            result.CreateDate = p.CreateDate;

            return result;
        }
        public static JamPushModel Mapping(JamPush p)
        {
            JamPushModel result = new JamPushModel();

            result.Id = p.Id;
            result.Title = p.Title;
            result.Summary = p.Summary;
            result.Status = p.Status;
            result.Response = p.Reponse;
            result.Request = p.Request;
            result.CreateEmpSN = p.CreateEmpSN;
            result.CreateDate = p.CreateDate;

            return result;
        }

        public static Expression<Func<JamPush, JamPushModel>> Map = p =>
         new JamPushModel()
         {
            Id = p.Id,
            Title = p.Title,
            Summary = p.Summary,
            Status = p.Status,
            Response = p.Reponse,
            Request = p.Request,
            CreateEmpSN = p.CreateEmpSN,
            CreateDate = p.CreateDate
         };
       
        #region Web Ap

        /// <summary>
        /// 取得某個人的待交辦事項 記錄
        /// </summary>
        /// <param name="EmpSn">目前登入業務的SN</param>
        /// <param name="EmpId">選擇的業務編號(For 業務主管)</param>
        /// <param name="Type">Memo : 待辦事項 /Assignment : 交辦事項</param>
        /// <param name="page">篩選的Pager</param>
        /// <returns></returns>
        public JamPushListModel GetList(string EmpSn, string EmpId, JamPushListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            JamPushListModel model = new JamPushListModel();

            if (cntEmp != null)
            {
                var o_query = from p in basedb.JamPush
                              where p.CreateEmpSN == EmpSn
                              select p;


                var query = o_query.OrderByDescending(p => p.CreateDate);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JamPushModel Get(string EmpSN, string id)
        {
            JamPushModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                Data = base.basedb.JamPush.Where(p => p.Id == gid).Select(Map).FirstOrDefault();
            }

            return Data;
        }

        public static string REST_CODE
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["JamPushRestCode"].ToString();
            }
        }

        public static string APP_KEY
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["JamPushAppKey"].ToString();
            }
        }

        public bool PushSender(JamPushModel entity)
        {
            RestAPI client = new RestAPI(REST_CODE, APP_KEY);
            try
            {
                var response = client.ActiveMessage(entity.Title, entity.Summary);
                entity.Response = Newtonsoft.Json.JsonConvert.SerializeObject(response);
                entity.Status = 1;
                return true;
            }
            catch (Exception ex)
            {
                // need to save the error message to databasoe
                if (ex.InnerException != null)
                {
                    throw new Exception("Push Sender Error:" + ex.InnerException.Message);
                }
                else
                {
                    throw new Exception("Push Sender Error:" + ex.Message);
                }
            }
        }

        public bool Create(string EmpSN, JamPushModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            try
            {
                entity.CreateEmpSN = EmpSN;
                using (basedb)
                {
                    if (PushSender(entity))
                    {
                        JamPush dbEntity = Mapping(entity);
                        base.basedb.JamPush.AddObject(dbEntity);
                        base.basedb.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("JamPushService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("JamPushService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            Guid gid = Guid.Empty;
            if (!Guid.TryParse(id, out gid))
            {
                return false;
            }
            var Data = base.basedb.JamPush.Where(p => p.Id == gid).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    base.basedb.JamPush.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(string EmpSN, JamPushModel entity, out Dictionary<string, Object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();
            using (base.basedb)
            {
                JamPush dbEntity = base.basedb.JamPush.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    //dbEntity = Mapping(entity);

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
            //return false;
        }
        #endregion

    }
}