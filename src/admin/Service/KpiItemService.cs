﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
using System.Linq.Expressions;
using System.Web.Mvc;
using Models;

    public class KpiItemService : BaseService, ISQLiteDbConverter<KpiItem>
    {
        public KpiItemService()
            : base()
        {
        }
        public static KpiItem Mapping(KpiItemModel p)
        {
            KpiItem result = new KpiItem();

            result.Id = p.Id;
            result.RootId = p.RootId;
            result.Name = p.Name;
            result.Layer = p.Layer;
            result.CreateDate = p.CreateDate;
            result.Brand = p.Brand;

            return result;
        }
        public static KpiItemModel Mapping(KpiItem p)
        {
            KpiItemModel result = new KpiItemModel();

            result.Id = p.Id;
            result.RootId = p.RootId;
            result.Name = p.Name;
            result.Layer = p.Layer;
            result.CreateDate = p.CreateDate;
            result.Brand = p.Brand;

            return result;
        }

        public static Expression<Func<KpiItem, KpiItemModel>> Map = p => 
         new KpiItemModel() { 
            Id = p.Id,
            RootId = p.RootId,
            Brand = p.Brand,
            Name = p.Name, 
            Layer = p.Layer,
            CreateDate = p.CreateDate
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IsNeedSetRoot"></param>
        /// <returns></returns>
        public List<SelectListItem> GetRootLayer(bool IsNeedSetRoot)
        {
            List<SelectListItem> rList = new List<SelectListItem>();
            if (IsNeedSetRoot)
            {
                rList.Add(new SelectListItem() { Value = Guid.Empty.ToString(), Text = "設為大類別" });
            }
            var RootNode = (from p in basedb.KpiItem
                            where p.RootId == Guid.Empty
                            select p).ToList().Select(p => new SelectListItem() { Value = p.Id.ToString(), Text = p.Name });


            foreach (var node in RootNode)
            {
                rList.Add(node);
            }

            return rList;
        }

        public List<SelectListItem> GetByBrand(string strBrand)
        {
            List<SelectListItem> rList = new List<SelectListItem>();

            var RootNode = (from p in basedb.KpiItem
                            where p.Brand == strBrand && p.RootId == Guid.Empty
                            select p).ToList().Select(p => new SelectListItem() { Value = p.Id.ToString(), Text = p.Name });

            foreach (var node in RootNode)
            {
                rList.Add(node);
            }
            return rList;
        }

        public List<SelectListItem> GetByParentId(string strParentId)
        {
            List<SelectListItem> rList = new List<SelectListItem>();

            Guid ParentId;
            if (Guid.TryParse(strParentId, out ParentId))
            {
                var RootNode = (from p in basedb.KpiItem
                                where p.RootId == ParentId
                                select p).ToList().Select(p => new SelectListItem() { Value = p.Id.ToString(), Text = p.Name });

                foreach (var node in RootNode)
                {
                    rList.Add(node);
                }
            }
            return rList;
        }

        public List<KpiItemModel> TreeMenu(KpiItemModel CurrntNode, List<KpiItemModel> Nodes, int Layer)
        {
            List<KpiItemModel> returnList = new List<KpiItemModel>();

            var ChildNode = Nodes.Where(p => p.RootId == CurrntNode.Id);

            foreach (var node in ChildNode)
            {
                node.Layer = Layer;
                node.Childern = TreeMenu(node, Nodes, Layer + 1);
                returnList.Add(node);
            }

            return returnList;
        }

        public KpiItemListModel GetList(string EmpSn, KpiItemListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            if (cntEmp != null)
            {
                var o_query = from p in basedb.KpiItem
                            select p;

                if (!string.IsNullOrEmpty(page.SelectBrand))
                {
                    o_query = o_query.Where(p => p.Brand == page.SelectBrand);
                }

                var query = o_query.OrderByDescending(p => p.CreateDate);
                try
                {
                    page.TotalRecords = query.Select(p => p.Id).Count();
                    var TempNodes = query.Select(Map).ToList();

                    page.Data = TreeMenu(new KpiItemModel() { Id = Guid.Empty }, TempNodes, 1);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return page;
        }

        public KpiItemModel Get(string id)
        {
            Guid gid;

            if (!Guid.TryParse(id, out gid))
            {
                throw new Exception("id 格式錯誤");
            }

            using (base.basedb)
            {
                KpiItemModel Data = base.basedb.KpiItem.Where(p => p.Id == gid).Select(Map).FirstOrDefault();

                if (Data != null)
                {
                    Data.RootName = basedb.KpiItem.Where(p => p.Id == Data.RootId).Select(p => p.Name).FirstOrDefault();
                    if (Data.RootId != Guid.Empty)
                    {
                        Data.RootLayer = GetRootLayer(true);
                    }
                    else
                    {
                        Data.RootLayer = GetRootLayer(false);
                    }
                }

                
                
                return Data;
            }
        }

        public bool Create(KpiItemModel entity)
        {
            try
            {
                KpiItem dbEntity = Mapping(entity);

                using (basedb)
                {
                    base.basedb.KpiItem.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(string id)
        {
            Guid gid;

            if (!Guid.TryParse(id, out gid))
            {
                throw new Exception("id 格式錯誤");
            }

            using (base.basedb)
            {
                var Data = base.basedb.KpiItem.Where(p => p.Id == gid).FirstOrDefault();
                if (Data != null)
                {
                    try
                    {
                        //代表有子階層
                        if (Data.RootId == Guid.Empty)
                        {
                            var ChildData = basedb.KpiItem.Where(p => p.RootId == Data.Id);
                            foreach (var Node in ChildData)
                            {
                                basedb.KpiItem.DeleteObject(Node);
                            }
                        }
                        base.basedb.KpiItem.DeleteObject(Data);
                        base.basedb.SaveChanges();

                        

                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return false;
            }
        }

        public bool Update(KpiItemModel entity)
        {
            using (base.basedb)
            {
                KpiItem dbEntity = base.basedb.KpiItem.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    dbEntity.Name = entity.Name;
                    dbEntity.RootId = entity.RootId;
                    dbEntity.Brand = entity.Brand;

                    if (dbEntity.RootId == Guid.Empty)
                    {
                        dbEntity.Layer = 0;
                    }
                    else
                    {
                        dbEntity.Layer = 1;
                    }

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }

                return false;
            }
        }


        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<KpiItem> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.KpiItem ( 
                    Id, RootId, Name, Layer, CreateDate, Brand
                    )
                    Values 
                    (
                    @Id, @RootId, @Name, @Layer, @CreateDate, @Brand
                    )";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@RootId", Row.RootId.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Name", Row.Name));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Layer", Row.Layer));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Brand", Row.Brand));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}