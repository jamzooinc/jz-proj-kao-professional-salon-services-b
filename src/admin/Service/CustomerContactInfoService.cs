﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    
    public class CustomerContactInfoService : BaseService, ISQLiteDbConverter<CustomerContactInfo>
    {
        public CustomerContactInfoService()
            : base()
        {
        }
        public static CustomerContactInfo Mapping(CustomerContactInfoModel p)
        {
            CustomerContactInfo result = new CustomerContactInfo();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Name = p.Number;
            result.Nick = p.Nick;
            result.IdentityTitle = p.IdentityTitle;
            result.IdentityNum = p.IdentityNum;
            result.Email = p.Email;
            result.CreateDate = p.CreateDate;
            result.Tel = p.Tel;
            result.EmpSN = p.EmpSN;

            return result;
        }
        public static CustomerContactInfoModel Mapping(CustomerContactInfo p)
        {
            CustomerContactInfoModel result = new CustomerContactInfoModel();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Name = p.Number;
            result.Nick = p.Nick;
            result.IdentityTitle = p.IdentityTitle;
            result.IdentityNum = p.IdentityNum;
            result.Email = p.Email;
            result.CreateDate = p.CreateDate;
            result.Tel = p.Tel;
            result.EmpSN = p.EmpSN;
         
            return result;
        }

        public static Expression<Func<CustomerContactInfo, CustomerContactInfoModel>> Map = p => 
         new CustomerContactInfoModel() { 
                Id = p.Id,
                Number = p.Number,
                Name = p.Number,
                Nick = p.Nick,
                IdentityTitle = p.IdentityTitle,
                IdentityNum = p.IdentityNum,
                Email = p.Email,
                CreateDate = p.CreateDate,
                Tel = p.Tel,
                EmpSN = p.EmpSN
        };

       

        #region Web Ap
        public CustomerContactInfoListModel GetList(string EmpSn, CustomerContactInfoListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);
            CustomerContactInfoListModel model = new CustomerContactInfoListModel();
           

            if (cntEmp != null)
            {
                var o_query = from p in basedb.CustomerContactInfo
                            select p;

                if (!string.IsNullOrEmpty(page.SearchText))
                {
                    o_query = o_query.Where(p => p.Number == page.SearchText);
                }

            
                var query = o_query.OrderBy(p => p.Id);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        public CustomerContactInfoModel Get(string id)
        {
            using (base.basedb)
            {
                CustomerContactInfoModel Data = base.basedb.CustomerContactInfo.Where(p => p.Number == id).Select(Map).FirstOrDefault();

                if (Data != null)
                {
                    var cus = basedb.Customer.Where(p => p.Number == Data.Number).FirstOrDefault();
                    if (cus != null)
                    {
                        Data.CustomerName = cus.Name;
                    }
                }

                return Data;
            }
        }

        public bool Create(string EmpSN, CustomerContactInfoModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();
            try
            {
                CustomerContactInfo dbEntity = Mapping(entity);
                dbEntity.CreateDate = DateTime.Now;

                base.basedb.CustomerContactInfo.AddObject(dbEntity);
                base.basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("CustomerContactInfoService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("CustomerContactInfoService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            var Data = base.basedb.CustomerContactInfo.Where(p => p.Number == id).FirstOrDefault();

            if (Data != null)
            {
                try
                {

                    base.basedb.CustomerContactInfo.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(string EmpSN, CustomerContactInfoModel entity, out  Dictionary<string,object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();

            using (base.basedb)
            {
                CustomerContactInfo dbEntity = base.basedb.CustomerContactInfo.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    //if (dbEntity.

                    dbEntity.IdentityTitle = entity.IdentityTitle;
                    dbEntity.IdentityNum = entity.IdentityNum;
                    dbEntity.Nick = entity.Nick;
                    dbEntity.Name = entity.Name;
                    dbEntity.Email = entity.Email;
                    dbEntity.EmpSN = entity.EmpSN;
                    dbEntity.Tel = entity.Tel;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
        }
        #endregion

        /// <summary>
        /// 實作要丟到SQLite DB 的方法
        /// </summary>
        /// <param name="Conn"></param>
        /// <param name="Trans"></param>
        /// <param name="source"></param>
        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<CustomerContactInfo> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.CustomerContactInfo( 
                    Id, Number, Name, Nick, IdentityTitle, IdentityNum, Email, CreateDate, Tel, EmpSN)
                    Values 
                    (@Id, @Number, @Name, @Nick, @IdentityTitle, @IdentityNum, @Email, @CreateDate, @Tel, @EmpSN)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Number", Row.Number));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Name", Row.Name));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Nick", Row.Nick));
                cmdInsert.Parameters.Add(new SQLiteParameter("@IdentityTitle", Row.IdentityTitle));
                cmdInsert.Parameters.Add(new SQLiteParameter("@IdentityNum", Row.IdentityNum));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Email", Row.Email));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Tel", Row.Tel));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
             

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}