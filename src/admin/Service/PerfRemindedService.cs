﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
    using System.Linq.Expressions;
    using LinqToExcel;
    using Models;
    using Extensions;
    
    public class PerfRemindedService : BaseService
    {
        public PerfRemindedService()
            : base()
        {
        }

        public static PerfReminded Mapping(PerfRemindedModel p)
        {
            PerfReminded result = new PerfReminded();

            result.Id = p.Id;
            result.DayOfMonth = p.DayOfMonth;
            result.Status = p.Status;
            result.EmpSN = p.EmpSN;
            result.Limitation = p.Limitation;
            result.CreateDate = p.CreateDate;

            return result;
        }
        public static PerfRemindedModel Mapping(PerfReminded p)
        {
            PerfRemindedModel result = new PerfRemindedModel();

            result.Id = p.Id;
            result.DayOfMonth = p.DayOfMonth;
            result.Status = p.Status;
            result.EmpSN = p.EmpSN;
            result.Limitation = p.Limitation;
            result.CreateDate = p.CreateDate;

            return result;
        }

        public static Expression<Func<PerfReminded, PerfRemindedModel>> Map = p =>
         new PerfRemindedModel()
         {
            Id = p.Id,
            DayOfMonth = p.DayOfMonth,
            Status = p.Status,
            EmpSN = p.EmpSN,
            Limitation = p.Limitation,
            CreateDate = p.CreateDate
         };

        #region Web Ap

        /// <summary>
        /// 取得某個人的待交辦事項 記錄
        /// </summary>
        /// <param name="EmpSn">目前登入業務的SN</param>
        /// <param name="EmpId">選擇的業務編號(For 業務主管)</param>
        /// <param name="Type">Memo : 待辦事項 /Assignment : 交辦事項</param>
        /// <param name="page">篩選的Pager</param>
        /// <returns></returns>
        public PerfRemindedListModel GetList(string EmpSn, string EmpId, 
            PerfRemindedListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            PerfRemindedListModel model = new PerfRemindedListModel();

            if (cntEmp != null)
            {
                var o_query = from p in basedb.PerfReminded
                              where p.EmpSN == EmpSn
                              select p;

                var query = o_query.OrderByDescending(p => p.Id);

                try
                {
                    model.Page = page.Page;
                    model.PageSize = page.PageSize;
                    model.TotalRecords = query.Select(p => p.Id).Count();
                    model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return model;
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PerfRemindedModel Get(string EmpSN, string id)
        {
            PerfRemindedModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                Data = base.basedb.PerfReminded.Where(p => p.Id == gid
                    && p.EmpSN == EmpSN
                    ).Select(Map).FirstOrDefault();
            }

            return Data;
        }

        public bool Create(string EmpSN, PerfRemindedModel entity, 
            out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            try
            {
                entity.EmpSN = EmpSN;
                PerfReminded dbEntity = Mapping(entity);
                dbEntity.EmpSN = EmpSN;

                using (basedb)
                {
                    base.basedb.PerfReminded.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("PerfRemindedService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("PerfRemindedService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            Guid gid = Guid.Empty;
            if (!Guid.TryParse(id, out gid))
            {
                return false;
            }
            var Data = base.basedb.PerfReminded.Where(p => p.Id == gid && p.EmpSN == EmpSN).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    base.basedb.PerfReminded.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(string EmpSN, PerfRemindedModel entity, out Dictionary<string, Object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();
            using (base.basedb)
            {
                PerfReminded dbEntity = base.basedb.PerfReminded.Where(p => p.Id == entity.Id).FirstOrDefault();

                if (dbEntity != null)
                {
                    dbEntity.DayOfMonth = entity.DayOfMonth;
                    dbEntity.Status = entity.Status;
                    dbEntity.EmpSN = EmpSN;
                    dbEntity.Limitation = entity.Limitation;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
            //return false;
        }
        #endregion

    }
}