﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using admin.Filter;

namespace admin.Service
{
    using Models;

    public class AccountService  : BaseService
    {
        /// <summary>
        /// 取得員工
        /// </summary>
        /// <param name="SN"></param>
        /// <returns></returns>
        public static AccountModel GetBySN(string SN)
        {
            using (KpssDb db = new KpssDb())
            {

                var query = (from p in db.Employees
                             where p.EmpSN == SN
                             select new AccountModel()
                             {
                                 SN = p.EmpSN,
                                 Permission = p.Permission,
                                 Name = p.Name,
                                 Dep = p.DepName,
                                 Email = p.Email,
                                 SalesCode = p.SalesCode,
                                 SalesOffice = p.SalesOffice
                             }).FirstOrDefault();

                return query;
            }
        }

        public static bool IsMyCustomer(string EmpSN, string CustomerNumber)
        {
            using (KpssDb db = new KpssDb())
            {
                int query = (from p in db.Customer
                             where p.EmpSN == EmpSN && p.Number == CustomerNumber
                             select p.Number).Count();

                return query > 0;
            }
        }

        public bool Validate(string Id, string Psd,
           out List<string> ErrorMessge, out AccountModel User)
        {
            return Validate(Id, Psd, null, null, out ErrorMessge, out User);
        }

        /// <summary>
        /// 驗證
        /// </summary>
        /// <param name="Id">員工編號 (emp sn)</param>
        /// <param name="Psd"></param>
        /// <param name="ErrorMessge"></param>
        /// <returns></returns>
        public bool Validate(string Id, string Psd, 
            string Mac, string DeviceId,
            out List<string> ErrorMessge, out AccountModel User)
        {
            ErrorMessge = new List<string>();

            //1. 挑出 Employees 的資料
            var Emp = (from p in basedb.Employees
                       where p.EmpSN == Id
                       select p).FirstOrDefault();

            if (Emp == null)
            {
                ErrorMessge.Add("無此帳號");
                User = null;
                return false;
            }
            else
            {
                //把 mac address 及 device id 加到 database 中
                if (!string.IsNullOrEmpty(Mac))
                {
                    Emp.Mac = Mac;
                }
                if (!string.IsNullOrEmpty(DeviceId))
                {
                    Emp.DeviceID = DeviceId;
                }
                basedb.SaveChanges();

                //2.挑出該 Emp 的驗證資料
                var EmpShip = (from p in basedb.Employeeship
                               where p.EmpSN == Emp.EmpSN
                               select p).FirstOrDefault();

                if (EmpShip == null)
                {
                    //驗證時沒有密碼, 就先初始化密碼
                    CreateDefaultEmpShip(Emp.EmpSN);
                    User = new AccountModel() { Dep = Emp.DepId, Name = Emp.Name, Permission = Emp.Permission, SN = Emp.EmpSN };
                    return true;
                }
                else //如果已經有密碼, 就進入驗證流程
                {
                    if (EmpShip.Password == Psd)
                    {
                        User = new AccountModel() { Dep = Emp.DepId, Name = Emp.Name, Permission = Emp.Permission, SN = Emp.EmpSN };
                        return true;
                    }
                    else
                    {
                        ErrorMessge.Add("密碼錯誤");
                        User = null;
                        return false;
                    }
                }
            }
        }

        public void CreateDefaultEmpShip(string EmpSN)
        {
            Employeeship EmpShip = new Employeeship();
            EmpShip.EmpSN = EmpSN;
            EmpShip.EmpId = EmpSN;
            EmpShip.Password = EmpSN;
            EmpShip.LoginErrorCount = 0;
            EmpShip.IsApproved = true;
            EmpShip.IsLockedOut = false;
            EmpShip.CreateDate = DateTime.Now;

            basedb.Employeeship.AddObject(EmpShip);
            basedb.SaveChanges();
        }
    }
}