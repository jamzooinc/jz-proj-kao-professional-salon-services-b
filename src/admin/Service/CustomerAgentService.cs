﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using admin.Models;

namespace admin.Service
{
    public class CustomerAgentService : BaseService, ISQLiteDbConverter<CustomerAgent>
    {
        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<CustomerAgent> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.CustomerAgent ( 
                    Number, EmpSN, CreateDate)
                    Values 
                    (@Number, @EmpSN, @CreateDate)
                    ";
                SQLiteCommand cmdBulletinInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Number", Row.Number.ToString()));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd")));

                int nEffect = cmdBulletinInsert.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// 取得列表
        /// </summary>
        /// <param name="number"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public CustomerAgentListModel GetList(string number, out string errorMsg)
        {
            errorMsg = string.Empty;
            var o_query = from p in basedb.CustomerAgent
                          join e in basedb.Employees on p.EmpSN equals e.EmpSN
                          select new CustomerAgentModel()
                          { 
                            Number = p.Number,
                            EmpSN = p.EmpSN,
                            EmpName = e.Name,
                            CreateDate = p.CreateDate
                          };

            if (!string.IsNullOrEmpty(number))
            {
                o_query = o_query.Where(p => p.Number == number);
            }

            CustomerAgentListModel data = new CustomerAgentListModel();

            try
            {
                data.Data = o_query.ToList();
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            return data;
        }

        //public void Create(

        public CustomerAgentModel Create(string empsn, string jsnumber, out string errorMsg)
        {
            errorMsg = string.Empty;
            bool IsExist = basedb.CustomerAgent.Where(p => p.EmpSN == empsn && p.Number == jsnumber).Select(p => p.Number).FirstOrDefault() != null;

            if (IsExist)
            {
                errorMsg = "已存在";
                return null;
            }
            else
            {
                CustomerAgent newAgent = new CustomerAgent();
                newAgent.Number = jsnumber;
                newAgent.EmpSN = empsn;
                newAgent.CreateDate = DateTime.Now;

                basedb.CustomerAgent.AddObject(newAgent);

                try
                {
                    basedb.SaveChanges();

                    var empName = basedb.Employees.Where(p => p.EmpSN == empsn).Select(p => p.Name).FirstOrDefault() ?? "無";
                    return new CustomerAgentModel() { EmpSN = empsn, Number = jsnumber, EmpName = empName };
                }
                catch (Exception Ex)
                {

                    errorMsg = Ex.Message;
                    return null;
                }
            }
        }

        public void Delete(string empsn, string number, out string errorMsg)
        {
            errorMsg = string.Empty;

            try
            {
                var deletequery = basedb.CustomerAgent.Where(p => p.Number == number && p.EmpSN == empsn).FirstOrDefault();

                if (deletequery != null)
                {
                    basedb.CustomerAgent.DeleteObject(deletequery);
                }
                else
                {
                    errorMsg = "無此筆資料";
                }

                basedb.SaveChanges();

            }
            catch (Exception Ex)
            {

                errorMsg = Ex.Message;
            }
        }
    }
}