﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Service
{
    using System.Data.SQLite;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;
using Models;

    
    public class CustomerTrainingService : BaseService, ISQLiteDbConverter<CustomerTraining>
    {
        public CustomerTrainingService()
            : base()
        {
        }

        public static CustomerTraining Mapping(CustomerTrainingModel p)
        {
            CustomerTraining result = new CustomerTraining();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Date = p.Date;
            result.Subject = p.Subject;
            result.Teacher = p.Teacher;
            result.Summary = p.Summary;
            result.StdCount = p.StdCount;
            result.Suggestion = p.Suggestion;
            result.React = p.React;
            result.IsSales = p.IsSales;
            result.CreateDate = p.CreateDate;
            result.EmpSN = p.EmpSN;
            result.Status = p.Status;

            return result;
        }

        public static CustomerTrainingModel Mapping(CustomerTraining p)
        {
            CustomerTrainingModel result = new CustomerTrainingModel();

            result.Id = p.Id;
            result.Number = p.Number;
            result.Date = p.Date;
            result.Subject = p.Subject;
            result.Teacher = p.Teacher;
            result.Summary = p.Summary;
            result.StdCount = p.StdCount;
            result.Suggestion = p.Suggestion;
            result.React = p.React;
            result.IsSales = p.IsSales;
            result.CreateDate = p.CreateDate;
            result.EmpSN = p.EmpSN;
            result.Status = p.Status;
         
            return result;
        }

        public static Expression<Func<CustomerTraining, CustomerTrainingModel>> Map = p => 
         new CustomerTrainingModel() { 
            Id = p.Id,
            Number = p.Number,
            Date = p.Date,
            Subject = p.Subject,
            Teacher = p.Teacher,
            Summary = p.Summary,
            StdCount = p.StdCount,
            Suggestion = p.Suggestion,
            React = p.React,
            IsSales = p.IsSales,
            CreateDate = p.CreateDate,
            EmpSN = p.EmpSN,
            Status = p.Status
        };

        public static Expression<Func<CustomerTraining, Employees, Customer, CustomerTrainingModel>> CompositeMap = (p, e, c) =>
         new CustomerTrainingModel()
         {
             Id = p.Id,
             Number = p.Number,
             CustomerName = c.Name,
             Date = p.Date,
             Subject = p.Subject,
             Teacher = e.Name,
             Summary = p.Summary,
             StdCount = p.StdCount,
             Suggestion = p.Suggestion,
             React = p.React,
             IsSales = p.IsSales,
             CreateDate = p.CreateDate,
             EmpSN = p.EmpSN,
             ApproveDate = p.ApproveDate,
             Status = p.Status,
             CreateEmpName = p.CreateEmpName,
             CreateEmpSN = p.CreateEmpSN
         };


        public List<SelectListItem> GetStatus()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Value = "0", Text = "未審核" });
            list.Add(new SelectListItem() { Value = "1", Text = "已核准" });
            list.Add(new SelectListItem() { Value = "2", Text = "完成授課" });
            list.Add(new SelectListItem() { Value = "3", Text = "異常" });

            return list;
        }

        #region Web Ap

        /// <summary>
        /// 主管取得教育訓練記錄
        /// </summary>
        /// <param name="CurrentEmp"></param>
        /// <param name="EmpId"></param>
        /// <param name="Number"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CustomerTrainingListModel GetListFor主管(string CurrentEmp, string EmpId, string Number, CustomerTrainingListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(CurrentEmp);
            EmpService EmpService = new Service.EmpService();
            CustomerTrainingListModel model = new CustomerTrainingListModel();

            if (!cntEmp.IsInRoles(KpssRole.區業務主管))
            {
                throw new Exception("無權限");
            }

            //如果不是我的員工
            if (!EmpService.IsMyEmp(CurrentEmp, EmpId))
            {
                throw new Exception("非您所屬的員工");
            }

            //如果不是該員工的客戶
            if (!AccountService.IsMyCustomer(EmpId, Number))
            {
                throw new Exception("非您所屬客戶");
            }

            var o_query = from p in basedb.CustomerTraining
                          select p;

            if (!string.IsNullOrEmpty(Number))
            {
                o_query = o_query.Where(p => p.Number == Number);
            }

            var query = o_query.OrderByDescending(p => p.Date);

            try
            {
                model.Page = page.Page;
                model.PageSize = page.PageSize;
                model.TotalRecords = query.Select(p => p.Id).Count();
                model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;

        }

        public CustomerTrainingListModel GetListFor行政助理(string CurrentEmp, CustomerTrainingListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(CurrentEmp);

            page.StatusList = GetStatus();

            try
            {
                var o_query = from p in basedb.CustomerTraining
                              join e in basedb.Employees on p.EmpSN equals e.EmpSN
                              join c in basedb.Customer on p.Number equals c.Number
                              select new CustomerTrainingModel() 
                              {
                                  Id = p.Id,
                                  Number = p.Number,
                                  CustomerName = c.Name,
                                  Date = p.Date,
                                  Subject = p.Subject,
                                  Teacher = e.Name,
                                  Summary = p.Summary,
                                  StdCount = p.StdCount,
                                  Suggestion = p.Suggestion,
                                  React = p.React,
                                  IsSales = p.IsSales,
                                  CreateDate = p.CreateDate,
                                  EmpSN = p.EmpSN,
                                  ApproveDate = p.ApproveDate,
                                  Status = p.Status,
                                  CreateEmpName = p.CreateEmpName,
                                  CreateEmpSN = p.CreateEmpSN
                              };

                if (!string.IsNullOrEmpty(page.Status))
                {
                    int _s = -1; Int32.TryParse(page.Status, out _s);
                    o_query = o_query.Where(p => p.Status == _s);
                }

                var query = o_query.OrderByDescending(p => p.Date);

                try
                {
                    page.Page = page.Page;
                    page.PageSize = page.PageSize;
                    page.TotalRecords = query.Select(p => p.Id).Count();
                    page.Data = query.Skip(page.Skip).Take(page.Take).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("非您所屬客戶");
            }

            return page;
        }

        /// <summary>
        /// 取得教育訓練記錄
        /// </summary>
        /// <param name="EmpSn"></param>
        /// <param name="EmpId"></param>
        /// <param name="Number"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CustomerTrainingListModel GetList(string EmpSn, string Number, CustomerTrainingListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);

            CustomerTrainingListModel model = new CustomerTrainingListModel();

            if (AccountService.IsMyCustomer(EmpSn, Number))
            {
                if (cntEmp != null)
                {
                    var o_query = from p in basedb.CustomerTraining
                                  select p;

                    if (!string.IsNullOrEmpty(Number))
                    {
                        o_query = o_query.Where(p => p.Number == Number);
                    }

                    var query = o_query.OrderByDescending(p => p.Date);

                    try
                    {
                        model.Page = page.Page;
                        model.PageSize = page.PageSize;
                        model.TotalRecords = query.Select(p => p.Id).Count();
                        model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此客戶");
                }

            }
            else
            {
                throw new Exception("非您所屬客戶");
            }
            return model;
        }

        /// <summary>
        /// 取得事項
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomerTrainingModel Get(string EmpSN, string id)
        {
            CustomerTrainingModel Data = null;

            Guid gid = Guid.Empty;

            if (Guid.TryParse(id, out gid))
            {
                var o_query = from p in basedb.CustomerTraining
                              where p.Id == gid
                              join e in basedb.Employees on p.EmpSN equals e.EmpSN
                              join c in basedb.Customer on p.Number equals c.Number
                              select new CustomerTrainingModel()
                              {
                                  Id = p.Id,
                                  Number = p.Number,
                                  CustomerName = c.Name,
                                  Date = p.Date,
                                  Subject = p.Subject,
                                  Teacher = e.Name,
                                  Summary = p.Summary,
                                  StdCount = p.StdCount,
                                  Suggestion = p.Suggestion,
                                  React = p.React,
                                  IsSales = p.IsSales,
                                  CreateDate = p.CreateDate,
                                  EmpSN = p.EmpSN,
                                  ApproveDate = p.ApproveDate,
                                  Status = p.Status,
                                  CreateEmpName = p.CreateEmpName,
                                  CreateEmpSN = p.CreateEmpSN
                              };

                Data = o_query.FirstOrDefault();
                Data.strDate = Data.Date.ToString("yyyy/MM/dd");
            }
            
            return Data;
        }

        public bool Create(string EmpSN, CustomerTrainingModel entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();
            try
            {
                CustomerTraining dbEntity = Mapping(entity);
                dbEntity.Number = DateTime.Now.ToString("yyyyMMddHHmmssfff") + EmpSN;


                base.basedb.CustomerTraining.AddObject(dbEntity);
                base.basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("CustomerTrainingService.Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("CustomerTrainingService.Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, string id)
        {
            var Data = base.basedb.CustomerTraining.Where(p => p.Number == id).FirstOrDefault();

            if (Data != null)
            {
                try
                {
                   

                    base.basedb.CustomerTraining.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(string CurrentEmpSN, CustomerTrainingModel entity, out Dictionary<string,Object> dErrMsg)
        {
            dErrMsg = new Dictionary<string, object>();
            using (base.basedb)
            {
                CustomerTraining dbEntity = base.basedb.CustomerTraining.Where(p => p.Number == entity.Number).FirstOrDefault();

                if (dbEntity != null)
                {
                    dbEntity.Date = DateTime.ParseExact(
                                        entity.strDate, 
                                        "yyyy/MM/dd", 
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.AllowWhiteSpaces);

                    dbEntity.Id = entity.Id;
                    dbEntity.Number = entity.Number;
                    dbEntity.Subject = entity.Subject;
                    dbEntity.Teacher = entity.Teacher;
                    dbEntity.Summary = entity.Summary;
                    dbEntity.StdCount = entity.StdCount;
                    dbEntity.Suggestion = entity.Suggestion;
                    dbEntity.React = entity.React;
                    dbEntity.IsSales = entity.IsSales;
                    dbEntity.EmpSN = entity.EmpSN;
                    dbEntity.Status = entity.Status;
                    if (dbEntity.Status == 1)
                    {
                        dbEntity.ApproveDate = DateTime.Now;
                    }

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }
            }
        }
        #endregion

        /// <summary>
        /// 實作要丟到SQLite DB 的方法
        /// </summary>
        /// <param name="Conn"></param>
        /// <param name="Trans"></param>
        /// <param name="source"></param>
        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<CustomerTraining> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.CustomerTraining( 
                    Id, Date, Number, Subject, Teacher, Summary, StdCount, Suggestion, React, IsSales, CreateDate, EmpSN, Status)
                    Values 
                    (@Id, @Date, @Number, @Subject, @Teacher, @Summary, @StdCount, @Suggestion, @React, @IsSales, @CreateDate, @EmpSN, @Status)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Date", Row.Date.ToString("yyyy/MM/dd")));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Number", Row.Number));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Subject", Row.Subject));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Teacher", Row.Teacher));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Summary", Row.Summary));
                cmdInsert.Parameters.Add(new SQLiteParameter("@StdCount", Row.StdCount));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Suggestion", Row.Suggestion));
                cmdInsert.Parameters.Add(new SQLiteParameter("@React", Row.React));
                cmdInsert.Parameters.Add(new SQLiteParameter("@IsSales", Row.IsSales));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Status", Row.Status));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}