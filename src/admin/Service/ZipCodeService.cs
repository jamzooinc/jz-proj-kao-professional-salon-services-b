﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using admin.Models;

namespace admin.Service
{
    public class ZipCodeService : BaseService, ISQLiteDbConverter<zipcode>
    {

        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<zipcode> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.ZipCode( 
                    ZipCode, City, Area, DepCode)
                    Values 
                    (@ZipCode, @City, @Area, @DepCode)";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@ZipCode", Row.ZipCode1));
                cmdInsert.Parameters.Add(new SQLiteParameter("@City", Row.City));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Area", Row.Area));
                cmdInsert.Parameters.Add(new SQLiteParameter("@DepCode", Row.DepCode));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}