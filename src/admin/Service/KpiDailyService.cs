﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using LinqToExcel;

namespace admin.Service
{
    public class KpiDailyService : BaseService
    {
        public KpiDailyService() : base() { }

        public static KpiDaily Mapping(KpiDailyModel p)
        {
            KpiDaily result = new KpiDaily();

            result.Date = p.Date;
            result.Year = p.Year;
            result.Month = p.Month;
            result.Day = p.Day;
            result.EmpSN = p.EmpSN;
            //result.CustomerNumber = p.CustomerNumber;
            result.KpiItemId = p.KpiItemId;
            result.ActualAmt = p.ActualAmt;
            result.CreateDate = p.CreateDate;
            result.CreateEmpSN = p.CreateEmpSN;

            return result;
        }
        public static KpiDailyModel Mapping(KpiDaily p)
        {
            KpiDailyModel result = new KpiDailyModel();

            result.Date = p.Date;
            result.Year = p.Year;
            result.Month = p.Month;
            result.Day = p.Day;
            result.EmpSN = p.EmpSN;
            //result.CustomerNumber = p.CustomerNumber;
            result.KpiItemId = p.KpiItemId;
            result.ActualAmt = p.ActualAmt;
            result.CreateDate = p.CreateDate;
            result.CreateEmpSN = p.CreateEmpSN;

            return result;
        }

        public static Expression<Func<KpiDaily, KpiDailyModel>> Map = p =>
         new KpiDailyModel()
         {
            Date = p.Date,
            Year = p.Year,
            Month = p.Month,
            Day = p.Day,
            EmpSN = p.EmpSN,
            //CustomerNumber = p.CustomerNumber,
            KpiItemId = p.KpiItemId,
            ActualAmt = p.ActualAmt,
            CreateDate = p.CreateDate,
            CreateEmpSN = p.CreateEmpSN,
         };

        public KpiDailyPage NewInstance()
        {
            KpiDailyPage page = new KpiDailyPage();
            return page;
        }

        /// <summary>
        /// Excel 匯入
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="dErrMsg"></param>
        /// <returns></returns>
        public bool ExcelImport(string CurrentEmpSN, string Path, out Dictionary<string, object> dErrorMsg)
        {
            dErrorMsg = new Dictionary<string, object>();

            //日期,項目,當日業績,業務編號
            var excel = new ExcelQueryFactory(Path);
            excel.AddMapping("Date", "日期");
            excel.AddMapping("Item", "項目");
            excel.AddMapping("ActualAmt", "當日業績");
            excel.AddMapping("EmpSN", "業務編號");

            //先將項目讀到記憶體
            //Select p.Id ParentId, c.Id ChildId, p.Name Parent, c.Name Child
            //from 
            //  dbo.kpiItem p join dbo.kpiitem c on p.Id = c.rootId

            var KpiItemsView = (from p in basedb.KpiItem
                               join c in basedb.KpiItem on p.Id equals c.RootId
                               select new { 
                                   parentId = p.Id,
                                   childId = c.Id,
                                   parentName = p.Name,
                                   childName = c.Name
                               }).ToList();

            var query = from c in excel.Worksheet<KpiDailyExcelModel>("Data")
                        select c;

            int rowIndex = 1;

            foreach (KpiDailyExcelModel row in query)
            {
                List<string> strsErrorMsg = new List<string>();
                #region 處理每一筆
                try
                {
                    //如果 Excel 的每個欄位都有值，才需要進入判斷
                    if (!string.IsNullOrEmpty(row.Date) && !string.IsNullOrEmpty(row.Item) && !string.IsNullOrEmpty(row.ActualAmt) && !string.IsNullOrEmpty(row.EmpSN))
                    {
                        //
                        bool isValid = true;
                        //DateTime parse
                        DateTime _Date = DateTime.MinValue;
                        //Integer parse
                        int Amt = 0;

                        if (!DateTime.TryParse(row.Date, out _Date))
                        {
                            //解析日期失敗
                            isValid = false;
                            strsErrorMsg.Add("解析日期失敗,格式需為 yyyy/MM/dd");
                        }

                        if (!int.TryParse(row.ActualAmt, out Amt))
                        {
                            //解析數值失敗
                            isValid = false;
                            strsErrorMsg.Add("解析當日業績失敗,請需入無小數點的正整數");
                        }

                        //如果資料沒問題,才會新增到資料庫
                        if (isValid)
                        {
                            //1.判斷是否有這個 EMPSN
                            bool isEmpExist = basedb.Employees.Where(p => p.EmpSN == row.EmpSN).Select(p => p.EmpSN).FirstOrDefault() != null;

                            if (isEmpExist)
                            {
                                //2.抓出項目的編號 KpiItemId
                                string[] itemArray = row.Item.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                                if (itemArray.Length == 2)
                                {
                                    //KpiItem 的編號
                                    var KpiItem = KpiItemsView.Where(p =>
                                        p.parentName == itemArray[0] &&
                                        p.childName == itemArray[1]).Select(p => p.childId).FirstOrDefault();

                                    if (KpiItem != null)
                                    {
                                        KpiDailyModel dbData = new KpiDailyModel();

                                        dbData.Date = _Date;
                                        dbData.Year = _Date.Year;
                                        dbData.Month = _Date.Month;
                                        dbData.Day = _Date.Day;
                                        dbData.EmpSN = row.EmpSN;
                                        dbData.KpiItemId = KpiItem;         //KPI ITEM
                                        dbData.ActualAmt = Amt;
                                        dbData.CreateDate = DateTime.Now;
                                        dbData.CreateEmpSN = CurrentEmpSN;

                                        try
                                        {
                                            Dictionary<string, object> _derrMsg = new Dictionary<string, object>();
                                            Create(CurrentEmpSN, dbData, out _derrMsg);

                                            foreach (var k in _derrMsg.Keys)
                                            {
                                                strsErrorMsg.Add(_derrMsg[k].ToString());
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            //新增時出錯
                                            if (ex.InnerException != null)
                                            {
                                                strsErrorMsg.Add(ex.InnerException.Message);
                                            }

                                            strsErrorMsg.Add(ex.Message);
                                        }
                                    }
                                    else
                                    {
                                        //項目個數不符
                                        strsErrorMsg.Add("沒有這個產品項目");
                                    }
                                }
                                else
                                {
                                    //項目個數不符
                                    strsErrorMsg.Add("項目個數不符,必須是 [大項目名稱]/[小項目名稱]");
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    //新增時出錯
                    if (ex.InnerException != null)
                    {
                        strsErrorMsg.Add(ex.InnerException.Message);
                    }

                    strsErrorMsg.Add(ex.Message);
                }
                #endregion
                if (strsErrorMsg.Count != 0)
                {
                    dErrorMsg.Add(rowIndex.ToString(), strsErrorMsg);
                }

                rowIndex++;
            }

            try
            {
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                dErrorMsg.Add("SaveChangesError", ex.Message);
            }


            return dErrorMsg.Keys.Count() == 0;
        }


        /// <summary>
        /// 取得每天要回報的業務名單
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<DailyReportEmployee> GetDailyEmployees(string EmpSN, KpiDailyPage filter)
        {
            //要回傳的資料模型
            List<DailyReportEmployee> result = new List<DailyReportEmployee>();
            Guid KpiItemId = Guid.Empty;
            Guid.TryParse(filter.KpiChildId, out KpiItemId);
            var o_emps = from p in basedb.Employees
                         join kd in basedb.KpiDaily.Where(p => p.Date == filter.DateTime.Date && p.KpiItemId == KpiItemId) on p.EmpSN equals kd.EmpSN
                         select p;

            foreach (var emp in o_emps)
            {
                DailyReportEmployee DRE = new DailyReportEmployee();
                DRE.EmpSN = emp.EmpSN;
                DRE.Name = emp.Name;
                DRE.Records = new List<DailyReportRecord>();


                //fixed : 2013.4.9 - CustomerNumber 拿掉
                //var o_kpidaiay = from p in basedb.Customer.Where(p => p.EmpSN == emp.EmpSN)
                //        join kd in basedb.KpiDaily.Where(p => p.Date == filter.DateTime.Date && p.KpiItemId == KpiItemId) on p.Number equals kd.CustomerNumber
                //        select new 
                //        { 
                //            Number = p.Number,
                //            Name = p.Name,
                //            ActualAmt = kd.ActualAmt
                //        };

                var o_kpidaiay = from p in basedb.KpiDaily.Where(p => p.Date == filter.DateTime.Date && p.KpiItemId == KpiItemId)
                                 where p.EmpSN == emp.EmpSN
                                 select new
                                 {
                                     ActualAmt = p.ActualAmt
                                 };

                //right join 先不要使用 right join
                //var o_kpidaiay =
                //    from p in basedb.Customer.Where(p => p.EmpSN == emp.EmpSN)
                //    join kd in basedb.KpiDaily.Where(p => p.Date == filter.DateTime.Date && p.KpiItemId == KpiItemId) on p.Number equals kd.CustomerNumber
                //        into sr
                //    from x in sr.DefaultIfEmpty()
                //    select new 
                //    { 
                //        Number = p.Number,
                //        Name = p.Name,
                //        ActualAmt = x.ActualAmt == null ? 0:x.ActualAmt
                //    };


                foreach (var r in o_kpidaiay)
                {
                    DRE.Records.Add(new DailyReportRecord() { 
                         ActualAmt = r.ActualAmt,
                         CreateEmpSN = EmpSN,
                         //CustomerName = r.Name,
                         //CustomerNumber = r.Number
                    });
                }
                result.Add(DRE);
            }

            return result;
        }

        public void DeleteByTodayAndKpiItemId(DateTime Date, Guid KpiItemId)
        {
            var query = from p in basedb.KpiDaily
                        where p.Year == Date.Year
                        && p.Month == Date.Month
                        && p.Day == Date.Day
                        && p.KpiItemId == KpiItemId
                        select p;

            foreach (var r in query)
            {
                basedb.KpiDaily.DeleteObject(r);
            }

            basedb.SaveChanges();
        }

        public void AddDailyReport(
            string CurrentEmp,
            string strDate, 
            string strKpiItemId,
            string EmpSN,
            //string CustomerNumber,
            decimal Amt
            )
        {
            DateTime Date = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
            Guid KpiItemId = Guid.Parse(strKpiItemId);

            KpiDaily dbEntity = new KpiDaily();

            dbEntity.Date = Date;
            dbEntity.Year = Date.Year;
            dbEntity.Month = Date.Month;
            dbEntity.Day = Date.Day;
            dbEntity.EmpSN = EmpSN;
            //dbEntity.CustomerNumber = CustomerNumber;
            dbEntity.ActualAmt = (int)Amt;
            dbEntity.KpiItemId = KpiItemId;
            dbEntity.CreateEmpSN = CurrentEmp;
            dbEntity.CreateDate = DateTime.Now;

            try
            {
                basedb.KpiDaily.AddObject(dbEntity);
                basedb.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    throw ex.InnerException;
                }
                throw ex;
            }
        }

        
        public void UpdateOnDaily(string strDate, string KpiChildId, string EmpSN, 
            //string Number, 
            int Amt)
        {
            DateTime d = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
            Guid kid = Guid.Parse(KpiChildId);

            KpiDaily dbEntity = (from p in basedb.KpiDaily
                                 where p.Year == d.Year
                                 && p.Month == d.Month
                                 && p.Day == d.Day
                                 && p.EmpSN == EmpSN
                                 //&& p.CustomerNumber == Number
                                 && p.KpiItemId == kid
                                 select p).FirstOrDefault();

            if (dbEntity != null)
            {
                if (Amt < 1)
                {
                    basedb.KpiDaily.DeleteObject(dbEntity);
                }
                else
                {
                    dbEntity.ActualAmt = Amt;
                }
                basedb.SaveChanges();
            }
        }
        
        public void UpdateDailyReport(string EmpSN, KpiDailyPage Data)
        {
            DateTime _Date = DateTime.ParseExact(Data.strDate, "yyyy/MM/dd", null);
            Guid KpiItemId = Guid.Empty;
            if (!Guid.TryParse(Data.KpiChildId, out KpiItemId))
            {
                throw new Exception("Kpi Item Id Error");
            }
            //先刪掉全部的
            DeleteByTodayAndKpiItemId(_Date, KpiItemId);
            //再新增
            if (Data.Employees != null)
            {
                foreach (var Emp in Data.Employees)
                {
                    if (Emp.Records != null)
                    {
                        foreach (var DailyRecord in Emp.Records)
                        {
                            AddOrUpdate(
                                _Date.Year,
                                _Date.Month,
                                _Date.Day,
                                Emp.EmpSN,
                                //DailyRecord.CustomerNumber,
                                KpiItemId,
                                (int)DailyRecord.ActualAmt,
                                EmpSN);

                            
                        }
                    }
                }
            }
            basedb.SaveChanges();
        }

        public void AddOrUpdate(int Year, int Month, int Day, string EmpSN,
            Guid KpiItemId, int ActualAmt, string CreateEmpSN)
        {
            KpiDaily dbDailyRecord = new KpiDaily();

            dbDailyRecord.KpiItemId = KpiItemId;
            dbDailyRecord.CreateEmpSN = EmpSN;
            dbDailyRecord.Date = new DateTime(Year, Month, Day);
            dbDailyRecord.Year = Year;
            dbDailyRecord.Month = Month;
            dbDailyRecord.Day = Day;
            dbDailyRecord.EmpSN = EmpSN;
            //dbDailyRecord.CustomerNumber = Number;
            dbDailyRecord.ActualAmt = ActualAmt;
            dbDailyRecord.CreateDate = DateTime.Now;

            basedb.KpiDaily.AddObject(dbDailyRecord);
        }

        /// <summary>
        /// KPI Daily Data
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Page"></param>
        /// <returns></returns>
        public KpiDailyListModel GetList(string EmpSN, KpiDailyListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSN);

            KpiDailyListModel Model = new KpiDailyListModel();

            if (EmpSN != null)
            {
                var o_query = from p in basedb.KpiDaily
                              where p.CreateEmpSN == EmpSN
                              select p;


                var query = o_query.OrderByDescending(p => p.Date);


                try
                {
                    Model.Page = page.Page;
                    Model.PageSize = page.PageSize;
                    Model.TotalRecords = query.Select(p => p.EmpSN).Count();
                    Model.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return Model;
        }

        public KpiDailyModel Get(string EmpSN, DateTime Date, string TargetEmpSN, string CustomerNumber,
            Guid KpiItemId)
        {
            KpiDailyModel Entity = basedb.KpiDaily.Where(p =>
              p.Year == Date.Year &&
              p.Month == Date.Month &&
              p.Day == Date.Day &&
              p.EmpSN == TargetEmpSN &&
              //p.CustomerNumber == CustomerNumber &&
              p.KpiItemId == KpiItemId).Select(Map).FirstOrDefault();

           

            return Entity;
        }

        public bool Create(string EmpSN, KpiDailyModel Entity, out Dictionary<string, object> ErrorMsg)
        {
            ErrorMsg = new Dictionary<string, object>();

            try
            {
                KpiDaily dbEntity = Mapping(Entity);
                dbEntity.Year = dbEntity.Date.Year;
                dbEntity.Month = dbEntity.Date.Month;
                dbEntity.Day = dbEntity.Date.Day;

                dbEntity.CreateEmpSN = EmpSN;
                dbEntity.CreateDate = DateTime.Now;

                basedb.KpiDaily.AddObject(dbEntity);
                basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ErrorMsg.Add("Create.InnerException", ex.InnerException.Message);
                }
                else
                {
                    ErrorMsg.Add("Create.Exception", ex.Message);
                }
                return false;
            }
        }

        public bool Delete(string EmpSN, DateTime Date, string TargetEmpSN, string CustomerNumber,
            Guid KpiItemId)
        {

            KpiDaily dbEntity = basedb.KpiDaily.Where(p =>
                p.Year == Date.Year &&
                p.Month == Date.Month &&
                p.Day == Date.Day &&
                p.EmpSN == TargetEmpSN &&
                //p.CustomerNumber == CustomerNumber &&
                p.KpiItemId == KpiItemId).FirstOrDefault();

            try
            {
                basedb.KpiDaily.DeleteObject(dbEntity);
                basedb.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update(string EmpSN, KpiDailyModel entity, out Dictionary<string,Object> ErrMsg)
        {
            ErrMsg = new Dictionary<string, object>();

            KpiDaily dbEntity = basedb.KpiDaily.Where(p =>
                p.Year == entity.Date.Year &&
                p.Month == entity.Date.Month &&
                p.Day == entity.Date.Day &&
                p.EmpSN == entity.EmpSN &&
                //p.CustomerNumber == entity.CustomerNumber &&
                p.KpiItemId == entity.KpiItemId).FirstOrDefault();

            if (dbEntity != null)
            {
                dbEntity.EmpSN = entity.EmpSN;
                dbEntity.ActualAmt = entity.ActualAmt;

                try
                {
                    basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此資料");
            }
        }
    }
}
