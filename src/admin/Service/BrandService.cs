﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Service
{
    public class BrandService
    {
        public List<SelectListItem> GetBrands()
        {
            List <SelectListItem> BrandList = new List<SelectListItem>();
            BrandList.Add(new SelectListItem() { Text = "GOLDWELL", Value = "GOLDWELL" });
            BrandList.Add(new SelectListItem() { Text = "KMS", Value = "KMS" });
            return BrandList;
        }
    }
}