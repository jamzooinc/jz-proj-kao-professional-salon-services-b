﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;

namespace admin.Service
{
    public class KpiService : BaseService, ISQLiteDbConverter<Kpi>
    {
        public KpiService() : base() { }

        /// <summary>
        /// KPI 預估頁面用的
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Brand"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public List<KpiListModel> GetKpiData(string EmpSN, string Brand, int Year)
        {
            List<KpiListModel> Model = new List<KpiListModel>();

            #region Origin SQL
            //Select * From 
            //    (
            //    Select * From 
            //    dbo.KpiItem ,dbo.[Month]) tb1
            //Left join 
            //    (
            //    Select * From dbo.Kpi k 
            //    ) tb2
            // on tb1.[Month] = tb2.[Month] and tb1.Id = tb2.kpiitemid 
            #endregion

            var AllQuery = from tb1 in
                               (
                                    from ki in basedb.KpiItem.Where(p => p.Brand == Brand && p.RootId != Guid.Empty)
                                    join ki2 in basedb.KpiItem.Where(p => p.Brand == Brand && p.RootId == Guid.Empty) on ki.RootId equals ki2.Id 
                                    from m in basedb.Month
                                    select new
                                    {
                                        KpiRootName = ki2.Name,
                                        KpiItemName = ki.Name,
                                        KpiItemId = ki.Id,
                                        Month = m.Month1
                                    })
                           join tb2 in basedb.Kpi.Where(p => p.EmpSN == EmpSN && p.Year == Year) 
                                on new { tb1.Month, tb1.KpiItemId } equals new { tb2.Month, tb2.KpiItemId } into k
                           from mx in k.DefaultIfEmpty()
                           select new
                           {
                               Year = Year,
                               Month = tb1.Month,
                               RootName = tb1.KpiRootName ?? "N/A",
                               Name = tb1.KpiItemName ?? "N/A",
                               KpiItemId = tb1.KpiItemId,
                               EmpSN = mx.EmpSN ?? "N/A",
                               KpiItemName = tb1.KpiItemName ?? "N/A",
                               Estimate = mx.EstimateAmt == null ? 0 : mx.EstimateAmt,
                               ActualAmt = mx.ActualAmt == null ? 0 : mx.ActualAmt,
                               Customer = mx.Customer ?? "N/A",
                               CustomerAmt = mx.CustomerAmt == null ? 0 : mx.CustomerAmt,
                               CustomerCount = mx.CustomerCount == null ? 0 : mx.CustomerCount  //客戶數量
                           };

            #region Old
            //var nestQuery = from k in basedb.Kpi
            //                join ki in basedb.KpiItem on k.KpiItemId equals ki.Id
            //                where k.Year == Year && k.EmpSN == EmpSN
            //                select new
            //                {
            //                    ki.Id,
            //                    ki.Name,
            //                    k.Year,
            //                    k.Month,
            //                    k.EmpSN,
            //                    k.EstimateAmt,
            //                    k.ActualAmt,
            //                    k.Customer,
            //                    k.CustomerAmt
            //                };

            //var nestQuery2 = from ki in basedb.KpiItem
            //                 join k in basedb.Kpi.Where(p => p.Year == Year && p.EmpSN == EmpSN) on ki.Id equals k.KpiItemId into s
            //                 from mx in s.DefaultIfEmpty()
            //                 select new
            //                 {
            //                     ki.Id,
            //                     ki.Name,
            //                     Year = mx.Year == null ? 0 : mx.Year,
            //                     Month = mx.Month == null ? 1 : mx.Month,
            //                     EmpSN = mx.EmpSN ?? "N/A",
            //                     EstimateAmt = mx.EstimateAmt == null ? 0 : mx.EstimateAmt,
            //                     ActualAmt = mx.ActualAmt == null ? 0 : mx.ActualAmt,
            //                     Customer = mx.Customer ?? "N/A",
            //                     CustomerAmt = mx.CustomerAmt == null ? 0 : mx.CustomerAmt
            //                 };

            //var query =
            //    from m in basedb.Month
            //    join tb1 in nestQuery2 on m.Month1 equals tb1.Month into k
            //    from mx in k.DefaultIfEmpty()
            //    select new
            //    {
            //        Year = Year,
            //        Month = m.Month1,
            //        EmpSN = mx.EmpSN ?? "N/A",
            //        Name = mx.Name ?? "N/A",
            //        KpiItemName = mx.Name ?? "N/A",
            //        Estimate = mx.EstimateAmt == null ? 0 : mx.EstimateAmt,
            //        ActualAmt = mx.ActualAmt == null ? 0 : mx.ActualAmt,
            //        Customer = mx.Customer ?? "N/A",
            //        CustomerAmt = mx.CustomerAmt == null ? 0 : mx.CustomerAmt
            //    };
            #endregion

            var Data = AllQuery.ToList();

            //取出所有的項目
            var KpiItemGroup = Data.Select(p => new { p.RootName , p.KpiItemName, p.KpiItemId }).GroupBy(p => new { p.KpiItemId, p.KpiItemName, p.RootName });

            //然後把每一筆項目加到  page model 的 row
            foreach (var KpiItem in KpiItemGroup)
            {
                KpiListModel klModel = new KpiListModel();

                klModel.KpiItemRootName = KpiItem.Key.RootName;
                klModel.KpiItemId = KpiItem.Key.KpiItemId;
                klModel.KpiItemName = KpiItem.Key.KpiItemName;

                klModel.Data = new List<KpiModel>();

                var s = Data.Where(p => p.KpiItemId == KpiItem.Key.KpiItemId).OrderBy(p => p.Month);
                //KPI 每個月的預估值

                foreach (var row in s)
                {
                    klModel.Data.Add(new KpiModel()
                    {
                        CustomerCount = row.CustomerCount,
                        EmpSN = EmpSN,
                        Month = row.Month,
                        ActualAmt = row.ActualAmt,
                        EstimateAmt = row.Estimate,
                        CustomerAmt = row.CustomerAmt,
                        Customer = row.Customer,
                    });
                }

                Model.Add(klModel);
            }

            return Model;
        }

        //取得 control 要用的 page model
        public KpiSettingPage GetPage(string EmpSN, string Brand, int Year)
        {
            KpiSettingPage Model = new KpiSettingPage();
            Model.EmpSN = EmpSN;
            Model.Year = Year == 0 ? DateTime.Now.Year:Year;

            AccountModel cntEmp = AccountModel.Create(HttpContext.Current.User.Identity.Name);

            //如果目前使用者是業務主管及業務秘書,則可以來使用
            if (cntEmp.IsInRoles(new KpssRole[] { KpssRole.管理員, KpssRole.區業務主管, KpssRole.業務秘書 }))
            {
                var busData = from p in basedb.Employees
                              where (p.Permission & (int)KpssRole.駐區業務) == (int)KpssRole.駐區業務
                              select new SelectListItem() 
                              { 
                                Text = p.Name,
                                Value = p.EmpSN
                              };

                Model.EmpList = busData.ToList();
            }

            Model.Years = new List<SelectListItem>();
            for (int i = 0; i < 1; i++)
            {
                int cYear = DateTime.Now.Year + i;
                Model.Years.Add(new SelectListItem() { Value = cYear.ToString(), Text = cYear.ToString() });
            }


            if (!string.IsNullOrEmpty(EmpSN))
            {
                Model.Data = GetKpiData(EmpSN, Brand, Year);
            }

            return Model;
        }

        public bool Update(KpiSettingPage Page)
        {
            int Year = Page.Year;       // 1.年分
            string EmpSN = Page.EmpSN;  // 2.員工編號

            foreach (var KpiItem in Page.Data)
            {
                Guid KpiItemId = KpiItem.KpiItemId;     // 3.KPI 項目編號

                foreach (var Month in KpiItem.Data)
                {
                    int month = Month.Month;            // 4.月份

                    Kpi KpiRow = (from p in basedb.Kpi
                                  where p.Year == Year && p.Month == month && p.EmpSN == EmpSN && p.KpiItemId == KpiItemId
                                  select p).FirstOrDefault();

                    //新增
                    if (KpiRow == null)
                    {
                        KpiRow = new Kpi();
                        KpiRow.Year = Year;
                        KpiRow.EmpSN = EmpSN;
                        KpiRow.KpiItemId = KpiItemId;
                        KpiRow.Month = month;
                        KpiRow.EstimateAmt = Month.EstimateAmt;
                        KpiRow.ActualAmt = Month.ActualAmt;

                        basedb.Kpi.AddObject(KpiRow);
                    }
                    else //修改
                    {
                        KpiRow.EstimateAmt = Month.EstimateAmt;
                        KpiRow.ActualAmt = Month.ActualAmt;
                    }

                    basedb.SaveChanges();
                }

            }
            return true; 
        }

        public void Run(System.Data.SQLite.SQLiteConnection Conn, System.Data.SQLite.SQLiteTransaction Trans, IQueryable<Kpi> source)
        {
            foreach (var Row in source)
            {
                string strBulletinInsert = @"Insert Into main.Kpi ( 
                    Year, Month, EmpSN, KpiItemId, EstimateAmt, ActualAmt, CustomerAmt, LastYearAmt
                    )
                    Values 
                    (
                    @Year, @Month, @EmpSN, @KpiItemId, @EstimateAmt, @ActualAmt, @CustomerAmt, @LastYearAmt
                    )";

                SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, Conn, Trans);
                cmdInsert.Parameters.Add(new SQLiteParameter("@Year", Row.Year));
                cmdInsert.Parameters.Add(new SQLiteParameter("@Month", Row.Month));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Row.EmpSN));
                cmdInsert.Parameters.Add(new SQLiteParameter("@KpiItemId", Row.KpiItemId.ToString()));
                cmdInsert.Parameters.Add(new SQLiteParameter("@EstimateAmt", Row.EstimateAmt));
                cmdInsert.Parameters.Add(new SQLiteParameter("@ActualAmt", Row.ActualAmt));
                cmdInsert.Parameters.Add(new SQLiteParameter("@CustomerAmt", Row.CustomerAmt));
                cmdInsert.Parameters.Add(new SQLiteParameter("@LastYearAmt", Row.LastYearAmt));

                int nEffect = cmdInsert.ExecuteNonQuery();
            }
        }
    }
}
