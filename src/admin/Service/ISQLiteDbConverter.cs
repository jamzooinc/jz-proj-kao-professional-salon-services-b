﻿using System.Data.SQLite;
using System.Linq;

namespace admin.Service
{
    /// <summary>
    /// SQL Server 資料轉成 SQLite 資料用的 interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISQLiteDbConverter<T>
    {
        void Run(SQLiteConnection Conn, SQLiteTransaction Trans, IQueryable<T> source);
    }
}
