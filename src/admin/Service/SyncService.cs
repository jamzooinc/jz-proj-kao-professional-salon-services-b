﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using admin.Models;

namespace admin.Service
{
    using System.Data.EntityClient;
    using Extensions;
using NLog;

    public class SyncService : BaseService
    {
        protected Logger logger;
        public SyncService(Logger _logger)
            : base()
        {
            logger = _logger;
        }

        public void AddSyncLog(SyncDataModel Log)
        {
            SyncLog DbLog = new SyncLog();

            DbLog.Id = Guid.NewGuid();
            DbLog.ClientId = Log.Id.ToString(); //Log.Id 指的是 client 端的 Id
            DbLog.Command = Log.Command;
            DbLog.TableName = Log.TableName;
            DbLog.EmpSN = Log.EmpSN;
            DbLog.Result = Log.Result;
            DbLog.CreateDate = DateTime.Now;

            basedb.SyncLog.AddObject(DbLog);
        }

        public void Commit()
        {
            basedb.SaveChanges();
        }

        /// <summary>
        /// 同步模組
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Data"></param>
        /// <param name="ErrorMsgs"></param>
        /// <returns></returns>
        public bool Sync(string EmpSN, string Data, out Dictionary<string, object> ErrorMsgs)
        {
            ErrorMsgs = new Dictionary<string, object>();
            List<string> strResults = new List<string>();       //用來儲存執行 SQL Command 失敗後的 Client Id

            bool bResult = false;
            JavaScriptSerializer ser = new JavaScriptSerializer();

            try
            {
                //先把 client 傳過來的 data base64 decode
                string strJsonData = Data.FromBase64();

                logger.Debug("base decode data : {0}", strJsonData);

                //取得 json 後再傳成相關的 object
                List<SyncDataModel> obj = ser.Deserialize<List<SyncDataModel>>(strJsonData);

                foreach (var row in obj)
                {
                    string strCommand = row.Command;
                    string strException = string.Empty;

                    logger.Debug("command : {0}", strCommand);

                    try
                    {
                        //prevent the delete command
                        //execute the sql command                                        
                        int iEffectRow = basedb.ExecuteStoreCommand(strCommand);

                        logger.Debug("執行結果成功 : {0}", iEffectRow);
                    }
                    catch (Exception ex)
                    {
                        strException = ex.Message;
                        if (ex.InnerException != null)
                        {
                            strException += ex.InnerException.Message;
                        }
                        //Put the id of sql command into a array, if gets any errors.
                        logger.Error("執行結果失敗 : {0}", strException);
                        strResults.Add(row.Id.ToString());
                    }

                    try
                    {
                        row.EmpSN = EmpSN;
                        row.Result = strException;
                        AddSyncLog(row);
                    }
                    catch
                    {
                        strResults.Add(row.Id.ToString());
                    }
                }

                Commit();

                //Put the error command id collection into the error message.
                ErrorMsgs.Add("result", strResults);

                logger.Debug("更新資料庫成功 : {0}", strResults);

                //System.Web.Script.Serialization.JavaScriptSerializer ser = new JavaScriptSerializer();
                string strJsonResult = ser.Serialize(ErrorMsgs);
                logger.Debug("回傳結果 : {0}", strJsonResult);

                bResult = strResults.Count == 0;

                return true;
            }
            catch (Exception ex)
            {
                ErrorMsgs.Add("Exception", ex.Message);
                if (ex.InnerException != null)
                {
                    ErrorMsgs.Add("Inner Exception", ex.InnerException.Message);
                    logger.Error("失敗 InnerException: {0}", ex.InnerException.Message);
                }
                logger.Error("失敗 : {0}", ex.Message);

                return false;
            }
        }

    }
}