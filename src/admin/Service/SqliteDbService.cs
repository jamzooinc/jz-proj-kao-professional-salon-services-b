﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.SQLite;

using admin.Models;

namespace admin.Service
{
    public class SqliteDbService : BaseService
    {
        public static SqliteDb Mapping(SqliteDbModel p)
        {
            SqliteDb result = new SqliteDb();

            result.Major = p.Major;
            result.Minor = p.Minor;
            result.Fixed = p.Fixed;
            result.Version = p.Version;
            result.SqlFile = p.SqlFile;
            result.CreateDate = p.CreateDate;

            return result;
        }
        public static SqliteDbModel Mapping(SqliteDb p)
        {
            SqliteDbModel result = new SqliteDbModel();

            result.Major = p.Major;
            result.Minor = p.Minor;
            result.Fixed = p.Fixed;
            result.Version = p.Version;
            result.SqlFile = p.SqlFile;
            result.CreateDate = p.CreateDate;

            return result;
        }

        public static Expression<Func<SqliteDb, SqliteDbModel>> Map = p =>
         new SqliteDbModel()
         {
            Major = p.Major,
            Minor = p.Minor,
            Fixed = p.Fixed,
            Version = p.Version,
            SqlFile = p.SqlFile,
            CreateDate = p.CreateDate,
         };

        #region For Web Site

        public SqliteDbModel GetCurrent()
        {
            var query = (from p in basedb.SqliteDb
                         orderby new { p.Major, p.Minor, p.Fixed } descending
                         select p).FirstOrDefault();

            if (query != null)
            {
                return Mapping(query);
            }
            else
            {
                return null;
            }
        }

        public SqliteDbModel New()
        {
            SqliteDbModel m = new SqliteDbModel();

            var query = (from p in basedb.SqliteDb
                         orderby new { p.Major, p.Minor, p.Fixed } descending
                         select p).FirstOrDefault();

            if (query != null)
            {
                m.Major = query.Major;
                m.Minor = query.Minor;
                m.Fixed = query.Fixed + 1;
            }
            else
            {
                m.Fixed = 1;
            }

            return m;
        }


        /// <summary>
        /// 取得業務的資料
        /// </summary>
        /// <param name="EmpSn"></param>
        /// <param name="Permission"></param>
        /// <returns></returns>
        public SqliteDbListModel Get業務()
        {
            SqliteDbListModel page = new SqliteDbListModel();

            var query = from p in basedb.SqliteDb
                        orderby new { p.Major, p.Minor, p.Fixed } descending
                        select p;

            try
            {
                //不需要分頁,直接使用即可
                page.Data = query.Select(Map).ToList();
                page.TotalRecords = page.Data.Count();

                return page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //儲存 Sqlite sql File 的資料夾
        public static string SQL_FILE_FOLDER = "~/App_Data/SqliteSchema/";
        public static string SQL_FILE_NAME = "KpssDb.sql";

        //儲存 sqlite db 檔案的資料夾
        public static string SQLITE_DB_FOLDER = "~/App_Data/SqliteDb/";
        public static string SQLITE_DB_NAME = "KpssDb.db";

        public void SaveFile(SqliteDbModel entity)
        {
            CreateSqlFileFolder(entity.Version);
            if (entity.SqlFileFile != null)
            {
                 string strSqlFilePath = SQL_FILE_FOLDER + entity.Version + "/" + SQL_FILE_NAME;
                 entity.SqlFileFile.SaveAs(HttpContext.Current.Server.MapPath(strSqlFilePath));
                 entity.SqlFile = strSqlFilePath;
            }
        }

        public void CreateSqlFileFolder(string version)
        {
            string strSqlFileVersion = version;
            string strSqlFilePath = SQL_FILE_FOLDER + version + "/";
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(strSqlFilePath)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(strSqlFilePath));
            }
        }

        public SqliteDbListModel GetList(string EmpSn, SqliteDbListModel page)
        {
            AccountModel cntEmp = AccountModel.Create(EmpSn);
            if (cntEmp != null)
            {
                var query = from p in basedb.SqliteDb
                        orderby new { p.Major, p.Minor, p.Fixed } descending
                        select p;
                try
                {
                    page.TotalRecords = query.Select(p => p.Major).Count();
                    page.Data = query.Skip(page.Skip).Take(page.Take).Select(Map).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("無此員工");
            }

            return page;
        }

        public SqliteDbModel Get(int Major, int Minor, int Fixed)
        {
            using (base.basedb)
            {
                SqliteDbModel Data = base.basedb.SqliteDb
                    .Where(p => p.Major == Major && p.Minor == Minor && p.Fixed == Fixed)
                    .Select(Map).FirstOrDefault();

                return Data;
            }
        }

        public bool Create(SqliteDbModel entity)
        {
            try
            {
                entity.Version = string.Format("v{0}.{1}.{2}",
                    entity.Major, entity.Minor, entity.Fixed);

                SaveFile(entity);

                SqliteDb dbEntity = Mapping(entity);

                using (basedb)
                {
                    base.basedb.SqliteDb.AddObject(dbEntity);
                    base.basedb.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int Major, int Minor, int Fixed)
        {
            var Data = base.basedb.SqliteDb
                .Where(p => p.Major == Major && p.Minor == Minor && p.Fixed == Fixed)
                .FirstOrDefault();

            if (Data != null)
            {
                try
                {
                    base.basedb.SqliteDb.DeleteObject(Data);
                    base.basedb.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;
        }

        public bool Update(SqliteDbModel entity)
        {
            using (base.basedb)
            {
                SqliteDb dbEntity = basedb.SqliteDb
                    .Where(p => p.Major == entity.Major && p.Minor == entity.Minor && p.Fixed == entity.Fixed)
                    .FirstOrDefault();

                if (dbEntity != null)
                {
                    dbEntity.SqlFile = entity.SqlFile;

                    try
                    {
                        basedb.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("無此資料");
                }

                return false;
            }
        }


        #endregion

        #region For Rest-API

        /// <summary>
        /// 取得該業務的 SQLite 檔案
        /// </summary>
        /// <param name="Emp"></param>
        /// <returns></returns>
        public string GetSqliteDbByEmp(AccountModel Emp)
        {
            string strSqlDbPath = SQLITE_DB_FOLDER + Emp.SN + "/" + SQLITE_DB_NAME;

            if (File.Exists(HttpContext.Current.Server.MapPath(strSqlDbPath)))
            {
                return HttpContext.Current.Server.MapPath(strSqlDbPath);
            }
            else
            {
                throw new Exception("file not exist");
            }
        }

        /// <summary>
        /// 產生 Sqlite Database 的檔案
        /// </summary>
        /// <param name="Emp"></param>
        /// <returns></returns>
        public bool GenerateSqliteDb(AccountModel Emp, out string LastVersion, out string LastUpdate)
        {
            SqliteDbModel currentVersion = GetCurrent();
            LastVersion = currentVersion.Version;
            LastUpdate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            if (currentVersion != null)
            {
                string sqlDbDirectory = SQLITE_DB_FOLDER + Emp.SN + "/";
                string sqlDbPath = HttpContext.Current.Server.MapPath(sqlDbDirectory + SQLITE_DB_NAME);

                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(sqlDbDirectory)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(sqlDbDirectory));
                }

                if (!System.IO.File.Exists(sqlDbPath))
                {
                    SQLiteConnection.CreateFile(sqlDbPath);
                }
                
                using (SQLiteConnection con = new SQLiteConnection("Data Source=" + sqlDbPath + ";Version=3;UseUTF16Encoding=True;"))
                {
                    string strVersion = currentVersion.Version;
                    string sqlFilePath = HttpContext.Current.Server.MapPath(SQL_FILE_FOLDER + strVersion + "/" + SQL_FILE_NAME);

                    string strSqlCommand = System.IO.File.ReadAllText(sqlFilePath, System.Text.Encoding.UTF8);
                    SQLiteCommand cmd = new SQLiteCommand(strSqlCommand, con);
                    cmd.CommandType = System.Data.CommandType.Text;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    // 抓取該保養員的資料 丟到 Sqlite
                    GenerateSqliteData(Emp, con);

                    con.Close();
                    con.Dispose();
                    // return SQLITE_DB_FOLDER + Emp.SN + "/" + SQLITE_DB_NAME;
                    return true;
                }

            }
            else
            {
                throw new Exception("N○ .sql file");
            }

            //return false;
        }

        /// <summary>
        /// 產生資料
        /// </summary>
        /// <param name="con"></param>
        public void GenerateSqliteData(AccountModel Emp, SQLiteConnection con)
        {
            DateTime _Today = DateTime.Now;

            using (SQLiteTransaction trans = con.BeginTransaction())
            {
                //Customer
                #region 客戶資料
                var query = from p in basedb.Customer
                            //join t in basedb.CustomerRating on p.Rating equals t.Id
                            select new { 
                                p,
                                CountPerMonth = basedb.CustomerRating.Where(s => s.Id == p.Rating).Select(s => s.CountPerMonth).FirstOrDefault()
                            };

                if (Emp.IsInRoles(KpssRole.區業務主管))
                {
                    query = query.Where(p => p.p.SalesOffice == Emp.SalesOffice);
                }
                else if (Emp.IsInRoles(KpssRole.駐區業務))
                {
                    query = query.Where(p => p.p.SalesCode == Emp.SalesCode);
                }

                var agentCustomer = from p in basedb.Customer
                                    where basedb.CustomerAgent.Where(s => s.EmpSN == Emp.SalesCode).Select(s => s.Number).Contains(p.Number)
                                    select new
                                    {
                                        p,
                                        CountPerMonth = basedb.CustomerRating.Where(s => s.Id == p.Rating).Select(s => s.CountPerMonth).FirstOrDefault()
                                    };

                var o_query = query.Union(agentCustomer);

                foreach (var Customer in o_query)
                {
                    string strCustomerInsert = @"Insert Into main.Customer (
                                    Number, Type, SN, EmpSN, Name, NameAbbr, Owner, Tel, Tel_2, Fax, 
                                    Email, EmpCount, Dep, FirstTransDate, LastTransDate, SalesLevel, StaffLevel, Brand, Area, CreateDate, 
                                    ToType1Date, EnrollmentAddr, 
TransportAddr_City, 
TransportAddr_Area, 
TransportAddr_Postal, 
TransportAddr, 
TransportAddr_en, 
Postal, Payment, BranchCount, CloseDate, Postal_2, PaymentEmp, FirstTransDate_KMS, CloseDate_2, Rating, 
				                    VisitCount, Lng, Lat,
ProductType1,
ProductType2,
ProductType3,
ProductType4,
ProductType5,
Fee1,
Fee2,
Fee3,
Fee4,
Fee5,
OwnerNick,
OwnerTitle,
OwnerNum,
OwnerEmail,
OwnerTel,
OpenningDate,
Decorating,
NewPoint,
OrderForm,
ProductSource,
MaterialSource,
MaterialRating,
ExtProLine,
Staff,
BranchType,
OH,
ServiceType,
Billing,
S1,
S2,
S3,
KMS1,
KMS2,
KMS3,
OTC1,
OTC2,
WebSite,
SalesCode,
SapCode,
SalesOffice
                                ) Values (
                                    @Number, @Type, @SN, @EmpSN, @Name, @NameAbbr, @Owner, @Tel, @Tel_2, @Fax, @Email, @EmpCount, @Dep, @FirstTransDate, @LastTransDate,
                                    @SalesLevel, @StaffLevel, @Brand, @Area, @CreateDate, @ToType1Date, @EnrollmentAddr, 
@TransportAddr_City, 
@TransportAddr_Area, 
@TransportAddr_Postal, 
@TransportAddr, 
@TransportAddr_en, 
@Postal, @Payment, @BranchCount, @CloseDate, @Postal_2, @PaymentEmp,
                                    @FirstTransDate_KMS, @CloseDate_2, @Rating, 
				                    @VisitCount, @Lng, @Lat,
@ProductType1,
@ProductType2,
@ProductType3,
@ProductType4,
@ProductType5,
@Fee1,
@Fee2,
@Fee3,
@Fee4,
@Fee5,
@OwnerNick,
@OwnerTitle,
@OwnerNum,
@OwnerEmail,
@OwnerTel,
@OpenningDate,
@Decorating,
@NewPoint,
@OrderForm,
@ProductSource,
@MaterialSource,
@MaterialRating,
@ExtProLine,
@Staff,
@BranchType,
@OH,
@ServiceType,
@Billing,
@S1,
@S2,
@S3,
@KMS1,
@KMS2,
@KMS3,
@OTC1,
@OTC2,
@WebSite,
@SalesCode,
@SapCode,
@SalesOffice
                                )";

                    SQLiteCommand cmdCustomerInsert = new SQLiteCommand(strCustomerInsert, con, trans);
                    cmdCustomerInsert.CommandType = System.Data.CommandType.Text;
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Number", Customer.p.Number != null ? Customer.p.Number.Trim() : string.Empty));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Type", Customer.p.Type));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@SN", Customer.p.SN != null ? Customer.p.SN.Trim() : string.Empty));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@EmpSN", Customer.p.EmpSN));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Name", Customer.p.Name != null ? Customer.p.Name.Trim() : string.Empty));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@NameAbbr", Customer.p.NameAbbr));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Owner", Customer.p.Owner));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Tel", Customer.p.Tel));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Tel_2", Customer.p.Tel_2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fax", Customer.p.Fax));
                    
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Email", Customer.p.Email));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@EmpCount", Customer.p.EmpCount));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Dep", Customer.p.Dep));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@FirstTransDate", Customer.p.FirstTransDate));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@LastTransDate", Customer.p.LastTransDate));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@SalesLevel", Customer.p.SalesLevel));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@StaffLevel", Customer.p.StaffLevel));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Brand", Customer.p.Brand));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Area", Customer.p.Area));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Customer.p.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ToType1Date", Customer.p.ToType1Date));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@EnrollmentAddr", Customer.p.EnrollmentAddr));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@TransportAddr_City", Customer.p.TransportAddr_City));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@TransportAddr_Area", Customer.p.TransportAddr_Area));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@TransportAddr_Postal", Customer.p.TransportAddr_Postal));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@TransportAddr", Customer.p.TransportAddr));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@TransportAddr_en", Customer.p.TransportAddr_en));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Postal", Customer.p.Postal));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Payment", Customer.p.Payment));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@BranchCount", Customer.p.BranchCount));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@CloseDate", Customer.p.CloseDate));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Postal_2", Customer.p.Postal_2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@PaymentEmp", Customer.p.PaymentEmp));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@FirstTransDate_KMS", Customer.p.FirstTransDate_KMS));

                    //@FirstTransDate_KMS, @CloseDate_2, @Rating
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@CloseDate_2", Customer.p.CloseDate_2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Rating", Customer.p.Rating));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@VisitCount", Customer.CountPerMonth));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Lng", Customer.p.Lng));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Lat", Customer.p.Lat));

                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductType1", Customer.p.ProductType1));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductType2", Customer.p.ProductType2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductType3", Customer.p.ProductType3));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductType4", Customer.p.ProductType4));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductType5", Customer.p.ProductType5));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fee1", Customer.p.Fee1));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fee2", Customer.p.Fee2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fee3", Customer.p.Fee3));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fee4", Customer.p.Fee4));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Fee5", Customer.p.Fee5));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OwnerNick", Customer.p.OwnerNick));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OwnerTitle", Customer.p.OwnerTitle));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OwnerNum", Customer.p.OwnerNum));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OwnerEmail", Customer.p.OwnerEmail));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OwnerTel", Customer.p.OwnerTel));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OpenningDate", Customer.p.OpenningDate));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Decorating", Customer.p.Decorating));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@NewPoint", Customer.p.NewPoint));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OrderForm", Customer.p.OrderForm));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ProductSource", Customer.p.ProductSource));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@MaterialSource", Customer.p.MaterialSource));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@MaterialRating", Customer.p.MaterialRating));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ExtProLine", Customer.p.ExtProLine));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Staff", Customer.p.Staff));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@BranchType", Customer.p.BranchType));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OH", Customer.p.OH));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@ServiceType", Customer.p.ServiceType));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@Billing", Customer.p.Billing));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@S1", Customer.p.S1));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@S2", Customer.p.S2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@S3", Customer.p.S3));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@KMS1", Customer.p.KMS1));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@KMS2", Customer.p.KMS2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@KMS3", Customer.p.KMS3));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OTC1", Customer.p.OTC1));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@OTC2", Customer.p.OTC2));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@WebSite", Customer.p.WebSite));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@SalesCode", Customer.p.SalesCode));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@SapCode", Customer.p.SapCode));
                    cmdCustomerInsert.Parameters.Add(new SQLiteParameter("@SalesOffice", Customer.p.SalesOffice));

                    int nEffect = cmdCustomerInsert.ExecuteNonQuery();
                }
                #endregion

                #region 聯絡人資料
                ISQLiteDbConverter<CustomerContactInfo> contactInfo_s = new CustomerContactInfoService();

                var CustomerContactInfoQuery = from p in basedb.CustomerContactInfo
                                    select p;

                contactInfo_s.Run(con, trans, CustomerContactInfoQuery);
                #endregion

                #region 交/待辦事項
                var CustomerTaskQuery = from p in basedb.CustomerTask
                                        where p.FromEmp == Emp.SN || p.ToEmp == Emp.SN
                                        select p;

                foreach (var Task in CustomerTaskQuery)
                {
                    string strTaskInsert = @"
                    Insert Into main.CustomerTask ( 
                        Id, Date, Number, Title, Category, Type, Status, CloseDate, FromEmp, ToEmp, DeadLine, DepName, Context
                    )
                    Values (
                        @Id, @Date, @Number, @Title, @Category, @Type, @Status, @CloseDate, @FromEmp, @ToEmp, @DeadLine, @DepName, @Context
                    )";

                    SQLiteCommand cmdCustomerTaskInsert = new SQLiteCommand(strTaskInsert, con, trans);
                    cmdCustomerTaskInsert.CommandType = System.Data.CommandType.Text;
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Id", Task.Id.ToString()));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Date", Task.Date.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Number", Task.Number));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Title", Task.Title));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Category", Task.Category));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Type", Task.Type));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Status", Task.Status));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@CloseDate", Task.CloseDate ?? null));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@FromEmp", Task.FromEmp));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@ToEmp", Task.ToEmp));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@DeadLine", Task.DeadLine));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@DepName", Task.DepName));
                    cmdCustomerTaskInsert.Parameters.Add(new SQLiteParameter("@Context", Task.Context));

                    int nEffect = cmdCustomerTaskInsert.ExecuteNonQuery();

                }
                #endregion

                #region 公告
                //抓 20 筆
                var BulletinQuery = (from p in basedb.Bulletin
                                     where p.Status == 1
                                     orderby p.CreateDate
                                     select p).Take(20);

                foreach (var Row in BulletinQuery)
                {
                    string strBulletinInsert = @"Insert Into main.Bulletin ( 
                            Id, CreateEmpNo, Subject, Context, Status, CreateDate, FromDep, Category) 
                    Values ( @Id, @CreateEmpNo, @Subject, @Context, @Status, @CreateDate, @FromDep, @Category)";

                    SQLiteCommand cmdBulletinInsert = new SQLiteCommand(strBulletinInsert, con, trans);
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@CreateEmpNo", Row.CreateEmpNo));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Subject", Row.Subject));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Context", Row.Context));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Status", Row.Status));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@CreateDate", Row.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@FromDep", Row.FromDep));
                    cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Category", Row.Category));
                    //cmdBulletinInsert.Parameters.Add(new SQLiteParameter("@Id", Row.Id.ToString()));

                    int nEffect = cmdBulletinInsert.ExecuteNonQuery();
                }
                #endregion

                #region 業務排程
                ISQLiteDbConverter<Schedule> ss = new ScheduleService();

                var ScheduleQUery = from p in basedb.Schedule
                                    //where p.EmpSN == Emp.SN 
                                    //&& (p.Year == _Today.Year && p.Month == _Today.Month)
                                    where p.Status != -1
                                    select p;

                ss.Run(con, trans, ScheduleQUery);

                #endregion

                #region 拜訪記錄
                ISQLiteDbConverter<VisitLog> visitlog_s = new VisitService();

                var VisitQuery = from p in basedb.VisitLog
                                    //where p.EmpSN == Emp.SN
                                    select p;

                visitlog_s.Run(con, trans, VisitQuery);
                #endregion

                #region 教育訓練

                ISQLiteDbConverter<CustomerTraining> customerTraining_s = new CustomerTrainingService();

                var TrainingQuery  = from p in basedb.CustomerTraining
                                 where p.EmpSN == Emp.SN
                                 select p;

                customerTraining_s.Run(con, trans, TrainingQuery);

                #endregion

                #region 週報表

                ISQLiteDbConverter<Week> Week_s = new WeekService();

                var WeekQuery = from p in basedb.Week
                                where 
                                //p.EmpSN == Emp.SN && 
                                p.Year == _Today.Year
                                select p;

                Week_s.Run(con, trans, WeekQuery);

                #endregion

                #region 排程項目
                ISQLiteDbConverter<KpiItem> kpiItem_s = new KpiItemService();

                var kpiItemQuery = from p in basedb.KpiItem
                               select p;

                kpiItem_s.Run(con, trans, kpiItemQuery);
                #endregion

                #region 業務
                ISQLiteDbConverter<Employees> emp_s = new EmpService();

                var empQuery = from p in basedb.Employees
                                    select p;

                emp_s.Run(con, trans, empQuery);
                #endregion

                #region 產品
                ISQLiteDbConverter<Product> product_s = new ProductService();

                var productQuery = from p in basedb.Product
                              select p;

                product_s.Run(con, trans, productQuery);
                #endregion

                #region 智慧排程
                ISQLiteDbConverter<SmartScheduling> smartS_s = new SmartSchedulingService();

                var smartS_q = from p in basedb.SmartScheduling
                                   select p;

                smartS_s.Run(con, trans, smartS_q);
                #endregion

                #region 預估

                ISQLiteDbConverter<Kpi> Kpi_S = new KpiService();

                 var kpi_q = from p in basedb.Kpi
                                   select p;

                 Kpi_S.Run(con, trans, kpi_q);

                #endregion

                #region Zip Code

                ISQLiteDbConverter<zipcode> zipcode_s = new ZipCodeService();

                //var zipcode_query = from p in basedb.zipcode
                //        select p;

                //客戶地區資料
                var ppp = from p in o_query
                          group p by new { p.p.TransportAddr_City, p.p.TransportAddr_Area, p.p.TransportAddr_Postal } into g
                          select new { 
                            ZipCode1 = g.Key.TransportAddr_Postal,
                            City = g.Key.TransportAddr_City,
                            Area = g.Key.TransportAddr_Area,
                            DepCode = Emp.Dep
                          };

                foreach (var Row in ppp.Where(p => p.ZipCode1 != null && p.DepCode != null))
                {
                    string strBulletinInsert = @"Insert Into main.ZipCode( 
                    ZipCode, City, Area, DepCode)
                    Values 
                    (@ZipCode, @City, @Area, @DepCode)";

                    SQLiteCommand cmdInsert = new SQLiteCommand(strBulletinInsert, con, trans);
                    cmdInsert.Parameters.Add(new SQLiteParameter("@ZipCode", Row.ZipCode1));
                    cmdInsert.Parameters.Add(new SQLiteParameter("@City", Row.City));
                    cmdInsert.Parameters.Add(new SQLiteParameter("@Area", Row.Area));
                    cmdInsert.Parameters.Add(new SQLiteParameter("@DepCode", Row.DepCode));

                    int nEffect = cmdInsert.ExecuteNonQuery();
                }

                //zipcode_s.Run(con, trans, ppp.ToList().AsQueryable());
                    
                 
                #endregion

                #region 代理人

                ISQLiteDbConverter<CustomerAgent> agent_s = new CustomerAgentService();
                

                //var o_quer
                //var aaa = from p in o_query
                //          group p by new { } into g
                //          select new 
                //          { 
                //          }
                var agent_query = from p in basedb.CustomerAgent
                                  where p.EmpSN == Emp.SN
                                  select p;

                agent_s.Run(con, trans, agent_query);

                 
                #endregion

                 //最終 commit 到 database, 使用 using 會自動 rollback 嗎?
                //trans.commit();
                trans.Commit();
            }
        }

        #endregion
    }
}