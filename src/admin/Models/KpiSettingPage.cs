﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    /// <summary>
    /// KPI 設定頁面
    /// </summary>
    public class KpiSettingPage
    {
        /// <summary>
        /// 業務人員列表
        /// </summary>
        public List<SelectListItem> EmpList { get; set; }

        /// <summary>
        /// 選到的業務
        /// </summary>
        public string EmpSN { get; set; }

        /// <summary>
        /// 選擇的品牌
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// 品牌列表
        /// </summary>
        public List<SelectListItem> BrandList { get; set; }

        /// <summary>
        /// 年份的下拉
        /// </summary>
        public List<SelectListItem> Years { get; set; }

        /// <summary>
        /// 選到的
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 項目
        /// </summary>
        public List<KpiListModel> Data { get; set; }

        public KpiSettingPage()
        {
            //Data = new List<KpiListModel>();
            BrandList = new List<SelectListItem>();
            BrandList.Add(new SelectListItem() { Text = "GOLDWELL", Value = "GOLDWELL" });
            BrandList.Add(new SelectListItem() { Text = "KMS", Value = "KMS" });
            Brand = BrandList.FirstOrDefault().Value;
        }
    }
}