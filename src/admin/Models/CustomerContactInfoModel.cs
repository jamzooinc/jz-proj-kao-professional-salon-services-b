﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace admin.Models
{
    public class CustomerContactInfoListModel : PagerModel
    {
        public string SearchText { get; set; }

        public List<CustomerContactInfoModel> Data { get; set; }
    }
    /// <summary>
    /// 客戶聯絡資料
    /// </summary>
    public class CustomerContactInfoModel
    {
        public Guid Id { get; set; }

        public string Number { get; set; }

        public string CustomerName { get; set; }

        public string Name { get; set; }

        public string Nick { get; set; }

        public string IdentityTitle { get; set; }

        public string IdentityNum { get; set; }

        [RegularExpression("^([\\w\\.-]{1,64}@[\\w\\.-]{1,252}\\.\\w{2,4})$", ErrorMessage="格式錯誤")]
        public string Email { get; set; }

        public DateTime CreateDate { get; set; }

        public string Tel { get; set; }

        public string EmpSN { get; set; }
    }
}
