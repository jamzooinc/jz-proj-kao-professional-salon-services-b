﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace admin.Models
{
    public class BulletinListModel : PagerModel
    {
        public string SearchText { get; set; }

        public List<BulletinModel> Data { get; set; }
    }
    /// <summary>
    /// 客戶聯絡資料
    /// </summary>
    public class BulletinModel
    {
        public Guid Id { get; set; }

        public string CreateEmpNo { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Context { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public string FromDep { get; set; }

        public string Category { get; set; }

        public BulletinModel()
        {
            Id = Guid.NewGuid();
            CreateDate = DateTime.Now;
        }
    }
}
