﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    public class JamPushListModel : PagerModel
    {
        public List<JamPushModel> Data { get; set; }
    }

    /// <summary>
    /// jam push model
    /// </summary>
    public class JamPushModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public string CreateEmpSN { get; set; }

        public string Request { get; set; }

        public string Response { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public JamPushModel()
        {
            Id = Guid.NewGuid();
            CreateDate = DateTime.Now;
        }
    }
}