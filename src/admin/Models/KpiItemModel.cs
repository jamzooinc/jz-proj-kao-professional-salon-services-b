﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class KpiItemListModel : PagerModel
    {
        public List<SelectListItem> ListBrand { get; set; }
        
        public string SelectBrand { get; set; }

        public List<KpiItemModel> Data { get; set; }

        public KpiItemListModel()
        {
            ListBrand = new List<SelectListItem>();
            ListBrand.Add(new SelectListItem() { Text = "GOLDWELL", Value = "GOLDWELL" });
            ListBrand.Add(new SelectListItem() { Text = "KMS", Value = "KMS" });
            SelectBrand = ListBrand.FirstOrDefault().Value;
        }
    }


    public class KpiItemModel
    {
        [Required(ErrorMessage = "必填")]
        public Guid Id { get; set; }

        public Guid RootId { get; set; }

        //父節點名稱
        public string RootName {get; set; }

        //品牌
        [Required(ErrorMessage = "必填")]
        public string Brand { get; set; }

        public List<SelectListItem> RootLayer { get; set; }

        [Required(ErrorMessage = "必填")]
        public string Name { get; set; }

        public int Layer { get; set; }

        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 子節點
        /// </summary>
        public List<KpiItemModel> Childern { get; set; }
    }
}