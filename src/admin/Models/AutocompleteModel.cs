﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    //[{"id":"Mimus polyglottos","label":"Northern Mockingbird","value":"Northern Mockingbird"}]
    //自
    public class AutocompleteModel
    {
        /// <summary>
        /// 唯一值
        /// </summary>
        public string id { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string label { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }
    }
}