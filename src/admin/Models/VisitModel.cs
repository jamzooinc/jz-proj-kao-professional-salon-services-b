﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class VisitListModel : PagerModel
    {
        public List<SelectListItem> MyEmps { get; set; }
        
        /// <summary>
        /// 選擇的客戶編號
        /// </summary>
        public string EmpId { get; set; }

        /// <summary>
        /// 搜尋文字
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// 選擇的月份
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// 拜訪狀態 (0,1,2)
        /// </summary>
        public int Status { get; set; }


        public List<VisitModel> Data { get; set; }

        public VisitListModel()
        {
            Status = -1;
        }
    }
    /// <summary>
    /// 拜訪記錄
    /// </summary>
    public class VisitModel
    {
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date { get; set; }

        [RegularExpression("^[0-9]{4}[/](((0[13578]|(10|12))[/](0[1-9]|[1-2][0-9]|3[0-1]))|(02[/](0[1-9]|[1-2][0-9]))|((0[469]|11)[/](0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "格式錯誤")]
        [Required]
        public string strDate { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// 統一編號
        /// </summary>
        public int Times { get; set; }

        /// <summary>
        /// 所屬業務
        /// </summary>
        public string EmpSN { get; set; }

        /// <summary>
        /// 業務名稱
        /// </summary>
        public string EmpName { get; set; }
        
        /// <summary>
        /// 等級 /// </summary>
        [RegularExpression("^\\d{1}$", ErrorMessage = "格式錯誤")]
        [Range(1,5, ErrorMessage="請介於{1}~{2}之間")]
        [Required]
        public int Rating { get; set; }

        public string Context { get; set; }

        public string FromTask { get; set; }
        public string ToTask { get; set; }


        public DateTime StartTime { get; set; }

        [RegularExpression("^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$", ErrorMessage = "格式錯誤")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm:ss}")]
        public string strStartTime { get; set; }

        public DateTime EndTime { get; set; }

        [RegularExpression("^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$", ErrorMessage = "格式錯誤")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm:ss}")]
        public string strEndTime { get; set; }

        public int Status { get; set; }
    }
}