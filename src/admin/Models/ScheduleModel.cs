﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace admin.Models
{
    public class ScheduleListModel : PagerModel
    {
        /// <summary>
        /// 業務編號
        /// </summary>
        public string EmpId { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public List<ScheduleModel> Data { get; set; }
    }

    /// <summary>
    /// KPI 用的 Model
    /// </summary>
    public class ScheduleModel
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        [RegularExpression("^[0-9]{4}[/](((0[13578]|(10|12))[/](0[1-9]|[1-2][0-9]|3[0-1]))|(02[/](0[1-9]|[1-2][0-9]))|((0[469]|11)[/](0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "格式錯誤")]
        [Required]
        public string strDate { get; set; }

        //客戶
        public string CustomerNumber { get; set; }

        public string CustomerName { get; set; }

        public int Times { get; set; }

        //[Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }

        //[Required(ErrorMessage="必填")]
        public int Year { get; set; }

        //[Required(ErrorMessage="必填")]
        public int Month { get; set; }

        public int Day { get; set; }

        public DateTime? StartTime { get; set; }

        [RegularExpression("^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$", ErrorMessage = "格式錯誤")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm:ss}")]
        public string strStartTime { get; set; }

        public DateTime? EndTime { get; set; }

        [RegularExpression("^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$", ErrorMessage = "格式錯誤")]
        [Required]
        public string strEndTime { get; set; }

        public int Status { get; set; }

        /// <summary>
        /// 星期幾
        /// </summary>
        public int DayOfWeek { get; set; }

        /// <summary>
        /// 拜訪重點
        /// </summary>
        public string ToDo { get; set; }

        /// <summary>
        /// 異常原因
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// 拜訪類別
        /// </summary>
        public string Type { get; set; }


        public ScheduleModel()
        {
            Id = Guid.NewGuid();
            Date = DateTime.Now.Date;
            strDate = Date.ToString("yyyy/MM/dd");
            strStartTime = Date.ToString("HH:mm:ss");
            strEndTime = Date.ToString("HH:mm:ss");
        }
    }
}