﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    public class BaseListModel<T>
    {
        public List<T> Data { get; set; }

        public PagerModel Pager { get; set; }

        public BaseListModel()
        {
            Pager = new PagerModel();
        }
    }

    public class PagerModel
    {
        public int TotalRecords { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }

        public int PageSize { get; set; }
        public int Page { get; set; }
        //public int cPage { get; set; }

        public int Skip
        {
            get
            {
                if (Page <= 1)
                {
                    return 0;
                }
                else
                {
                    return (Page - 1) * PageSize;
                }
            }
        }

        public int Take
        {
            get
            {
                return PageSize;
            }
        }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)TotalRecords / (double)PageSize);
            }
        }

        public PagerModel()
        {
            PageSize = 50;
            Page = 1;
        }
    }
}