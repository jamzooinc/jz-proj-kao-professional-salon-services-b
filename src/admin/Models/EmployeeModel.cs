﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace admin.Models
{
    /// <summary>
    /// 角色
    /// </summary>
    public enum KpssRole
    {
        管理員 = 4096,
        總經理 = 2048,
        業務秘書 = 1024,
        教育講師 = 512,
        區業務主管 = 256,
        駐區業務 = 128,
        服務專員 = 64,
        企劃部 = 32,
        客服部 = 16, 
        儲運部 = 8,
        財務部 = 4,
        教育部 = 2,
        一般 = 1
    }

    public class EmployeeListModel : PagerModel
    {
        public string SearchText { get; set; }
        public string StartWith { get; set; }
        public List<EmployeeModel> Data { get; set; }
    }

    /// <summary>
    /// 業務員工 Model
    /// </summary>
    public class EmployeeModel
    {
        [Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }

        [Required(ErrorMessage="必填")]
        public string Name { get; set; }

        public string Name_en { get; set; }

        //[Required(ErrorMessage="必填")]
        public string DepName { get; set; }

        public string DepId { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int Permission { get; set; }

        //Excel 匯入用的 - (客服與儲運部門主管)
        public string PermissionRoleFromExcel { get; set; }

        public int[] Permissions { get; set; }

        public string Tel { get; set; }

        public string Mobile { get; set; }

        public string DeviceID { get; set; }

        public string Mac { get; set; }

        //[Required(ErrorMessage="必填")]
        public string LoginId { get; set; }

        public string LoginPsd { get; set; }

        //[Required(ErrorMessage="必填")]
        [RegularExpression("^([\\w\\.-]{1,64}@[\\w\\.-]{1,252}\\.\\w{2,4})$", ErrorMessage="格式錯誤")]
        public string Email { get; set; }

        [RegularExpression("\\d+", ErrorMessage = "格式錯誤")]
        public int LoginErrorCount { get; set; }

        public string BossSN { get; set; }

        public string SalesCode { get; set; }

        /// <summary>
        /// 主管別一
        /// </summary>
        public string Manage1 { get; set; }

        /// <summary>
        /// 主管別二
        /// </summary>
        public string Manage2 { get; set; }

        /// <summary>
        /// 主管別三
        /// 
        /// </summary>
        public string Manage3 { get; set; }

        /// <summary>
        /// 權限 Title
        /// </summary>
        public string PermissionTitle { get; set; }

        public string Area { get; set; }

        public string Title { get; set; }


        #region 2013.08.06 import

        public string SalesOffice { get; set; }
        public string SalesGroup { get; set; }
        public string SalesPerson { get; set; }
        public string SalesEmployee { get; set; }
        //public string SalesCode { get; set; }

        #endregion
    }
}