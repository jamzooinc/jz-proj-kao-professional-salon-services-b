﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    public class SmartSchedulinListModel : PagerModel
    {

        public int Year { get; set; }

        public int Month { get; set; }

        public string EmpSN { get; set; }

        public Guid ChildKpiItemId { get; set; }

        public List<SmartSchedulingModel> Data { get; set; }

        public SmartSchedulingModel EditViewModel { get; set; }

        public SmartSchedulinListModel()
        {
            EditViewModel = new SmartSchedulingModel();
        }
    }

    public class SmartSchedulingModel
    {
        public Guid Id { get; set; }

        public Guid RootKpiItemId { get; set; }

        public Guid ChildKpiItemId { get; set; }

        public string RootKpiItem { get; set; }

        public string ChildKpiItem { get; set; }

        public string Number { get; set; }

        public string CustomerName { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public string FromDep { get; set; }

        public string EmpSN { get; set; }
    }
}