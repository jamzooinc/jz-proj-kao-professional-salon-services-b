﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    public class CustomerAgentListModel : PagerModel
    {
        public string SearchText { get; set; }

        public string Number { get; set; }

        public List<CustomerAgentModel> Data { get; set; }
    }

    public class CustomerAgentModel
    {
        public string Number { get; set; }
        public string EmpSN { get; set; }
        public string EmpName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}