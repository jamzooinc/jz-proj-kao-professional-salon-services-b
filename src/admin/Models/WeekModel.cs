﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace admin.Models
{
    public class WeekListModel : PagerModel
    {
        public List<WeekModel> Data { get; set; }
    }

    /// <summary>
    /// 週報表用的 用的 Model
    /// </summary>
    public class WeekModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage="必填")]
        public int Year { get; set; }

        [Required(ErrorMessage="必填")]
        public int Month { get; set; }

        [Required(ErrorMessage="必填")]
        public int Weeks { get; set; }

        [Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }

        public string Comment { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}