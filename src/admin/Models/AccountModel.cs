﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace admin.Models
{
    using Service;

    /// <summary>
    /// 登入的 Model
    /// </summary>
    public class AccountModel
    {
        public string Token { get; set; }

        public string SN { get; set; }

        public int Permission { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Dep { get; set; }

        /// <summary>
        /// 地區的 code
        /// </summary>
        public string SalesOffice { get; set; }

        /// <summary>
        /// 業務的 code
        /// </summary>
        public string SalesCode { get; set; }

        public string Roles
        {
            get
            {
                List<string> rRole = new List<string>();

                foreach (var role in Enum.GetValues(typeof(KpssRole)))
                {
                    int r = (int)role;
                    if ((Permission & r) == r)
                    {
                        rRole.Add(role.ToString());
                    }
                }

                return string.Join(",", rRole.ToArray<string>());
            }
        }

        public string[] GetRoles()
        {
            List<string> rRole = new List<string>();

            foreach (var role in Enum.GetValues(typeof(KpssRole)))
            {
                int r = (int)role;
                if ((Permission & r) == r)
                {
                    rRole.Add(role.ToString());
                }
            }

            return rRole.ToArray<string>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Roles"></param>
        /// <returns></returns>
        public bool IsInRoles(params KpssRole[] Roles)
        {
            if (Roles == null) return false;
            if (Roles.Length == 0) return false;
            foreach (var Role in Roles)
            {
                int roleInt = (int)Role;
                if ((Permission & roleInt) == roleInt)
                    return true;
            }
            return false;
        }

        public static AccountModel Create(string SN)
        {
            return AccountService.GetBySN(SN);
        }
    }
}