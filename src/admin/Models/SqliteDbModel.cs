﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace admin.Models
{
    public class SqliteDbListModel : PagerModel
    {
        public List<SqliteDbModel> Data { get; set; }
    }

    /// <summary>
    /// 資料庫
    /// </summary>
    public class SqliteDbModel
    {
        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int Major { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int Minor { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int Fixed { get; set; }

        public string Version { get; set; }

        public string SqlFile { get; set; }

        public HttpPostedFileBase SqlFileFile { get; set; }

        public DateTime CreateDate { get; set; }

        public SqliteDbModel()
        {
            CreateDate = DateTime.Now;
        }
    }
}