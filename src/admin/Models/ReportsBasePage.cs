﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    /// <summary>
    /// 後台報表頁面的 BasePage
    /// </summary>
    public class ReportsBasePage
    {
        /// <summary>
        /// 年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 月份
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// 業務編號
        /// </summary>
        public string EmpId { get; set; }

        public ReportsBasePage()
        {
            Year = DateTime.Now.Year;
        }
    }

    /// <summary>
    /// 客戶類型
    /// </summary>
    public class CVTPage : ReportsBasePage
    {
        public int Type { get; set; }

        /// <summary>
        /// 年度
        /// </summary>
        public List<RepCVTByYear> ByYearData { get; set; }

        /// <summary>
        /// 依月份 及 等級
        /// </summary>
        public List<RepCVTByMonth> ByMonthData { get; set; }
    }


    /// <summary>
    /// 客戶拜訪
    /// </summary>
    public class CVRPage : ReportsBasePage
    {
        /// <summary>
        /// 客戶等級
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// 年度
        /// </summary>
        public List<RepCVRByYear> ByYearData { get; set; }

        /// <summary>
        /// 依年度 及 等級
        /// </summary>
        public List<RepCVRByYearRating> ByYearRatingData { get; set; }

        /// <summary>
        /// 依月份 及 等級
        /// </summary>
        public List<RepCVRByMonthRating> ByMonthRatingData { get; set; }

        public CVRPage()
        {
            Rating = -1;
        }
    }

    /// <summary>
    /// Kpi 
    /// </summary>
    public class KpiPage : ReportsBasePage
    {
        public List<SelectListItem> BrandList { get; set; }

        public string Brand { get; set; }

        /// <summary>
        /// 抓第幾層
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// KPI 項目編號
        /// </summary>
        public Guid KpiItemId { get; set; }

        /// <summary>
        /// 節點
        /// </summary>
        public List<KpiItemModel> KpiItemList { get; set; }


        public List<RepKPIByYear> ByYearData { get; set; }
        public List<RepKPIByItem> ByItemData { get; set; }
        public List<RepKPIByMonth> ByMonthData { get; set; }

        //public KpiPage()
        //{
        //    BrandList = new List<SelectListItem>();
        //    BrandList.Add(new SelectListItem() { Text = "GOLDWELL", Value = "GOLDWELL" });
        //    BrandList.Add(new SelectListItem() { Text = "KMS", Value = "KMS" });
        //    Brand = BrandList.FirstOrDefault().Value;
        //}

    }

    /// <summary>
    /// 業績統計
    /// </summary>
    public class PFSPage : ReportsBasePage
    {
        /// <summary>
        /// View 的 format (table, json, chart)
        /// </summary>
        public string Format { get; set; }


        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// 依年度
        /// </summary>
        public List<RepPFSByYear> ByYearData { get; set; }

        /// <summary>
        /// 依月份
        /// </summary>
        public List<RepPFSByMonth> ByMonthData { get; set; }

        public PFSPage()
        {
            ByYearData = new List<RepPFSByYear>();

            ByMonthData = new List<RepPFSByMonth>();

            Brand = "KMS";

        }
    }

}