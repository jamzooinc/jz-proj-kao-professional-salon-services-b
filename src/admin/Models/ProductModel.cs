﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class ProductListModel : PagerModel
    {
        /// <summary>
        /// 搜尋文字
        /// </summary>
        public string SearchText { get; set; }
        /// <summary>
        /// 產品列表
        /// </summary>
        public List<ProductModel> Data { get; set; }
    }
    /// <summary>
    /// 待/交辦事項
    /// </summary>
    public class ProductModel
    {
        public ProductModel()
        {
            Id = Guid.NewGuid();
            CreateDate = DateTime.Now;
            Price = 0;
        }
        /// <summary>
        /// 編號
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 品牌 (KMS、GOLDWELL、花王)
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// 類別 (染劑、護染、冷燙、護髮、熱塑)
        /// </summary>
        public string Type { get; set; }

        public string[] Types { get; set; }

        /// <summary>
        /// 子類別 (XXX 染劑 )
        /// </summary>
        public string Serial { get; set; }
        
        /// <summary>
        /// 序號
        /// </summary>
        public int SN { get; set; }

        /// <summary>
        /// 產品名稱
        /// </summary>
        public string Pname { get; set; }

        /// <summary>
        /// 規格
        /// </summary>
        public string Spec { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string Summmary { get; set; }

        /// <summary>
        /// 價格
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// 完成日期
        /// </summary>
        public DateTime CreateDate { get; set; }

    }
}