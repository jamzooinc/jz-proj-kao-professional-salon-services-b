﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class CustomerTrainingListModel : PagerModel
    {
        /// <summary>
        /// 觸發按鈕
        /// </summary>
        public string trigger { get; set; }
        //public List<SelectListItem> StatusList { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public List<SelectListItem> StatusList { get; set; }

        public string Status { get; set; }

        public List<CustomerTrainingModel> Data { get; set; }
    }
    /// <summary>
    /// 教育訓練記錄
    /// </summary>
    public class CustomerTrainingModel
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 日期 
        /// </summary>
        public DateTime Date { get; set; }

        [RegularExpression("^[0-9]{4}[/](((0[13578]|(10|12))[/](0[1-9]|[1-2][0-9]|3[0-1]))|(02[/](0[1-9]|[1-2][0-9]))|((0[469]|11)[/](0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "格式錯誤")]
        [Required]
        public string strDate { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 客戶名稱
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public string Subject { get; set; }
        
        /// <summary>
        /// 講師
        /// </summary>
        public string Teacher { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 學生數量
        /// </summary>
        public int StdCount { get; set; }

        /// <summary>
        /// 建議 
        /// </summary>
        public string Suggestion { get; set; }

        /// <summary>
        /// 上課反應狀況
        /// </summary>
        public string React { get; set; }

        /// <summary>
        /// 完成日期
        /// </summary>
        public bool IsSales { get; set; }
    
        /// <summary>
        /// 從誰而來
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 講師編號
        /// </summary>
        public string EmpSN { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 0:主管未確定;1:主管已決定;2:上完了;3:異常
        /// </summary>
        public string StatusTxt 
        {
            get
            {
                string _r = string.Empty;
                switch (Status)
                {
                    case 0: _r = "未審核"; break;
                    case 1: _r = "已核准"; break;
                    case 2: _r = "完成授課"; break;
                    case 3: _r = "異常"; break;  
                    default: _r = Status.ToString(); break;
                }
                return _r;
            }
        }

        /// <summary>
        /// 核准日期
        /// </summary>
        public DateTime? ApproveDate { get; set; }

        /// <summary>
        /// 建立員工的編號
        /// </summary>
        public string CreateEmpSN { get; set; }

        /// <summary>
        /// 建立員工的姓名
        /// </summary>
        public string CreateEmpName { get; set; }

        public CustomerTrainingModel()
        {
            Id = Guid.NewGuid();
            Date = DateTime.Now;
            strDate = Date.ToString("yyyy/MM/dd");
            CreateDate = DateTime.Now;
        }
    }
}