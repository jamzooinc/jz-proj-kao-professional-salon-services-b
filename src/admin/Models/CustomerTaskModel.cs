﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class CustomerTaskListModel : PagerModel
    {
        public List<SelectListItem> MyEmps { get; set; }

        public string EmpId { get; set; }

        public string SearchText { get; set; }

        public List<CustomerTaskModel> Data { get; set; }
    }
    /// <summary>
    /// 待/交辦事項
    /// </summary>
    public class CustomerTaskModel
    {
        public CustomerTaskModel()
        {
            Id = Guid.NewGuid();
        }
        /// <summary>
        /// 編號
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        [RegularExpression("^[0-9]{4}[/](((0[13578]|(10|12))[/](0[1-9]|[1-2][0-9]|3[0-1]))|(02[/](0[1-9]|[1-2][0-9]))|((0[469]|11)[/](0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "格式錯誤")]
        [Required(ErrorMessage="必填")]
        public string strDate { get; set; }
        
        /// <summary>
        /// 標題
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 類別
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Memo : 待辦事項 /Assignment : 交辦事項
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int Status { get; set; }


        /// <summary>
        /// 完成日期
        /// </summary>
        public DateTime? CloseDate { get; set; }
    
        /// <summary>
        /// 從誰而來
        /// </summary>
        public string FromEmp { get; set; }

        /// <summary>
        /// 給誰的
        /// </summary>
        public string ToEmp { get; set; }

        /// <summary>
        /// 期限
        /// </summary>
        public DateTime? DeadLine { get; set; }

        [RegularExpression("^[0-9]{4}[/](((0[13578]|(10|12))[/](0[1-9]|[1-2][0-9]|3[0-1]))|(02[/](0[1-9]|[1-2][0-9]))|((0[469]|11)[/](0[1-9]|[1-2][0-9]|30)))$", ErrorMessage = "格式錯誤")]
        [Required(ErrorMessage="必填")]
        public string strDeadLine { get; set; }

        /// <summary>
        /// 內容
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// 部門
        /// </summary>
        public string DepName { get; set; }
    }
}