﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class PerfRemindedListModel : PagerModel
    {
        /// <summary>
        /// 搜尋文字
        /// </summary>
        public string SearchText { get; set; }
        /// <summary>
        /// 產品列表
        /// </summary>
        public List<PerfRemindedModel> Data { get; set; }
    }
    /// <summary>
    /// 待/交辦事項
    /// </summary>
    public class PerfRemindedModel
    {
        public PerfRemindedModel()
        {
            Id = Guid.NewGuid();
            CreateDate = DateTime.Now;
        }

        /// <summary>
        /// 編號
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 每月的第幾天
        /// </summary>
        public int DayOfMonth { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 所屬業務 
        /// </summary>
        public string EmpSN { get; set; }
        
        /// <summary>
        /// 業績額度
        /// </summary>
        public int Limitation { get; set; }

        /// <summary>
        /// 完成日期
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}