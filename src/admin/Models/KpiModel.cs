﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace admin.Models
{
    public class KpiListModel
    {
        public string KpiItemRootName { get; set; }

        public string KpiItemName { get; set; }

        public Guid KpiItemId { get; set; }

        public List<KpiModel> Data { get; set; }
    }

    /// <summary>
    /// KPI 用的 Model
    /// </summary>
    public class KpiModel
    {
        //[Required(ErrorMessage="必填")]
        public int Year { get; set; }

        //[Required(ErrorMessage="必填")]
        public int Month { get; set; }

        //[Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }

        //[Required(ErrorMessage="必填")]
        [ScriptIgnore]
        public Guid KpiItemId { get; set; }

        [ScriptIgnore]
        public string KpiItem { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int ActualAmt { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int EstimateAmt { get; set; }

        //客戶
        public string Customer { get; set; }

        //客戶
        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int CustomerAmt { get; set; }


        public int CustomerCount { get; set; }
    }
}