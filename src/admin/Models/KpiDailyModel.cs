﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace admin.Models
{
    /// <summary>
    /// 例表頁
    /// </summary>
    public class KpiDailyListModel : PagerModel
    {
        public string KpiDailyItemRootName { get; set; }

        public string KpiDailyItemName { get; set; }

        public Guid KpiDailyItemId { get; set; }

        public List<KpiDailyModel> Data { get; set; }
    }

    /// <summary>
    /// 每日回報使用的 Model 
    /// </summary>
    public class DailyReportEmployee : EmployeeModel
    {
        public List<DailyReportRecord> Records { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DailyReportRecord
    {
        public decimal ActualAmt { get; set; }

        //public string CustomerName { get; set; }

        //public string CustomerNumber { get; set; }

        public string CreateEmpSN { get; set; }
    }

    /// <summary>
    /// 每日回報使用的
    /// </summary>
    public class KpiDailyPage
    {
        /// <summary>
        /// 品牌
        /// </summary>
        public string Brand { get; set; }

        public List<SelectListItem> BrandList { get; set; }

        /// <summary>
        /// 日期字串
        /// </summary>
        public string strDate { get; set; }

        public DateTime DateTime 
        {
            get
            {
                try
                {
                    return DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        public string KpiRootId { get; set; }

        public List<SelectListItem> KpiItemsRoot { get; set; }

        public string KpiChildId { get; set; }

        public List<SelectListItem> KpiItemsChild { get; set; }

        /// <summary>
        /// 每日的頁務
        /// </summary>
        public List<DailyReportEmployee> Employees { get; set; }

        /// <summary>
        /// 每日回報的 view
        /// </summary>
        public AddViewModel Create { get; set; }
    }

    public class AddViewModel
    {
        /// <summary>
        /// 業務編號
        /// </summary>
        [Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }
        /// <summary>
        /// 客戶編號
        /// </summary>
        //[Required(ErrorMessage="必填")]
        public string CustomerNumber { get; set; }
        /// <summary>
        /// 業績
        /// </summary>
        [RegularExpression("\\d+", ErrorMessage = "格式錯誤")]
        [Required(ErrorMessage="必填")]
        public decimal Amt { get; set; }
    }

    public class KpiDailyExcelModel
    {
        //日期,項目,當日業績,業務編號

        /// <summary>
        /// 日期
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// 項目
        /// </summary>
        public string Item { get; set; }

        /// <summary>
        /// 當日業績
        /// </summary>
        public string ActualAmt { get; set; }

        /// <summary>
        /// 業務編號
        /// </summary>
        public string EmpSN { get; set; }
        
    }

    /// <summary>
    /// KpiDaily 用的 Model
    /// </summary>
    public class KpiDailyModel
    {
        //[Required(ErrorMessage="必填")]
        public DateTime Date { get; set; }
        

        public int Year { get; set; }

        //[Required(ErrorMessage="必填")]
        public int Month { get; set; }

        public int Day { get; set; }

        //[Required(ErrorMessage="必填")]
        public string EmpSN { get; set; }

        public string CustomerNumber { get; set; }

        //[Required(ErrorMessage="必填")]
        [ScriptIgnore]
        public Guid KpiItemId { get; set; }

        public string KpiString { get; set; }

        [Required(ErrorMessage="必填")]
        [RegularExpression("\\d+", ErrorMessage="格式錯誤")]
        public int ActualAmt { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateEmpSN { get; set; }

        //選擇品牌
        public string Brand { get; set; }

        public List<SelectListItem> BrandList { get; set; }

        public List<KpiItemModel> KpiItemList { get; set; }


        #region Excel 欄位專用
        //項目

        /// <summary>
        /// 項目 (xxx/xxx/xx)
        /// </summary>
        public string ExcelRowItemName { get; set; }
        #endregion

        public KpiDailyModel()
        {
            BrandList = new List<SelectListItem>();
            BrandList.Add(new SelectListItem() { Text = "GOLDWELL", Value = "GOLDWELL" });
            BrandList.Add(new SelectListItem() { Text = "KMS", Value = "KMS" });
            Brand = BrandList.FirstOrDefault().Value;
        }
    }
}