﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class CustomerExcelModel
    {
        public string 客戶編號 { get; set; }
        public string 客戶簡稱 { get; set; }
        public string 客戶全名 { get; set; }
        public string 負責人 { get; set; }
        public string 聯絡電話1 { get; set; }
        public string 業務人員 { get; set; }
        public string 收款業務員 { get; set; }
        public string GW初次交易日 { get; set; }
        public string GW歇業日期 { get; set; }
        public string KMS初次交易日 { get; set; }
        public string KMS歇業日期 { get; set; }
        public string 最近交易日 { get; set; }
        public string 收款方式 { get; set; }
        public string 郵遞區號_登記地址 { get; set; }
        public string 登記地址 { get; set; }

        public string 郵遞區號_送貨地址 { get; set; }
        public string 送貨地址_縣市 { get; set; }
        public string 送貨地址_區域 { get; set; }
        public string 送貨地址 { get; set; }
        public string 送貨地址_英文 { get; set; }
        public string SalesCode { get; set; }


        #region 2013.08.05 新增
        public string SalesOffice {get;set;}
        #endregion

        #region 2013.12.10 新增
        public string SapCode { get; set; }
        #endregion

        #region 2015.02.02 新增
        public string 銷貨級數 { get; set; }
        #endregion

        public int 分店數 { get; set; }

        public Customer Mapping()
        {
            #region 對應
            Customer dbEntity = new Customer();
            dbEntity.Number = 客戶編號.Trim();
            dbEntity.Name = 客戶簡稱;
            dbEntity.NameAbbr = 客戶全名;
            dbEntity.Owner = 負責人;
            dbEntity.Tel = 聯絡電話1;
            dbEntity.EmpSN = 業務人員.Trim();
            dbEntity.PaymentEmp = 收款業務員;
            dbEntity.FirstTransDate = GW初次交易日;
            //dbEntity.CloseDate = GW歇業日期;
            dbEntity.FirstTransDate_KMS = KMS初次交易日;
            //dbEntity.CloseDate_2 = KMS歇業日期;
            dbEntity.LastTransDate = 最近交易日;
            dbEntity.Payment = 收款方式;
            dbEntity.Postal = 郵遞區號_登記地址;
            dbEntity.EnrollmentAddr = 登記地址;
            dbEntity.Postal_2 = 郵遞區號_送貨地址;
            dbEntity.BranchCount = 分店數;

            //dbEntity.TransportAddr_City = 
            dbEntity.TransportAddr_Postal = 郵遞區號_送貨地址;
            dbEntity.TransportAddr_City = 送貨地址_縣市;
            dbEntity.TransportAddr_Area = 送貨地址_區域;
            dbEntity.TransportAddr = 送貨地址_縣市 + 送貨地址_區域 + 送貨地址;
            dbEntity.TransportAddr_en = 送貨地址_英文;
            dbEntity.Rating = Convert.ToInt32(銷貨級數);
            dbEntity.SalesCode = SalesCode;
            dbEntity.SalesOffice = SalesOffice;
            dbEntity.SapCode = SapCode;

            return dbEntity;
            #endregion
        }
    }

    public class CustomerListModel : PagerModel
    {
        public List<SelectListItem> MyEmps { get; set; }

        public string EmpId { get; set; }

        public List<SelectListItem> Areas { get; set; }

        /// <summary>
        /// 地區選擇
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 業務編號
        /// </summary>
        public string EmpSN { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 客戶名稱
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 統一編號
        /// </summary>
        public string SN { get; set; }

        public List<CustomerModel> Data { get; set; }

        public string Name { get; set; }
    }
    /// <summary>
    /// 客戶資料
    /// </summary>
    public class CustomerModel
    {
        /// <summary>
        /// 系統唯一識別值
        /// </summary>
        [Display(Name = "系統唯一識別值")]
        public Guid NID { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        [Display(Name = "客戶編號")]
        public string Number { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        [Display(Name = "新的客戶編號")]
        [Required(ErrorMessage = "必填")]
        public string NewNumber { get; set; }

        /// <summary>
        /// 1.合約客戶：由後台匯入之客戶資料 
        /// 2.潛在客戶：由APP新增且尚無交易的客戶。
        /// 3.潛在客戶轉合約
        /// </summary>
        //[RegularExpression("")]
        [Display(Name = "類戶類別")]
        [Required(ErrorMessage = "必填")]
        public int Type { get; set; }

        /// <summary>
        /// 統一編號
        /// </summary>
        public string SN { get; set; }

        /// <summary>
        /// 所屬業務
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string EmpSN { get; set; }

        /// <summary>
        /// 客戶全名
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string Name { get; set; }
        [Required(ErrorMessage = "必填")]
        public string NameAbbr { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Owner { get; set; }

        [Required(ErrorMessage = "必填")]
        public string Tel { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Tel_2 { get; set; }

        public string Fax { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage = "必填")]
        public int EmpCount { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Dep { get; set; }
        /// <summary>
        /// 首次交易日
        /// </summary>
        public string FirstTransDate { get; set; }
        /// <summary>
        /// 最近交易日
        /// </summary>
        public string LastTransDate { get; set; }
        //銷貨等級
        public string SalesLevel { get; set; }
        //員工等級
        [Required(ErrorMessage = "必填")]
        public string StaffLevel { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Brand { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Area { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ToType1Date { get; set; }

        /// <summary>
        /// 登記地址
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string EnrollmentAddr { get; set; }

        [Required(ErrorMessage = "必填")]
        public string TransportAddr_City { get; set; }
        [Required(ErrorMessage = "必填")]
        public string TransportAddr_Area { get; set; }
        [Required(ErrorMessage = "必填")]
        public string TransportAddr_Postal { get; set; }

        /// <summary>
        /// 送貨地址
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string TransportAddr { get; set; }

        /// <summary>
        /// 送貨地址 _ 英文
        /// </summary>
        public string TransportAddr_en { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Postal { get; set; }
        /// <summary>
        /// 收款方式(MA041)
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string Payment { get; set; }
        //public string Payment { get; set; }

        /// <summary>
        /// 分店數
        /// </summary>
        public int BranchCount { get; set; }

        ///歇業日期(MA068
        public DateTime? CloseDate { get; set; }

        /// <summary>
        /// 郵遞區號2
        /// </summary>
        public string Postal_2 { get; set; }

        /// <summary>
        /// 收款業務員
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public string PaymentEmp { get; set; }

        /// <summary>
        /// KMS初次交易日
        /// </summary>
        public string FirstTransDate_KMS { get; set; }

        /// <summary>
        /// 歇業日期
        /// </summary>
        public DateTime? CloseDate_2 { get; set; }

        /// <summary>
        /// 客戶等級
        /// </summary>
        [Required(ErrorMessage = "必填")]
        public int Rating { get; set; }


        #region 聯絡人

        //`ContactName[string][聯絡人姓名]`  
        //`ContactTel[string][聯絡人電話]`  
        //`ContactEmail[string][聯絡人Email]`  

        public string ContactName { get; set; }
        public string ContactTel { get; set; }
        public string ContactEmail { get; set; }
        #endregion

        #region 追加欄位
        [Required(ErrorMessage = "必填")]
        public string ProductType1 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string ProductType2 { get; set; }
        public string ProductType3 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string ProductType4 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string ProductType5 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Fee1 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Fee2 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Fee3 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Fee4 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Fee5 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OwnerNick { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OwnerTitle { get; set; }
        public string OwnerNum { get; set; }
        public string OwnerEmail { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OwnerTel { get; set; }
        public string OpenningDate { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Decorating { get; set; }

        //新點分項
        public string NewPoint { get; set; }
        public string[] NewPoints { get; set; }

        //訂貸方式
        public string OrderForm { get; set; }
        public string[] OrderForms { get; set; }
        //產品資訊來源
        public string ProductSource { get; set; }
        public string[] ProductSources { get; set; }
        //材料來源
        public string MaterialSource { get; set; }
        public string[] MaterialSources { get; set; }
        //全材料用貨
        public string MaterialRating { get; set; }
        public string[] MaterialRatings { get; set; }

        //可擴展產品線
        public string ExtProLine { get; set; }
        public string[] ExtProLines { get; set; }
        [Required(ErrorMessage = "必填")]
        public string Staff { get; set; }

        //店數(店別)
        public string BranchType { get; set; }
        public string[] BranchTypes { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OH { get; set; }

        //服務方式
        public string ServiceType { get; set; }
        public string[] ServiceTypes { get; set; }

        //請款方式
        public string Billing { get; set; }
        public string[] Billings { get; set; }
        [Required(ErrorMessage = "必填")]
        public string S1 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string S2 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string S3 { get; set; }
        public string KMS1 { get; set; }
        public string KMS2 { get; set; }
        public string KMS3 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OTC1 { get; set; }
        [Required(ErrorMessage = "必填")]
        public string OTC2 { get; set; }
        public string WebSite { get; set; }
        #endregion

        //是否已關戶
        public bool IsAccountClose { get; set; }
        //關戶日
        public DateTime? AccountCloseDate { get; set; }
        //關戶原因
        public string AccountCloseNotes { get; set; }
        public string[] AccountCloseNotess { get; set; }
        public string AccountCloseNotesText { get; set; }
        [Required(ErrorMessage = "必填")]
        public string SalesCode { get; set; }

        [Required(ErrorMessage = "必填")]
        public string SalesOffice { get; set; }
        public string SapCode { get; set; }

        public CustomerModel()
        {
            Type = 2;
        }
    }
}