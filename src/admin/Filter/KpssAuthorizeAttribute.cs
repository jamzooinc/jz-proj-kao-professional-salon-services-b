﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Filter
{
    /// <summary>
    /// 自訂的角色授權
    /// </summary>
    public class KpssAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly KpssRole[] _acceptedRoles;
        public KpssAuthorizeAttribute(params KpssRole[] acceptedRoles)
        {
            if (acceptedRoles.Length == 0)
            {
                List<KpssRole> krs = new List<KpssRole>();
               
                foreach (var n in Enum.GetValues(typeof(KpssRole)))
                {
                    krs.Add((KpssRole)n);
                }
                _acceptedRoles = krs.ToArray();
            }
            else
            {
                _acceptedRoles = acceptedRoles; 
            }
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            List<string> errors = new List<string>();
            try
            {
               
                AccountModel emp;
                string token = filterContext.HttpContext.Request["token"].ToString();
                if (TokenService.Actived(token, out errors, out emp))
                {
                    if (!emp.IsInRoles(_acceptedRoles))
                    {
                        errors.Add("role permission error");
                    }
                }

            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
                if (ex.InnerException != null)
                {
                    errors.Add(ex.InnerException.Message);
                }
            }

            if (errors.Count > 0)
            {
                filterContext.Result = new JsonResult()
                {
                    Data = new
                    {
                        RS = "ERROR",
                        errors = errors
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
}