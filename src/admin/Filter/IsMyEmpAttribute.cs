﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Filter
{
    /// <summary>
    /// 讓業務主管判斷是否是自已的業務員工 
    /// ( 只能和 KpssAuthorize 共用 )
    /// </summary>
    public class IsMyEmpAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            
            if (filterContext.ActionParameters["Emp"] != null)
            {
                EmpService empService = new EmpService();

                AccountModel emp = (AccountModel)filterContext.ActionParameters["Emp"];
                string EmpId = (string)filterContext.ActionParameters["EmpId"];
                
                if (emp.IsInRoles(KpssRole.區業務主管))
                {
                    if (emp != null && !string.IsNullOrEmpty(EmpId))
                    {
                        if (!empService.IsMyEmp(emp.SN, EmpId))
                        {
                            //沒有這個 token 要回應
                            filterContext.Result = new JsonResult()
                            {
                                Data = new { RS = "ERROR", errors = new string[] { "不屬於您的員工" } },
                                ContentEncoding = System.Text.Encoding.UTF8,
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                    }
                    else
                    {
                        //沒有這個 token 要回應
                        filterContext.Result = new JsonResult()
                        {
                            Data = new { RS = "ERROR", errors = new string[] { "請選擇一位您的員工" } },
                            ContentEncoding = System.Text.Encoding.UTF8,
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                }
            }
        }
    }
}
