﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Filter
{
    /// <summary>
    /// 用來驗證 API Token 的 Filter 
    /// </summary>
    public class ApiAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.HttpContext.Request["token"] == null)
            {
                //沒有這個 token 要回應
                filterContext.Result = new JsonResult()
                {
                    Data = new { RS = "ERROR", errors = new string[] { "No Token" } },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                //進入驗證 Token 的規則
                string token = filterContext.HttpContext.Request["token"].ToString();

                List<string> errors = new List<string>();
                AccountModel emp = null;
                if (TokenService.Actived(token, out errors, out emp))
                {
                    emp.Token = token;
                    filterContext.ActionParameters["Emp"] = emp;
                }
                else
                {
                    //沒有這個 token 要回應
                    filterContext.Result = new JsonResult()
                    {
                        Data = new { RS = "ERROR", errors = errors },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
        }
    }
}