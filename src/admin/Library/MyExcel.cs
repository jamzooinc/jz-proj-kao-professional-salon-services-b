﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using System.IO;

namespace admin.Library
{
    public class MyExcel
    {
        /// <summary>
        /// 檔案名稱
        /// </summary>
        public string FileName { get; private set; }

        public HSSFWorkbook workbook;

        public MyExcel(string FileName)
        {
            this.FileName = FileName;

            workbook = new HSSFWorkbook();
        }

        public Stream Export()
        {
            if (workbook != null)
            {
                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);

                ms.Flush();
                ms.Position = 0;

                return ms;
            }
            else
            {
                return null;
            }
        }

        public void CreateNewSheet(string SheetName, DataTable SourceTable)
        {
            //MemoryStream ms = new MemoryStream()
            if (workbook.GetSheetIndex(SheetName) == -1)
            {
                HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet(SheetName);

                // header
                HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);
                foreach (DataColumn column in SourceTable.Columns)
                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

                // han
                int rowIndex = 1;
                foreach (DataRow row in SourceTable.Rows)
                {
                    HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in SourceTable.Columns)
                    {
                        dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                    }
                    rowIndex++;
                }
            }
        }
    }
}