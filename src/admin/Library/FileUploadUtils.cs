﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;

namespace admin.Library
{
    /// <summary>
    /// 檔案儲存的 Library
    /// </summary>
    public class FileUploadUtils
    {
        public static string FileUploadPath = "uploadfile";

        #region 另存縮圖

        private static string GetPreviewImageFileName(string prefix, string relativePath)
        {
            string newFileName = string.Format("{0}{1}", prefix, Path.GetFileName(relativePath));
            string newRelativePath = Path.Combine(Path.GetDirectoryName(relativePath), newFileName);
            return newRelativePath;
        }

        public static string GetBackendPreviewImageName(string relativePath)
        {
            return GetPreviewImageFileName("bkp_", relativePath);
        }

        public static string GetListPreviewImageName(string relativePath)
        {
            return GetPreviewImageFileName("list_", relativePath);
        }

        public static string GetDetailPreviewImageName(string relativePath)
        {
            return GetPreviewImageFileName("Detail_", relativePath);
        }

        /// <summary>
        /// 另存 Backend Preivew 的圖檔
        /// </summary>
        /// <param name="relativePath"></param>
        public static void SaveBackendPreviewImage(string relativePath)
        {
            try
            {
                Image SourceImg = Image.FromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath));
                Image r = resizeImage(SourceImg, new Size(200, 200));

                // 把 r fit 到新的 100 * 100 圖框裏面去, 並非原圖
                Bitmap b = new Bitmap(100, 100);
                Graphics g = Graphics.FromImage(b);
                g.DrawImage(r, new Point(0, 0));
                g.Dispose();
                //存檔
                b.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetBackendPreviewImageName(relativePath)));
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// 另存 List Preview 的圖檔
        /// </summary>
        /// <param name="relativePath"></param>
        public static void SaveListPreviewImage(string relativePath)
        {
            try
            {
                Image SourceImg = Image.FromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath));
                Image r = resizeImage(SourceImg, new Size(200, 200));
                r.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetListPreviewImageName(relativePath)));
            }
            catch { }
        }

        /// <summary>
        ///  另存 Detail Preview 的圖檔
        /// </summary>
        /// <param name="relativePath"></param>
        public static void SaveDetailImage(string relativePath)
        {
            try
            {
                Image SourceImg = Image.FromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath));
                Image r = resizeImage(SourceImg, new Size(200, 200));
                r.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetDetailPreviewImageName(relativePath)));
            }
            catch { }
        }

        private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);

            g.Dispose();

            return (System.Drawing.Image)b;
        }
        #endregion

        public static string GetCustomImageFileName(string prefix, string relativePath, int width)
        {
            string newFileName = string.Format("{0}_{1}{2}", "Custom", width, Path.GetFileName(relativePath));
            string newRelativePath = Path.Combine(Path.GetDirectoryName(relativePath), newFileName);
            return newRelativePath;
        }

        /// <summary>
        /// 另存 Backend Preivew 的圖檔
        /// </summary>
        /// <param name="relativePath"></param>
        public static void SaveCustomImage(string relativePath, int width)
        {
            try
            {
                Image SourceImg = Image.FromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath));
                //自訂長寬
                int nHeight = (int)((float)SourceImg.Height * ((float)width / (float)SourceImg.Width));

                Image r = resizeImage(SourceImg, new Size(width, nHeight));

                // 把 r fit 到新的 100 * 100 圖框裏面去, 並非原圖
                Bitmap b = new Bitmap(width, nHeight);
                Graphics g = Graphics.FromImage(b);
                g.DrawImage(r, new Point(0, 0));
                g.Dispose();
                //存檔 (取得新的檔名)

                b.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetCustomImageFileName("Custom_", relativePath, width)));
            }
            catch (Exception ex) { }
        }


        public static bool DeleteFile(string relativePath)
        {
            if (!string.IsNullOrEmpty(relativePath))
            {
                relativePath = (relativePath.StartsWith("~/")) ? relativePath.Remove(0, 2) : relativePath;

                string absolutePath =
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
                string bgkPath = FileUploadUtils.GetBackendPreviewImageName(absolutePath);
                string listPath = FileUploadUtils.GetListPreviewImageName(absolutePath);
                string detailPath = FileUploadUtils.GetDetailPreviewImageName(absolutePath);

                string[] delteFilePaths = { 
                                    absolutePath,
                                    bgkPath,
                                    listPath,
                                    detailPath
                                };

                foreach (var p in delteFilePaths)
                {
                    if (File.Exists(p))
                    {
                        try
                        {
                            File.Delete(p);
                        }
                        catch
                        { }
                    }
                    else
                    {
                    }
                }
                return true;
            }

            return false;
        }


        private static void BackendSaveImage(object a)
        {
            //存 list
            //再存 detail
            //再存 backend preview
            SaveListPreviewImage((string)a);
            SaveDetailImage((string)a);
            //SaveBackendPreviewImage((string)a);
        }

        /// <summary>
        /// 儲存其他類型的檔案
        /// </summary>
        /// <param name="Parent"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string SaveOtherFile(string Parent, HttpPostedFileBase file)
        {
            string RelativePath = Path.Combine(FileUploadPath, Parent, GenFileName(file.FileName));
            //string SavedPath = HttpContext.Current.Server.MapPath(RelativePath);
            string SavedPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, RelativePath);

            DirectoryInfo dinfo = Directory.GetParent(SavedPath);
            if (!dinfo.Exists) dinfo.Create();

            file.SaveAs(SavedPath);

            return "~/" + RelativePath;
        }

        /// <summary>
        /// Save File
        /// </summary>
        /// <param name="Parent">Prefix</param>
        /// <param name="file">http posted file Base Object</param>
        /// <returns>~/relativePath</returns>
        public static string SaveFile(string Parent, HttpPostedFileBase file)
        {
            string RelativePath = Path.Combine(FileUploadPath, Parent, GenFileName(file.FileName));
            //string SavedPath = HttpContext.Current.Server.MapPath(RelativePath);
            string SavedPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, RelativePath);

            DirectoryInfo dinfo = Directory.GetParent(SavedPath);
            if (!dinfo.Exists) dinfo.Create();

            file.SaveAs(SavedPath);

            //非同步的儲存 Image
            //Thread thread = new Thread(new ParameterizedThreadStart(BackendSaveImage));
            Thread newThread = new Thread(
                new ParameterizedThreadStart(BackendSaveImage));

            newThread.Start(RelativePath);

            SaveBackendPreviewImage(RelativePath);

            return "~/" + RelativePath;
        }

        /// <summary>
        /// Save File
        /// </summary>
        /// <param name="Parent">Prefix</param>
        /// <param name="file">http posted file Base Object</param>
        /// <returns>~/relativePath</returns>
        public static string SaveFile(string Parent, HttpPostedFileBase file, int[] Widths)
        {
            string RelativePath = Path.Combine(FileUploadPath, Parent, GenFileName(file.FileName));
            //string SavedPath = HttpContext.Current.Server.MapPath(RelativePath);
            string SavedPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, RelativePath);

            DirectoryInfo dinfo = Directory.GetParent(SavedPath);
            if (!dinfo.Exists) dinfo.Create();

            file.SaveAs(SavedPath);

            //非同步的儲存 Image
            //Thread thread = new Thread(new ParameterizedThreadStart(BackendSaveImage));
            Thread newThread = new Thread(
                new ParameterizedThreadStart(BackendSaveImage));

            newThread.Start(RelativePath);

            SaveBackendPreviewImage(RelativePath);

            //儲存自訂縮圖
            foreach (int width in Widths)
            {
                SaveCustomImage(RelativePath, width);
            }

            return "~/" + RelativePath;
        }

        /// <summary>
        /// 重新取得檔案名稱
        /// timespan + randomValue + CacheIncValue
        /// </summary>
        /// <param name="fileName">old File Name</param>
        /// <returns>new File Name</returns>
        public static string GenFileName(string fileName)
        {
            TimeSpan timespan = TimeSpan.FromTicks(DateTime.Now.Ticks);
            int processId = new Random(Guid.NewGuid().GetHashCode()).Next();

            //ProcessModelInfo.GetCurrentProcessInfo().ProcessID;
            int inc = 0;
            if (HttpContext.Current.Cache["FileUploadInc"] != null)
            {
                inc = Convert.ToInt32(HttpContext.Current.Cache["FileUploadInc"]);
                inc += 1;
                HttpContext.Current.Cache["FileUploadInc"] = inc;
            }
            else
            {
                HttpContext.Current.Cache["FileUploadInc"] = 1;
            }

            String.Format("{0:X}", timespan.Ticks);

            return string.Format("{0}{1}{2}{3}",
                    String.Format("{0:X}", timespan.Ticks),
                    String.Format("{0:X}", processId),
                    String.Format("{0:X}", inc),
                    Path.GetExtension(fileName)
                );
        }
    }

}