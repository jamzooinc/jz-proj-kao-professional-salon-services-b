﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;

namespace admin.Library
{
    public class Utils
    {
        public static DateTime[] GetCurrentDaysOfWeek(int WeekOfYear)
        {
            return GetCurrentDaysOfWeek(DateTime.Now.Year, WeekOfYear);
        }
        /// <summary>
        /// 取得本週
        /// </summary>
        /// <param name="WeekOfYear"></param>
        /// <returns></returns>
        public static DateTime[] GetCurrentDaysOfWeek(int Year, int WeekOfYear)
        {
            string filePath = HttpContext.Current.Server.MapPath("~/App_Data/Weeks/" + Year + ".json");
            if (!File.Exists(filePath))
            {
                throw new Exception("檔案不存在");
            }
            List<List<string>> _weeks = null;
            string strJsonWeek = string.Empty;
            using (var outStream = new FileStream(
                filePath,
                FileMode.Open,
                FileAccess.Read,
                FileShare.ReadWrite))
            {
                using (StreamReader sr = new StreamReader(outStream))
                {
                    strJsonWeek = sr.ReadToEnd();
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    try
                    {
                        _weeks = ser.Deserialize<List<List<string>>>(strJsonWeek);
                    }
                    catch
                    {
                        throw new Exception("json file parse error");
                    }
                }
            }

            if (WeekOfYear < _weeks.Count())
            {
                List<DateTime> dtWeeks = new List<DateTime>();
                foreach (string strDate in _weeks[WeekOfYear])
                {
                    try
                    {
                        dtWeeks.Add(DateTime.ParseExact(strDate, "yyyy/MM/dd", null));
                    }
                    catch
                    {
                        throw new Exception("parse datetime string error : " + strDate);
                    }
                }
                return dtWeeks.ToArray();
            }

            return null;

        }

        /// <summary>
        /// 發送求救
        /// </summary>
        /// <param name="senderMail"></param>
        /// <param name="senderName"></param>
        /// <param name="body"></param>
        /// <param name="targets"></param>
        /// <returns></returns>
        public static string SendHelp(string senderMail, string senderName, string body, List<MailAddress> targets)
        {
            string errorMsg = string.Empty;

            if (targets.Count < 1)
            {
                return errorMsg;
            }
            MailMessage message = new MailMessage("Service@kpss.com.tw", "Service@kpss.com.tw");//MailMessage(寄信者, 收信者)

            foreach (var Mail in targets)
            {
                message.Bcc.Add(Mail);
            }
            message.IsBodyHtml = false;
            message.BodyEncoding = System.Text.Encoding.UTF8;//E-mail編碼
            message.Subject = string.Format("求救郵件-{0} 向您發出求救", senderName);     //E-mail主旨
            message.Body = body;//E-mail內容

            SmtpClient smtpClient = new SmtpClient("127.0.0.1", 25);//設定E-mail Server和port
            smtpClient.Send(message);

            return errorMsg;
        }
    }
}