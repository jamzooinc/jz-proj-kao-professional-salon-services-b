﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;

namespace admin.Areas.app.Controllers
{
    using Filter;
    using Service;

    [ApiAttribute]
    public class KpiController : BaseAPIController
    {
        KpiService Service;

        public KpiController()
        {
            Service = new KpiService();
        }

        // 業務主管 用來查詢業務業績
        // GET: /app/kpi/emp?token &empsn= &year=
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult Emp(AccountModel CurrentEmp,string Brand, string EmpSN, int Year = 0)
        {
            try
            {
                if (String.IsNullOrEmpty(EmpSN))
                {
                    throw new Exception("no emp sn");
                }
                var Data = Service.GetKpiData(EmpSN, Brand, Year == 0 ? DateTime.Now.Year : Year);

                return Json(new { 
                    RS = "OK",
                    empsn = EmpSN,
                    year = Year,
                    data = Data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();

                errors.Add(ex.Message);
                if (ex.InnerException != null)
                {
                    errors.Add(ex.InnerException.Message);
                }

                return Json(new { 
                    RS = "OK",
                    errors = errors
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
