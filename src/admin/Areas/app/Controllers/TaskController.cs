﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Areas.app.Controllers
{
    using admin.Models;
    using Filter;
    using Service;

    [ApiAttribute]
    public class TaskController : BaseAPIController
    {
        //
        // GET: /app/Task/
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult Index(AccountModel Emp)
        {
            CustomerTaskModel Model = new CustomerTaskModel();

            Model.FromEmp = Emp.SN;

            return View(Model);
        }

        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [HttpPost]
        //POST: /app/task/create
        public ActionResult Create(AccountModel Emp, CustomerTaskModel Model)
        {
            int iResult = 0;
            CustomerTaskService Service = new CustomerTaskService();
            Dictionary<string, object> errMsg = new Dictionary<string,object>();

            if (ModelState.IsValid)
            {
                iResult = (Service.Create(Emp.SN, Model, out errMsg)) ? 1 : 0;

                if (iResult == 1)
                {
                    return Json(
                    new
                    {
                        succeed = iResult,
                        data = new
                        {
                            Id = Model.Id.ToString(),
                            Date = Model.Date.ToString("yyyy/MM/dd HH:mm:ss"),
                            Number = Model.Number,
                            Title = Model.Title,
                            Category = Model.Category,
                            Type = Model.Type,
                            Status = Model.Status,
                            CloseDate = (Model.CloseDate.HasValue)? Model.CloseDate.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty,
                            FromEmp = Model.FromEmp,
                            ToEmp = Model.ToEmp,
                            DeadLine = Model.DeadLine,
                            Context = Model.Context,
                            DepName = Model.DepName
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var key in errMsg.Keys)
                    {
                        ModelState.AddModelError(key, errMsg[key].ToString());
                    }
                    return View("Index", Model);
                }
            }
            else
            {
                return View("Index", Model);
            }
           
        }
    }
}
