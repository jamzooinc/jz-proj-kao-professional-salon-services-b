﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Mvc;

namespace admin.Areas.app.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /app/Test/
        public ActionResult Index(string mysubject, string address, string msg)
        {
            try
            {
                #region m

                MailMessage message = new MailMessage("Servcie@kpss.com.tw", address);  //MailMessage(寄信者, 收信者)
                message.IsBodyHtml = false;
                message.BodyEncoding = System.Text.Encoding.UTF8;                       //E-mail 編碼
                message.Subject = mysubject;                                            //E-mail 主旨
                message.Body = msg;                                                     //E-mail 內容
                //message.To.Add(new MailAddress("ajen.chen@jamzoo.com.tw", "ajen.chen"));
                //message.Bcc.Add(new MailAddress("ajen.chen@jamzoo.com.tw", "ajen.chen"));
                //message.Bcc.Add(new MailAddress("yc.hsieh@jamzoo.com.tw", "yc.hsieh"));

                //foreach (var att in attachmentContent)
                //{
                //    byte[] b = Encoding.GetEncoding("Big5").GetBytes(att);
                //    MemoryStream ms = new MemoryStream(b);
                //    message.Attachments.Add(new Attachment(ms, mysubject + ".csv"));
                //}

                SmtpClient smtpClient = new SmtpClient("127.0.0.1", 25);                //設定 E-mail Server 和 port
                smtpClient.Send(message);

                #endregion

                return Content("OK");

            }
            catch (Exception Ex)
            {
                return Content(Ex.Message);
            }
        }

    }
}
