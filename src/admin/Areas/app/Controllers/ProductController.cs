﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using System.Net.Mail;
using System.Text;
using System.Net;

namespace admin.Areas.app.Controllers
{
    using System.IO;
    using Filter;
    using Service;

    [ApiAttribute]
    public class ProductController : BaseAPIController
    {
        KpiService Service;

        public ProductController()
        {
            Service = new KpiService();
        }

        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult Brands(AccountModel emp)
        {
            ProductPage m = new ProductPage();
            ProductService s = new ProductService();
            m.Brands = s.GetBrands();
            return View(m);
        }

        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult Category(AccountModel emp, string Brand)
        {
            ProductPage m = new ProductPage();
            ProductService s = new ProductService();
            m.Serials = s.GetSerialsByBrand(Brand);
            m.Brand = Brand;

            return View(m);
        }

        // 取得產品的頁面
        // GET: /app/product/index?token=
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult Index(AccountModel emp,string Brand, string Serial)
        {
            return View(new OrderForm() {
                name = emp.Name,
                mail = emp.Email,
                serial = Serial,
                brand = Brand
            });
        }

        // 購物車頁面
        // GET: /app/product/cars?token=
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult Cars(AccountModel emp, string brand, string serial)
        {
            return View(new OrderForm()
            {
                name = emp.Name,
                mail = emp.Email,
                brand = brand,
                serial = serial
            });
        }


        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult List(AccountModel emp, string serial)
        {
            ProductService s = new ProductService();

            var Query = s.GetList(emp.SN, "", serial, new ProductListModel());

            return Json(Query.Data.Take(100).ToList(), JsonRequestBehavior.AllowGet);
        }

        // 業務主管 用來查詢業務業績
        // GET: /app/product/order?token=
        [HttpPost]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書, KpssRole.駐區業務)]
        public ActionResult Order(AccountModel emp)
        {
            string MailLogPath = Server.MapPath("~/App_Data/Mail/Log/mail.txt");

            string documentContents;

            if (Request.InputStream != null)
                Request.InputStream.Position = 0;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            string strJsonOrder = documentContents;

            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
            OrderForm of = ser.Deserialize<OrderForm>(strJsonOrder);
            try
            {
                send_email(
                    of != null ? of.getCSV() : string.Empty, 
                    "[歌薇]" + emp.Name + "的產品追加單",
#if (DEBUG)
 //"lucien.liu@jamzoo.com.tw",
  //"niko.chen@kpss-hair.com.tw",
  "eorder@kpss-hair.com.tw",
#endif
#if (!DEBUG)
                    "eorder@kpss-hair.com.tw",
#endif

 new string[] { of.GetContentCSV() }
                    );
                //MailLog(MailLogPath, "發送成功");

                return Json(new { rs = "OK" });
            }
            catch (Exception ex)
            {
                //MailLog(MailLogPath, ex.Message);
                if (ex.InnerException != null)
                { 
                    return Json(new { rs = ex.InnerException.Message });
                }
                else
                {
                    return Json(new { rs = ex.Message });
                }
            }

            
        }

        public static void MailLog(string Path, string Message)
        {
            using (StreamWriter w = System.IO.File.AppendText("log.txt"))
            {
                Log("Test1", w);
            }
        }

        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }

        public void send_email(string msg, string mysubject, string address, string[] attachmentContent)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                MailMessage message = new MailMessage("Servcie@kpss.com.tw", address);  //MailMessage(寄信者, 收信者)
                message.IsBodyHtml = false;
                message.BodyEncoding = System.Text.Encoding.UTF8;                       //E-mail 編碼
                message.Subject = mysubject;                                            //E-mail 主旨
                message.Body = msg;                                                     //E-mail 內容
                message.Bcc.Add(new MailAddress("ajen.chen@jamzoo.com.tw", "ajen.chen"));
                message.Bcc.Add(new MailAddress("yc.hsieh@jamzoo.com.tw", "yc.hsieh"));
                message.Bcc.Add(new MailAddress("lucien.liu@jamzoo.com.tw", "lucien.liu"));

                foreach (var att in attachmentContent)
                {
                    byte[] b = Encoding.GetEncoding("Big5").GetBytes(att);
                    MemoryStream ms = new MemoryStream(b);
                    message.Attachments.Add(new Attachment(ms, mysubject + ".csv"));
                }
                SmtpClient smtpClient = new SmtpClient("127.0.0.1", 25);                //設定 E-mail Server 和 port
                smtpClient.Send(message);
            }
        }
    }
}


namespace admin.Models
{
    public class ProductPage
    {
        public List<SelectListItem> Brands { get; set; }

        public string Brand { get; set; }

        public List<SelectListItem> Serials { get; set; }
    }

    //訂購單
    public class OrderForm
    {
        public string mail { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string addr { get; set; }

        public string serial { get; set; }
        public string brand { get; set; }

        public List<Dictionary<string, object>> pros { get; set; }

        public string GetContentCSV()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var r in pros)
            {
                if (r.Keys.Contains("amt"))
                {
                    int iAmt = 0;
                    Int32.TryParse(r["amt"].ToString(), out iAmt);
                    int Price = 0;
                    Int32.TryParse(r["price"].ToString(), out Price);
                    if (iAmt > 0)
                    {
                        string strRow = string.Empty;
                        foreach (var k in r.Keys)
                        {
                            if (r[k] != null)
                            {
                                if (k == "price")
                                {
                                    int Total = iAmt * Price;
                                    strRow += Total.ToString();
                                }
                                else
                                {
                                    strRow += r[k].ToString();
                                }
                            }
                            strRow += ",";
                        }
                        sb.AppendLine(strRow);

                    }
                }
            }

            return sb.ToString();
        }

        public string getCSV()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("姓名:" + name);
            sb.AppendLine("信箱:" + mail);
            sb.AppendLine("手機:" + mobile);
            sb.AppendLine("地址:" + addr);
            sb.AppendLine("=======================");

            foreach (var r in pros)
            {
                if (r.Keys.Contains("amt"))
                {
                    int iAmt = 0;
                    Int32.TryParse(r["amt"].ToString(), out iAmt);
                    int Price = 0;
                    Int32.TryParse(r["price"].ToString(), out Price);
                    if (iAmt > 0)
                    {
                        string strRow = string.Empty;
                        foreach (var k in r.Keys)
                        {
                            if (r[k] != null)
                            {
                                if (k == "price")
                                {
                                    int Total = iAmt * Price;
                                    strRow += Total.ToString();
                                }
                                else
                                {
                                    strRow += r[k].ToString();
                                }
                                
                            }
                            strRow += ",";
                        }
                        sb.AppendLine(strRow);

                    }
                }
            }
            return sb.ToString();
        }
    }
}
