﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Areas.app.Controllers
{
    using Filter;
using System.Net.Mail;

    public class AccountController : Controller
    {
        AccountService AccountService;

        public AccountController()
        {
            AccountService = new AccountService();
        }


        public ActionResult Send()
        {
            string _errorMsg = string.Empty;

            List<MailAddress> target = new List<MailAddress>();

            target.Add(new MailAddress("ajen.chen@jamzoo.com.tw", "ajen chen", System.Text.Encoding.UTF8));

            try
            {
                Library.Utils.SendHelp("service@jamzoo.com.tw", "ajen", "teste mail", target);
            }
            catch (Exception Ex)
            {
                if (Ex.InnerException != null)
                {
                    _errorMsg = Ex.InnerException.Message;
                }
                else
                {
                    _errorMsg = Ex.Message;
                }
            }

            return Json(new
            {
                RS = string.IsNullOrEmpty(_errorMsg) ? "OK" : "ERROR",
                errors = _errorMsg
            }, JsonRequestBehavior.AllowGet); 
        }
 

        public ActionResult LoginImage(decimal LastVersion)
        {
            return Json(new {
                    lastVersion = 1.0,
                    succesd = 1,
                    msg = "成功",
                    imageURL = "www.123.12/qwnxhues"
		        });
        }

        // POST: /app/account/signin
        // account:
        // passwd:
        //[HttpPost]
        public ActionResult SignIn(
            string account, string passwd,
            string mac, string deviceid
            )
        {
            List<string> errorMsg;
            AccountModel emp;
            string Token = string.Empty;

            if (AccountService.Validate(account, passwd,
                mac, deviceid,
                out errorMsg, out emp))
            {
                if (TokenService.Add(emp.SN, out Token, out errorMsg))
                {
                    return Json(new
                    {
                        succeed = 1,
                        msg = "成功",
                        token = Token,
                        name = emp.Name,
                        userID = emp.SN,
                        groupID = emp.Dep,
                    },JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new 
            {
                succeed = 0,
                msg = errorMsg.FirstOrDefault() ?? string.Empty 
            }, JsonRequestBehavior.AllowGet);
        }

        //GET: /app/account/info
        [ApiAttribute]
        public ActionResult Info(AccountModel Emp)
        {
            return Json(new 
            { 
                RS = "OK",
                data = Emp
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
