﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Filter;
using admin.Models;
using admin.Extensions; 

namespace admin.Models
{
    public class CustomerCriteria
    {
        //電話
        public string Phone { get; set; }

        //客戶名稱
        public string Name { get; set; }

        //統編
        public string Number { get; set; }

        //地區編號
        public string Area { get; set; }
    }
}

namespace admin.Areas.app.Controllers
{
    using Service;
    using Models;

    public class CustomerController : BaseAPIController
    {
        CustomerService CusService;

        public CustomerController()
        {
            CusService = new CustomerService();
        }

        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult Training(AccountModel Emp, string EmpId, string c, int page = 1, int pageSize = 50)
        {
            CustomerTrainingService TrainingService = new CustomerTrainingService();
            CustomerTrainingListModel Model = null;
            try
            {
                if (Emp.IsInRoles(KpssRole.區業務主管))
                {
                    Model = TrainingService.GetListFor主管(Emp.SN, EmpId, c, new CustomerTrainingListModel() { Page = page, PageSize = pageSize });
                }
                else
                {
                    //非主管
                    Model = TrainingService.GetList(Emp.SN, c, new CustomerTrainingListModel() { Page = page, PageSize = pageSize });
                }

                return Json(
                    new { 
                        succeed = 1, 
                        data = Model.Data.Select(p => new {
                            Id = p.Id,
                            Number = p.Number,
                            Date = p.Date.Date.ToString("yyyy/MM/dd"),
                            Subject = p.Subject,
                            Teacher = p.Teacher,
                            Summary = p.Summary,
                            StdCount = p.StdCount,
                            Suggestion = p.Suggestion,
                            React = p.React,
                            IsSales = p.IsSales,
                            CreateDate = p.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                        })
                    }, 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new { succeed = 0, msg = ex.Message}, 
                    JsonRequestBehavior.AllowGet);
            }

        }
        

        //回傳 Sqlite
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult List(AccountModel Emp)
        {
            try
            {
                List<CustomerModel> Data = new List<CustomerModel>();

                if (Emp.IsInRoles(KpssRole.區業務主管))
                {
                    Data = CusService.GetAll();
                }
                else//業務
                {
                    Data = CusService.GetAllByEmp(Emp.SN);
                }

                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    succeed = 0,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
          
        }

        //查詢
        // GET: /app/customer/query
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult Query(AccountModel Emp, CustomerCriteria Criteria)
        {
            try
            {
                var Query = CusService.Query(
                    Emp.SN,
                    Criteria.Phone,
                    Criteria.Name,
                    Criteria.Number, Criteria.Area);

                return Json(new
                {
                    succeed = 1,
                    msg = "成功",
                    customers = Query.ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    succeed = 0,
                    msg = ex.Message
                });
            }
        }


        //新增業務資料
        // POST : /app/customer/create
        [HttpPost]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult Create(AccountModel Emp, CustomerModel Customer)
        {
            Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
            if (ModelState.IsValid)
            {
                //看誰登入, 就直接塞給他
                Customer.SalesOffice = Emp.SalesOffice;
                Customer.SalesCode = Emp.SalesCode;

                if (CusService.Create(Emp.SN, Customer, out ErrorMessage))
                {
                    return Json(new
                    {
                        succeed = 1,
                        msg = "成功"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    succeed = 0,
                    msg = (ErrorMessage.Keys.Count() > 0) ? ErrorMessage.FirstOrDefault().Value : "N/A"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    succeed = 0,
                    msg = ModelState.GetErrorsString(",")
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //修改客戶資料
        [HttpPost]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult Update(AccountModel Emp, CustomerModel Customer)
        {
            Dictionary<string, object> ErrorMessage = new Dictionary<string,object>();

            if (CusService.Update(Emp.SN, Customer, out ErrorMessage))
            {
                return Json(new {
                    succeed = 1,
                    msg = "成功"
                }, JsonRequestBehavior.AllowGet);
            }
            
            return Json(new {
                succeed = 0,
                msg = (ErrorMessage.Keys.Count() > 0) ?ErrorMessage.FirstOrDefault().Value:"N/A"
            }, JsonRequestBehavior.AllowGet);
        }

        //新增照片
        [HttpPost]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        public ActionResult AddPhoto(AccountModel Emp, string Number, string Title, HttpPostedFileBase Photo)
        {
            Dictionary<string, object> ErrorMessage = new Dictionary<string,object>();

            if (CusService.AddPhoto(Emp.SN, Number, Title, Photo, out ErrorMessage))
            {
                return Json(new {
                    succeed = 1,
                    msg = "成功"
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                succeed = 0,
                msg = (ErrorMessage.Keys.Count() > 0) ? ErrorMessage.FirstOrDefault().Value : "N/A"
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
