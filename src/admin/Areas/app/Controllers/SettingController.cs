﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Areas.app.Controllers
{
    using Service;
    using Models;

    public class SettingController : Controller
    {
        public AppSettingService AppSettingService;

        public SettingController()
        {
            AppSettingService = new AppSettingService();
        }

        //Get: /app/setting/LoginImage
        public ActionResult LoginImage(decimal LastVersion = 0)
        {
            string action = RouteData.Values["action"].ToString();
            return Get(action , LastVersion);
        }

        public ActionResult Get(string Key, decimal LastVersion)
        {
            try
            {
                var _Last = AppSettingService.Valid(Key, LastVersion);
                if (_Last != null)
                {
                    return Json(
                        new {
                            lastVersion = (decimal)_Last["LastVersion"],
                            imageURL = (string)_Last["Value"],
                            succesd = 1,
                            msg = "成功"
                    },JsonRequestBehavior.AllowGet); 
                }
                else
                {
                    return Json(
                        new {
                            succesd = 0,
                            msg = "無資料"
                    }, JsonRequestBehavior.AllowGet); 

                }
            }
            catch (Exception ex)
            {
                return Json(
                    new {
                        succesd = 0,
                        msg = ex.Message
                }, JsonRequestBehavior.AllowGet); 
            }
        }


        //
        // GET: /app/AppSetting/Keys
        [HttpGet]
        public ActionResult Keys()
        {
            try
            {
                var query = AppSettingService.GetAllKeys();
                return Json(
                    new
                    {
                        succesd = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(
                    new { 
                        succesd = 0,
                        msg = ex.Message 
                    }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
