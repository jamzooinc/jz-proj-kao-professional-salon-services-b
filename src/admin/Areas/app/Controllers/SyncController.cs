﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using admin.Extensions;
using admin.Filter;
using admin.Models;

namespace admin.Models
{
    /// <summary>
    /// 同步模組
    /// </summary>
    public class SyncDataModel
    {
        public int Id { get; set; }

        public string Command { get; set; }

        public string TableName { get; set; }

        public string EmpSN { get; set; }

        public string Result { get; set; }

    }

}

namespace admin.Areas.app.Controllers
{
    using NLog;
    //using log4net.Repository.Hierarchy;
    using Service;

    public class SyncController : BaseAPIController
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        SyncService Service;

        public SyncController()
        {
            Service = new SyncService(logger);
        }

        public ActionResult Test()
        {
            logger.Debug("test");
            return Content("OK");
        }

        //
        // GET: /app/sync/update
        // data = base64Encode 的 josn
        //同步行事曆
        [HttpPost]
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.總經理, KpssRole.教育講師, KpssRole.業務秘書, KpssRole.管理員)]
        public ActionResult Update(AccountModel Emp, string data)
        {
            Dictionary<string, object> dErrMsg = new Dictionary<string, object>();

            logger.Debug("收到 data : {0}", data);

            Service.Sync(Emp.SN, data, out dErrMsg);

            return Json(dErrMsg, JsonRequestBehavior.AllowGet);
        }

    }
}
