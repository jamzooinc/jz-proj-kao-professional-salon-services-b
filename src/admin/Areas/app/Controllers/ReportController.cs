﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Filter;
using admin.Models;
using admin.Service;

namespace admin.Areas.app.Controllers
{
    /// <summary>
    /// 報表模組
    /// </summary>
    public class ReportController : BaseAPIController 
    {

        protected KpssDb db;

        protected EmpService EmpService;

        public ReportController()
        {
            db = new KpssDb();

            EmpService = new EmpService();
        }

        KpssRole[] LeaderRole = new KpssRole[1] { KpssRole.區業務主管 };
        KpssRole[] MemberRole = new KpssRole[1] { KpssRole.駐區業務 };

        #region 業績統計

        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult PFSYB(AccountModel Emp, string EmpId, int Year)
        {
            var o_query = from p in db.RepPFSByYear
                          where p.Year == Year
                          select p;
            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            var t = from p in o_query
                    group p by new { p.Brand, p.Year, p.EmpSN } into g
                    select new {
                        EmpSN = g.Key.EmpSN,
                        Brand = g.Key.Brand,
                        ActualAmt = g.Sum(p => (int?)p.ActualAmt) ?? 0
                    };


            return Json(
                new
                {
                    succeed = 1,
                    data = t.ToList()
                }, JsonRequestBehavior.AllowGet);
        }


        //業績統計 - 依年度
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult PFSByYear(AccountModel Emp, string EmpId, string Brand, int Year, string format = "chart")
        {
            var o_query = from p in db.RepPFSByYear
                          where p.Brand == Brand && p.Year == Year
                          select p;
            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            o_query = o_query.OrderBy(p => p.Month);

          

            if (format.ToLower() == "json")
            {
                var query = o_query.ToList().Select(p => new
                {
                    EmpSN = p.EmpSN,
                    Brand = p.Brand,
                    Year = p.Year,
                    Month = p.Month,
                    ActualAmt = p.ActualAmt,
                    TargetAmt = p.TargetAmt,
                    ReachRate = p.ReachRate,
                    LastYearAmt = p.LastYearAmt,
                    DiffLastYear = p.DiffLastYear,
                    CreateDate = p.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                var query = o_query.ToList();
                return View(new PFSPage() {
                    Year = Year,
                    Brand = Brand,
                    ByYearData = query
                });
            }

        }

        //業績統計 - 依年月份
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult PFSByMonth(AccountModel Emp, string EmpId, string Brand, int Year, int Month, string format = "chart")
        {
            string strYearM = string.Format("{0:0000}{1:00}", Year, Month);

            var o_query = from p in db.RepPFSByMonth
                          where p.Brand == Brand && p.YearM == strYearM
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            o_query = o_query.OrderBy(p => p.Day);


            if (format.ToLower() == "json")
            {

                var query = o_query.ToList().Select(p => new
                {
                    EmpSN = p.EmpSN,
                    Brand = p.Brand,
                    YearM = p.YearM,
                    Day = p.Day,
                    DayOfWeek = p.DayOfWeek,
                    TargetAmt = p.TargetAmt,
                    ActualAmt = p.ActualAmt,
                    ReachRate = p.ReachRate,
                    LastYearAmt = p.LastYearAmt,
                    DiffLastYear = p.DiffLastYear,
                    CreateDate = p.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                }).ToList();

                return Json(
                   new
                   {
                       succeed = 1,
                       data = query
                   }, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                var query = o_query.ToList();
                return View(new PFSPage()
                {
                    Year = Year,
                    Month = Month,
                    Brand = Brand,
                    ByMonthData = query
                });

            }

        }

        #endregion

        #region 客戶拜訪統計

        //客戶拜訪統計(級數) - 依年度
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult CVRByYear(AccountModel Emp, string EmpId, int Year, string format = "chart")
        {
            var o_query = from p in db.RepCVRByYear
                          where p.Year == Year
                          select p;
            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }
            if (format.ToLower() == "json")
            {
                var query = o_query.OrderBy(p => p.Rating).Select(p =>
                    new
                    {
                        EmpSN = p.EmpSN,
                        Year = p.Year,
                        Rating = p.Rating,
                        Times = p.Times,
                        LastYearTimes = p.LastYearTimes
                    }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();

                return View(new CVRPage() {
                    Year = Year,
                    ByYearData = query
                });
            }
        }

        //客戶拜訪統計 - 依年度 + 級數
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult CVRByYearRating(AccountModel Emp, string EmpId, int Year, int Rating, string format = "chart")
        {

            var o_query = from p in db.RepCVRByYearRating
                          where p.Year == Year && p.Rating == Rating
                          select p;
            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            if (format.ToLower() == "json")
            {
                var query = o_query.OrderBy(p => p.Month).Select(p =>
                    new
                    {
                        EmpSN = p.EmpSN,
                        Year = p.Year,
                        Month = p.Month,
                        Rating = p.Rating,
                        Times = p.Times,
                        LastYearTimes = p.LastYearTimes
                    }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();
                return View(
                    new CVRPage() { 
                        Year = Year,
                        Rating = Rating,
                        ByYearRatingData = query
                    }
                    );
            }

        }

        //客戶拜訪統計 - 依年月份 + 級數
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult CVRByMonthRating(AccountModel Emp, string EmpId, int Year, int Month, int Rating, string format = "chart")
        {
            string strYearM = string.Format("{0:0000}{1:00}", Year, Month);

            var o_query = from p in db.RepCVRByMonthRating
                          where p.YearM == strYearM && p.Rating == Rating
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            if (format.ToLower() == "json")
            {
                var query = o_query.OrderBy(p => p.Day).Select(p =>
                    new
                    {
                        EmpSN = p.EmpSN,
                        YearM = p.YearM,
                        Rating = p.Rating,
                        Times = p.Times,
                        LastYearTimes = p.LastYearTimes
                    }).ToList();
                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();

                return View(new CVRPage() { 
                    ByMonthRatingData = query
                });
            }
        }

        #endregion

        #region KPI 淨度

        //GET: KPI 淨度 - 依年度
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult KPIByYear(AccountModel Emp, string EmpId, int Year, string format = "chart")
        {
            var o_query = from p in db.RepKPIByYear
                          where p.Year == Year
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            if (format.ToLower() == "json")
            {
                var query = o_query.OrderBy(p => new { p.RootItem, p.ChildItem }).ToList();

                return Json(
                    new 
                    { 
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();

                return View(new KpiPage() { 
                    Year = Year,
                    ByYearData = query
                });
            }

        }

        //GET: KPI 淨度 - 依項目
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult KPIByItem(AccountModel Emp, string EmpId, int Year, Guid ItemId, string format = "chart")
        {
            var o_query = from p in db.RepKPIByItem
                          where p.Year == Year && p.ItemId == ItemId
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

           if (format.ToLower().Equals("json"))
           {
                var query = o_query.OrderBy(p => new { p.RootItem, p.ChildItem }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
           }
           else
           {
               var query = o_query.ToList();

                return View(new KpiPage() { 
                    Year = Year,
                    KpiItemId = ItemId,
                    ByItemData = query
                });
           }

        }

        //GET: KPI 淨度 - 依月份
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult KPIByMonth(AccountModel Emp, string EmpId, int Year, int Month, Guid ItemId, string format = "chart")
        {
            string strYearM = string.Format("{0:0000}{1:00}", Year, Month);

            var o_query = from p in db.RepKPIByMonth
                          where p.YearM == strYearM && p.ItemId == ItemId
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }
            
            if (format.ToLower().Equals("json"))
            {
                 var query = o_query.OrderBy(p => p.Day).ToList();

                return Json(
                    new {
                        succeed = 1,
                        data = query
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
               var query = o_query.ToList();

                return View(new KpiPage() { 
                    Year = Year,
                    ByMonthData = query
                });
            }
           
        }

        #endregion

        #region 客戶類型統計

         //GET: 
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult CVTByYear(AccountModel Emp, string EmpId, int Type, int Year, string format = "chart")
        {
            var o_query = from p in db.RepCVTByYear
                          where p.Year == Year && p.Type == Type
                          select p;
            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            if (format.ToLower() == "json")
            {
                var query = o_query.OrderBy(p => p.Month).Select(p => new
                {
                    EmpSN = p.EmpSN,
                    Year = p.Year,
                    Month = p.Month,
                    Type = p.Type,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();
                return View(new CVTPage()
                {
                    Year = Year,
                    Type = Type,
                    ByYearData = query
                });

            }

        }

        //GET: 
        [HttpGet]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.駐區業務)]
        [IsMyEmpAttribute] //用來判斷是否為我的 EMP
        public ActionResult CVTByMonth(AccountModel Emp, string EmpId, int Type, int Year, int Month, string format = "chart")
        {
            string strYearM = string.Format("{0:0000}{1:00}", Year, Month);

            var o_query = from p in db.RepCVTByMonth
                          where p.YearM == strYearM && p.Type == Type
                          select p;

            //主管
            if (Emp.IsInRoles(LeaderRole))
            {
                o_query = o_query.Where(p => p.EmpSN == EmpId);
            }
            else if (Emp.IsInRoles(MemberRole)) //業務自已(非主管)
            {
                o_query = o_query.Where(p => p.EmpSN == Emp.SN);
            }

            if (format.ToLower().Equals("json"))
            {
                var query = o_query.OrderBy(p => p.Day).Select(p => new {
                    EmpSN = p.EmpSN,
                    YearMonth = p.YearM,
                    Day = p.Day,
                    Type = p.Type,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).ToList();

                return Json(
                    new
                    {
                        succeed = 1,
                        data = query
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var query = o_query.ToList();
                return View(new CVTPage()
                {
                    Year = Year,
                    Month = Month,
                    Type = Type,
                    ByMonthData = query
                });
            }
        }

        #endregion
    }
}
