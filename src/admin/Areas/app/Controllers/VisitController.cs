﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Filter;
using admin.Models;

namespace admin.Areas.app.Controllers
{
    using Service;

    //拜訪記錄模組
    public class VisitController : BaseAPIController
    {
        VisitService Service;

        public VisitController()
        {
            Service = new VisitService();
        }

        //public ActionResult Get

        //取得拜訪記錄
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult List(AccountModel Emp,
           string c,
           int y, int m
           )
        {
            Dictionary<string, string> ErrorMsg = new Dictionary<string, string>();

            try
            {
                VisitListModel Data = Service.GetList(Emp.SN, c, y, m);

                return Json(new
                {
                    succeed = 0,
                    msg = "成功",
                    data = Data.Data 
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorMsg.Add("Server Internal Error", ex.Message);
            }

            return Json(new
            {
                succeed = ErrorMsg.Count > 0 ? 0 : 1,
                msg = ((ErrorMsg.Count > 0) ? ErrorMsg.LastOrDefault().Value : "N/A")
            }, JsonRequestBehavior.AllowGet);
        }


        //新增拜訪記錄
        //c: customerNumber, y: year, m: month, d: day, st: starttime, et: endtime
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult Add(AccountModel Emp,
            string c,
            int y, int m, int d,
            string st, string et
            )
        {
            Dictionary<string, object> ErrorMsg = new Dictionary<string, object>();

            try
            {
                if (Service.Create(Emp.SN, c, new DateTime(y, m, d), 0, string.Empty, out ErrorMsg))
                {

                }

            }
            catch (Exception ex)
            {
                ErrorMsg.Add("Server Internal Error", ex.Message);
            }

            return Json(new
            {
                succeed = ErrorMsg.Count > 0 ? 0 : 1,
                msg = ((ErrorMsg.Count > 0) ? ErrorMsg.LastOrDefault().Value : "N/A")
            }, JsonRequestBehavior.AllowGet);
        }

        //移除拜訪記錄
        [HttpPost]
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult Remove(AccountModel Emp,
            string c,
            int y, int m, int d,
            string st, string et)
        {
            Dictionary<string, object> ErrorMsg = new Dictionary<string, object>();

            try
            {
                if (Service.Delete(Emp.SN, c, new DateTime(y, m, d), out ErrorMsg))
                {

                }
            }
            catch (Exception ex)
            {
                ErrorMsg.Add("Server Internal Error", ex.Message);
            }

            return Json(new
            {
                succeed = ErrorMsg.Count > 0 ? 0 : 1,
                msg = ((ErrorMsg.Count > 0) ? ErrorMsg.LastOrDefault().Value : "N/A")
            }, JsonRequestBehavior.AllowGet);
        }


    }
}
