﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Areas.app.Controllers
{
    using Service;
    using Filter;
    using Models;
    using Extensions;
    using System.IO;
    using System.Net.Mail;

    /// <summary>
    /// 員工登入的
    /// </summary>
    public class MeController : BaseAPIController
    {
        KpiService KpiService;

        EmpService EmpService;

        public MeController()
        {
            KpiService = new KpiService();
            EmpService = new EmpService();
        }

        // POST: /app/me/send?Token=
        // Target=:對象員工編號
        // Message=:訊息
        // { RS : "", errors : "" } RS = OK
        public ActionResult Send(AccountModel Emp, string Target, string Message)
        {
            string _errorMsg = string.Empty;

            var targetEmp = EmpService.Get(Target);

            if (targetEmp != null)
            {
                if (!string.IsNullOrEmpty(targetEmp.Email))
                {

                    List<MailAddress> target = new List<MailAddress>();

                    target.Add(new MailAddress(targetEmp.Email, targetEmp.Name, System.Text.Encoding.UTF8));

                    try
                    {
                        Library.Utils.SendHelp(Emp.Email, Emp.Name, Message, target);
                    }
                    catch (Exception Ex)
                    {
                        if (Ex.InnerException != null)
                        {
                            _errorMsg = Ex.InnerException.Message;
                        }
                        else
                        {
                            _errorMsg = Ex.Message;
                        }
                    }
                }
                else
                {
                    _errorMsg = "求救對象沒有 Email";
                }
            }
            else
            {
                _errorMsg = "業務資料不存在"; 
            } 

            return Json(new
            {
                RS = string.IsNullOrEmpty(_errorMsg)? "OK":"ERROR",
                errors = string.IsNullOrEmpty(_errorMsg) ? "發送成功":_errorMsg
            }, JsonRequestBehavior.AllowGet); 
        }


        //GET: /app/me/emp
        [KpssAuthorize(KpssRole.區業務主管)]
        public ActionResult Emp(AccountModel Emp)
        {
            List<string> errors = new List<string>();
            try
            {
                var Query = EmpService.Get業務(Emp.SN);

                return Json(new
                {
                    RS = "OK",
                    data = Query.Data.Select(p => new { SN = p.EmpSN, Name = p.Name})
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
                if (ex.InnerException != null)
                {
                    errors.Add(ex.InnerException.Message);
                }

                return Json(new
                {
                    RS = "ERROR",
                    errors = errors
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //GET: /app/me/kpi
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult Kpi(AccountModel Emp, string Brand, int Year)
        {
            List<string> errors = new List<string>();
            try
            {
                var Data = KpiService.GetKpiData(Emp.SN, Brand, Year);

                return Json(new
                {
                    RS = "OK",
                    data = Data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message);
                if (ex.InnerException != null)
                {
                    errors.Add(ex.InnerException.Message);
                }

                return Json(new
                {
                    RS = "ERROR",
                    errors = errors
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //GET: /app/me/info
        public ActionResult Info(AccountModel Emp)
        {
            return Json(new
            {
                RS = "OK",
                data = Emp
            }, JsonRequestBehavior.AllowGet);
        }

        //POST: /app/me/newpassword?token
        //[HttpPost]
        public ActionResult NewPassword(AccountModel Emp, string OldPasswd, string NewPasswd)
        {
            Dictionary<string, string> ErrorMsg = new Dictionary<string, string>();
            try
            {
                if (EmpService.UpdatePassword(Emp.SN, OldPasswd, NewPasswd, out ErrorMsg))
                {
                    return Json(new {
                        succeed = 1
                    },JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ErrorMsg.Add("Server Internal Error", ex.Message);
            }

            return Json(new
            {
                succeed = 0,
                msg = ((ErrorMsg.Count > 0)? ErrorMsg.LastOrDefault().Value:"N/A")
            }, JsonRequestBehavior.AllowGet);

        }

        #region Client Sqlite

        //GET: /app/me/checkdb
        // Version : 客戶端DB版本
        // LastUpdate : 客戶端最終更新日期
        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult CheckDb(AccountModel Emp, string Version, string LastUpdate)
        {
            int uResult = 0;
            string strMsg = string.Empty;
            string strDbUrl = string.Empty;
            string strLastVersion = string.Empty, strLastUpdate = string.Empty;
            //1.先檢查客戶端 DB 版本，不同版本的 

            //2.客戶端最終更新日期
            SqliteDbService sqliteServcie = new SqliteDbService();
            try
            {
                sqliteServcie.GenerateSqliteDb(Emp, out strLastVersion, out strLastUpdate);
                
                strMsg = "成功";
                uResult = 1;
                strDbUrl = "~/App/Me/DownloadDb?token=".ToAbsoluteUrl() + Emp.Token;
            }
            catch (Exception ex)
            {
                strMsg = ex.Message;
            }

            return Json(
                new 
                {
                    succeed = uResult,
                    msg = strMsg,
                    dbURL = strDbUrl, 
                    lastVersion = strLastVersion,
                    lastUpdate = strLastUpdate
                },
                JsonRequestBehavior.AllowGet);
        }

        [KpssAuthorize(KpssRole.駐區業務, KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult DownloadDb(AccountModel Emp)
        {
            SqliteDbService sqliteServcie = new SqliteDbService();

            try
            {
                string strSqliteFilePath = sqliteServcie.GetSqliteDbByEmp(Emp);

                //byte[] fileByte = System.IO.File.ReadAllBytes(strSqliteFilePath);

                FileStream fs = new FileStream(strSqliteFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var result = new FileStreamResult(fs, "application/x-sqlite3");
                result.FileDownloadName = "kpss.db";
                return result;
                //return FileStream(fs, "application/x-sqlite3", "kpss.db");
            }
            catch (Exception ex)
            {
                return Json(
                new
                {
                    succeed = 0,
                    msg = ex.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region 行事曆

        [HttpPost]
        [KpssAuthorize(KpssRole.區業務主管, KpssRole.業務秘書)]
        public ActionResult UpdateCalendar(AccountModel Emp)
        {

            string CalendarBody = new StreamReader(Request.InputStream).ReadToEnd();

            return Json(new {
                succeed = 1,
                context = CalendarBody
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
