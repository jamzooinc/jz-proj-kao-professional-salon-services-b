/*
Navicat SQLite Data Transfer

Source Server         : test
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-01-21 15:30:40
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."Customer"
-- ----------------------------
DROP TABLE IF EXISTS "main"."Customer";
CREATE TABLE "Customer" (
"Number"  TEXT(50) NOT NULL,
"Type"  INTEGER NOT NULL,
"SN"  TEXT(50),
"EmpSN"  TEXT(32) NOT NULL,
"Name"  TEXT(50),
"NameAbbr"  TEXT(50),
"Owner"  TEXT(50),
"Tel"  TEXT(50),
"Tel_2"  TEXT(50),
"Fax"  TEXT(50),
"Email"  TEXT(256),
"EmpCount"  INTEGER NOT NULL,
"Dep"  TEXT(50),
"FirstTransDate"  TEXT(8),
"LastTransDate"  TEXT(8),
"SalesLevel"  TEXT(2),
"StaffLevel"  TEXT(2),
"Brand"  TEXT,
"Area"  TEXT(50),
"CreateDate"  TEXT NOT NULL,
"ToType1Date"  TEXT,
"EnrollmentAddr"  TEXT(1024),
"TransportAddr"  TEXT(1024),
"Postal"  TEXT(10),
"Payment"  TEXT(2),
"BranchCount" INTEGER NOT NULL,
"CloseDate" TEXT,
"Postal_2"  TEXT(10),
"PaymentEmp" TEXT(32),
"FirstTransDate_KMS"  TEXT(10),
"CloseDate_2" TEXT,
"Rating" INTEGER NOT NULL,
"VisitCount" INTEGER NOT NULL,
"Lng" TEXT(32) NULL,
"Lat" TEXT(32) NULL,
"ProductType1" TEXT NULL,
"ProductType2" TEXT NULL,
"ProductType3" TEXT NULL,
"ProductType4" TEXT NULL,
"ProductType5" TEXT NULL,
"Fee1" TEXT NULL,
"Fee2" TEXT NULL,
"Fee3" TEXT NULL,
"Fee4" TEXT NULL,
"Fee5" TEXT NULL,
"OwnerNick" TEXT NULL,
"OwnerTitle" TEXT NULL,
"OwnerEmail" TEXT NULL,
"OwnerNum" TEXT NULL,
"OwnerTel" TEXT NULL,
"OpenningDate" TEXT NULL,
"Decorating" TEXT NULL,
"NewPoint" TEXT NULL,
"OrderForm" TEXT NULL,
"ProductSource" TEXT NULL,
"MaterialSource" TEXT NULL,
"MaterialRating" TEXT NULL,
"ExtProLine" TEXT NULL,
"Staff" TEXT NULL,
"BranchType" TEXT NULL,
"OH" TEXT NULL,
"ServiceType" TEXT NULL,
"Billing" TEXT NULL,
"S1" TEXT NULL,
"S2" TEXT NULL,
"S3" TEXT NULL,
"KMS1" TEXT NULL,
"KMS2" TEXT NULL,
"KMS3" TEXT NULL,
"OTC1" TEXT NULL,
"OTC2" TEXT NULL,
"WebSite" TEXT NULL,
PRIMARY KEY ("Number")
);

-- ----------------------------
-- Records of Customer
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."CustomerContactInfo"
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerContactInfo";
CREATE TABLE "CustomerContactInfo" (
"Id"  TEXT(50) NOT NULL,
"Number" TEXT(50) NOT NULL,
"Name"  TEXT(50),
"Nick"  TEXT(50),
"IdentityTitle"  TEXT(50),
"IdentityNum"  TEXT(50),
"Email"  TEXT(256),
"CreateDate"  TEXT NOT NULL,
"EmpSN"  TEXT(32) NOT NULL,
"Tel"  TEXT(256) NOT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Records of CustomerContactInfo
-- ----------------------------

-- ----------------------------
-- Task table for Employees
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerTask";
CREATE TABLE "CustomerTask" (
"Id" TEXT(50) NOT NULL,
"Date" TEXT NOT NULL,
"Number" TEXT(50) NULL,
"Title" TEXT(50) NOT NULL,
"Category" TEXT(50) NOT NULL,
"Type" TEXT(50) NOT NULL,
"Status" INTEGER NOT NULL,
"CloseDate" TEXT,
"DeadLine" TEXT,
"FromEmp" TEXT(32),
"ToEmp" TEXT(32),
"Context" TEXT,
"DepName" TEXT(50),
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Bulletin Table
-- ----------------------------
DROP TABLE IF EXISTS "main"."Bulletin";
CREATE TABLE "Bulletin" (
"Id" TEXT(50) NOT NULL,
"CreateEmpNo" TEXT(32) NOT NULL,
"Subject" TEXT(1024) NOT NULL,
"Context" TEXT NOT NULL,
"Status" INTEGER NOT NULL,
"CreateDate" TEXT NOT NULL,
"FromDep" TEXT,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- VisitLog Table (拜訪記錄)
-- ----------------------------
DROP TABLE IF EXISTS "main"."VisitLog";
CREATE TABLE "VisitLog" (
"Id" TEXT(50) NOT NULL,
"Date" TEXT NOT NULL,
"CustomerNumber" TEXT(50) NOT NULL,
"Times" INTEGER,
"EmpSN"	TEXT(32),
"Rating" INTEGER,
"Context" TEXT,
"FromTask" Text(50),
"ToTask" Text(50),
"StartTime" Text,
"EndTime" Text,
"Status" INTEGER NOT NULL,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- CustomerTraining Table (教育訓練)
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerTraining";
CREATE TABLE CustomerTraining (
"Id" Text(50) NOT NULL,
"Date" Text NOT NULL,
"Number" Text(50) NOT NULL,
"Subject" Text(256),
"Teacher" Text(50),
"Summary" Text,
"StdCount" INTEGER,
"Suggestion" Text,
"React" Text,
"IsSales" Text(50),
"CreateDate" TEXT,
"EmpSN"	TEXT(32),
"Status" INTEGER,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- 排程
-- ----------------------------
DROP TABLE IF EXISTS "main"."Schedule";
CREATE TABLE Schedule (
"Id" TEXT(50) NOT NULL,
"Date" TEXT NOT NULL,
"Year" INTEGER NOT NULL,
"Month" INTEGER NOT NULL,
"Day" INTEGER NOT NULL,
"Number" TEXT(50) NOT NULL,
"Name" TEXT(256),
"Type" TEXT(50),
"ToDo" TEXT,
"Reason" TEXT,
"EmpId" TEXT(32) NOT NULL,
"Status" INTEGER,
"SubTitle" TEXT(256),
"Summary" TEXT,
"DayOfWeek" INTEGER,
"StartTime" TEXT,
"EndTime" TEXT,
"Category" TEXT(256),
PRIMARY KEY ("Id")
);


-- ----------------------------
-- 週報表
-- ----------------------------
DROP TABLE IF EXISTS "main"."Week";
CREATE TABLE Week (
"Id" TEXT(50) NOT NULL,
"Year" INTEGER NOT NULL,
"Month" INTEGER NOT NULL,
"Weeks" INTEGER NOT NULL,
"EmpSN" TEXT(32) NOT NULL,
"Comment" TEXT(256),
"CreateDate" TEXT,
"StartDate" TEXT,
"EndDate" TEXT,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- 智慧排程
-- ----------------------------
DROP TABLE IF EXISTS "main"."SmartScheduling";
CREATE TABLE SmartScheduling (
"Id" TEXT NOT NULL,
"RootKpiItemId" TEXT NULL,
"ChildKpiItemId" TEXT NULL,
"RootKpiItem" TEXT NULL,
"ChildKpiItem" TEXT	NULL,
"Number" TEXT NULL,	
"CustomerName" TEXT	NULL,
"Year" INTEGER NULL,
"Month" INTEGER	NULL,
"FromDep" TEXT NULL,
"EmpSN" TEXT NULL,
"Comment" TEXT NULL,
PRIMARY KEY ("Id")
);



-- ----------------------------
-- 產品 
-- ----------------------------
DROP TABLE IF EXISTS "main"."Product";
CREATE TABLE Product (
"Id" TEXT NOT NULL,
"Brand" TEXT NULL,
"Type" TEXT NULL,
"Serial" TEXT NULL,
"SN" TEXT NULL,
"Pname" TEXT NULL,	
"Spec" TEXT	NULL,
"Summary" TEXT NULL,
"CreateDate" TEXT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- 員工
-- ----------------------------
DROP TABLE IF EXISTS "main"."Employee";
CREATE TABLE Employee (
"EmpSN" TEXT(32) NOT NULL,
"Name" TEXT,
"Name_en" TEXT,
"DepId" TEXT,
"DepName" TEXT,
"Title" TEXT,
"Permission" INTEGER,
"Tel" TEXT,
"TelExt" TEXT,
"Mobile" TEXT,
"Email" TEXT,
"BossSN"TEXT,
PRIMARY KEY ("EmpSN")
);

DROP TABLE IF EXISTS "main"."KpiItem";
CREATE TABLE KpiItem (
"Id" TEXT NOT NULL,
"RootId" TEXT NOT NULL,
"Name" TEXT(256) NOT NULL,
"Layer" INTEGER	NOT NULL,
"CreateDate" TEXT NOT NULL,
"Brand" TEXT(50) NOT NULL,
PRIMARY KEY ("Id")
);
