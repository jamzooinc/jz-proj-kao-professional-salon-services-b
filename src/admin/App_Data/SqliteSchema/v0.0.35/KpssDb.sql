/*
Navicat SQLite Data Transfer

Source Server         : test
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-01-21 15:30:40
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."Customer"
-- ----------------------------
DROP TABLE IF EXISTS "main"."Customer";
CREATE TABLE "Customer" (
"Number"  TEXT NOT NULL,
"Type"  INTEGER NOT NULL,
"SN"  TEXT,
"EmpSN"  TEXT NOT NULL,
"Name"  TEXT,
"NameAbbr"  TEXT,
"Owner"  TEXT,
"Tel"  TEXT,
"Tel_2"  TEXT,
"Fax"  TEXT,
"Email"  TEXT,
"EmpCount"  INTEGER NOT NULL,
"Dep"  TEXT,
"FirstTransDate"  TEXT,
"LastTransDate"  TEXT,
"SalesLevel"  TEXT,
"StaffLevel"  TEXT,
"Brand"  TEXT,
"Area"  TEXT,
"CreateDate"  TEXT NOT NULL,
"ToType1Date"  TEXT,
"EnrollmentAddr"  TEXT,
"TransportAddr_City"  TEXT,
"TransportAddr_Area"  TEXT,
"TransportAddr_Postal"  TEXT,
"TransportAddr"  TEXT,
"TransportAddr_en"  TEXT,
"Postal"  TEXT,
"Payment"  TEXT,
"BranchCount" INTEGER NOT NULL,
"CloseDate" TEXT,
"Postal_2"  TEXT,
"PaymentEmp" TEXT,
"FirstTransDate_KMS"  TEXT,
"CloseDate_2" TEXT,
"Rating" INTEGER NOT NULL,
"VisitCount" INTEGER NULL,
"Lng" TEXT NULL,
"Lat" TEXT NULL,
"ProductType1" TEXT NULL,
"ProductType2" TEXT NULL,
"ProductType3" TEXT NULL,
"ProductType4" TEXT NULL,
"ProductType5" TEXT NULL,
"Fee1" TEXT NULL,
"Fee2" TEXT NULL,
"Fee3" TEXT NULL,
"Fee4" TEXT NULL,
"Fee5" TEXT NULL,
"OwnerNick" TEXT NULL,
"OwnerTitle" TEXT NULL,
"OwnerEmail" TEXT NULL,
"OwnerNum" TEXT NULL,
"OwnerTel" TEXT NULL,
"OpenningDate" TEXT NULL,
"Decorating" TEXT NULL,
"NewPoint" TEXT NULL,
"OrderForm" TEXT NULL,
"ProductSource" TEXT NULL,
"MaterialSource" TEXT NULL,
"MaterialRating" TEXT NULL,
"ExtProLine" TEXT NULL,
"Staff" TEXT NULL,
"BranchType" TEXT NULL,
"OH" TEXT NULL,
"ServiceType" TEXT NULL,
"Billing" TEXT NULL,
"S1" TEXT NULL,
"S2" TEXT NULL,
"S3" TEXT NULL,
"KMS1" TEXT NULL,
"KMS2" TEXT NULL,
"KMS3" TEXT NULL,
"OTC1" TEXT NULL,
"OTC2" TEXT NULL,
"WebSite" TEXT NULL,
"SalesCode" TEXT NULL,
PRIMARY KEY ("Number")
);

-- ----------------------------
-- Records of Customer
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."CustomerContactInfo"
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerContactInfo";
CREATE TABLE "CustomerContactInfo" (
"Id"  TEXT NOT NULL,
"Number" TEXT NOT NULL,
"Name"  TEXT,
"Nick"  TEXT,
"IdentityTitle"  TEXT,
"IdentityNum"  TEXT,
"Email"  TEXT,
"CreateDate"  TEXT NOT NULL,
"EmpSN"  TEXT NOT NULL,
"Tel"  TEXT NOT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Records of CustomerContactInfo
-- ----------------------------

-- ----------------------------
-- Task table for Employees
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerTask";
CREATE TABLE "CustomerTask" (
"Id" TEXT NOT NULL,
"Date" TEXT NOT NULL,
"Number" TEXT NULL,
"Title" TEXT NOT NULL,
"Category" TEXT NOT NULL,
"Type" TEXT NOT NULL,
"Status" INTEGER NOT NULL,
"CloseDate" TEXT,
"DeadLine" TEXT,
"FromEmp" TEXT,
"ToEmp" TEXT,
"Context" TEXT,
"DepName" TEXT,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Bulletin Table
-- ----------------------------
DROP TABLE IF EXISTS "main"."Bulletin";
CREATE TABLE "Bulletin" (
"Id" TEXT NOT NULL,
"CreateEmpNo" TEXT NOT NULL,
"Subject" TEXT NOT NULL,
"Context" TEXT NOT NULL,
"Status" INTEGER NOT NULL,
"CreateDate" TEXT NOT NULL,
"FromDep" TEXT,
"Category" TEXT,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- VisitLog Table (拜訪記錄)
-- ----------------------------
DROP TABLE IF EXISTS "main"."VisitLog";
CREATE TABLE "VisitLog" (
"Id" TEXT NOT NULL,
"Date" TEXT NOT NULL,
"CustomerNumber" TEXT NOT NULL,
"Times" INTEGER,
"EmpSN"	TEXT,
"Rating" INTEGER,
"Context" TEXT,
"FromTask" TEXT,
"ToTask" TEXT,
"StartTime" Text,
"EndTime" Text,
"Status" INTEGER NOT NULL,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- CustomerTraining Table (教育訓練)
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerTraining";
CREATE TABLE CustomerTraining (
"Id" TEXT NOT NULL,
"Date" Text NOT NULL,
"Number" TEXT NOT NULL,
"Subject" TEXT,
"Teacher" TEXT,
"Summary" Text,
"StdCount" INTEGER,
"Suggestion" Text,
"React" Text,
"IsSales" TEXT,
"CreateDate" TEXT,
"EmpSN"	TEXT,
"Status" INTEGER,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- 排程
-- ----------------------------
DROP TABLE IF EXISTS "main"."Schedule";
CREATE TABLE Schedule (
"Id" TEXT NOT NULL,
"Date" TEXT NOT NULL,
"Year" INTEGER NOT NULL,
"Month" INTEGER NOT NULL,
"Day" INTEGER NOT NULL,
"Number" TEXT NOT NULL,
"Name" TEXT,
"Type" TEXT,
"ToDo" TEXT,
"Reason" TEXT,
"EmpId" TEXT NOT NULL,
--"EmpSN" TEXT NOT NULL,
"Status" INTEGER,
"SubTitle" TEXT,
"Summary" TEXT,
"DayOfWeek" INTEGER,
"StartTime" TEXT,
"EndTime" TEXT,
"Category" TEXT,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- 週報表
-- ----------------------------
DROP TABLE IF EXISTS "main"."Week";
CREATE TABLE Week (
"Id" TEXT NOT NULL,
"Year" INTEGER NOT NULL,
"Month" INTEGER NOT NULL,
"Weeks" INTEGER NOT NULL,
"EmpSN" TEXT NOT NULL,
"Comment" TEXT,
"CreateDate" TEXT,
"StartDate" TEXT,
"EndDate" TEXT,
PRIMARY KEY ("Id")
);


-- ----------------------------
-- 智慧排程
-- ----------------------------
DROP TABLE IF EXISTS "main"."SmartScheduling";
CREATE TABLE SmartScheduling (
"Id" TEXT NOT NULL,
"RootKpiItemId" TEXT NULL,
"ChildKpiItemId" TEXT NULL,
"RootKpiItem" TEXT NULL,
"ChildKpiItem" TEXT	NULL,
"Number" TEXT NULL,	
"CustomerName" TEXT	NULL,
"Year" INTEGER NULL,
"Month" INTEGER	NULL,
"FromDep" TEXT NULL,
"EmpSN" TEXT NULL,
"Comment" TEXT NULL,
PRIMARY KEY ("Id")
);



-- ----------------------------
-- 產品 
-- ----------------------------
DROP TABLE IF EXISTS "main"."Product";
CREATE TABLE Product (
"Id" TEXT NOT NULL,
"Brand" TEXT NULL,
"Type" TEXT NULL,
"Serial" TEXT NULL,
"SN" TEXT NULL,
"Pname" TEXT NULL,	
"Spec" TEXT	NULL,
"Summary" TEXT NULL,
"CreateDate" TEXT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- 員工
-- ----------------------------
DROP TABLE IF EXISTS "main"."Employee";
CREATE TABLE Employee (
"EmpSN" TEXT NOT NULL,
"Name" TEXT,
"Name_en" TEXT,
"DepId" TEXT,
"DepName" TEXT,
"Title" TEXT,
"Permission" INTEGER,
"Tel" TEXT,
"TelExt" TEXT,
"Mobile" TEXT,
"Email" TEXT,
"BossSN" TEXT,
"SalesCode" TEXT,
"Manage1" TEXT,
"Manage2" TEXT,
"Manage3" TEXT,
"PermissionTitle" TEXT,
PRIMARY KEY ("EmpSN")
);

-- ----------------------------
-- KPI 項目
-- ---------------------------
DROP TABLE IF EXISTS "main"."KpiItem";
CREATE TABLE KpiItem (
"Id" TEXT NOT NULL,
"RootId" TEXT NOT NULL,
"Name" TEXT NOT NULL,
"Layer" INTEGER	NOT NULL,
"CreateDate" TEXT NOT NULL,
"Brand" TEXT NOT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- KPI 預估
-- ---------------------------
DROP TABLE IF EXISTS "main"."Kpi";
CREATE TABLE Kpi (
"Year" TEXT NOT NULL,
"Month" TEXT NOT NULL,
"EmpSN" TEXT NOT NULL,
"KpiItemId" TEXT NOT NULL,
"EstimateAmt" INTEGER NOT NULL,
"ActualAmt" INTEGER NOT NULL,
"CustomerAmt" INTEGER NOT NULL,
"LastYearAmt" TEXT NOT NULL,
PRIMARY KEY ("Year", "Month", "EmpSN", "KpiItemId")
);

-- ----------------------------/
-- ZipCode
-- ----------------------------
DROP TABLE IF EXISTS "main"."ZipCode";
CREATE TABLE ZipCode (
"ZipCode" TEXT NOT NULL,
"City" TEXT NOT NULL,
"Area" TEXT NOT NULL,
"DepCode" TEXT NOT NULL
);


-- ----------------------------/
-- Customer Agent 
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerAgent";
CREATE TABLE CustomerAgent (
"Number" TEXT NOT NULL,
"EmpSN" TEXT NOT NULL,
"CreateDate" TEXT NOT NULL
);
