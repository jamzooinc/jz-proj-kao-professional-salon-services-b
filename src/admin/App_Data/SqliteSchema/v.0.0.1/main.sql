/*
Navicat SQLite Data Transfer

Source Server         : test
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-01-21 15:30:40
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."Customer"
-- ----------------------------
DROP TABLE IF EXISTS "main"."Customer";
CREATE TABLE "Customer" (
"Type"  INTEGER NOT NULL,
"Ｎumber"  TEXT(50),
"SN"  TEXT(50) NOT NULL,
"EmpSN"  TEXT(32) NOT NULL,
"Name"  TEXT(50),
"SalesLevel"  TEXT(2),
"StaffLevel"  TEXT(2),
"Brand"  INTEGER,
"Tel"  TEXT(50),
"Area"  TEXT(50),
"CreateDate"  TEXT NOT NULL,
"ToType1Date"  TEXT,
PRIMARY KEY ("SN")
);

-- ----------------------------
-- Records of Customer
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."CustomerContactInfo"
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerContactInfo";
CREATE TABLE "CustomerContactInfo" (
"Id"  TEXT(50),
"SN"  TEXT(50),
"Name"  TEXT(50),
"Nick"  TEXT(50),
"IdentityTitle"  TEXT(50),
"IdentityNum"  TEXT(50),
"Email"  TEXT(256),
"CreateDate"  TEXT,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Records of CustomerContactInfo
-- ----------------------------
