/*
Navicat SQLite Data Transfer

Source Server         : test
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-01-21 15:30:40
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."Customer"
-- ----------------------------
DROP TABLE IF EXISTS "main"."Customer";
CREATE TABLE "Customer" (
"Number"  TEXT(50) NOT NULL,
"Type"  INTEGER NOT NULL,
"SN"  TEXT(50),
"EmpSN"  TEXT(32) NOT NULL,
"Name"  TEXT(50),
"NameAbbr"  TEXT(50),
"Owner"  TEXT(50),
"Tel"  TEXT(50),
"Tel_2"  TEXT(50),
"Fax"  TEXT(50),
"Email"  TEXT(256),
"EmpCount"  INTEGER NOT NULL,
"Dep"  TEXT(50),
"FirstTransDate"  TEXT(8),
"LastTransDate"  TEXT(8),
"SalesLevel"  TEXT(2),
"StaffLevel"  TEXT(2),
"Brand"  INTEGER,
"Area"  TEXT(50),
"CreateDate"  TEXT NOT NULL,
"ToType1Date"  TEXT,
"EnrollmentAddr"  TEXT(1024),
"TransportAddr"  TEXT(1024),
"Postal"  TEXT(10),
"Payment"  TEXT(2),
"BranchCount" INTEGER NOT NULL,
"CloseDate" TEXT,
"Postal_2"  TEXT(10),
"PaymentEmp" TEXT(32),
"FirstTransDate_KMS"  TEXT(10),
"CloseDate_2" TEXT,
"Rating" INTEGER NOT NULL,
"VisitCount" INTEGER NOT NULL,
"Lng" TEXT(32) NULL,
"Lat" TEXT(32) NULL,
PRIMARY KEY ("Number")
);

-- ----------------------------
-- Records of Customer
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."CustomerContactInfo"
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerContactInfo";
CREATE TABLE "CustomerContactInfo" (
"Id"  TEXT(50) NOT NULL,
"Number" TEXT(50) NOT NULL,
"Name"  TEXT(50),
"Nick"  TEXT(50),
"IdentityTitle"  TEXT(50),
"IdentityNum"  TEXT(50),
"Email"  TEXT(256),
"CreateDate"  TEXT NOT NULL,
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Records of CustomerContactInfo
-- ----------------------------

-- ----------------------------
-- Task table for Employees
-- ----------------------------
DROP TABLE IF EXISTS "main"."CustomerTask";
CREATE TABLE "CustomerTask" (
"Id" TEXT(50) NOT NULL,
"Date" TEXT NOT NULL,
"Number" TEXT(50) NOT NULL,
"Title" TEXT(50) NOT NULL,
"Category" TEXT(50) NOT NULL,
"Type" TEXT(50) NOT NULL,
"Status" INTEGER NOT NULL,
"CloseDate" TEXT,
"DeadLine" TEXT,
"FromEmp" TEXT(32),
"ToEmp" TEXT(32),
"Context" TEXT,
"DepName" TEXT(50),
PRIMARY KEY ("Id")
);

-- ----------------------------
-- Bulletin Table
-- ----------------------------
DROP TABLE IF EXISTS "main"."Bulletin";
CREATE TABLE "Bulletin" (
"Id" TEXT(50) NOT NULL,
"CreateEmpNo" TEXT(32) NOT NULL,
"Subject" TEXT(1024) NOT NULL,
"Context" TEXT NOT NULL,
"Status" INTEGER NOT NULL,
"CreateDate" TEXT NOT NULL,
"FromDep" TEXT,
PRIMARY KEY ("Id")
);
