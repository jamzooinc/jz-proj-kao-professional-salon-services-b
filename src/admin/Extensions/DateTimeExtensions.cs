﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace admin.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// 取得今天是在今年的第幾週
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public static int GetWeekOfYear(this DateTime Value, CalendarWeekRule Rule)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(Value, Rule, DayOfWeek.Monday);
            return weekNum;
        }

        public static int GetWeekOfYear(this DateTime Value)
        {
            return GetWeekOfYear(Value, CalendarWeekRule.FirstDay);
        }
    }
}