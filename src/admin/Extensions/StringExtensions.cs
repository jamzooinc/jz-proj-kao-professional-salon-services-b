﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace admin.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string ToAbsoluteUrl(this string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                   url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }


        public static string[] SplitToArrary(this string value, string splitString)
        {
            return value.Split(new string[] { splitString }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string ToHash256(this string oString)
        {
            HMACSHA256 hash256 = new HMACSHA256();

            byte[] b = System.Text.Encoding.Default.GetBytes(oString);

            byte[] h = hash256.ComputeHash(b);

            string e = string.Empty;

            foreach (var r in h)
            {
                e += r.ToString("x2");
            }
            return e;
        }

        /// <summary>
        /// ConvertTo Base 64
        /// </summary>
        /// <param name="oString"></param>
        /// <returns></returns>
        public static string ToBase64(this string oString)
        {
            try
            {
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(oString));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Convert from base64
        /// </summary>
        /// <param name="oString"></param>
        /// <returns></returns>
        public static string FromBase64(this string oString)
        {
            try
            {
                return Encoding.UTF8.GetString(Convert.FromBase64String(oString));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}