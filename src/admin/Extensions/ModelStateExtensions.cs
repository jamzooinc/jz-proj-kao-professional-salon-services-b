﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace admin.Extensions
{
    public static class ModelStateExtensions
    {

        public static string GetErrorsString(this ModelStateDictionary ModelState, string SeparateWord)
        {
            string strAllErrors = string.Empty;
            foreach (ModelState modelState in ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                  
                    strAllErrors += string.Format("{0}{1}", error.ErrorMessage, SeparateWord);
                }
            }
            return strAllErrors;
        }


    }
}