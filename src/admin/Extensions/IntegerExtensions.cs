﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace admin.Extensions
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string[] GenerateArray(this int Value)
        {
            string[] ary = new string[Value];

            for (int i = 0; i < Value; i++)
            {
                ary[i] = (i + 1).ToString();
            }

            return ary;
        }
    }
}