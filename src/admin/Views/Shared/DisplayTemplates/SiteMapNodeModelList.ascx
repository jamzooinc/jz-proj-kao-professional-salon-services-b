﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl`1[[MvcSiteMapProvider.Web.Html.Models.SiteMapNodeModelList,MvcSiteMapProvider]]" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="MvcSiteMapProvider.Web.Html.Models" %>

<ul class="nav nav-list">
<% foreach (var node in Model) {
     
       var _class = string.Empty;

       if (node.IsCurrentNode)  // || node.IsInCurrentPath
           _class = "active";

       if (node.IsRootNode)
           _class = "nav-header";
%>
    <li class="<% =_class %>"><%=Html.DisplayFor(m => node)%>
    <% if (node.Children.Any()) { %>
        <%=Html.DisplayFor(m => node.Children)%>
    <% } %>
    </li>
<% } %>
</ul>