﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace admin.Module.Security
{
    using Service;
    using Models;
    using System.Web.Providers;

    /// <summary>
    /// 自訂的 Role Provider
    /// </summary>
    public class KpssRoleProvider : DefaultRoleProvider
    {
        // Emp SN
        public override string[] GetRolesForUser(string username)
        {
            AccountModel m = AccountService.GetBySN(username);

            if (m != null)
            {
                return m.GetRoles();
            }

            return null;
        }
    }
}