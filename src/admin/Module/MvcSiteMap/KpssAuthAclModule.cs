﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using admin.Service;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Extensibility;

namespace admin.Module.MvcSiteMap
{
    /// <summary>
    /// 可以參考 
    /// http://xharze.blogspot.tw/2012/04/inside-mvcsitemapprovider-part-4.html
    /// </summary>
    public class KpssAuthAclModule : IAclModule
    {
        public bool IsAccessibleToUser(IControllerTypeResolver controllerTypeResolver, DefaultSiteMapProvider provider, HttpContext context, SiteMapNode node)
        {
            // Is security trimming enabled?
            if (!provider.SecurityTrimmingEnabled)
            {
                return true;
            }

            // Is it a regular node?
            var mvcNode = node as MvcSiteMapNode;
            if (mvcNode == null)
            {
                if (provider.ParentProvider != null)
                {
                    return provider.ParentProvider.IsAccessibleToUser(context, node);
                }
            }

            // If we have roles assigned, check them against the roles defined in the sitemap
            if (node.Roles != null && node.Roles.Count > 0)
            {
                // if there is an authenticated user and the role allows anyone authenticated ("*"), show it
                if ((context.User.Identity.IsAuthenticated) && node.Roles.Contains("*"))
                {
                    return true;
                }

                // if there is no user, but the role allows unauthenticated users ("?"), show it
                if ((!context.User.Identity.IsAuthenticated) && node.Roles.Contains("?"))
                {
                    return true;
                }

                // if the user is in one of the listed roles, show it
                if (node.Roles.OfType<string>().Any(role => context.User.IsInRole(role)))
                {
                    return true;
                }

                // if we got this far, deny showing
                return false;
            }

            // Everything seems OK...
            return true;
        }
    }
}