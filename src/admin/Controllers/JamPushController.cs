﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class JamPushController : Controller
    {
        protected JamPushService Service;

        public JamPushController()
        {
            Service = new JamPushService();
        }

        //
        // GET: /JamPush/List
        public ActionResult List(string EmpId, JamPushListModel criteria)
        {
            try
            {
                JamPushListModel model = Service.GetList(User.Identity.Name, EmpId, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }

            return View(criteria);
        }

        public ActionResult Add()
        {
            JamPushModel entity = new JamPushModel();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Create(string EmpSN, JamPushModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(User.Identity.Name, model, out ErrorMessage))
                    {
                        return RedirectToAction("List");
                    }

                    ModelState.AddModelError(" ", "新增失敗");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(string id)
        {
            JamPushModel model = Service.Get(User.Identity.Name, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(JamPushModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dictionary<string, Object> dErrMsg = new Dictionary<string, object>();

                    if (Service.Update(User.Identity.Name, model, out dErrMsg))
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(User.Identity.Name, id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }
    }
}
