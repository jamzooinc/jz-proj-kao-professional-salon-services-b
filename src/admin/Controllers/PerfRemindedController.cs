﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理")]
    public class PerfRemindedController : Controller
    {
        protected PerfRemindedService Service;

        public PerfRemindedController()
        {
            Service = new PerfRemindedService();
        }

        //
        // GET: /PerfReminded/List
        public ActionResult List(string EmpId, PerfRemindedListModel criteria)
        {
            try
            {
                PerfRemindedListModel model = Service.GetList(User.Identity.Name, EmpId, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }

            return View(criteria);
        }

        public ActionResult Add()
        {
            PerfRemindedModel entity = new PerfRemindedModel();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Create(PerfRemindedModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(User.Identity.Name, model, out ErrorMessage))
                    {
                        return RedirectToAction("List");
                    }

                    ModelState.AddModelError(" ", "新增失敗" + ErrorMessage.LastOrDefault().Value);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(string id)
        {
            PerfRemindedModel model = Service.Get(User.Identity.Name, id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(PerfRemindedModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dictionary<string, Object> dErrMsg = new Dictionary<string, object>();

                    if (Service.Update(User.Identity.Name, model, out dErrMsg))
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(User.Identity.Name, id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }
    }
}
