﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using System.Net;
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class KpiDailyController : Controller
    {
         protected KpiDailyService Service;

        public KpiDailyController()
        {
            Service = new KpiDailyService();
        }

        public ActionResult Commit(KpiDailyPage Model)
        {
            KpiItemService kpiItemService = new KpiItemService();
            BrandService brandService = new BrandService();

            if (Model == null)
            {
                Model = Service.NewInstance();
            }

            Model.strDate = Model.strDate ?? DateTime.Now.Date.ToString("yyyy/MM/dd");

            Model.BrandList = brandService.GetBrands();

            if (Model.BrandList.Count > 0)
            {
                Model.Brand = Model.Brand ?? Model.BrandList.FirstOrDefault().Value;

                Model.KpiItemsRoot = kpiItemService.GetByBrand(Model.Brand);

                if (Model.KpiItemsRoot.Count > 0)
                {
                    Model.KpiRootId = Model.KpiRootId ?? Model.KpiItemsRoot.FirstOrDefault().Value;

                    Model.KpiItemsChild = kpiItemService.GetByParentId(Model.KpiRootId);

                    if (Model.KpiItemsChild.Count > 0)
                    {
                        Model.KpiChildId = Model.KpiChildId ?? Model.KpiItemsChild.FirstOrDefault().Value;
                    }
                }
            }

            Model.Employees = Service.GetDailyEmployees(User.Identity.Name, Model);
            Model.Create = new AddViewModel();

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Grid", Model);
                //return View(Model);
            }
            else
            {
                return View(Model);
            }
        }

        //每日匯入 Excel 功能。
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase f)
        {
            Dictionary<string, object> dErrMsg = new Dictionary<string, object>();

            if (f != null)
            {
                if (System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xlsx") ||
                    System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xls")
                    )
                {
                    string strFileName = System.IO.Path.GetFileName(f.FileName);
                    string strAbsFilePath = Server.MapPath("~/App_Data/Temp/" + strFileName);

                    f.SaveAs(strAbsFilePath);

                    Service.ExcelImport(User.Identity.Name, strAbsFilePath, out dErrMsg);
                }
            }
            else
            {
                if (dErrMsg.Keys.Contains("file"))
                {
                    dErrMsg["file"] = "無上傳檔案";
                }
                else
                {
                    dErrMsg.Add("file", "無上傳檔案");
                }
            }
            string strRs = dErrMsg.Keys.Count() == 0 ? "OK" : "Error";
            string strErrMsg = dErrMsg.Keys.Count() == 0 ? string.Empty : "有錯誤訊息，請查看記錄檔。";

            return Content("<script type='text/javascript'>parent.excel.onComplete({RS:'" + strRs + "',msg:'" + strErrMsg + "'});</script>");
        }

        [HttpPost]
        public ActionResult UpdateDailay(KpiDailyPage Model)
        {
            try
            {
                Service.UpdateDailyReport(User.Identity.Name, Model);
                //return Json(new { });
                return Content("更新成功");
            }
            catch (Exception ex)
            {
                return Content("更新失敗 : [" + ex.Message + "]");
            }
        }

        // 每日的業績回報
        [HttpPost]
        public ActionResult AddDailay(KpiDailyPage Model)
        {

            if (ModelState.IsValid)
            {
                if (Model.Create != null)
                {
                    //CustomerService s = new CustomerService();
                    //if (!s.IsMyCustomer(Model.Create.EmpSN, Model.Create.CustomerNumber))
                    //{
                    //    Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    //    return Content("該客戶不屬於該業務");
                    //}

                    try
                    {
                        Service.AddDailyReport(
                            User.Identity.Name,
                            Model.strDate,
                            Model.KpiChildId,
                            Model.Create.EmpSN,
                            //Model.Create.CustomerNumber,
                            Model.Create.Amt);
                        return Content("更新成功");
                    }
                    catch (Exception ex)
                    {
                        Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return Content(ex.Message);
                    }
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    return Content("無資料");
                }
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Content("資料錯誤");
            }
        }

        [HttpPost]
        public ActionResult UpdateOnDaily(
            string strDate, 
            string KpiChildId,
            string EmpSN,
            //string Number,
            int ActualAmt,
            string submitButton
            )
        {
            try
            {
                Service.UpdateOnDaily(strDate,
                    KpiChildId,
                    EmpSN,
                    //Number,
                    ActualAmt);
                return Content("更新成功");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Content(ex.Message);
            }
        }


        //
        // GET: /KpiDaily/List
        public ActionResult List(KpiDailyListModel criteria)
        {
            try
            {
                KpiDailyListModel model = Service.GetList(User.Identity.Name, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(criteria);
        }

        public ActionResult Add(KpiDailyModel entity)
        {
            //KpiDailyModel entity = new KpiDailyModel();

            KpiItemService KpiItemService = new KpiItemService();
            var KpiItemMenu = KpiItemService.GetList(User.Identity.Name, new KpiItemListModel() { SelectBrand = entity.Brand });

            if (KpiItemMenu != null)
            {
                entity.KpiItemList = KpiItemMenu.Data;
            }
            entity.Date = DateTime.Now.Date;
            
            return View(entity);
        }
        

        [HttpPost]
        public ActionResult Create(string EmpSN, KpiDailyModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(EmpSN, model, out ErrorMessage))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(KpiDailyModel criteria)
        {
            try
            {
                KpiDailyModel model = Service.Get(
                    User.Identity.Name,
                    new DateTime(criteria.Year, criteria.Month, criteria.Day),
                    criteria.EmpSN,
                    criteria.CustomerNumber,
                    criteria.KpiItemId);
                KpiItemService KpiItemService = new KpiItemService();

                if (model != null)
                {
                    var kkk = KpiItemService.Get(model.KpiItemId.ToString());
                    if (kkk != null)
                    {
                        model.KpiString = kkk.RootName + "/" + kkk.Name;
                    }
                }
                

               
                if (model != null)
                {
                    return View(model);
                }
                else
                {
                    return Content("查無資料");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Update(KpiDailyModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dictionary<string,Object> dErrMsg = new Dictionary<string,object>();

                    if (Service.Update(User.Identity.Name, model, out dErrMsg))
                    {
                        return RedirectToAction("List"); 
                    }
                    else
                    {
                        ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(KpiDailyModel model)
        {
            try
            {
                if (Service.Delete(User.Identity.Name, 
                        model.Date, 
                        model.EmpSN,
                        model.CustomerNumber,
                        model.KpiItemId
                    ))
                {
                    TempData["delete"] = true;
                }

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }
    }
}
