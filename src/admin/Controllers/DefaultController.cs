﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JamZoo.JamPush.Demo;

namespace admin.Controllers
{
    using Service;
    using Models;
    using System.Web.Security;
    using admin.Filter;
    using NLog;

    public class DefaultController : Controller
    {
        AccountService accountService;

        public DefaultController()
        {
            accountService = new AccountService();
        }

        //
        // GET: /Default/
        public ActionResult Index()
        {
            //using (KpssDb db = new KpssDb())
            //{
            //    var query = from p in db.Employees
            //                select p;
            //}
            //var error = new List<string>();
            //AccountModel user = new AccountModel();
            //if (accountService.Validate("leehom59", "test", out error, out user))
            //{
            //    FormsAuthentication.SetAuthCookie(user.SN, false);
            //    return Content("已登入 : " + user.Permission);
            //}
            //else
            //{
            //    return Content("無登入");
            //}

            return Content(string.Empty);
        }

        //[KpssAuthorizeAttribute(KpssRole.總經理)]
        public ActionResult test()
        {
            string _restCode = System.Configuration.ConfigurationManager.AppSettings["JamPushRestCode"].ToString();
            string _appKey = System.Configuration.ConfigurationManager.AppSettings["JamPushAppKey"].ToString();
            RestAPI jpAPI = new RestAPI(_restCode, _appKey, LogManager.GetLogger("DoDoBackend"));
            JamPushRestResponse res = jpAPI.ActiveMessage("長標題測試", "內文喔!內文喔內文喔內文喔內文喔內文喔");
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}
