﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using admin.Service;

namespace admin.Controllers
{
    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class CustomerAgentController : Controller
    {
        CustomerAgentService Service;

        public CustomerAgentController()
        {
            Service = new CustomerAgentService();
        }

        //
        // GET: /CustomerAgent/List
        public ActionResult List(string number)
        {
            string errorMsg = string.Empty;

            var data = Service.GetList(number, out errorMsg);

            return Json(new
            {
                RS = errorMsg.Length,
                data = data != null ? data.Data.Select(p => new {
                    empsn = p.EmpSN,
                    empname = p.EmpName
                }): null
            },
            JsonRequestBehavior.AllowGet);
        }

        //partial A
        // 建立
        [HttpPost]
        public ActionResult Create(string empsn, string number)
        {
            string errorMsg = string.Empty;
            admin.Models.CustomerAgentModel data = null;

            if (User.Identity.Name == empsn)
            {
                errorMsg = "無法指定自已為代理人";
            }

            if (string.IsNullOrEmpty(errorMsg))
            {
                data = Service.Create(empsn, number, out errorMsg);
            } 

            return Json(
                new
                {
                    RS = errorMsg.Length,
                    message = errorMsg,
                    data = data != null ? new { empsn = data.EmpSN, empname = data.EmpName } : null
                });
        }

        //刪除資料
        [HttpPost]
        public ActionResult Delete(string empsn, string number)
        {
            string errorMsg = string.Empty;

            Service.Delete(empsn, number, out errorMsg);

            return Json(
                new
                {
                    RS = errorMsg.Length,
                    message = errorMsg
                });
        }


    }
}
