﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    public class SignPage
    {
        [Required(ErrorMessage = "必須")]
        public string EmpNo { get; set; }

        [Required(ErrorMessage = "必須")]
        public string EmpPsd { get; set; }

      
    }
}
namespace admin.Controllers
{
    using System.Web.Security;
    using Models;
    using Service;

    public class AccountController : Controller
    {
        AccountService AccountService;

        public AccountController()
        {
            AccountService = new AccountService();
        }

        #region 認證模組

        // SignIn
        // GET: /Account/SignIn
        public ActionResult Index()
        {
            SignPage Model = new SignPage();
            return View(Model);
        }


        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return Redirect("~/");
        }

        // LogIn
        // POST: /Account/LogIn
        [HttpPost]
        public ActionResult LogIn(SignPage Model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                AccountModel emp = null;
                List<string> errmsg = new List<string>();

                try
                {
                    if (AccountService.Validate(Model.EmpNo, Model.EmpPsd,
                        out errmsg, out emp))
                    {
                        FormsAuthentication.SetAuthCookie(emp.SN, false);

                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(" ", errmsg.FirstOrDefault());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Index", Model);
        }

        #endregion
       
    }
}
