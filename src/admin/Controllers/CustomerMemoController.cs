﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,區業務主管,駐區業務")]
    public class CustomerMemoController : CustomerTaskBaseController
    {
        public CustomerMemoController() : base()
        {
            base.TaskType = CustomerTaskType.Memo;
        }
    }
}
