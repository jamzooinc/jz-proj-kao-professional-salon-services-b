﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class CustomerTrainingController : Controller
    {
        protected CustomerTrainingService Service;

        public CustomerTrainingController()
        {
            Service = new CustomerTrainingService();
        }

        //
        // GET: /CustomerTraining/List
        public ActionResult List(CustomerTrainingListModel criteria)
        {
            try
            {
                CustomerTrainingListModel model = Service.GetListFor行政助理(User.Identity.Name, criteria);

                if (criteria.trigger == "下載")
                {
                    var DT = JamZoo.Net.Library.NPOI.Lib.ToDataTable(model.Data);

                    var st = JamZoo.Net.Library.NPOI.Lib.RenderDataTableToExcel(DT);

                    return File(st, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "教育訓練" + DateTime.Now.ToString("(yyyy/MM/dd HH:mm:ss)") + ".xls");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(criteria);
        }

        public ActionResult Add()
        {
            CustomerTrainingModel entity = new CustomerTrainingModel();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Create(string EmpSN, CustomerTrainingModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(EmpSN, model, out ErrorMessage))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        /// <summary>
        /// 編輯
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            CustomerTrainingModel model = Service.Get(User.Identity.Name, id);
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return Content("查無資料");
            }
        }

        [HttpPost]
        public ActionResult Update(CustomerTrainingModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dictionary<string,Object> dErrMsg = new Dictionary<string,object>();

                    if (Service.Update(User.Identity.Name, model, out dErrMsg))
                    {
                        return RedirectToAction("List"); 
                    }
                    else
                    {
                        ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(User.Identity.Name, id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
                TempData["error"] = false;
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("List");
        }
    }
}
