﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Controllers
{
    using Service;
    using Models;

    [Authorize(Roles="管理員,業務秘書")]
    public class KpiItemController : Controller
    {
        protected KpiItemService Service;

        public KpiItemController()
        {
            Service = new KpiItemService();
        }

        //
        // GET: /Employee/List
        public ActionResult List(KpiItemListModel page)
        {
            try
            {
                KpiItemListModel model = Service.GetList(User.Identity.Name, page);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(page);
        }

        public ActionResult Add(string SelectBrand, Guid? RootId)
        {
            KpiItemModel entity = new KpiItemModel();
            
            if (RootId.HasValue)
            {
                entity.Id = Guid.NewGuid();
                entity.RootId = RootId.Value;
                entity.RootLayer = Service.GetRootLayer(false);
            }
            else
            {
                entity.Id = Guid.NewGuid();
            }

            entity.Brand = SelectBrand;
            entity.CreateDate = DateTime.Now;

            return View(entity);
        }

        [HttpPost]
        public ActionResult Add(KpiItemModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Create(model))
                    {
                        return RedirectToAction("List", new { SelectBrand = model.Brand });
                        //return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(Guid id)
        {
            KpiItemModel model = Service.Get(id.ToString());
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(KpiItemModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Update(model))
                    {
                        return RedirectToAction("List", new { SelectBrand = model.Brand });
                    }
                    else
                    {
                        ModelState.AddModelError("", "修改失敗");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }


        public ActionResult GetByBrand(string Brand)
        {
            var Model = Service.GetList(User.Identity.Name, new KpiItemListModel() { SelectBrand = Brand });

            if (Model != null)
            {
                return Json(Model.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetByBrandv2(string Brand)
        {
            var Data = Service.GetByBrand(Brand);

            return Json(new { Data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetByParent(string ParentId)
        {
            var Data = Service.GetByParentId(ParentId);

            return Json(new { Data }, JsonRequestBehavior.AllowGet);
        }


    }
}
