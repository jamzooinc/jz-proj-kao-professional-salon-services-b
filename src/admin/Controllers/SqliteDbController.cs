﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;

namespace admin.Controllers
{
    using admin.Service;

    [Authorize(Roles = "管理員")]
    public class SqliteDbController : Controller
    {
        protected SqliteDbService Service;

        public SqliteDbController()
        {
            Service = new SqliteDbService();
        }

        //
        // GET: /SqliteDb/List
        public ActionResult List(SqliteDbListModel page)
        {
            try
            {
                SqliteDbListModel model = Service.GetList(User.Identity.Name, page);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(page);
        }

        public ActionResult Add()
        {
            SqliteDbModel entity = Service.New();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase f)
        {

            return Content("OK");
        }

        [HttpPost]
        public ActionResult Create(SqliteDbModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Create(model))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(int Major, int Minor, int Fixed)
        {
            SqliteDbModel model = Service.Get(Major, Minor, Fixed);
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(SqliteDbModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Update(model))
                    {
                        return RedirectToAction("List"); 
                    }
                    else
                    {
                        ModelState.AddModelError("", "修改失敗");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(int Major, int Minor, int Fixed)
        {
            try
            {
                if (Service.Delete(Major, Minor, Fixed))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }

    }
}
