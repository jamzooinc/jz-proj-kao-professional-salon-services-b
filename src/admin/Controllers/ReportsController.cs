﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Controllers
{
    //Pfs,Cvr,Kpi
    [Authorize(Roles = "管理員,總經理,業務秘書,駐區業務,企劃部")]
    public class ReportsController : Controller
    {
        protected KpssDb db;

        public ReportsController()
        {
            db = new KpssDb();
        }

        //
        // GET: /Reports/Pfs
        public ActionResult Pfs(PFSPage c)
        {
            if (c.Month == 0)
            {
                var o_query = from p in db.RepPFSByYear
                              where p.Brand == c.Brand && p.Year == c.Year
                              select p;

                //如果是 EmpId 是 null
                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                o_query = o_query.OrderBy(p => p.Month);

                var query = from p in o_query
                            group p by new { p.Brand, p.Year, p.Month } into g
                            select new
                            { 
                                Year = g.Key.Year, Month = g.Key.Month, Brand = g.Key.Brand
                                , TargetAmt = g.Sum(p => p.TargetAmt)
                                , ActualAmt = g.Sum(p => p.ActualAmt)
                                , LastYearAmt = g.Sum(p => p.LastYearAmt)
                            };

                c.ByYearData = query.ToList().Select(p => new RepPFSByYear()
                {
                    Year = p.Year,
                    Month = p.Month,
                    Brand = p.Brand,
                    TargetAmt = p.TargetAmt,
                    ActualAmt = p.ActualAmt,
                    LastYearAmt = p.LastYearAmt
                }).ToList();

                return View(c);

            }
            else
            {
                string strYearM = string.Format("{0}{1:00}", c.Year, c.Month);
                var o_query = from p in db.RepPFSByMonth
                              where p.Brand == c.Brand && p.YearM == strYearM
                              select p;

                //如果是 EmpId 是 null
                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                o_query = o_query.OrderBy(p => p.Day);

                var query = from p in o_query
                            group p by new { p.Brand, p.YearM, p.Day } into g
                            select new
                            {
                                YearM = g.Key.YearM,
                                Day = g.Key.Day,
                                Brand = g.Key.Brand,
                                TargetAmt = g.Sum(p => p.TargetAmt),
                                ActualAmt = g.Sum(p => p.ActualAmt),
                                LastYearAmt = g.Sum(p => p.LastYearAmt)
                            };

                c.ByMonthData = query.ToList().Select(p => new RepPFSByMonth()
                {
                    YearM = p.YearM,
                    Day = p.Day,
                    Brand = p.Brand,
                    TargetAmt = p.TargetAmt,
                    ActualAmt = p.ActualAmt,
                    LastYearAmt = p.LastYearAmt
                }).ToList();
                //c.ByMonthData = o_query.ToList();

                return View(c);
            }
        }

        //
        // GET: /Reports/Cvr
        public ActionResult Cvr(CVRPage c)
        {
            //依年、月、級數
            if (c.Rating > 0 && c.Month > 0 && c.Year > 0)
            {
                string strYearM = string.Format("{0:0000}{1:00}", c.Year, c.Month);

                var o_query = from p in db.RepCVRByMonthRating
                              where p.YearM == strYearM && p.Rating == c.Rating
                              select p;

                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                var query = from p in o_query
                            group p by new { p.Rating, p.YearM, p.Day } into g
                            select new
                            {
                                YearM = g.Key.YearM,
                                Day = g.Key.Day,
                                Rating = g.Key.Rating,
                                Times = g.Sum(p => p.Times),
                                LastYearTimes = g.Sum(p => p.LastYearTimes)
                            };

                c.ByMonthRatingData = query.ToList().Select(p => new RepCVRByMonthRating()
                {
                    YearM = p.YearM,
                    Day = p.Day,
                    Rating = p.Rating,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).OrderBy(p => p.Rating).ToList();
            }
            //依年、級數
            else if (c.Year > 0 && c.Rating >= 0)
            {
                var o_query = from p in db.RepCVRByYearRating
                              where p.Year == c.Year && p.Rating == c.Rating
                              select p;

                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                var query = from p in o_query
                            group p by new { p.Rating, p.Year, p.Month } into g
                            select new
                            {
                                Year = g.Key.Year,
                                Month = g.Key.Month,
                                Rating = g.Key.Rating,
                                Times = g.Sum(p => p.Times),
                                LastYearTimes = g.Sum(p => p.LastYearTimes)
                            };

                c.ByYearRatingData= query.ToList().Select(p => new RepCVRByYearRating()
                {
                    Year = p.Year,
                    Month = p.Month,
                    Rating = p.Rating,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).OrderBy(p => p.Month).ToList();

                //c.ByYearRatingData = query.OrderBy(p => p.Rating).ToList();
            }
            //依年
            else
            {
                var o_query = from p in db.RepCVRByYear
                              where p.Year == c.Year
                              select p;

                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                var query = from p in o_query
                            group p by new { p.Rating, p.Year } into g
                            select new
                            {
                                Year = g.Key.Year,
                                Rating = g.Key.Rating,
                                Times = g.Sum(p => p.Times),
                                LastYearTimes = g.Sum(p => p.LastYearTimes)
                            };

                c.ByYearData = query.ToList().Select(p => new RepCVRByYear()
                {
                    Year = p.Year,
                    Rating = p.Rating,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).OrderBy(p => p.Rating).ToList();

                //var query = o_query.OrderBy(p => p.Rating).ToList();
                //c.ByYearData = query;
            }

            return View(c);
        }

        //
        // GET: /Reports/Kpi
        public ActionResult Kpi(KpiPage c)
        {
            KpiItemService kiService = new KpiItemService();

            //依月
            if (c.KpiItemId != Guid.Empty && c.Month > 0 && c.Year > 0)
            {
                string strYearM = string.Format("{0:0000}{1:00}", c.Year, c.Month);

                var o_query = from p in db.RepKPIByMonth
                              where p.EmpSN == c.EmpId && p.YearM == strYearM
                              && p.ItemId == c.KpiItemId
                              select p;

                c.ByMonthData = o_query.ToList();
            }
            //依年、級數
            else if (c.Year > 0 && c.KpiItemId != Guid.Empty)
            {
                var o_query = from p in db.RepKPIByItem
                             where 
                             //p.EmpSN == c.EmpId &&
                             p.Year == c.Year &&
                             p.ItemId == c.KpiItemId
                             select p;

                c.ByItemData = o_query.ToList();
            }
            //依年
            else
            {
                var o_query = from p in db.RepKPIByYear
                              where p.Year == c.Year
                              //&& p.EmpSN == c.EmpId
                              select p;

                
                var query = o_query.ToList();

                if (query.Count > 0)
                {
                    c.ByYearData = query;
                }
            }
            //return View(c);
            return View("Kpi.v2", c);
        }

        //
        // GET: /Reports/Cvt
        public ActionResult Cvt(CVTPage c)
        {
            if (c.Year > 0 && c.Month > 0)
            {
                string strYearM = string.Format("{0:0000}{1:00}", c.Year, c.Month);

                var o_query = from p in db.RepCVTByMonth
                              where p.YearM == strYearM
                              && p.Type == c.Type
                              select p;

                //如果是 EmpId 是 null
                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                o_query = o_query.OrderBy(p => p.Day);

                var query = from p in o_query
                            group p by new { p.Type, p.YearM, p.Day } into g
                            select new
                            {
                                Type = g.Key.Type,
                                YearM = g.Key.YearM,
                                Day = g.Key.Day,
                                Times = g.Sum(p => p.Times),
                                LastYearTimes = g.Sum(p => p.LastYearTimes)
                            };

                c.ByMonthData = query.ToList().Select(p => new RepCVTByMonth()
                {
                    YearM = p.YearM,
                    Day = p.Day,
                    Type = p.Type,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).ToList();
                
            }
            else //依年度
            {
                var o_query = from p in db.RepCVTByYear
                              where 
                              p.Year == c.Year
                              && p.Type == c.Type
                              select p;

                //如果是 EmpId 是 null
                if (!string.IsNullOrEmpty(c.EmpId))
                {
                    o_query = o_query.Where(p => p.EmpSN == c.EmpId);
                }

                o_query = o_query.OrderBy(p => p.Month);

                var query = from p in o_query
                            group p by new { p.Type, p.Year, p.Month } into g
                            select new
                            {
                                Type = g.Key.Type,
                                Year = g.Key.Year,
                                Month = g.Key.Month,
                                Times = g.Sum(p => p.Times),
                                LastYearTimes = g.Sum(p => p.LastYearTimes)
                            };

                c.ByYearData = query.ToList().Select(p => new RepCVTByYear()
                {
                    Year = p.Year,
                    Month = p.Month,
                    Type = p.Type,
                    Times = p.Times,
                    LastYearTimes = p.LastYearTimes
                }).ToList();

            }

            return View("Cvt", c);
        }

        public ActionResult Cvt1(CVTPage c)
        {
            c.Type = 1;
            return Cvt(c);
        }
        public ActionResult Cvt2(CVTPage c)
        {
            c.Type = 2;
            return Cvt(c);
        }
        public ActionResult Cvt3(CVTPage c)
        {
            c.Type = 3;
            return Cvt(c);
        }
    }
}
