﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員")]
    public class LoadingController : Controller
    {
        //
        // GET: /Loading/List
        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Update(HttpPostedFileBase Image)
        {
            if (Image != null)
            {
                try
                {
                    Image.SaveAs(Server.MapPath("~/Content/Images/login_pic.jpg"));
                }
                catch (Exception ex)
                {
                    return Content(ex.Message);
                }
            }
            return RedirectToAction("List");
        }
    }
}
