﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Controllers
{
    using admin.Models;
    using Service;

    [Authorize(Roles="管理員,總經理,業務主管,業務秘書")]
    public class KpiController : Controller
    {
        KpiService Service;

        public KpiController()
        {
            Service = new KpiService();
        }

        //
        // GET: /Kpi/List
        public ActionResult List(string EmpSn, string Brand, int Year = 0)
        {
            KpiSettingPage Model;

            if (Request.IsAjaxRequest())
            {
                Model = new KpiSettingPage();
                Model.Year = Year;
                Model.EmpSN = EmpSn;
                Model.Data = Service.GetKpiData(EmpSn, Brand, Year);

                return PartialView("_Grid", Model);
            }
            else
            {
                Model = Service.GetPage(EmpSn, Brand, Year);
                return View(Model);
            }

        }

        [HttpPost]
        public ActionResult Update(KpiSettingPage page)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Update(page))
                    {


                        return Content("更新成功");
                    }
                }
                catch (Exception ex)
                {
                    return Content("更新失敗 : [" + ex.Message + "]");
                }
            }
            
            return Content("更新失敗");
        }


    }
}
