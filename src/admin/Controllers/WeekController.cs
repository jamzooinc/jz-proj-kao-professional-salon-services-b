﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Models
{
    /// <summary>
    /// 週報表
    /// </summary>
    public class WeekPage
    {
        public DateTime Today { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public List<SelectListItem> Emps { get; set; }

        public string EmpId { get; set; }

        public List<DateOfWeek> DateOfThisWeek { get; set; }

        /// <summary>
        /// 本週重點
        /// </summary>
        public string WeekToDo { get; set; }

        public WeekPage()
        {
            DateOfThisWeek = new List<DateOfWeek>();
        }
    }

    /// <summary>
    /// 每天的資料
    /// </summary>
    public class DateOfWeek
    {
        public DateTime Date { get; set; }

        //本天的客戶
        public List<CustomerWeekModel> Customers { get; set; }

        public DateOfWeek()
        {
            Customers = new List<CustomerWeekModel>();
        }
    }

    public class CustomerWeekModel
    {
        public string VisitNote { get; set; }
        public string Type { get; set; }

        /// <summary>
        /// 客戶編號
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// 客戶名
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 業務編號
        /// </summary>
        public string EmpSN { get; set; }

        /// <summary>
        /// 業務名稱
        /// </summary>
        public string EmpName { get; set; }

        /// <summary>
        /// 拜訪日期
        /// </summary>
        public DateTime VisitDate { get; set; }

        /// <summary>
        /// 拜訪類別
        /// </summary>
        public string VisitType { get; set; }

        /// <summary>
        /// 內容
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 異常原因
        /// </summary>
        public string ReasonForError { get; set; }
    }
}


namespace admin.Controllers
{
    using Service;
    using admin.Models;
    using Extensions;
    using Library;

    [Authorize(Roles = "管理員,總經理,業務秘書,區業務主管,駐區業務")]
    public class WeekController : Controller
    {
        //
        // GET: /Week/
        public ActionResult Index(string EmpId, DateTime? Today, DateTime? Start, DateTime? End, string export)
        {
            if (!Today.HasValue)
            {
                Today = DateTime.Now;
            }

            if (!Start.HasValue || !End.HasValue)
            {
                int weekIndex = Today.Value.GetWeekOfYear();
                DateTime[] dts = Utils.GetCurrentDaysOfWeek(Today.Value.Year, weekIndex);
                if (dts.Length > 0)
                {
                    Start = dts[0];
                    End = dts[dts.Length-1];
                }
            }

            //KpssRole.業務秘書
            WeekService Service = new WeekService();
            EmpService EmpService = new EmpService();

            //1. 舊版
            //WeekPage ViewModel = Service.GetList(Today.Value, EmpId ?? User.Identity.Name);
            //2. 新版
            WeekPage ViewModel = Service.GetListByDateRange(Start.Value, End.Value, EmpId ?? User.Identity.Name);

            if (!string.IsNullOrEmpty(export))
            {
                //新建報表
                MyExcel mxls = new MyExcel("test");

                //1.本週重點
                //var tt = Service.GetExcelData(Today.Value, export.Equals("下載個人報表") ? EmpId : string.Empty);
                var tt = Service.GetExcelDataByDayRange(Start.Value, End.Value, export.Equals("下載個人報表") ? EmpId : string.Empty);
                mxls.CreateNewSheet("本週重點", tt);

                //2.排程
                ScheduleService scheduleSevice = new ScheduleService();
                mxls.CreateNewSheet("排程", 
                    //scheduleSevice.GetExcelData(Today.Value, export.Equals("下載個人報表") ? EmpId : string.Empty)
                    scheduleSevice.GetExcelDataByDateRange(Start.Value, End.Value, export.Equals("下載個人報表") ? EmpId : string.Empty)
                    );

                //3.拜訪記錄
                VisitService visitService = new VisitService();
                mxls.CreateNewSheet("拜訪記錄", 
                    //visitService.GetExcelData(Today.Value, export.Equals("下載個人報表") ? EmpId : string.Empty)
                    visitService.GetExcelDataByDateRange(Start.Value, End.Value, export.Equals("下載個人報表") ? EmpId : string.Empty)
                    );

                return File(mxls.Export(), "application/vnd.ms-excel",
                    (export.Equals("下載個人報表") ? EmpId : "全部") + "-" + Today.Value.ToString("yyyyMMdd") + ".xls");
            }
            else
            {
                //取得下拉選單
                ViewModel.Emps = EmpService.GetListForSelector(User.Identity.Name).Data.Select(p => new SelectListItem()
                {
                    Value = p.EmpSN,
                    Text = string.Format("{0}({1})", p.Name, p.EmpSN)
                }).ToList();
            }

            return View(ViewModel);
        }

    }
}
