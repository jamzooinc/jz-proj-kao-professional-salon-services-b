﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,區業務主管,駐區業務")]
    public class CustomerTaskBaseController : Controller
    {
        protected CustomerTaskService Service;

        protected CustomerTaskType TaskType;

        public CustomerTaskBaseController()
        {
            Service = new CustomerTaskService();

            TaskType = CustomerTaskType.Memo;
        }

        //
        // GET: /CustomerTask/List
        public ActionResult List(string EmpId, CustomerTaskListModel criteria)
        {
            try
            {
                CustomerTaskListModel model = Service.GetList(User.Identity.Name, EmpId, TaskType, criteria);

                return View("~/Views/CustomerTask/List.cshtml", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }

            return View("~/Views/CustomerTask/List.cshtml", criteria);
        }

        //public ActionResult Add()
        //{
        //    CustomerTaskModel entity = new CustomerTaskModel();

        //    return View(entity);
        //}

        //[HttpPost]
        //public ActionResult Import(HttpPostedFileBase f)
        //{

        //    return Content("OK");
        //}

        //[HttpPost]
        //public ActionResult Create(string EmpSN, CustomerTaskModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
        //        try
        //        {
        //            if (Service.Create(EmpSN, model, out ErrorMessage))
        //            {
        //                return RedirectToAction("List"); 
        //            }

        //            ModelState.AddModelError(" ", "新增失敗");
                    
        //        }
        //        catch (Exception ex)
        //        {
        //            ModelState.AddModelError(" ", ex.Message);
        //        }
        //    }
        //    return View("Add", model);
        //}

        //public ActionResult Edit(string id)
        //{
        //    CustomerTaskModel model = Service.Get(id);
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult Update(CustomerTaskModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            Dictionary<string,Object> dErrMsg = new Dictionary<string,object>();

        //            if (Service.Update(User.Identity.Name, model, out dErrMsg))
        //            {
        //                return RedirectToAction("List"); 
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ModelState.AddModelError(" ", ex.Message);
        //        }
        //    }
        //    return View("Edit", model);
        //}

        //public ActionResult Delete(string id)
        //{
        //    try
        //    {
        //        if (Service.Delete(User.Identity.Name, id))
        //        {
        //            TempData["delete"] = true;
        //        }
        //        return RedirectToAction("List");
        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(" ", ex.Message);
        //    }
        //    return RedirectToAction("List");
        //}
    }
}
