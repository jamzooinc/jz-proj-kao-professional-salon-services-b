﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using admin.Service;
using admin.Models;

namespace admin.Controllers
{
    [Authorize(Roles = "管理員,教育講師,業務秘書,區業務主管,駐區業務,服務專員")]
    public class ScheduleController : Controller
    {
        ScheduleService SchService;

        public ScheduleController()
        {
            SchService = new ScheduleService();
        }

        //
        // GET: /Schedule/In
        public ActionResult Index(string EmpId)
        {
            //admin.Models.KpssRole.教育講師
            ScheduleListModel Model = new ScheduleListModel();

            Model.Year = DateTime.Now.Year;
            Model.Month = DateTime.Now.Month;
            if (string.IsNullOrEmpty(EmpId))
            {
                Model.EmpId = User.Identity.Name;
            }
            else
            {
                Model.EmpId = EmpId;
            }

            return View(Model);
        }

        [HttpGet]
        // GET: /Schedule/GetEvents
        public ActionResult GetEvents(ScheduleListModel Criteria)
        {
            ScheduleListModel EventData = SchService.GetList(User.Identity.Name, Criteria);

            try
            {
                return Json(
                    new
                    {
                        RS = "OK",
                        data = EventData.Data.Select(p =>
                            new { id = p.Id.ToString(), title = p.CustomerName, y = p.Year, m = p.Month, d = p.Day })
                    },
                    JsonRequestBehavior.AllowGet
                    );
            }
            catch (Exception ex)
            {
                return Json(
                     new
                     {
                         RS = "Error",
                         Msg = ex.Message
                     },
                     JsonRequestBehavior.AllowGet
                     );
            }
        }

        public ActionResult Edit(Guid Id)
        {
            ScheduleModel Model = SchService.Get(Id);

            return View(Model);
        }

        public ActionResult Add(ScheduleModel entity)
        {
            return View(entity);
        }

         [HttpPost]
         public ActionResult Create(string EmpSN, ScheduleModel model)
         {
             if (ModelState.IsValid)
             {
                 Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                 try
                 {
                     if (SchService.Create(EmpSN, model, out ErrorMessage))
                     {
                         return RedirectToAction("Index", new { EmpId = model.EmpSN });
                     }

                     foreach (var err in ErrorMessage.Keys)
                     {
                         ModelState.AddModelError(err, ErrorMessage[err].ToString());
                     }

                 }
                 catch (Exception ex)
                 {
                     ModelState.AddModelError(" ", ex.Message);
                 }
             }
             return View("Add", model);
         }

         public ActionResult Delete(Guid Id)
         {
             ScheduleModel sche = null;
             Dictionary<string,object> dErrMsg = new Dictionary<string,object>();

             if (SchService.Delete(Id, out sche, out dErrMsg))
             {
                 return RedirectToAction("Index", new { EmpId = sche.EmpSN });
             }
             else
             {
                 return View("Edit", sche);
             }
         }
            

    }
}
