﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using System.Data;
    using System.IO;
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class VisitLogController : Controller
    {
         protected VisitService Service;

        public VisitLogController()
        {
            Service = new VisitService();
        }

        public ActionResult Q(string EmpId, string Number)
        {
            var model = Service.Q(EmpId, Number, 20, 0);

            return View(model);
        }

        public ActionResult ExcelExport()
        {
            return Content("Excel Export");
        }

        //
        // GET: /VisitLog/List
        public ActionResult List(string EmpId, VisitListModel criteria,
            string excelExport
            )
        {
            try
            {
                VisitListModel model = Service.GetList(User.Identity.Name, EmpId, criteria);

                if (excelExport == "匯出拜訪記錄")
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("客戶編號", typeof(string)));
                    dt.Columns.Add(new DataColumn("客戶名稱", typeof(string)));
                    dt.Columns.Add(new DataColumn("業務編號", typeof(string)));
                    dt.Columns.Add(new DataColumn("拜訪日期", typeof(string)));
                    dt.Columns.Add(new DataColumn("拜訪類別", typeof(string)));
                    dt.Columns.Add(new DataColumn("內容", typeof(string)));
                    dt.Columns.Add(new DataColumn("狀態", typeof(string)));
                    dt.Columns.Add(new DataColumn("異常原因", typeof(string)));

                    foreach (var row in model.Data)
                    {
                        DataRow nRow = dt.NewRow();
                        nRow["客戶編號"] = row.CustomerNumber;
                        nRow["客戶名稱"] = row.CustomerNumber;
                        nRow["業務編號"] = row.EmpSN;
                        nRow["拜訪日期"] = row.Date.ToString("yyyy/MM/dd");
                        nRow["拜訪類別"] = "拜訪類別?";
                        nRow["內容"] = row.Context;
                        nRow["狀態"] = row.Status.ToString();
                        nRow["異常原因"] = "異常原因";
                        dt.Rows.Add(nRow);
                    }

                    Stream fileStream = JamZooMng.Library.NPOILibrary.RenderDataTableToExcel(dt);
                    
                    return File(
                            fileStream, 
                            "application/vnd.ms-excel", 
                            string.Format("拜訪記錄-{0}-{1}.xls", EmpId, DateTime.Now.ToString("yyyy-MM-dd")));
                    //return ExcelExport();
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(criteria);
        }

        public ActionResult Add(string SearchText)
        {
            VisitModel entity = new VisitModel();

            if (!string.IsNullOrEmpty(SearchText))
            {
                entity.CustomerNumber = SearchText;
            }

            return View(entity);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase f)
        {

            return Content("OK");
        }

        [HttpPost]
        public ActionResult Create(string EmpSN, VisitModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(EmpSN, model.CustomerNumber, model.Date, model.Rating, model.Context, out ErrorMessage))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        //public ActionResult Edit(string id)
        //{
        //    VisitModel model = Service.Get(User.Identity.Name, id);
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult Update(VisitModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            Dictionary<string,Object> dErrMsg = new Dictionary<string,object>();

        //            if (Service.Update(User.Identity.Name, model, out dErrMsg))
        //            {
        //                return RedirectToAction("List"); 
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ModelState.AddModelError(" ", ex.Message);
        //        }
        //    }
        //    return View("Edit", model);
        //}

        //public ActionResult Delete(string id)
        //{
        //    try
        //    {
        //        if (Service.Delete(User.Identity.Name, id))
        //        {
        //            TempData["delete"] = true;
        //        }
        //        return RedirectToAction("List");
        //    }
        //    catch (Exception ex)
        //    {
        //        ModelState.AddModelError(" ", ex.Message);
        //    }
        //    return RedirectToAction("List");
        //}
    }
}
