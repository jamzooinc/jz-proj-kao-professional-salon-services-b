﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;

namespace admin.Controllers
{
    using admin.Service;
    using LinqToExcel;

    [Authorize(Roles = "管理員,總經理,業務主管,業務秘書")]
    public class EmployeeController : Controller
    {
        protected EmpService Service;

        public EmployeeController()
        {
            Service = new EmpService();
        }

        [HttpGet]
        public ActionResult Selector()
        {
            var Data = Service.GetListForSelector(User.Identity.Name);

            return View(Data);
        }

        //
        // GET: /Employee/List
        public ActionResult List(EmployeeListModel page)
        {
            try
            {
                EmployeeListModel model = Service.GetList(User.Identity.Name, page);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(page);
        }

        public ActionResult Add()
        {
            EmployeeModel entity = new EmployeeModel();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase f)
        {
            Dictionary<string, object> dErrMsg = new Dictionary<string,object>();

            if (f != null)
            {
                if (System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xlsx") ||
                    System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xls")
                    )
                {
                    string strFileName = System.IO.Path.GetFileName(f.FileName);
                    string strAbsFilePath = Server.MapPath("~/App_Data/Temp/" + strFileName);

                    f.SaveAs(strAbsFilePath);

                    Service.ExcelImport(strAbsFilePath, out dErrMsg);
                }
            }
            else
            {
                dErrMsg.Add("file", "無上傳檔案");
            }

            string strRs = dErrMsg.Keys.Count() == 0 ? "OK" : "Error";
            string strErrMsg = dErrMsg.Keys.Count() == 0 ? "" : dErrMsg[dErrMsg.Keys.FirstOrDefault()].ToString();

            return Content("<script type='text/javascript'>parent.excel.onComplete({RS:'" + strRs + "',msg:'" + strErrMsg + "'});</script>");
        }

        [HttpPost]
        public ActionResult Create(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    List<string> liErrMsgs = new List<string>();

                    if (Service.Create(model, out liErrMsgs))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", liErrMsgs.LastOrDefault());
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(string id)
        {
            EmployeeModel model = Service.Get(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(EmployeeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Service.Update(model))
                    {
                        return RedirectToAction("List"); 
                    }
                    else
                    {
                        ModelState.AddModelError("", "修改失敗");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return RedirectToAction("List");
        }

        //[{"id":"Mimus polyglottos","label":"Northern Mockingbird","value":"Northern Mockingbird"}]
        //給自動完成用的 action 
        //GET: Employee/SearchByName
        public ActionResult SearchByName(string term)
        {
            //[{"id":"Mimus polyglottos","label":"Northern Mockingbird","value":"Northern Mockingbird"}]

            var Query = Service.GetList(User.Identity.Name,
                new EmployeeListModel() { StartWith = term, Page = 1, PageSize = 10 });

            if (Query.Data != null)
            {
                var o_query = Query.Data.Select(p => new AutocompleteModel()
                {
                    id = p.EmpSN,
                    label = string.Format("{0}({1})", p.Name, p.EmpSN),
                    value = p.EmpSN
                });

                return Json(o_query.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content(string.Empty);
            }
        }
    }
}
