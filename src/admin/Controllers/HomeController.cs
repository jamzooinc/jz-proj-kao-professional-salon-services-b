﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SQLite;

namespace admin.Controllers
{
    using Filter;
    using admin.Models;

    //[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Test()
        {
            if (Request.QueryString["t"] != null)
            {
                throw new Exception("測試");
            }
            return View();
        }
        //
        // GET: /Home/
        public ActionResult Index(string sql)
        {
            //KpssDb db = new KpssDb();

            //db.ExecuteStoreCommand(sql, null);

            return View();
            //return Content("OK");
        }

        public ActionResult SQLite()
        {
            string version = "v.0.0.1";
            string sqlFile = Server.MapPath("~/App_Data/SqliteSchema/" + version + "/main.sql");
            string sqlitePath = Server.MapPath("~/App_Data/kpssClient_" + version + ".db");

            if (!System.IO.File.Exists(sqlitePath))
            {
                SQLiteConnection.CreateFile(sqlitePath);
            }

            using (SQLiteConnection con = new SQLiteConnection("Data Source=" + sqlitePath + ";Version=3;UseUTF16Encoding=True;"))
            {
                string strSqlCommand = System.IO.File.ReadAllText(sqlFile, System.Text.Encoding.UTF8);
                SQLiteCommand cmd = new SQLiteCommand(strSqlCommand, con);
                cmd.CommandType = System.Data.CommandType.Text;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }

            return Content("OK");
        }

    }
}