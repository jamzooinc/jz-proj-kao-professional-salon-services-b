﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Extensions;

namespace admin.Controllers
{
    using Models;
    using Service;

    [Authorize(Roles = "管理員,總經理,區業務主管,駐區業務")]
    public class CustomerController : Controller
    {
         protected CustomerService Service;

        public CustomerController()
        {
            Service = new CustomerService();
        }

        //
        // GET: /Customer/List
        public ActionResult List(CustomerListModel criteria)
        {
            try
            {
                CustomerListModel model = Service.GetList(User.Identity.Name, criteria);

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
            }
            return View(criteria);
        }

        public ActionResult Add()
        {
            CustomerModel entity = new CustomerModel();

            return View(entity);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase f)
        {
            Dictionary<string, object> dErrMsg = new Dictionary<string, object>();

            if (f != null)
            {
                if (System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xlsx") ||
                    System.IO.Path.GetExtension(f.FileName).ToLower().Equals(".xls")
                    )
                {
                    string strFileName = System.IO.Path.GetFileName(f.FileName);
                    string strAbsFilePath = Server.MapPath("~/App_Data/Temp/" + strFileName);

                    f.SaveAs(strAbsFilePath);

                    Service.ExcelImport(strAbsFilePath, out dErrMsg);
                }
            }
            else
            {
                dErrMsg.Add("file", "無上傳檔案");
            }
            string strRs = dErrMsg.Keys.Count() == 0 ? "OK" : "Error";
            string strErrMsg = dErrMsg.Keys.Count() == 0 ? "" : dErrMsg[dErrMsg.Keys.FirstOrDefault()].ToString();

            return Content("<script type='text/javascript'>parent.excel.onComplete({RS:'" + strRs + "',msg:'" + strErrMsg + "'});</script>");
        }

        [HttpPost]
        public ActionResult Create(string EmpSN, CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> ErrorMessage = new Dictionary<string, object>();
                try
                {
                    if (Service.Create(EmpSN, model, out ErrorMessage))
                    {
                        return RedirectToAction("List"); 
                    }

                    ModelState.AddModelError(" ", "新增失敗");
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Add", model);
        }

        public ActionResult Edit(string id)
        {
            CustomerModel model = Service.Get(id);
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return Content("查無資料");
            }
        }

        [HttpPost]
        public ActionResult Update(CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Dictionary<string,Object> dErrMsg = new Dictionary<string,object>();

                    if (Service.Update(User.Identity.Name, model, out dErrMsg))
                    {
                        return RedirectToAction("List"); 
                    }
                    else
                    {
                        ModelState.AddModelError(" ", "修改失敗 : " + dErrMsg[dErrMsg.Keys.LastOrDefault()]);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(" ", ex.Message);
                }
            }
            return View("Edit", model);
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (Service.Delete(User.Identity.Name, id))
                {
                    TempData["delete"] = true;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(" ", ex.Message);
                TempData["error"] = false;
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("List");
        }

        //給自動完成用的 action 
        //GET: Employee/SearchByName
        public ActionResult SearchByName(string term)
        {
            var Query = Service.GetList(User.Identity.Name, 
                new CustomerListModel() {
                    Name = term
                });

    
            if (Query.Data != null)
            {
                var o_query = Query.Data.Select(p => new AutocompleteModel()
                {
                    id = p.Number,
                    label = string.Format("{0}({1})", p.Name, p.Number),
                    value = p.Number
                });

                return Json(o_query.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content(string.Empty);
            }
        }
    }
}
