﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using admin.Models;
using admin.Service;

namespace admin.Controllers
{
    public class SmartSchedulingController : Controller
    {
        public SmartSchedulingService Service;

        public SmartSchedulingController()
        {
            Service = new SmartSchedulingService();
        }

        //
        // GET: /SmartScheduling/
        public ActionResult Index(
           SmartSchedulinListModel m
            )
        {
            try
            {
                var Model = Service.GetList(m.Year, m.Month, m.EmpSN, m.ChildKpiItemId);

                return View(Model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(m);
            }
        }

        [HttpPost]
        public ActionResult Create(SmartSchedulinListModel m)
        {
            try
            {
                Service.Create(User.Identity.Name, m.ChildKpiItemId, m.EditViewModel.Number,
                    m.Year,
                    m.Month, 
                    m.EmpSN);

                TempData["customerCountAction"] = 1;

                return RedirectToAction("Index", m);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return RedirectToAction("Index", m);
        }


        public ActionResult Delete(int Year, int Month, string EmpSN, Guid ChildKpiItemId, Guid Id)
        {
            try
            {
                Service.Delete(Id);

                TempData["customerCountAction"] = 2;
                
            }
            catch (Exception ex)
            {

                throw new Exception("別緊張,還沒防呆");
            }

            return RedirectToAction("Index", new SmartSchedulingModel()
            {
                Year = Year,
                Month = Month,
                EmpSN = EmpSN,
                ChildKpiItemId = ChildKpiItemId
            });
        }

    }
}
