﻿function jzDatabase(dbName, version) {
    var _createTbScript = 'CREATE TABLE Product ("Id" TEXT NOT NULL,"Brand" TEXT NULL,"Type" TEXT NULL,"Serial" TEXT NULL,"SN" TEXT NULL,"Pname" TEXT NULL,"Spec" TEXT NULL,"Summary" TEXT NULL,"CreateDate" TEXT NULL, "Price" INTEGER NULL, "Amt" INTEGER NULL,PRIMARY KEY ("Id"))';

    var self = this;
    self.db = openDatabase(dbName, version, 'Cars', 1024 * 1024);

    //建立 table
    self.db.transaction(function (t) {
        t.executeSql(_createTbScript);
    }, function (e) { });

    self.dropTable = function () {
        self.db.transaction(function (tx) {
            tx.executeSql("DROP TABLE Product", [],
                function (tx) { alert('OK'); },
                function (e) { });
        });
    }

    self.add = function (p, callback) {
        self.db.transaction(function (t) {
            t.executeSql(
                "Insert into product (id,brand,type,serial,sn,pname,spec,summary,createdate, price, amt) values (?, ?, ?, ?, ? , ?, ? ,?, ?, ?, ?)",
                [p.id, p.brand, p.type, p.serial, p.sn, p.pName, p.spec, p.summary, p.createdate, p.price, p.amt()]
                );

            if (callback != null) {
                callback(p);
            }

        },
        function (e) {
            alert(e.message);
        });
    }

    self.remove = function (id, callback) {
        self.db.transaction(function (t) {
            t.executeSql(
                "Delete From Product Where Id = ?",
                [id]
                );
            if (callback != null) {
                callback(id);
            }
        },
        function (e) {
           alert(e.message);
       });
    }

    self.clearAll = function (callback) {
        self.db.transaction(function (t) {
            t.executeSql("Delete From Product", []);
            if (callback != null) {
                callback();
            }
        },
        function (ex) {
            alert(ex.message);
        });
    }

    self.list = function (callback) {
        self.db.transaction(
            function (tx) {
                tx.executeSql("Select id,brand,type,serial,sn,pname,spec,summary,createdate, price, amt From Product", [], function (tx, result) {
                    
                    if (callback != null) {
                        callback(result);
                    }
                });
            },
            function (x) { }
        );
    }
}

var jzDataContext = new jzDatabase('cars', '1.0.0');
