﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JamZoo.JamPush.RestAPI.Enums
{
    /// <summary>
    /// 發送排程，可輸入值為
    /// now, schedule, 
    /// </summary>
    public enum ScheduleType
    {
        now = 1, 
        schedule = 2
    }
}
