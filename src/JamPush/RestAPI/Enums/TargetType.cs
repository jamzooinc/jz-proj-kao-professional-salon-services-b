﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JamZoo.JamPush.RestAPI.Enums
{
    /// <summary>
    /// 發送對象，可輸入值為 
    /// </summary>
    public enum TargetType
    {
        all = 1,
        mac = 2,
        device_id = 3,
        appid = 4,
        device_type = 5
    }
}
