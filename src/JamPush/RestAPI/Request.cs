﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JamZoo.JamPush.RestAPI.Enums;

namespace JamZoo.JamPush.RestAPI
{
    /// <summary>
    /// 發 Push 的 Request
    /// </summary>
    public class Request
    {
        /// <summary>
        /// 推播標題
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 開啟推播按鈕的字樣，預設為 “OPEN”。
        /// </summary>
        public string Button { get; set; }

        /// <summary>
        /// Ctrl Url 
        /// </summary>
        public string Ctrl { get; set; }
        
        /// <summary>
        /// 自訂欄位。
        /// </summary>
        public string Custom { get; set; }

        /// <summary>
        /// 發送對像
        /// </summary>
        public TargetType TargetType { get; set; }

        /// <summary>
        /// 排程類型
        /// </summary>
        public string ScheduleType { get; set; }


        /// <summary>
        /// 取得 Post 的參數字串
        /// </summary>
        /// <returns></returns>
        public virtual byte[] GetPostParameters(string restCode, string appKey)
        {
            StringBuilder strPostString = new StringBuilder();

            strPostString.AppendFormat("rest_code={0}&", restCode);
            strPostString.AppendFormat("appkey={0}&", appKey);
            strPostString.AppendFormat("ctrl={0}&", Ctrl);
            strPostString.AppendFormat("subject={0}&", Subject);
            strPostString.AppendFormat("button={0}&", Button);
            strPostString.AppendFormat("target_type={0}&", TargetType.ToString());
            strPostString.AppendFormat("schedule_type={0}&", ScheduleType);

            return UTF8Encoding.UTF8.GetBytes(strPostString.ToString());
        }
    }

    public class ActiveAlert : Request
    {
        public ActiveAlert(string _subject)
        {
            base.Ctrl = "sendpush_alert";
            base.ScheduleType = "now";
            base.TargetType = TargetType.mac;
            base.Button = "OPEN";
            base.Subject = _subject;
        }
    }

}
