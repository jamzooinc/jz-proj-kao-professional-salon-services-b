﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RestSharp;
using Newtonsoft.Json;
using NLog;

namespace JamZoo.JamPush.Demo
{
    /// <summary>
    /// 回應的 JamPush Rest API response
    /// </summary>
    public class JamPushRestResponse
    {
        public string error_code { get; set; }
        public string error_msg { get; set; }
        public string pid { get; set; }
        public string total { get; set; }
        public string points { get; set; }
    }
    
    public class RestAPI
    {
        private string baseUrl = "https://api.jampush.com.tw/rest-api.php";
        private string rest_code;
        private string appkey;

        static Logger logger;

        private RestClient client;

        public RestAPI(string RestCode, string AppKey)
            : this(RestCode, AppKey, LogManager.GetLogger("JamPushRestAPI"))
        { }

        public RestAPI(string RestCode, string AppKey, Logger _logger)
        {
            rest_code = RestCode;
            appkey = AppKey;
            client = new RestClient(baseUrl);
            logger = _logger;
        }

        /// <summary>
        /// 發送推播
        /// </summary>
        /// <param name="subject">您的停車累積開單金額目前已?您設定的上限金額XXX元，謝謝。</param>
        /// <param name="mac_address">iOS 的 Mac Address</param>
        /// <returns>回傳 JamPushRestResponse</returns>
        public JamPushRestResponse ActiveAlert(
            string subject
            )
        {
            string ctrl = "sendpush_alert";
            string schedule_type = "now";
            string target_type = "all";
            string button = "OPEN";

            RestRequest active = new RestRequest();
            active.Resource = "";
            active.Method = Method.POST;
            active.AddParameter("ctrl", ctrl, ParameterType.GetOrPost);
            active.AddParameter("rest_code", rest_code, ParameterType.GetOrPost);
            active.AddParameter("appkey", appkey, ParameterType.GetOrPost);
            active.AddParameter("schedule_type", schedule_type, ParameterType.GetOrPost);
            active.AddParameter("target_type", target_type, ParameterType.GetOrPost);
            active.AddParameter("subject", subject, ParameterType.GetOrPost);
            active.AddParameter("button", button, ParameterType.GetOrPost);

            //string strMacAddresses = Newtonsoft.Json.JsonConvert.SerializeObject(mac_address);
            //active.AddParameter("mac_address", strMacAddresses, ParameterType.GetOrPost);

            JamPushRestResponse model;

            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                RestResponse response = (RestResponse)client.Execute(active);
                model = JsonConvert.DeserializeObject<JamPushRestResponse>(response.Content);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    logger.Warn(response.ErrorMessage);
                    logger.Warn(response.StatusCode.ToString());
                    logger.Warn(response.Content);
                }
            }
            catch (Exception ex)
            {
                model = new JamPushRestResponse();
                model.error_code = "001";
                model.error_msg = ex.Message;
            }

            return model;
        }

        public JamPushRestResponse ActiveMessage
           (
           string subject,
           string content
           )
        {
            return ActiveMessage(subject, content, null);
        }

        public JamPushRestResponse ActiveMessage
            (
            string subject,
            string content,
            string[] mac_address
            )
        {
            string ctrl = "sendpush_message";
            string schedule_type = "now";
            string target_type = "all";
            string button = "OPEN";

            RestRequest active = new RestRequest();
            active.Resource = "";
            active.Method = Method.POST;
            active.AddParameter("ctrl", ctrl, ParameterType.GetOrPost);
            active.AddParameter("rest_code", rest_code, ParameterType.GetOrPost);
            active.AddParameter("appkey", appkey, ParameterType.GetOrPost);
            active.AddParameter("schedule_type", schedule_type, ParameterType.GetOrPost);
            active.AddParameter("target_type", target_type, ParameterType.GetOrPost);
            active.AddParameter("subject", subject, ParameterType.GetOrPost);
            active.AddParameter("content", content, ParameterType.GetOrPost);
            active.AddParameter("button", button, ParameterType.GetOrPost);

            if (mac_address != null)
            {
                string strMacAddresses = Newtonsoft.Json.JsonConvert.SerializeObject(mac_address);
                active.AddParameter("mac_address", strMacAddresses, ParameterType.GetOrPost);
            }

            JamPushRestResponse model;

            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                RestResponse response = (RestResponse)client.Execute(active);
                model = JsonConvert.DeserializeObject<JamPushRestResponse>(response.Content);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    logger.Warn(response.ErrorMessage);
                    logger.Warn(response.StatusCode.ToString());
                    logger.Warn(response.Content);
                }
            }
            catch (Exception ex)
            {
                model = new JamPushRestResponse();
                model.error_code = "001";
                model.error_msg = ex.Message;
            }

            return model;
        }
    }
}