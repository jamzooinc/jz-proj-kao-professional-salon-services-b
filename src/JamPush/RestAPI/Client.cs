﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace JamZoo.JamPush.RestAPI
{
    /// <summary>
    /// 發送 Push 的 Cient
    /// </summary>
    public class Client
    {
        /// <summary>
        /// Jam Push Rest API - Base Uri
        /// </summary>
        protected Uri JAMPUSH_BASE_URI = new Uri("https://api.jampush.com.tw/rest-api.php");

        /// <summary>
        /// REST-API 認證碼，在後端註冊時於管理介面取得。
        /// </summary>
        protected string RestCode { get; set; }

        /// <summary>
        /// APP-Key為開發者自行於 SDK 中設定的參數。
        /// </summary>
        protected string AppKey { get; set; }

        public Client(string _restCode, string _appKey)
        {
            this.RestCode = _restCode;
            this.AppKey = _appKey;
        }

        /// <summary>
        /// 送 Push
        /// </summary>
        /// <param name="Req"></param>
        /// <returns></returns>
        public bool Push(Request Req)
        {
            // *** Init the JamPush Rest api request
            HttpWebRequest _request =
                (HttpWebRequest)WebRequest.Create(JAMPUSH_BASE_URI);
            _request.Method = "POST";

            //把 request 的 post parameter 寫到 request stream 裏面去
            byte[] byteData = Req.GetPostParameters(RestCode, AppKey);
            _request.ContentLength = byteData.Length;
            using (var dataStream = _request.GetRequestStream())
            {
                dataStream.Write(byteData, 0, byteData.Length);
            }
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            // *** Retrieve request info headers
            HttpWebResponse loWebResponse = 
                (HttpWebResponse)_request.GetResponse();

            if (loWebResponse.StatusCode == HttpStatusCode.OK)
            {

                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
