﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using admin.Models;
using NLog;

namespace SDVisitLog
{
    public class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public KpssDb db;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        public void Run(string EmpSN, DateTime s, DateTime e)
        {
            var cus = (from p in db.Customer
                       where p.EmpSN == EmpSN
                       select new { p.Number, p.Rating }).ToList();

            TimeSpan ts = e.Subtract(s);
            for (int i = 0; i < ts.Days; i++)
            {
                DateTime cDate = s.AddDays(i);
                logger.Debug("{0}", cDate.ToString("yyyy/MM/dd"));
                logger.Debug("=================================");

                //get 1 ~ 4
                int times = new Random().Next(1, 5);

                for (int t = 1; t <= times; t++)
                {
                    var d = cus.OrderBy(p => Guid.NewGuid()).FirstOrDefault();
                    VisitLog vl = new VisitLog();
                    vl.Id = Guid.NewGuid();
                    vl.Date = cDate.Date;
                    vl.CustomerNumber = d.Number;
                    vl.Times = t;
                    vl.EmpSN = EmpSN;
                    vl.Rating = vl.Rating;
                    vl.Context = "for testing";
                    vl.FromTask = EmpSN;
                    vl.ToTask = EmpSN;
                    vl.StartTime = cDate.AddHours(8 + ((t - 1) * 2));
                    vl.EndTime = cDate.AddHours(8 + (t * 2)).AddSeconds(-1);
                    vl.Status = 1;
                    db.VisitLog.AddObject(vl);
                }

                db.SaveChanges();
                logger.Debug("產生拜訪記錄 : {0} 筆", times);
            }
        }



        static void Main(string[] args)
        {
            DateTime _s = new DateTime(2013, 3, 1);
            DateTime _e = new DateTime(2013, 5, 20);

            string _EMPSN = "0023";

            Mainconsole process = new Mainconsole();

            try
            {
                logger.Debug("開始-產生每日 KPI 回報的測試資料");
                process.Run(_EMPSN, _s, _e);
                logger.Debug("開始-產生每日 KPI 回報的測試資料");


            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}
