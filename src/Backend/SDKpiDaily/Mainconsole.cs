﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using admin.Models;
using NLog;

namespace SDKpiDaily
{
    public class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public KpssDb db;

        public Mainconsole()
        {
            db = new KpssDb();
        }


        public void Run(string EmpSN, DateTime s, DateTime e)
        {
            TimeSpan ts = e.Subtract(s);
            for (int i = 0; i < ts.Days; i++)
            {
                DateTime cDate = s.AddDays(i);
                logger.Debug("{0}", cDate.ToString("yyyy/MM/dd"));
                logger.Debug("----------------------------------");
                foreach (Guid kpiitem in GetKeyItemId())
                {
                    KpiDaily kd = new KpiDaily();
                    kd.Date = cDate.Date;
                    kd.Year = cDate.Year;
                    kd.Month = cDate.Month;
                    kd.Day = cDate.Day;
                    kd.EmpSN = EmpSN;
                    kd.KpiItemId = kpiitem;
                    kd.ActualAmt = GetActualAmt();
                    kd.CreateDate = DateTime.Now;
                    kd.CreateEmpSN = "by computer";

                    db.KpiDaily.AddObject(kd);
                    logger.Debug("{0} -- {1}", kpiitem.ToString(), kd.ActualAmt);
                }

                db.SaveChanges();
            }
        }

        public int GetActualAmt()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 1000);

            return randomNumber;
        }

        public List<Guid> GetKeyItemId()
        {
            var query = (from p in db.KpiItem
                         where p.RootId != Guid.Empty
                         select p.Id).ToList();

            return query;
        }

        static void Main(string[] args)
        {
            DateTime _s = new DateTime(2013, 3, 1);
            DateTime _e = new DateTime(2013, 5, 20);

            string _EMPSN = "0023";

            Mainconsole process = new Mainconsole();

            try
            {
                logger.Debug("開始-產生每日 KPI 回報的測試資料");
                process.Run(_EMPSN, _s, _e);
                logger.Debug("開始-產生每日 KPI 回報的測試資料");


            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}
