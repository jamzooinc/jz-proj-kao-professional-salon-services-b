﻿/*
 * Usage Limits
 * Use of the Google Geocoding API is subject to a query limit of 2,500 geolocation requests per day. 
 * (User of Google Maps API for Business may perform up to 100,000 requests per day.) 
 * This limit is enforced to prevent abuse and/or repurposing of the Geocoding API, and this limit may be changed in the future without notice.
 * Additionally, we enforce a request rate limit to prevent abuse of the service. If you exceed the 24-hour limit or otherwise abuse the service, the Geocoding API may stop working for you temporarily. If you continue to exceed this limit, your access to the Geocoding API may be blocked.
 * Note: the Geocoding API may only be used in conjunction with a Google map; geocoding results without displaying them on a map is prohibited. For complete details on allowed usage, consult the Maps API Terms of Service License Restrictions.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using admin.Models;


namespace Kpss.Backend.RepCVR
{
    /// <summary>
    /// 客戶拜訪統計 (依年度、依年度+級數、依月份+級數)
    /// </summary>
    public class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public KpssDb db;

        public static string Filter_EmpSN = string.Empty;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        #region 依年度
        /// <summary>
        /// ByYear
        /// </summary>
        public void ByYearRun(DateTime Date)
        {
            //參數 1 : 年份
            int ThisYear = Date.Year;

            //參數 2 : 業務員
            var o_emps = (from p in this.db.Employees
                          //where p.EmpSN == "0023"
                          select p.EmpSN);

            if (!string.IsNullOrEmpty(Filter_EmpSN))
            {
                o_emps = o_emps.Where(p => p == Filter_EmpSN);
            }

            var emps = o_emps.ToList();

            var o_rating = from p in this.db.CustomerRating
                           select p.Id;

            //依每個業務來計算
            foreach (string strEmpSN in emps)
            {
                //1.先抓該業務出來算
                var o_query = from c in this.db.Customer.Where(p => p.EmpSN == strEmpSN)
                              join r in this.db.CustomerRating on c.Rating equals r.Id
                              join l in this.db.VisitLog on c.Number equals l.CustomerNumber
                              where c.EmpSN == strEmpSN
                              group c by new { c.EmpSN, c.Rating, l.Date.Year } into g
                              select new { g.Key.EmpSN, g.Key.Rating, g.Key.Year, count = g.Count() };

                //每個級別都要算
                foreach (int iRating in o_rating)
                {
                    //今年的資料
                    var thisYearData = o_query.Where(p => p.EmpSN == strEmpSN && p.Year == ThisYear && p.Rating == iRating).FirstOrDefault();
                    //去年的資料
                    var lastYearData = o_query.Where(p => p.EmpSN == strEmpSN && p.Year == (ThisYear - 1) && p.Rating == iRating).FirstOrDefault();

                    try
                    {
                        AddOrUpdate(strEmpSN, ThisYear, iRating,
                            thisYearData != null ? thisYearData.count : 0,
                            lastYearData != null ? lastYearData.count : 0);

                        logger.Debug("新增一筆 {0},{1},{2}", strEmpSN, ThisYear, iRating);
                    }
                    catch (Exception ex)
                    {
                        logger.Fatal("依年度計算次數錯誤 @ AddOrUpdate({0},{1},{2},{3},{4})   ({5})",
                            strEmpSN, ThisYear, iRating,
                            thisYearData != null ? thisYearData.count : 0,
                            lastYearData != null ? lastYearData.count : 0,
                            ex.Message);
                    }


                }
                this.db.SaveChanges();
            }


        }

        public void AddOrUpdate(string EmpSN, int ThisYear, int Rating, int ThisCount, int LastCount)
        {
            RepCVRByYear data = this.db.RepCVRByYear.Where(p => p.EmpSN == EmpSN && p.Year == ThisYear && p.Rating == Rating).FirstOrDefault();

            if (data == null)
            {
                data = new RepCVRByYear();
                data.EmpSN = EmpSN;
                data.Year = ThisYear;
                data.Rating = Rating;
                data.Times = ThisCount;
                data.LastYearTimes = LastCount;
                data.CreateDate = DateTime.Now;
                this.db.RepCVRByYear.AddObject(data);
            }
            else
            {
                data.Times = ThisCount;
                data.LastYearTimes = LastCount;
            }

        }

        #endregion

        #region 依年度 + 級數

        public void ByYearRatingRun(DateTime Date)
        {
            //參數 1 : 年份
            int ThisYear = Date.Year;

            //參數 2 : 級數
            List<int> Rating = (from p in db.CustomerRating
                                select p.Id).ToList();

            //參數 2 : 業務員
            var o_emps = from p in this.db.Employees
                         select p.EmpSN;

            if (!string.IsNullOrEmpty(Filter_EmpSN))
            {
                o_emps = o_emps.Where(p => p == Filter_EmpSN);
            }

            var emps = o_emps.ToList();

            foreach (string strEmpSN in emps)
            {
                //1.先抓該業務的出來算
                var o_query = from c in this.db.Customer.Where(p => p.EmpSN == strEmpSN)
                              join r in this.db.CustomerRating on c.Rating equals r.Id
                              join l in this.db.VisitLog on c.Number equals l.CustomerNumber
                              group c by new { c.EmpSN, c.Rating, l.Date.Year, l.Date.Month } into g
                              select new { g.Key.EmpSN, g.Key.Rating, g.Key.Year, g.Key.Month, count = g.Count() };

                //每個等級
                foreach (int iRating in Rating)
                {
                    for (int m = 1; m <= 12; m++)
                    {
                        var thisYearData = o_query.Where(p => p.Year == ThisYear && p.Rating == iRating && p.Month == m).FirstOrDefault();
                        var lastYearData = o_query.Where(p => p.Year == (ThisYear - 1) && p.Rating == iRating && p.Month == m).FirstOrDefault();

                        try
                        {
                            AddOrUpdateByYearRating(strEmpSN, ThisYear, m, iRating,
                                thisYearData != null ? thisYearData.count : 0,
                                lastYearData != null ? lastYearData.count : 0);

                            logger.Debug("新增一筆 業務:{0}, 年/月:{1}/{2}, 級別:{3}", strEmpSN, ThisYear, m, iRating);
                        }
                        catch (Exception ex)
                        {
                            logger.Fatal("ByYearRatingRun({0},{1},{2},{3},{4},{5})   ({6})",
                            strEmpSN, ThisYear, m, iRating,
                            thisYearData != null ? thisYearData.count : 0,
                            lastYearData != null ? lastYearData.count : 0,
                            ex.Message);
                        }
                    }
                }
                try
                {
                    this.db.SaveChanges();
                    logger.Debug("更新資料成功 業務:{0}", strEmpSN);
                }
                catch (Exception ex)
                {
                    logger.Fatal("更新資料失敗({0})", ex.Message);
                }
            }
        }

        public void AddOrUpdateByYearRating(string EmpSN, int ThisYear, int Month, int Rating, int ThisCount, int LastCount)
        {

            RepCVRByYearRating data = this.db.RepCVRByYearRating.Where(p => p.EmpSN == EmpSN && p.Year == ThisYear && p.Month == Month && p.Rating == Rating).FirstOrDefault();

            if (data == null)
            {
                data = new RepCVRByYearRating();
                data.EmpSN = EmpSN;
                data.Year = ThisYear;
                data.Month = Month;
                data.Rating = Rating;
                data.Times = ThisCount;
                data.LastYearTimes = LastCount;
                data.CreateDate = DateTime.Now;
                this.db.RepCVRByYearRating.AddObject(data);
            }
            else
            {
                data.Times = ThisCount;
                data.LastYearTimes = LastCount;
            }
        }


        #endregion

        #region 依年度 + 月份 + 級數

        public void ByYearMRatingRun(DateTime Date)
        {
            //參數 0 : 年份
            int ThisYear = Date.Year;

            //參數 1 : 級數
            List<int> Rating = (from p in db.CustomerRating
                                select p.Id).ToList();

            //參數 2 : 業務員
            var o_emps = from p in this.db.Employees
                         select p.EmpSN;

            if (!string.IsNullOrEmpty(Filter_EmpSN))
            {
                o_emps = o_emps.Where(p => p == Filter_EmpSN);
            }

            var emps = o_emps.ToList();

            foreach (string strEmpSN in emps)
            {
                //1.先抓該業務的出來算
                var o_query = from c in this.db.Customer.Where(p => p.EmpSN == strEmpSN)
                              join r in this.db.CustomerRating on c.Rating equals r.Id
                              join l in this.db.VisitLog on c.Number equals l.CustomerNumber
                              group c by new { c.EmpSN, c.Rating, l.Date.Year, l.Date.Month, l.Date.Day } into g
                              select new { g.Key.EmpSN, g.Key.Rating, g.Key.Year, g.Key.Month, g.Key.Day, count = g.Count() };

                //每個等級
                foreach (int iRating in Rating)
                {
                    //這個月
                    int m = Date.Month;
                    var query = o_query.Where(p => p.EmpSN == strEmpSN && p.Year == ThisYear && p.Month == m && p.Rating == iRating).ToList();

                    //DateTime thisMonth = new DateTime(ThisYear, m, 1);

                    int DaysOfMonth = System.DateTime.DaysInMonth(ThisYear, m);

                    for (int d = 1; d <= DaysOfMonth; d++)
                    {
                        var thisData = o_query.Where(p => p.Year == ThisYear && p.Rating == iRating && p.Month == m && p.Day == d).FirstOrDefault();
                        var lastData = o_query.Where(p => p.Year == (ThisYear - 1) && p.Rating == iRating && p.Month == m && p.Day == d).FirstOrDefault();

                        try
                        {
                            int thisI = thisData != null ? thisData.count : 0;
                            int lastI = lastData != null ? lastData.count : 0;

                            AddOrUpdateByMonthRating(strEmpSN, ThisYear, m, d, iRating,
                                thisData != null ? thisData.count : 0,
                                lastData != null ? lastData.count : 0);

                            logger.Debug("業務:{0}, {1}/{2}/{3}, rating:{4}, this:{5}, last:{6}", strEmpSN, ThisYear, m, d, iRating, thisI, lastI);
                         
                        }
                        catch (Exception ex)
                        {
                            logger.Fatal("ByYearMRatingRun({0},{1},{2},{3},{4},{5},{6})   ({7})",
                                strEmpSN, ThisYear, m, d, iRating,
                                thisData != null ? thisData.count : 0,
                                lastData != null ? lastData.count : 0,
                                ex.Message);
                        }
                    }
                }
                try
                {
                    this.db.SaveChanges();
                    logger.Debug("更新資料成功 業務:{0}", strEmpSN);
                }
                catch (Exception ex)
                {
                    logger.Fatal("更新資料失敗({0})", ex.Message);
                }
            }
        }

        public void AddOrUpdateByMonthRating(string EmpSN, int ThisYear, int Month, int Day, int Rating, int ThisCount, int LastCount)
        {
            string strYearM = string.Format("{0:0000}{1:00}", ThisYear, Month);

            RepCVRByMonthRating data = this.db.RepCVRByMonthRating.Where(p => p.EmpSN == EmpSN && p.YearM == strYearM && p.Day == Day && p.Rating == Rating).FirstOrDefault();
            //RepCVRByMonthRating data = new RepCVRByMonthRating();
            if (data == null)
            {
                data = new RepCVRByMonthRating();

                data.EmpSN = EmpSN;
                data.YearM = strYearM;
                data.Day = Day;
                data.Rating = Rating;
                data.Times = ThisCount;
                data.LastYearTimes = LastCount;
                data.CreateDate = DateTime.Now;
                this.db.RepCVRByMonthRating.AddObject(data);
            }
            else
            {
                data.Times = ThisCount;
                data.CreateDate = DateTime.Now;
            }
        }


        #endregion

        static void Main(string[] args)
        {
            Mainconsole process = new Mainconsole();

            DateTime dt = DateTime.Now.AddDays(-1);

            //Filter_EmpSN = "0023";

            try
            {
                logger.Debug("開始-客戶拜訪統計(級數) - 依年度");
                process.ByYearRun(dt);
                logger.Debug("結束-客戶拜訪統計(級數) - 依年度");

                logger.Debug("開始-客戶拜訪統計 - 依年度 + 級數");
                process.ByYearRatingRun(dt);
                logger.Debug("結束-客戶拜訪統計 - 依年度 + 級數");

                logger.Debug("開始-客戶拜訪統計 - 依年月份 + 級數");
                process.ByYearMRatingRun(dt);
                logger.Debug("結束-客戶拜訪統計 - 依年月份 + 級數");
                
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }


    }
}
