﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using admin.Models;

namespace Kpss.Backend.RepCVT
{
    /// <summary>
    /// 客戶拜訪統計 (依客戶類別)
    /// </summary>
    public class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public KpssDb db;

        public static string Filter_EmpSN = string.Empty;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        /// <summary>
        /// 1.合約客戶：由後台匯入之客戶資料 
        /// 2.潛在客戶：由APP新增且尚無交易的客戶。
        /// 3.潛在客戶轉合約
        /// </summary>
        public int[] type = { 1, 2, 3 };

        #region 依年度
        public void ByYearRun(int Year, int Month, int Day)
        {
            //參數 2 : 業務員
            var o_emps = from p in this.db.Employees
                          select p.EmpSN;

            if (!string.IsNullOrEmpty(Filter_EmpSN))
            {
                o_emps = o_emps.Where(p => p == Filter_EmpSN);
            }

            var emps = o_emps.ToList();

            //依每個業務來計算
            foreach (string strEmpSN in emps)
            {
                //1.先抓業務出來算
                var o_query = from c in this.db.Customer
                              join l in this.db.VisitLog on c.Number equals l.CustomerNumber
                              //where l.Date.Year == Year
                              group c by new { c.EmpSN, c.Type, l.Date.Year, l.Date.Month } into g
                              select new { 
                                  g.Key.EmpSN,
                                  g.Key.Type,
                                  g.Key.Year,
                                  g.Key.Month,
                                  count = g.Count()
                              };

                foreach (int t in type)
                {
                    int m = Month;
                    //今年的資料
                    var thisYearData = o_query.Where(p => p.EmpSN == strEmpSN && p.Year == Year && p.Type == t && p.Month == m).FirstOrDefault();
                    //去年的資料
                    var lastYearData = o_query.Where(p => p.EmpSN == strEmpSN && p.Year == (Year - 1) && p.Type == t && p.Month == m).FirstOrDefault();

                    try
                    {
                        //run the month data
                        ByYearMonthRun(strEmpSN, t, Year, m, Day);

                        // add this year data
                        AddOrUpdate(strEmpSN, Year, m, t,
                            thisYearData != null ? thisYearData.count : 0,
                            lastYearData != null ? lastYearData.count : 0);

                        logger.Debug("新增一筆 {0},{1},{2}", strEmpSN, Year, t);
                    }
                    catch (Exception ex)
                    {
                        logger.Fatal("依年度計算次數錯誤 @ AddOrUpdate({0},{1},{2},{3},{4})   ({5})",
                            strEmpSN, Year, t,
                            thisYearData != null ? thisYearData.count : 0,
                            lastYearData != null ? lastYearData.count : 0,
                            ex.Message);
                    }
                   
                }
                this.db.SaveChanges();
            }
        }
        /// <summary>
        /// 新增一筆資斗
        /// </summary>
        /// <param name="strEmpSN">emp SN</param>
        /// <param name="Year"></param>
        /// <param name="t">customer type (0,1,2)</param>
        /// <param name="p1">this year data</param>
        /// <param name="p2">last year data</param>
        private void AddOrUpdate(string strEmpSN, int Year, int m, int t, int p1, int p2)
        {
            RepCVTByYear data = this.db.RepCVTByYear.Where(p => p.EmpSN == strEmpSN 
                && p.Year == Year 
                && p.Month == m
                && p.Type == t).FirstOrDefault();

            if (data == null)
            {
                data = new RepCVTByYear();
                data.EmpSN = strEmpSN;
                data.Year = Year;
                data.Month = m;
                data.Type = t;
                data.Times = p1;
                data.LastYearTimes = p2;
                data.CreateDate = DateTime.Now;
                this.db.RepCVTByYear.AddObject(data);
            }
            else
            {
                data.Times = p1;
                data.LastYearTimes = p2;
            }
        }
        #endregion

        #region 依月份

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="t">類別</param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        public void ByYearMonthRun(string EmpSN, int t, int Year, int Month, int Day)
        {
            int DaysOfThisMonth = DateTime.DaysInMonth(Year, Month);
            //int d = Day;
            for (int d = 1; d <= DaysOfThisMonth; d++)
            {
                //1.先抓業務出來算
                var o_query = from c in this.db.Customer
                              join l in this.db.VisitLog on c.Number equals l.CustomerNumber
                              group c by new { c.EmpSN, c.Type, l.Date.Year, l.Date.Month, l.Date.Day } into g
                              select new
                              {
                                  g.Key.EmpSN,
                                  g.Key.Type,
                                  g.Key.Year,
                                  g.Key.Month,
                                  g.Key.Day,
                                  count = g.Count()
                              };

                //今年的資料
                var thisYearData = o_query.Where(p => p.EmpSN == EmpSN && p.Year == Year && p.Type == t && p.Month == Month && p.Day == d).FirstOrDefault();
                //去年的資料
                var lastYearData = o_query.Where(p => p.EmpSN == EmpSN && p.Year == (Year - 1) && p.Type == t && p.Month == Month && p.Day ==d).FirstOrDefault();

                try
                {
                    //run the month data
                    //ByYearMonthRun(strEmpSN, t, Year, m);

                    // add this year data
                    AddOrUpdateByMonth(EmpSN, Year, Month, d, t,
                        thisYearData != null ? thisYearData.count : 0,
                        lastYearData != null ? lastYearData.count : 0);

                    logger.Debug("新增一筆 sn:{0},type:{1},day:{2}/{3}/{4}, times:{5}, times:{6}", EmpSN, t, Year, Month, d, thisYearData, lastYearData);
                }
                catch (Exception ex)
                {
                    logger.Fatal("依年度計算次數錯誤 @ AddOrUpdate({0},{1},{2},{3},{4})   ({5})",
                        EmpSN, Year, t,
                        thisYearData != null ? thisYearData.count : 0,
                        lastYearData != null ? lastYearData.count : 0,
                        ex.Message);
                }

            } //end of month
            this.db.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="d">日</param>
        /// <param name="t">客戶類別</param>
        /// <param name="p1">今年資料</param>
        /// <param name="p2">去年資料</param>
        private void AddOrUpdateByMonth(string EmpSN, int Year, int Month, int d, int t, int p1, int p2)
        {
            string strYearM = string.Format("{0}{1:00}", Year, Month);

            RepCVTByMonth data = this.db.RepCVTByMonth.Where(p => p.EmpSN == EmpSN && p.YearM == strYearM && p.Type == t && p.Day == d).FirstOrDefault();

            if (data == null)
            {
                data = new RepCVTByMonth();
                data.EmpSN = EmpSN;
                data.YearM = strYearM;
                data.Day = d;
                data.Type = t;
                data.Times = p1;
                data.LastYearTimes = p2;
                data.CreateDate = DateTime.Now;
                this.db.RepCVTByMonth.AddObject(data);
            }
            else
            {
                data.Times = p1;
                data.LastYearTimes = p2;
            }
        }
        
        #endregion

        static void Main(string[] args)
        {
            Mainconsole process = new Mainconsole();

            //執行昨天的資料
            DateTime _Yesterday = DateTime.Now.AddDays(-1);

            try
            {
                Filter_EmpSN = "0023";
                logger.Debug("開始-客戶拜訪統計(客戶類型) - 依年度");
                //process.ByYearRun(_Yesterday.Year, _Yesterday.Month, _Yesterday.Day);

                
                //process.ByYearRun(_Yesterday.Year, 1, _Yesterday.Day);
                //process.ByYearRun(_Yesterday.Year, 2, _Yesterday.Day);
                logger.Debug("結束-客戶拜訪統計(客戶類型) - 依年度");

               

            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }

    }
}
