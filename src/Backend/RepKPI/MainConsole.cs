﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using admin.Models;

namespace Kpss.Backend.RepKPI
{
    class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public KpssDb db;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        #region 依年度

        public void RunByYear(DateTime Date)
        {
            int Year = Date.Year;
            int Month = Date.Month;
            logger.Debug("===== 年度 : {0} =====", Year);

            //參數 1 : 業務員
            var o_emps = (from p in this.db.Employees
                          //where p.EmpSN == "0023"
                          select p.EmpSN).ToList();

            var kpiItems = from c in db.KpiItem
                           join p in db.KpiItem on c.RootId equals p.Id
                           select new { ItemId = c.Id, RootItem = p.Name, ChildItem = c.Name };

            foreach (var emp in o_emps)
            {
                logger.Debug("業務 : {0}", emp);
                foreach (var KpiItem in kpiItems)
                {
                    logger.Debug("項目 : {0}", KpiItem.ItemId, KpiItem.RootItem, KpiItem.ChildItem);
                    //取得預估值
                    int o_1 = (from p in db.Kpi
                               where p.Year == Year && p.EmpSN == emp && p.KpiItemId == KpiItem.ItemId
                               select p).Sum(p => (int?)p.EstimateAmt) ?? 0;
                    //去年值
                    int lastYearSum = (from p in db.KpiDaily
                                       where p.EmpSN == emp && p.Year == (Year - 1) && p.KpiItemId == KpiItem.ItemId
                                       select p).Sum(p => (int?)p.ActualAmt) ?? 0;

                    //今天值
                    int thisYearSum = (from p in db.KpiDaily
                                       where p.EmpSN == emp && p.Year == Year && p.KpiItemId == KpiItem.ItemId
                                       select p).Sum(p => (int?)p.ActualAmt) ?? 0;


                    try
                    {
                        AddOrUpdate(emp, Year, KpiItem.ItemId, KpiItem.RootItem, KpiItem.ChildItem, thisYearSum, lastYearSum);

                      
                        //依項目 及年度
                        logger.Debug("開始處理依項目及年度");
                        for (int i = 0; i < Month; i++)
                        {
                            RunByItem(Year, (i + 1), emp, KpiItem.ItemId, KpiItem.RootItem, KpiItem.ChildItem);
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        logger.Fatal("新增失敗 : {0}", ex.Message);
                    }
                    
                   
                }
                try
                {
                    db.SaveChanges();
                    logger.Debug("更新至資料庫成功");
                }
                catch (Exception ex)
                {
                    logger.Fatal("新增失敗 : {0}", ex.Message);
                }
            }
        }

        public void AddOrUpdate(string EmpSN, int Year, Guid ItemId, string RootItem, string ChildItem,
            decimal Amt, decimal LastYearAmt)
        {
            RepKPIByYear Entity = (from p in db.RepKPIByYear
                                   where p.EmpSN == EmpSN
                                   && p.Year == Year
                                   && p.ItemId == ItemId
                                   select p).FirstOrDefault();

            if (Entity == null)
            {
                Entity = new RepKPIByYear();

                Entity.EmpSN = EmpSN;
                Entity.Year = Year;
                Entity.ItemId = ItemId;
                Entity.RootItem = RootItem;
                Entity.ChildItem = ChildItem;
                Entity.Amt = Amt;
                Entity.LastYearAmt = LastYearAmt;
                Entity.CreateDate = DateTime.Now;
                db.RepKPIByYear.AddObject(Entity);
                logger.Debug("新增 : {0},{1},{2}", EmpSN, Year, ItemId);
            }
            else
            {
                Entity.RootItem = RootItem;
                Entity.ChildItem = ChildItem;
                Entity.Amt = Amt;
                Entity.LastYearAmt = LastYearAmt;
                logger.Debug("修改 : {0},{1},{2}", EmpSN, Year, ItemId);
            }
        }

        #endregion

        #region 依項目

        public void RunByItem(int Year, int Month, string EmpSN, Guid ItemId, string RootItem, string ChildItem)
        {
            //int i = DateTime.Now.Month;
            //月份
            int i = Month;

            //實際值
            decimal thisAmt = (from p in db.KpiDaily
                               where p.Year == Year
                               && p.Month == i
                               && p.EmpSN == EmpSN
                               && p.KpiItemId == ItemId
                               select p).Sum(p => (decimal?)p.ActualAmt) ?? 0;

            decimal lastYearAmt = (from p in db.RepKPIByItem
                                   where p.Year == Year - 1
                                   && p.EmpSN == EmpSN
                                   && p.ItemId == ItemId
                                   && p.Month == i
                                   select p).Sum(p => (decimal?)p.Amt) ?? 0;

            RepKPIByItem entity = (from p in db.RepKPIByItem
                                   where p.EmpSN == EmpSN
                                   && p.Year == Year
                                   && p.ItemId == ItemId
                                   && p.Month == i
                                   select p).FirstOrDefault();
            if (entity == null)
            {
                entity = new RepKPIByItem();
                entity.EmpSN = EmpSN;
                entity.Year = Year;
                entity.ItemId = ItemId;
                entity.Month = i;
                entity.RootItem = RootItem;
                entity.ChildItem = ChildItem;
                entity.Amt = (int)thisAmt;
                entity.Estimate = 0;
                entity.LastYearAmt = (int)lastYearAmt;
                entity.CreateDate = DateTime.Now;

                db.RepKPIByItem.AddObject(entity);
                logger.Debug("新增 : {0},{1},{2}", EmpSN, Year, ItemId);
            }
            else
            {
                entity.RootItem = RootItem;
                entity.ChildItem = ChildItem;
                entity.Amt = (int)thisAmt;
                entity.Estimate = 0;
                entity.LastYearAmt = (int)lastYearAmt;

                logger.Debug("修改 : {0},{1},{2}", EmpSN, Year, ItemId);
            }


            logger.Debug("處理本月資料 : {0},{1},{2}", EmpSN, Year, i);
            int iDaysOfThisMonth = DateTime.DaysInMonth(Year, i);
            for (int d = 0; d < iDaysOfThisMonth; d++)
            {
                logger.Debug("=========={0}/{1}/{2}==========", Year, i, d);
                RunByDay(EmpSN, Year, i, d + 1, ItemId, RootItem, ChildItem);
            }
           
        }

        #endregion

        public void RunByDay(string EmpSN, int Year, int Month, int Day, Guid ItemId, string RootItem, string ChildItem)
        {
            //int d = DateTime.Now.Day;
            DateTime cDate = new DateTime(Year, Month, Day);
            int d = Day;
            int iDayOfWeek = (int)cDate.DayOfWeek;
            //今年的 index string
            string strYearM = string.Format("{0}{1:00}", cDate.Year, cDate.Month);
            //去年的 index string 
            string strLastYearM = string.Format("{0}{1:00}", cDate.Year - 1, cDate.Month);

            //實際值
            decimal thisAmt = (from p in db.KpiDaily
                               where p.Year == cDate.Year 
                               && p.Month == cDate.Month 
                               && p.Day == d
                               && p.EmpSN == EmpSN
                               && p.KpiItemId == ItemId
                               select p).Sum(p => (decimal?)p.ActualAmt) ?? 0;

            decimal lastYearAmt = (from p in db.RepKPIByMonth
                                   where p.YearM == strLastYearM
                                   && p.EmpSN == EmpSN
                                   && p.ItemId == ItemId
                                   && p.Day == Day
                                   select p).Sum(p => (decimal?)p.Amt) ?? 0;

            RepKPIByMonth entity = (from p in db.RepKPIByMonth
                                   where p.EmpSN == EmpSN
                                   && p.YearM == strYearM
                                   && p.ItemId == ItemId
                                   && p.Day == Day
                                   select p).FirstOrDefault();
            if (entity == null)
            {
                entity = new RepKPIByMonth();
                entity.EmpSN = EmpSN;
                entity.YearM = strYearM;
                entity.ItemId = ItemId;
                entity.Day = d;
                entity.DayOfWeek = iDayOfWeek;
                entity.RootItem = RootItem;
                entity.ChildItem = ChildItem;
                entity.Amt = (int)thisAmt;
                entity.Estimate = 0;
                entity.LastYearAmt = (int)lastYearAmt;
                entity.CreateDate = DateTime.Now;

                db.RepKPIByMonth.AddObject(entity);
                logger.Debug("新增 : {0},{1}/{2}/{3},{4}", EmpSN, Year, Month, Day, ChildItem);
            }
            else
            {
                entity.DayOfWeek = iDayOfWeek;
                entity.RootItem = RootItem;
                entity.ChildItem = ChildItem;
                entity.Amt = (int)thisAmt;
                entity.Estimate = 0;
                entity.LastYearAmt = (int)lastYearAmt;

                logger.Debug("修改 : {0},{1}/{2}/{3},{4}", EmpSN, Year, Month, Day, ChildItem);
            }

        }
        public void SaveChanges()
        {
            db.SaveChanges();
        }

        static void Main(string[] args)
        {
            DateTime _Now = DateTime.Now.Date;

            Mainconsole process = new Mainconsole();

            try
            {
                // Unit Test
                //int iDaysOfThisMonth = DateTime.DaysInMonth(_Now.Year, _Now.Month);
                //Guid g = new Guid("e9bdf6aa-ac48-4501-9441-12dd8a02e4c2");
                //for (int d = 0; d < iDaysOfThisMonth; d++)
                //{
                //    //logger.Debug("=========={0}/{1}/{2}==========", Year, i, d);

                //    process.RunByDay("0047", _Now.Year, _Now.Month, d + 1, g, "KMS-大項目", "小項目1");
                //}
                //process.SaveChanges();
                //return;

                logger.Debug("開始-KPI By 年度統計");
                process.RunByYear(_Now);
                logger.Debug("結束-KPI By 年度統計");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}
