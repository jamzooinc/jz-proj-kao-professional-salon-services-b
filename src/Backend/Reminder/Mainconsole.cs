﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using admin.Models;
using JamZoo.JamPush.Demo;

 namespace Kpss.Backend.Reminder
{
    /// <summary>
    /// 每月提醒
    /// </summary>
    public class Mainconsole
    {
        /// <summary>
        /// 業績統計BY品牌 (依年度、依月份)
        /// </summary>
        protected static Logger logger = LogManager.GetLogger("Reminder");

        string[] BrandList = { "KMS", "GOLDWELL" };

        public KpssDb db;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        #region 每日檢查
        public void DailyCheck(DateTime Date)
        {
            //1. get the check settings
            var q_settings = from p in db.PerfReminded
                             join m in db.Employees on p.EmpSN equals m.EmpSN
                             where p.DayOfMonth == Date.Day
                             select new { p, m.Mac };

            int cnt = q_settings.Count();
            logger.Debug("取得設定值, 共 {0} 筆", cnt);

            //2. 如果大於 零, 就開始統計
            if (cnt > 0)
            {
                //當月的KPI 業績
                var totalKpiByMonth = (from p in db.KpiDaily
                                       where 
                                       p.Year == Date.Year 
                                       && p.Month == Date.Month
                                       select p.ActualAmt).Sum();

                logger.Debug("取得當月業績 : {0}", totalKpiByMonth);

                //3. 依序取出提醒設定
                foreach (var setting in q_settings)
                {
                    logger.Debug("取出提醒設定值-{0}, {1}, {2}", setting.p.EmpSN, setting.p.DayOfMonth, setting.Mac);
                    // 如果當月的 KPI 業績小於設定的
                    if (totalKpiByMonth < setting.p.Limitation)
                    {
                        string strMAC = setting.Mac;
                        if (!string.IsNullOrEmpty(strMAC))
                        {
                            logger.Debug("開始發送推撥-{0},{1}", setting.p.EmpSN, setting.Mac);
                            PushSender("測試之", "該月月績沒有超過您的設定值", setting.p.EmpSN, strMAC);
                            logger.Debug("結束發送推撥");
                        }
                    }
                } 
            }
        }


        public bool PushSender(string subject, string content, string EmpSN, string mac)
        {
            RestAPI client = new RestAPI(
                System.Configuration.ConfigurationManager.AppSettings["JamPushRestCode"].ToString(),
                System.Configuration.ConfigurationManager.AppSettings["JamPushAppKey"].ToString()
                );

            var response = client.ActiveMessage(subject, content, new string[] { mac });
            if (response.error_code == "200")
            {
                logger.Error("Push 成功:{0}", response.error_code);
                string strRes = Newtonsoft.Json.JsonConvert.SerializeObject(response);
                AddPushLog(subject, content, EmpSN, strRes);
                return true;
            }
            else
            {
                logger.Error("Push 錯誤:{0} - {1}", response.error_code, response.error_msg);
                return false;
            }
        }

        public void AddPushLog(string subject, string summary, string empsn, string response)
        {
            JamPush log = new JamPush();
            log.Id = Guid.NewGuid();
            log.Title = subject;
            log.Summary = summary;
            log.CreateEmpSN = empsn;
            log.Request = string.Empty;
            log.Reponse = response;
            db.JamPush.AddObject(log);
            db.SaveChanges();
        }



        #endregion


        static void Main(string[] args)
        {
            DateTime TODAY = DateTime.Now.Date;

            Mainconsole process = new Mainconsole();
            //logger
            try
            {
                logger.Debug("開始");
                process.DailyCheck(TODAY.AddDays(-1));
                logger.Debug("結束");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}
