﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using admin.Models;

namespace Kpss.Backend.RepPFS
{
    public class Mainconsole
    {
        /// <summary>
        /// 業績統計BY品牌 (依年度、依月份)
        /// </summary>
        protected static Logger logger = LogManager.GetLogger("RepPFS");

        string[] BrandList = { "KMS", "GOLDWELL" };

        public KpssDb db;

        public Mainconsole()
        {
            db = new KpssDb();
        }

        #region 依年度
        public void RunByYear(int Year, int Month)
        {
            //參數 1 : 年份
            var o_emps = (from p in this.db.Employees
                          select p.EmpSN).ToList();
            //品牌
            foreach (string strBrand in BrandList)
            {
                logger.Debug("-品牌({0})", strBrand);
                foreach (var emp in o_emps)
                {
                    logger.Debug("--業務({0})", emp);

                    var o_amt = from p in db.KpiDaily
                        join ki in db.KpiItem on p.KpiItemId equals ki.Id
                        where p.EmpSN == emp && ki.Brand == strBrand
                        select p;

                    int 預估KPI = (from p in db.Kpi
                                 join ki in db.KpiItem on p.KpiItemId equals ki.Id
                                 where
                                 ki.Brand == strBrand
                                 && p.Year == Year
                                 && p.Month == Month
                                 && p.EmpSN == emp
                                 select p.EstimateAmt).FirstOrDefault();

                    int LastYearSum = o_amt.Where(p =>
                        p.Year == Year - 1
                        && p.Month == Month).Select(p => p.ActualAmt).Sum(x => (int?)x) ?? 0;

                    int ThisYearSum = o_amt.Where(p =>
                        p.Year == Year
                        && p.Month == Month).Select(p => p.ActualAmt).Sum(x => (int?)x) ?? 0;

                    try
                    {
                        AddOrUpdate(emp,
                            strBrand,
                            Year,
                            Month,
                            預估KPI,
                            ThisYearSum,
                            LastYearSum
                            );

                        logger.Debug("開始處理該月的每日資料");
                        RunByMonth(emp, strBrand, Year, Month);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("新增失敗:({0},{1},{2},{3},{4},{5}) , {6}",
                            strBrand,
                            Year,
                            Month,
                            預估KPI,
                            LastYearSum,
                            ThisYearSum, ex.Message);
                    }
                }

                try
                {
                    db.SaveChanges();
                    logger.Debug("更新至資料庫成功");
                }
                catch (Exception ex)
                {
                    logger.Debug("更新至資料庫失敗 : {0}", ex.Message);
                }
            }
        }

        public void AddOrUpdate(string EmpSN, string Brand, int Year, int Month, int TargetAmt,
            int ActualAmt, int LastYearAmt)
        {
            RepPFSByYear data = db.RepPFSByYear.Where(p =>
                p.EmpSN == EmpSN
                && p.Brand == Brand
                && p.Year == Year
                && p.Month == Month).FirstOrDefault();

            if (data == null)
            {
                data = new RepPFSByYear();
                data.EmpSN = EmpSN;
                data.Brand = Brand;
                data.Year = Year;
                data.Month = Month;
                data.TargetAmt = TargetAmt;
                data.ActualAmt = ActualAmt;
                if (data.ActualAmt > 0)
                {
                    data.ReachRate = (double)((data.TargetAmt / data.ActualAmt) * (decimal)100);
                }
                else
                {
                    data.ReachRate = 0;
                }
                data.LastYearAmt = LastYearAmt;
                data.DiffLastYear = ActualAmt - LastYearAmt;
                data.CreateDate = DateTime.Now;

                db.RepPFSByYear.AddObject(data);

                logger.Debug("---新增({0},{1},{2},{3},{4},{5})",
                        Brand,
                        Year,
                        Month,
                        TargetAmt,
                        ActualAmt,
                        LastYearAmt);
            }
            else
            {
                data.TargetAmt = TargetAmt;
                data.ActualAmt = ActualAmt;
                if (data.ActualAmt > 0)
                {
                    data.ReachRate = (double)((data.TargetAmt / data.ActualAmt) * (decimal)100);
                }
                else
                {
                    data.ReachRate = 0;
                }
                data.LastYearAmt = LastYearAmt;
                data.DiffLastYear = ActualAmt - LastYearAmt;

                logger.Debug("---修改({0},{1},{2},{3},{4},{5})",
                        Brand,
                        Year,
                        Month,
                        TargetAmt,
                        ActualAmt,
                        LastYearAmt);
            }
        }
        #endregion

        #region 依月份

        //每日就沒有預估值
        public void RunByMonth(string EmpSN, string Brand, int Year, int Month)
        {
            logger.Debug("{0}/{1}", Year, Month);

            int DaysOfMonth = System.DateTime.DaysInMonth(Year, Month);

            //去年的
            var lastYearQuery = (from p in db.KpiDaily
                                 join ki in db.KpiItem on p.KpiItemId equals ki.Id
                                 where p.EmpSN == EmpSN && ki.Brand == Brand
                                 && p.Year == Year - 1
                                 && p.Month == Month
                                 select p).ToList();

            var thisYearQuery = (from p in db.KpiDaily
                                 join ki in db.KpiItem on p.KpiItemId equals ki.Id
                                 where p.EmpSN == EmpSN && ki.Brand == Brand
                                 && p.Year == Year
                                 && p.Month == Month
                                 select p).ToList();

            for (int i = 1; i <= DaysOfMonth; i++)
            {
                //今年的
                decimal iThisYearAmt = thisYearQuery.Where(p => p.Day == i).Sum(p => (int?)p.ActualAmt) ?? 0;
                //去年的
                decimal iLastYearAmt = lastYearQuery.Where(p => p.Day == i).Sum(p => (int?)p.ActualAmt) ?? 0;

                AddOrUpdateByDay(EmpSN, Brand, Year, Month, i, iThisYearAmt, iLastYearAmt);
                try
                {
                    db.SaveChanges();
                    logger.Debug("更新至資料庫成功");
                }
                catch (Exception ex)
                {
                    logger.Debug("更新至資料庫失敗 : {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// 新增每日的資料
        /// </summary>
        /// <param name="EmpSN"></param>
        /// <param name="Brand"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Day"></param>
        /// <param name="ActualAmt"></param>
        /// <param name="LastYearAmt"></param>
        public void AddOrUpdateByDay(string EmpSN,string Brand, int Year, int Month, int Day, 
            decimal ActualAmt,
            decimal LastYearAmt)
        {
            string strYearM = string.Format("{0}{1:00}", Year, Month);
            DateTime CurrentDate = new DateTime(Year, Month, Day);
            int iDayOfWeek = (int)CurrentDate.DayOfWeek;


            RepPFSByMonth data = db.RepPFSByMonth.Where(p =>
              p.EmpSN == EmpSN
              && p.Brand == Brand
              && p.YearM == strYearM
              && p.Day == Day).FirstOrDefault();

            if (data == null)
            {
                data = new RepPFSByMonth();
                data.EmpSN = EmpSN;
                data.Brand = Brand;
                data.YearM = strYearM;
                data.Day = Day;
                data.DayOfWeek = iDayOfWeek;
                data.TargetAmt = 0;
                data.ActualAmt = ActualAmt;
                data.LastYearAmt = LastYearAmt;
                data.DiffLastYear = ActualAmt - LastYearAmt;
                data.CreateDate = DateTime.Now;

                db.RepPFSByMonth.AddObject(data);

                logger.Debug("---新增({0},{1},{2},{3},{4})",
                        Brand,
                        strYearM,
                        Month,
                        ActualAmt,
                        LastYearAmt);
            }
            else
            {
                data.DayOfWeek = iDayOfWeek;
                data.ActualAmt = ActualAmt;
                data.LastYearAmt = LastYearAmt;
                data.DiffLastYear = ActualAmt - LastYearAmt;

                logger.Debug("---修改({0},{1},{2},{3},{4})",
                        Brand,
                        Year,
                        Month,
                        ActualAmt,
                        LastYearAmt);
            }
        }
        

        #endregion

        static void Main(string[] args)
        {
            int Year = DateTime.Now.Year;
            int Month = DateTime.Now.Month;

            Mainconsole process = new Mainconsole();
            //logger
            try
            {
                logger.Debug("開始-業績統計");
                process.RunByYear(Year, Month);
                process.RunByYear(Year, Month - 1);
                logger.Debug("開始-業績統計");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }
    }
}
