﻿/*
 * Usage Limits
 * Use of the Google Geocoding API is subject to a query limit of 2,500 geolocation requests per day. 
 * (User of Google Maps API for Business may perform up to 100,000 requests per day.) 
 * This limit is enforced to prevent abuse and/or repurposing of the Geocoding API, and this limit may be changed in the future without notice.
 * Additionally, we enforce a request rate limit to prevent abuse of the service. If you exceed the 24-hour limit or otherwise abuse the service, the Geocoding API may stop working for you temporarily. If you continue to exceed this limit, your access to the Geocoding API may be blocked.
 * Note: the Geocoding API may only be used in conjunction with a Google map; geocoding results without displaying them on a map is prohibited. For complete details on allowed usage, consult the Maps API Terms of Service License Restrictions.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Engine;

using admin.Models;
using GoogleMapsApi.Entities.Geocoding.Response;


namespace Kpss.Backend.LngLat
{
    /// <summary>
    /// 每天跑座標
    /// </summary>
    public class Mainconsole
    {
        /// <summary>
        /// Logger : 記錄器.
        /// </summary>
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public void Run()
        {
            // 資料庫
            KpssDb db = new KpssDb();

            GeocodingEngine geocodingEngine = new GeocodingEngine();

            var o_query = from p in db.Customer
                          where p.Lat == null && p.Lng == null
                          select p;

            foreach (var Row in o_query)
            {
                try
                {
                    // 送貨地址
                    string _addr = Row.TransportAddr;
                    if (!string.IsNullOrEmpty(_addr))
                    {
                        GeocodingRequest geocodeRequest = new GeocodingRequest()
                        {
                            Address = _addr
                        };

                        logger.Debug("開始取座標 : " + Row.Number);
                        GeocodingResponse geocode = geocodingEngine.GetGeocode(geocodeRequest);

                        if (geocode.Results.FirstOrDefault() != null)
                        {
                            string strLat = geocode.Results.FirstOrDefault().Geometry.Location.Latitude.ToString();
                            string strLng = geocode.Results.FirstOrDefault().Geometry.Location.Longitude.ToString();

                            logger.Debug("結束取座標. Lat : {0}, Lng : {1}", strLat, strLng);

                            Row.Lat = strLat;
                            Row.Lng = strLng;
                        }
                        else
                        {
                            logger.Debug("此地址沒有座標 : " + Row.EnrollmentAddr);
                        }

                    }
                    else
                    {
                        logger.Debug("沒有地址 - {0}", Row.Number);
                    }

                }
                catch (Exception ex)
                {
                    logger.Fatal(ex.Message);
                }
            }
            logger.Debug("更新至 DB");
            db.SaveChanges();
            logger.Debug("結束更新至 DB");
        }

        static void Main(string[] args)
        {
            Mainconsole process = new Mainconsole();
            try
            {
                process.Run();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex.Message);
            }
        }

      
    }
}
